<?php
require 'prolog.php';
if (!$decoded || !in_array('staff', $decoded->login->groups)) {
    header("location: /");
    exit();
}

$base_language = getenv('BASE_LANGUAGE')

?>
<!DOCTYPE html>
<html lang="<?php echo($base_language) ?>" class="h-100">
  <head>
    <meta charset="UTF-8">
    <title>calo.news - modifica articolo</title>
    <script type="text/javascript" src="/js/jquery.slim.min.js"></script>
    <script type="text/javascript" src="/js/vue.min.js"></script>
    <link rel="stylesheet" href="css/bootstrap.min.css">
    <link rel="stylesheet" href="css/style.css">
    <link rel="stylesheet" type="text/css" href="css/trix.css">
    <link rel="apple-touch-icon" sizes="180x180" href="/apple-touch-icon.png">
    <link rel="icon" type="image/png" href="/favicon-32x32.png" sizes="32x32">
    <link rel="icon" type="image/png" href="/favicon-16x16.png" sizes="16x16">
    <link rel="manifest" href="/manifest.json">
    <link rel="mask-icon" href="/safari-pinned-tab.svg" color="#5bbad5">
    <meta name="theme-color" content="#ffffff">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  </head>
  <body id="page-top" class="d-flex flex-column h-100">
<?php require 'header.php'; ?>
    <main id="new_article" role="main" class="flex-shrink-0">
      <div class="container">
        <h2>Modifica una notizia</h2>
        <form>
          <div class="form-group">
            <label for="title">Titolo della notizia</label>
            <input autocomplete="off" autocorrect="off" autocapitalize="off" spellcheck="false" type="text" class="form-control" id="title" v-model="article.title">
          </div>
          <div class="form-group">
            <label for="title">Titolo della notizia in lingua originale</label>
            <input autocomplete="off" autocorrect="off" autocapitalize="off" spellcheck="false" type="text" class="form-control" id="title" v-model="article.title_original">
          </div>
          <div class="form-group">
            <label for="url"><a target="_blank" href="#" v-bind:href="article.url">URL</a></label>
            <input type="url" class="form-control" id="url" v-model="article.url">
            
          </div>
          <div class="form-group">
            <label for="author">Autore</label>
            <input autocomplete="off" autocorrect="off" autocapitalize="off" spellcheck="false" type="text" class="form-control" id="author" v-model="article.author">
          </div>
          <div class="form-group">
            <label for="language">Lingua</label>
            <select class="form-control" id="language" v-model="article.language">
              <?php require 'languages.php'; ?>
            </select>
          </div>
          <ul class="nav nav-tabs" role="tablist">
            <li class="nav-item">
              <a class="nav-link active" data-toggle="tab" role="tab" href="#tab_content">Contenuto</a>
            </li>
            <li class="nav-item">
              <a class="nav-link" data-toggle="tab" role="tab" href="#tab_content_original">Contenuto in lingua originale</a>
            </li>
          </ul>
          <div class="tab-content">
            <div class="tab-pane active" id="tab_content" role="tabpanel">
              <div class="form-group">
                <label for="content">Testo completo della notizia</label> <input style="float: right;" type="checkbox" v-model="collapsed" title="mostra / nascondi testo completo">
                <div v-bind:class="{ 'collapse': collapsed, 'collapse.show': !collapsed }">
                  <input type="hidden" id="content" name="content">
                  <trix-editor autocomplete="off" autocorrect="off" autocapitalize="off" spellcheck="false" input="content" id="content_editor"></trix-editor>
                </div> <!-- collapsible -->
              </div>
            </div>
            <div class="tab-pane" id="tab_content_original" role="tabpanel">
              <div class="form-group">
                <label for="content_original">Testo completo della notizia nella lingua originale</label> <input style="float: right;" type="checkbox" v-model="collapsed_original" title="mostra / nascondi testo completo in lingua originale">
                <div v-bind:class="{ 'collapse': collapsed_original, 'collapse.show': !collapsed_original }">
                  <input type="hidden" id="content_original" name="content_original">
                  <trix-editor autocomplete="off" autocorrect="off" autocapitalize="off" spellcheck="false" input="content_original" id="content_original_editor"></trix-editor>
                </div> <!-- collapsible -->
              </div>
            </div>
          </div>
          <button v-on:click="send();" type="button" class="btn btn-primary">Invia</button>
          <hr/>
          <a href="#page-top">Torna su</a>
        </form>
      </div>
    </main>
    <script type="text/javascript" src="/js/common.js"></script>
    <script type="text/javascript" src="/js/envsubst.js"></script>
    <script type="text/javascript" src="/js/edit_article.js"></script>
    <script type="text/javascript" src="/js/trix.js"></script>
<?php require 'footer.php'; ?>

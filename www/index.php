<?php
require 'prolog.php';

if ( (substr($_SERVER['REQUEST_URI'], 0, 9) == '/article/') && (!$decoded || !in_array('soci', $decoded->login->groups)) ) {
    $id = substr($_SERVER['REQUEST_URI'], 9);
    $aggregator_hostname = $_SERVER['HTTP_HOST'];
    $url = "https://$aggregator_hostname/article.php?id=$id";
    header("location: " . $url);
    exit();
}
$base_language = getenv('BASE_LANGUAGE');
?>
<!DOCTYPE html>
<html lang="<?php echo($base_language) ?>" class="h-100">
  <head>
    <meta charset="UTF-8">
    <title>calo.news - notizie</title>
    <script type="text/javascript" src="/js/vue.runtime.min.js"></script>
    <script type="text/javascript" src="/js/vue-infinite-scroll.js"></script>
    <script type="text/javascript" src="/js/vue-router.min.js"></script>
    <link rel="stylesheet" href="/css/bootstrap.min.css">
    <link rel="stylesheet" href="/css/style.css">
    <link rel="apple-touch-icon" sizes="180x180" href="/apple-touch-icon.png">
    <link rel="icon" type="image/png" href="/favicon-32x32.png" sizes="32x32">
    <link rel="icon" type="image/png" href="/favicon-16x16.png" sizes="16x16">
    <link rel="manifest" href="/manifest.json">
    <link rel="mask-icon" href="/safari-pinned-tab.svg" color="#5bbad5">
<?php require 'prefetch.html'; ?>
    <meta name="theme-color" content="#ffffff">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  </head>
  <body class="d-flex flex-column h-100">
    <script type="text/javascript">
      // for https://github.com/vue-perf-devtool/vue-perf-devtool
      // Vue.config.devtools = true
      // Vue.config.performance = true
    </script>
<?php require 'header.php'; ?>
    <main role="main" class="flex-shrink-0">
      <div id="app">
      </div>
    </main>
    <script type="text/javascript" src="/js/common.js"></script>
    <script type="text/javascript" src="/js/envsubst.js"></script>
    <script type="text/javascript" src="/js/font_size.js"></script>
    <script type="text/javascript" src="/js/shallow-equals.js"></script>
    <script type="text/javascript" src="/js/es6-promise.auto.min.js"></script>
    <script type="text/javascript" src="/templates/download-send-share.js"></script>
    <script type="text/javascript" src="/templates/article-item.js"></script>
    <script type="text/javascript" src="/templates/negative-rating.js"></script>
    <script type="text/javascript" src="/templates/positive-rating.js"></script>
    <script type="text/javascript" src="/templates/rating-toolbar.js"></script>
    <script type="text/javascript" src="/templates/actions.js"></script>
    <script type="text/javascript" src="/templates/feed-item.js"></script>
    <script type="text/javascript" src="/js/vue-select.js"></script>
    <script type="text/javascript" src="/js/jquery.slim.min.js"></script>
    <script type="text/javascript" src="/js/slip.js"></script>
<?php if (($decoded) and in_array('soci', $decoded->login->groups)): ?>
    <!-- active -->
    <script type="text/javascript" src="/templates/news-panel.js"></script>
    <script type="text/javascript" src="/templates/news-grid.js"></script>
    <script type="text/javascript" src="/templates/news-item.js"></script>
    <script type="text/javascript" src="/templates/news-list.js"></script>
<?php else: ?>
    <!-- inactive or anonymous -->
    <script type="text/javascript" src="/templates/news-item-simple.js"></script>
    <script type="text/javascript" src="/templates/news-list-simple.js"></script>    
    <script type="text/javascript">
      var NewsGrid = NewsList;
    </script>
<?php endif; ?>
    <script type="text/javascript" defer src="/js/index.js"></script>
    <script type="text/javascript" src="/templates/app.js"></script>
    <script type="text/javascript" src="/js/tts.js"></script>
<?php require 'footer.php'; ?>

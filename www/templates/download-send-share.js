// configuration for jshint
/* jshint browser: true, devel: true */
/* global Vue, $ */

var url = window.location.href;

var bus = new Vue();

Vue.component('download-send-share', {
  props: ['ids', 'format', 'clean', 'share'],
  render: function(_c) {
    with(this){return _c('div',{staticClass:"btn-group btn-group-sm",attrs:{"role":"group","aria-label":"download send share"}},[_c('div',{staticClass:"modal fade",attrs:{"id":"modal_email_sent","tabindex":"-1","role":"dialog","aria-labelledby":"Estratto notizie inviato","aria-hidden":"true"}},[_c('div',{staticClass:"modal-dialog",attrs:{"role":"document"}},[_c('div',{staticClass:"modal-content"},[_m(0),_v(" "),_c('div',{staticClass:"modal-body"},[_c('p',[_v("L'estratto notizie è stato inviato per e-mail come ebook nel formato "+_s(format)+".")]),_v(" "),_c('div',{directives:[{name:"show",rawName:"v-show",value:(clean),expression:"clean"}],staticClass:"form-check"},[_m(1)])]),_v(" "),_m(2)])])]),_v(" "),_c('div',{staticClass:"dropdown btn-group btn-group-sm",attrs:{"role":"group"}},[_c('button',{staticClass:"btn btn-primary",attrs:{"type":"button","id":"download_menu","data-toggle":"dropdown","aria-haspopup":"true","aria-expanded":"false","title":"Scarica"}},[_c('svg',{staticStyle:{"fill":"white"},attrs:{"width":"1em","height":"1em"}},[_c('use',{attrs:{"xlink:href":"#download"}})])]),_v(" "),_c('div',{staticClass:"dropdown-menu",attrs:{"aria-labelledby":"download_menu"}},[_c('a',{staticClass:"dropdown-item disabled",attrs:{"href":"#","tabindex":"-1","aria-disabled":"true"}},[_v("Scarica nel formato che perferisci:")]),_v(" "),_c('button',{staticClass:"dropdown-item btn-primary",attrs:{"type":"button","title":"Scarica in formato html"},on:{"click":function($event){return window_open('/api/articles.html?ids=' + ids);}}},[_c('svg',{attrs:{"width":"1em","height":"1em"}},[_c('use',{attrs:{"xlink:href":"#code"}})]),_v(" "),_c('span',[_v(" html")])]),_v(" "),_c('button',{staticClass:"dropdown-item btn-primary",attrs:{"type":"button","title":"Scarica come ebook in formato epub"},on:{"click":function($event){return window_open('/api/articles.epub?ids=' + ids);}}},[_c('svg',{attrs:{"width":"1em","height":"1em"}},[_c('use',{attrs:{"xlink:href":"#book"}})]),_v(" "),_c('span',[_v(" epub")])]),_v(" "),_c('button',{staticClass:"dropdown-item btn-primary",attrs:{"type":"button","title":"Scarica come ebook in formato amazon kindle"},on:{"click":function($event){return window_open('/api/articles.mobi?ids=' + ids);}}},[_c('svg',{attrs:{"width":"1em","height":"1em"}},[_c('use',{attrs:{"xlink:href":"#amazon"}})]),_v(" "),_c('span',[_v(" mobi")])]),_v(" "),_c('button',{staticClass:"dropdown-item btn-primary",attrs:{"type":"button","title":"Scarica come ebook in formato pdf"},on:{"click":function($event){return window_open('/articles_pdf.php?ids=' + ids);}}},[_c('svg',{attrs:{"width":"1em","height":"1em"}},[_c('use',{attrs:{"xlink:href":"#file-pdf"}})]),_v(" "),_c('span',[_v(" pdf")])]),_v(" "),_c('button',{staticClass:"dropdown-item btn-primary",attrs:{"type":"button","title":"Scarica in formato testo"},on:{"click":function($event){return window_open('/api/articles.txt?ids=' + ids);}}},[_c('svg',{attrs:{"width":"1em","height":"1em"}},[_c('use',{attrs:{"xlink:href":"#align-justify"}})]),_v(" "),_c('span',[_v(" txt")])])])]),_v(" "),_c('div',{staticClass:"dropdown btn-group btn-group-sm",attrs:{"role":"group"}},[_c('button',{staticClass:"btn btn-info",attrs:{"type":"button","id":"send_menu","data-toggle":"dropdown","aria-haspopup":"true","aria-expanded":"false","title":"Invia per e-mail"}},[_c('svg',{staticStyle:{"fill":"white"},attrs:{"width":"1em","height":"1em"}},[_c('use',{attrs:{"xlink:href":"#envelope"}})])]),_v(" "),_c('div',{staticClass:"dropdown-menu",attrs:{"aria-labelledby":"send_menu"}},[_c('a',{staticClass:"dropdown-item disabled",attrs:{"href":"#","tabindex":"-1","aria-disabled":"true"}},[_v("Invia per e-mail a "+_s(login.login.email)+" nel formato che preferisci:")]),_v(" "),_c('button',{staticClass:"dropdown-item btn-info",attrs:{"type":"button","title":"Invia per e-mail in formato html"},on:{"click":function($event){return email('html');}}},[_c('svg',{attrs:{"width":"1em","height":"1em"}},[_c('use',{attrs:{"xlink:href":"#code"}})]),_v(" "),_c('span',[_v(" html")])]),_v(" "),_c('button',{staticClass:"dropdown-item btn-info",attrs:{"type":"button","title":"Invia per e-mail come ebook in formato epub"},on:{"click":function($event){return email('epub');}}},[_c('svg',{attrs:{"width":"1em","height":"1em"}},[_c('use',{attrs:{"xlink:href":"#book"}})]),_v(" "),_c('span',[_v(" epub")])]),_v(" "),_c('button',{staticClass:"dropdown-item btn-info",attrs:{"type":"button","title":"Invia per e-mail come ebook in formato amazon kindle"},on:{"click":function($event){return email('mobi');}}},[_c('svg',{attrs:{"width":"1em","height":"1em"}},[_c('use',{attrs:{"xlink:href":"#amazon"}})]),_v(" "),_c('span',[_v(" mobi")])]),_v(" "),_c('button',{staticClass:"dropdown-item btn-info",attrs:{"type":"button","title":"Invia per e-mail come ebook in formato pdf"},on:{"click":function($event){return email('pdf');}}},[_c('svg',{attrs:{"width":"1em","height":"1em"}},[_c('use',{attrs:{"xlink:href":"#file-pdf"}})]),_v(" "),_c('span',[_v(" pdf")])]),_v(" "),_c('button',{staticClass:"dropdown-item btn-info",attrs:{"type":"button","title":"Invia per e-mail in formato testo"},on:{"click":function($event){return email('txt');}}},[_c('svg',{attrs:{"width":"1em","height":"1em"}},[_c('use',{attrs:{"xlink:href":"#align-justify"}})]),_v(" "),_c('span',[_v(" txt")])])])]),_v(" "),_c('div',{directives:[{name:"show",rawName:"v-show",value:(share),expression:"share"}],staticClass:"dropdown btn-group btn-group-sm",attrs:{"role":"group"}},[_c('button',{staticClass:"btn btn-danger",attrs:{"type":"button","id":"share_menu","data-toggle":"dropdown","aria-haspopup":"true","aria-expanded":"false","title":"Condividi"}},[_c('svg',{staticStyle:{"fill":"white"},attrs:{"width":"1em","height":"1em"}},[_c('use',{attrs:{"xlink:href":"#share"}})])]),_v(" "),_c('div',{staticClass:"dropdown-menu",attrs:{"aria-labelledby":"share_menu"}},[_c('a',{staticClass:"dropdown-item disabled",attrs:{"href":"#","tabindex":"-1","aria-disabled":"true"}},[_v("Condividi questo articolo:")]),_v(" "),_c('a',{staticClass:"dropdown-item btn-danger",attrs:{"href":"#","title":"Copia link nel clipboard","role":"button"},on:{"click":function($event){return copy_link();}}},[_c('svg',{attrs:{"width":"1em","height":"1em"}},[_c('use',{attrs:{"xlink:href":"#link"}})]),_v(" "),_c('span',[_v(" copia link")])]),_v(" "),_c('a',{staticClass:"dropdown-item btn-danger",attrs:{"href":'https://www.facebook.com/sharer.php?u=https%3A%2F%2F' + window.location.host + '%2Farticle%2F' + ids,"title":"Condividi con Facebook","role":"button","target":"_blank"}},[_c('svg',{attrs:{"width":"1em","height":"1em"}},[_c('use',{attrs:{"xlink:href":"#facebook"}})]),_v(" "),_c('span',[_v(" facebook")])]),_v(" "),_c('a',{staticClass:"dropdown-item btn-danger",attrs:{"href":'https://www.linkedin.com/shareArticle?mini=true&url=https%3A%2F%2F' + window.location.host + '%2Farticle%2F' + ids,"title":"Condividi con Linkedin","role":"button","target":"_blank"}},[_c('svg',{attrs:{"width":"1em","height":"1em"}},[_c('use',{attrs:{"xlink:href":"#linkedin-in"}})]),_v(" "),_c('span',[_v(" linkedin")])]),_v(" "),_c('a',{staticClass:"dropdown-item btn-danger",attrs:{"href":'https://mastodon.social/share?text=https%3A%2F%2F' + window.location.host + '%2Farticle%2F' + ids,"title":"Condividi con Mastodon","role":"button","target":"_blank"}},[_c('svg',{attrs:{"width":"1em","height":"1em"}},[_c('use',{attrs:{"xlink:href":"#mastodon"}})]),_v(" "),_c('span',[_v(" mastodon")])]),_v(" "),_c('a',{staticClass:"dropdown-item btn-danger",attrs:{"href":'https://www.reddit.com/submit?url=https%3A%2F%2F' + window.location.host + '%2Farticle%2F' + ids,"title":"Condividi con Reddit","role":"button","target":"_blank"}},[_c('svg',{attrs:{"width":"1em","height":"1em"}},[_c('use',{attrs:{"xlink:href":"#reddit"}})]),_v(" "),_c('span',[_v(" reddit")])]),_v(" "),_c('a',{staticClass:"dropdown-item btn-danger",attrs:{"href":'https://telegram.me/share/url?url=https%3A%2F%2F' + window.location.host + '%2Farticle%2F' + ids,"title":"Condividi con Telegram","role":"button","target":"_blank"}},[_c('svg',{attrs:{"width":"1em","height":"1em"}},[_c('use',{attrs:{"xlink:href":"#telegram"}})]),_v(" "),_c('span',[_v(" telegram")])]),_v(" "),_c('a',{staticClass:"dropdown-item btn-danger",attrs:{"href":'https://twitter.com/intent/tweet?url=https%3A%2F%2F' + window.location.host + '%2Farticle%2F' + ids,"title":"Condividi con Twitter","role":"button","target":"_blank"}},[_c('svg',{attrs:{"width":"1em","height":"1em"}},[_c('use',{attrs:{"xlink:href":"#twitter"}})]),_v(" "),_c('span',[_v(" twitter")])]),_v(" "),_c('a',{staticClass:"dropdown-item btn-danger",attrs:{"href":'https://api.whatsapp.com/send?text=https%3A%2F%2F' + window.location.host + '%2Farticle%2F' + ids,"title":"Condividi con Whatsapp","role":"button","target":"_blank"}},[_c('svg',{attrs:{"width":"1em","height":"1em"}},[_c('use',{attrs:{"xlink:href":"#whatsapp"}})]),_v(" "),_c('span',[_v(" whatsapp")])])])]),_v(" "),(share && login.login.groups.indexOf('staff') != -1)?_c('a',{staticClass:"btn btn-warning",attrs:{"href":'/edit_article.php?id=' + ids,"role":"button","id":"edit_menu","aria-haspopup":"true","aria-expanded":"false","title":"Modifica l'articolo"}},[_c('svg',{staticStyle:{"fill":"white"},attrs:{"width":"1em","height":"1em"}},[_c('use',{attrs:{"xlink:href":"#pencil-alt"}})])]):_e()])} 
  },
  staticRenderFns: [function(_c) {with(this){return _c('div',{staticClass:"modal-header"},[_c('h5',{staticClass:"modal-title"},[_v("Estratto notizie inviato!")]),_v(" "),_c('button',{staticClass:"close",attrs:{"type":"button","data-dismiss":"modal","aria-label":"Chiudi"}},[_c('span',{attrs:{"aria-hidden":"true"}},[_v("×")])])])}},function(_c) {with(this){return _c('label',{staticClass:"form-check-label"},[_c('input',{staticClass:"form-check-input",attrs:{"type":"checkbox","id":"cleanup"}}),_v("\n                svuota la lista degli articoli interessanti\n              ")])}},function(_c) {with(this){return _c('div',{staticClass:"modal-footer"},[_c('button',{staticClass:"btn btn-secondary",attrs:{"type":"button","data-dismiss":"modal"}},[_v("OK")])])}},],
  methods: {
    window_open: function(url) {
      window.open(url)
    },
    copy_link: function() {
      // https://stackoverflow.com/a/30810322
      var textArea = document.createElement("textarea");

      // *** This styling is an extra step which is likely not required. ***
      //
      // Why is it here? To ensure:
      // 1. the element is able to have focus and selection.
      // 2. if element was to flash render it has minimal visual impact.
      // 3. less flakyness with selection and copying which **might** occur if
      //    the textarea element is not visible.
      //
      // The likelihood is the element won't even render, not even a
      // flash, so some of these are just precautions. However in
      // Internet Explorer the element is visible whilst the popup
      // box asking the user for permission for the web page to
      // copy to the clipboard.

      // Place in top-left corner of screen regardless of scroll position.
      textArea.style.position = 'fixed';
      textArea.style.top = 0;
      textArea.style.left = 0;

      // Ensure it has a small width and height. Setting to 1px / 1em
      // doesn't work as this gives a negative w/h on some browsers.
      textArea.style.width = '2em';
      textArea.style.height = '2em';

      // We don't need padding, reducing the size if it does flash render.
      textArea.style.padding = 0;

      // Clean up any borders.
      textArea.style.border = 'none';
      textArea.style.outline = 'none';
      textArea.style.boxShadow = 'none';

      // Avoid flash of white box if rendered for any reason.
      textArea.style.background = 'transparent';

      textArea.value = 'https://' + window.location.host + '/article/' + this.ids;

      document.body.appendChild(textArea);
      /* Select the text field */
      textArea.focus();
      textArea.select();
      textArea.setSelectionRange(0, 99999); // For mobile devices

      try {
        var successful = document.execCommand('copy');
        var msg = successful ? 'successful' : 'unsuccessful';
        console.log('Copying text command was ' + msg);
      } catch (err) {
        console.log('Oops, unable to copy');
      }

      document.body.removeChild(textArea);
    }, 
    email: function(format) {
      var xhr = new XMLHttpRequest();
      this.format = format;
      var url = '/email.php?format=' + format + '&ids=' + this.ids;
      xhr.open('GET', url);
      xhr.onload = function () {
        $('#modal_email_sent').modal({keyboard: false});
      };
      xhr.send();
    }
  }
});




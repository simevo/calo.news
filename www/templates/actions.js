// configuration for jshint
/* jshint browser: true, devel: true */
/* global app, Vue, api, bus, sendReceiveData */

Vue.component('actions', {
  props: ['news', 'index'],
  render: function(_c) {
    with(this){return _c('div',{staticClass:"btn-toolbar",attrs:{"role":"toolbar","aria-label":"Toolbar with button groups"}},[_c('div',{staticClass:"btn-group mr-2",attrs:{"role":"group","aria-label":"First group"}},[_c('button',{staticClass:"btn btn-outline-primary button-narrow",class:{ 'icon-primary': news.dismissed, 'icon-light': !news.dismissed },attrs:{"type":"button","title":news.dismissed ? 'non nascondere l\'articolo' : 'nascondi l\'articolo'},on:{"click":function($event){return toggle_dismiss();}}},[_c('svg',{staticClass:"icon",attrs:{"width":"1em","height":"1em"}},[_c('use',{attrs:{"xlink:href":"#trash-alt"}})])]),_v(" "),_c('button',{staticClass:"btn btn-outline-info button-narrow",class:{ 'icon-info': news.to_read, 'icon-light': !news.to_read },attrs:{"type":"button","id":'selected_' + news.id,"title":news.to_read ? 'non segnare l\'articolo come interessante' : 'segna l\'articolo come interessante'},on:{"click":function($event){return toggle_to_read();}}},[_c('svg',{staticClass:"icon",attrs:{"width":"1em","height":"1em"}},[_c('use',{attrs:{"xlink:href":"#shopping-cart"}})])]),_v(" "),_c('button',{staticClass:"btn btn-outline-secondary button-narrow",class:{ 'icon-danger': news.my_rating < 0, 'icon-light': news.my_rating == 0, 'icon-success': news.my_rating > 0 },attrs:{"type":"button","title":"valuta l'articolo"},on:{"click":function($event){return rate(index, news);}}},[_c('svg',{staticClass:"icon",style:({transform: 'rotate(' + (news.my_rating < 0 ? 0 : 180) + 'deg)'}),attrs:{"width":"1em","height":"1em"}},[_c('use',{attrs:{"xlink:href":"#thumbs-down"}})])])])])} 
  },
  methods: {
    toggle_to_read: function () {
      var n = this.news;
      console.log('set to_read for ' + n.id + ' to ' + !n.to_read);
      document.getElementById('selected_' + n.id).setAttribute('disabled', 'disabled');
      var dataTo = { to_read: !n.to_read };
      sendReceiveData(dataTo, 'POST', api + '/articles/' + n.id, function () {
        n.to_read = !n.to_read;
        bus.$emit('marked', n.id, n.to_read);
        document.getElementById('selected_' + n.id).removeAttribute('disabled');
      });
    },
    rate: function(index, item) {
      bus.$emit('rating', index, item);
    },
    toggle_dismiss: function() {
      var n = this.news;
      // update the value server-side
      var dataTo = { dismissed: !n.dismissed };
      sendReceiveData(dataTo, 'POST', api + '/articles/' + n.id, function () {
        n.dismissed = !n.dismissed;
        // update the cached value
        bus.$emit('dismissed', n.id, n.dismissed);
      });
    }
  }
});



// configuration for jshint
/* jshint browser: true, devel: true */
/* global Vue */

var NewsGrid = {
  render: function(_c) {
    with(this){return _c('div',[_c('div',{staticStyle:{"position":"absolute","z-index":"100","left":"0.5em"}},[_c('button',{staticClass:"btn btn-small btn-info",class:{ active: filter_blacklist },attrs:{"type":"button","aria-haspopup":"true","aria-expanded":"false","title":"Filtra articoli in base alla blacklist"},on:{"click":function($event){return blacklist_onclick()}}},[_c('svg',{staticStyle:{"fill":"white"},attrs:{"width":"1em","height":"1em"}},[_c('use',{attrs:{"xlink:href":"#ban"}})])]),_v(" "),_c('button',{staticClass:"btn btn-small btn-warning",class:{ active: filter_old },staticStyle:{"position":"inherit","left":"0","top":"2.5em"},attrs:{"type":"button","aria-haspopup":"true","aria-expanded":"false","title":"Nascondi articoli vecchi"},on:{"click":function($event){filter_old = !filter_old;}}},[_c('svg',{staticStyle:{"fill":"white"},attrs:{"width":"1em","height":"1em"}},[_c('use',{attrs:{"xlink:href":"#atom"}})])])]),_v(" "),_c('div',{directives:[{name:"infinite-scroll",rawName:"v-infinite-scroll",value:(loadMore),expression:"loadMore"}],staticClass:"wrapper",attrs:{"infinite-scroll-disabled":"busy","infinite-scroll-distance":"100"}},_l((news_filtered),function(n,i){return _c('news-panel',{key:n.id,attrs:{"news":n,"index":i}})}),1),_v(" "),_c('div',{directives:[{name:"show",rawName:"v-show",value:(!busy && news_filtered.length == 0),expression:"!busy && news_filtered.length == 0"}],staticClass:"text-center"},[_v("\n      Non ci sono ancora notizie per te, riprova tra qualche minuto !\n    ")]),_v(" "),_c('div',{directives:[{name:"show",rawName:"v-show",value:(busy),expression:"busy"}],staticClass:"text-center"},[_m(0)])])} 
  },
  staticRenderFns: [function(_c) {with(this){return _c('div',{staticClass:"spinner-border text-primary m-5",attrs:{"role":"status"}},[_c('span',{staticClass:"sr-only"},[_v("Loading...")])])}},],
  data: function () {
    return {
      blacklist: '',
      filter_blacklist: false,
      atfll: 1E6,
      filter_old: false,
      active: false,
      filter1: 'filtered',
      news: [],
      retrieved: -1,
      busy: false,
      feed: -1,
      query: '',
      view: '',
      author: '',
      cache_age: 0,
      freeze: true,
      pick_feed: -1,
      only_read: false,
      selected_feed: {
        incomplete: true,
        active: true,
        language: "it",
        tags: ['blog'],
        title: "",
        id: 4,
        url: "",
        average_time_from_last_post: 5897,
        last_polled: "2017-11-25T13:37:08.908792+00:00",
        rating: null,
        epoch: 1511617028,
        license: "",
        my_rating: 1,
        salt_url: false,
        views: 1431,
        count: 4254,
        icon: "images/internazionale-icon.png",
        homepage: ""
      }
    };
  },
  watch: {
    '$route': function(to, fro) {
      // react to route changes...
      var old_status = this.status;
      console.log('NewsGrid go from ' + JSON.stringify(fro.params) + ' to ' + JSON.stringify(to.params) + ' status = ' +  JSON.stringify(old_status));
      this.query = '';
      this.view = '';
      this.author = '';
      this.feed = -1;
      if (('feed' in to.params) && (to.params.feed != -1))
        this.feed = to.params.feed;
      if (('author' in to.params) && (to.params.author !== ''))
        this.author = to.params.author;
      if (('query' in to.params) && (to.params.query != ''))
        this.query = to.params.query;
      if (('view' in to.params) && (to.params.view != ''))
        this.view = to.params.view;
      if (this.active) {
        this.reset();
        this.loadMore();
      }
    }
  },
  computed: {
    feeds: function() {
      var fs = {};
      this.news.forEach(function (a) {
        if (!(a.feed_id in fs)) {
          fs[a.feed_id] = {'icon': a.icon, 'name': a.feed, 'premium': a.premium};
        }
      });
      return fs;
    },
    news_filtered: function() {
      var self = this;
      var regexp = '(' + self.blacklist.toLowerCase().replace(/ /g, '|') + ')';
      var now = Date.now() / 1000.0; // Date.now() method returns the number of milliseconds elapsed since January 1, 1970 00:00:00 UTC
      console.log('regexp = ' , regexp);
      var X = 3.0;
      var news0 = self.news.filter(function(n) {
        return !n.dismissed && (!self.filter_blacklist || self.blacklist == '' || (n.title + n.title_original + n.excerpt).toLowerCase().match(regexp) == null) && (!self.filter_old || 
        now - n.epoch < X * self.atfll);
      });
      var news1;
      if (self.only_read) {
        news1 = news0.filter(function(n) { return n.read; });
      } else {
        news1 = news0;
      }
      if (self.pick_feed != -1) {
        return news1.filter(function(n) { return n.feed_id == self.pick_feed; });
      } else {
        return news1;
      }
    },
    description_filter1: function() {
      var self = this;
      window.focus();
      if (document.activeElement) {
        document.activeElement.blur();
      }
      if (self.filter1) {
        var new_filter1 = self.filter1;
        console.log('old filter1 = ' + old_filter1 + ' new filter1 = ' + new_filter1);
        var is_same = (new_filter1 == old_filter1);
        if (old_filter1 && !is_same) {
          localStorage.setItem("filter", self.filter1);
          old_filter1 = new_filter1;
          self.empty_list();
          self.loadMore();
        }
        return description(self.filter1);
      } else {
        return '';
      }
    },
    to_read_news: function() {
      return this.news.filter(function(n) { return n.to_read; });
    },
    to_read_ids: function() {
      var ids = [];
      this.to_read_news.forEach(function (a) { ids.push(a.id); });
      return ids.join(',');
    },
    offset: function() {
      return this.news.length;
    },
    status: function() {
      return {login_name: login_name, view: this.view, query: this.query, filter: this.filter1, author: this.author, feed: this.feed, component: 'grid'};
    }
  },
  methods: {
    blacklist_onclick: function() {
      if (this.blacklist == '') {
        window.location.href = 'https://' + window.location.host + '/settings.php#blacklists';
      } else {
        this.filter_blacklist = !this.filter_blacklist;
      }
    },
    loadFeed: function() {
      var self = this;
      sendReceiveData(null, 'GET', api + '/feeds/' + self.feed, function (dataFrom) {
        self.selected_feed = dataFrom.feeds[0];
      });
    },
    empty_list: function() {
      console.log('empty list');
      this.retrieved = -1;
      this.news = [];
    },
    loadMore: function() {
      console.log('loadMore, active = ', this.active);
      var atfll = storage.get('average_time_from_last_login');
      if (atfll == null) {
        this.atfll = 1E6; // initialize as 1E6 s = 278 h = 11.6 d
      } else {
        var ll = storage.get('last_login');
        var tfll = (Date.now() - ll) / 1E3; // s
        var weight = 0.3;
        this.atfll = Math.max(3600, tfll * weight + atfll * (1.0 - weight));
      }
      storage.set('last_login', Date.now());
      storage.set('average_time_from_last_login', this.atfll);
      if (!this.active)
        return;
      var self = this;
      console.log('busy = ', self.busy);
      console.log('retrieved = ', self.retrieved);
      console.log('news.length = ', self.news.length);
      if (self.busy || (self.retrieved === 0))
        return;
      self.busy = true;
      sendReceiveData(null, 'GET', self.endpoint(), function (dataFrom) {
        var new_articles = dataFrom.articles.filter(function(n) { return !n.read && !n.interesting; });
        new_articles.forEach(function (a) {
          if (a.excerpt) {
            var excerpt_length = 200;
            if (a.views > 0 && !a.read) 
              excerpt_length = 400;
            a.excerpt = a.excerpt.replace(/<\/?[^>]+(>|$)/g, "").substring(0, excerpt_length);
          }
        });
        console.log('received articles: ' + new_articles.length);
        self.news = self.news.concat(new_articles);
        self.save_cache(self.retrieved == -1);
        self.retrieved = new_articles.length;
        self.blacklist = dataFrom.blacklist;
        self.busy = false;
      });
    },
    email: function(format) {
      var xhr = new XMLHttpRequest();
      var url = '/email.php?format=' + format + '&ids=' + this.to_read_ids;
      xhr.open('GET', url);
      xhr.onload = function () {
        $('#myModal').modal({keyboard: false});
      };
      xhr.send();
    },
    reset: function() {
      console.log('NewsGrid reset');
      this.retrieved = -1;
      this.cache_age = 0;
      this.news = [];
      this.save_cache(true);
      this.freeze = true;
      this.selected_feed.title = '';
    },
    save_cache: function(fresh) {
      var self = this;
      console.log('entered save_cache');
      if (fresh) {
        storage.set('news_cached', Date.now());
      }
      storage.set('news_status', self.status);
      storage.set('news', self.news);
    },
    endpoint: function() {
      if (Object.keys(login).length === 0) {
        // user is not logged in
        return api + '/articles?offset=' + this.offset;
      } else if (this.view !== '') {
        return api + '/articles.smart?offset=' + this.offset + '&filter=' + encodeURIComponent(this.view);
      } else if (this.query !== '') {
        // full-text search query
        return api + '/articles?offset=' + this.offset + '&query=' + encodeURIComponent(this.query);
      } else if (this.author !== '') {
        // author query
        return api + '/articles?offset=' + this.offset + '&author=' + encodeURIComponent(this.author);
      } else if (this.feed != -1) {
        // feed query
        this.loadFeed();
        return api + '/articles?offset=' + this.offset + '&feed=' + this.feed;
      } else {
        return api + '/articles.smart?offset=' + this.offset +  "&filter=mixed&timestamp=" + new Date().getTime();
      }
    },
    unmark_all: function() {
      var self = this;
      sendReceiveData(null, 'DELETE', api + '/articles/to_read', function () {
        self.to_read_news.forEach(function (a) { a.to_read = false; });
        self.save_cache(false);
        self.empty_list();
        self.loadMore();
      });
      return false;
    },
    mark_all: function() {
      this.news.filter(function(n) {
        if (!n.to_read) {
          var dataTo = { to_read: true };
          sendReceiveData(dataTo, 'POST', api + '/articles/' + n.id, function () {
            n.to_read = true;
          });
        }
      });
      this.save_cache(false);
      return false;
    },
    update_news: function(id, read, to_read, rating, topic_id, dismissed) {
      console.log('updating news ', id);
      var self = this;
      var index = -1;
      var found = self.news.some(function(n, i, a) {
        if (n.id == id) {
          index = i;
          if (read !== undefined) { n.read = read; n.views =  n.views + 1; }
          if (to_read !== undefined) { n.to_read = to_read; }
          if (dismissed !== undefined) { n.dismissed = dismissed; }
          if (rating !== undefined) { n.my_rating = rating; }
          if (topic_id !== undefined) { n.comments = 1; n.topic_id = topic_id; }
          console.log('updated news item: ', n);
          return true;
        } else {
          return false;
        }
      });
      if (found) {
        console.log('updating news in local storage');
        storage.set('news', self.news);
      }
    },
    rgb: function(age) {
      // returns a CSS rgb color that goes from white when the age is 0
      // to black when the age is equal to or greater than 3600 seconds
      var graylevel = 255 - Math.min(255, Math.round(256.0 * Math.max(age, 0.0) / 3600.0));
      var decColor = (1 + 256 * (1 + 256)) * graylevel;
      return '#' + decColor.toString(16);
    },
    update_cache_age: function() {
      var news_cached = storage.get('news_cached');
      this.cache_age = (Date.now() - news_cached) / 1000.0; // seconds
      setTimeout(this.update_cache_age, 60*1000.0);
    }
  },
  created: function() {
    var self = this;
    console.log('NewsGrid created with parameters: ' + JSON.stringify(this.$route.params));
    this.busy = true;
    if (('query' in this.$route.params) && (this.$route.params.query != '')) {
      this.query = this.$route.params.query;
      document.getElementById("query").value = this.query;
    }
    if (('view' in this.$route.params) && (this.$route.params.view!= '')) {
      this.view = this.$route.params.view;
    }
    if (('feed' in this.$route.params) && (this.$route.params.feed != -1)) {
      this.feed = this.$route.params.feed;
      this.loadFeed();
    }
    if (('author' in this.$route.params) && (this.$route.params.author !== '')) {
      this.author = this.$route.params.author;
    }
    this.filter1 = old_filter1 = (localStorage.getItem("filter") || 'filtered');
    bus.$on('read', function(id) {
      console.log('read article: ', id);
      self.update_news(id, true, undefined, undefined, undefined, undefined);
    });
    bus.$on('marked', function(id, to_read) {
      console.log('marked article: ', id, ' to read: ', to_read);
      self.update_news(id, undefined, to_read, undefined, undefined, undefined);
    });
    bus.$on('dismissed', function(id, dismissed) {
      console.log('marked article: ', id, ' dismissed: ', dismissed);
      self.update_news(id, undefined, undefined, undefined, undefined, dismissed);
    });
    bus.$on('rated', function(id, rating) {
      console.log('rated article: ', id, ' = ', rating);
      self.update_news(id, undefined, undefined, rating, undefined, undefined);
    });
    bus.$on('commented', function(id, topic_id) {
      console.log('commented article: ', id);
      self.update_news(id, undefined, undefined, undefined, topic_id, undefined);
    });
    bus.$on('unmark', function() {
      self.unmark_all();
    });
  },
  updated: function() {
    console.log('unfreeze');
    this.freeze = false;
  },
  activated: function() {
    console.log('NewsGrid activated');
    this.active = true;
    this.busy = false;
    this.loadMore();
  },
  deactivated: function() {
    console.log('NewsGrid deactivated');
    this.active = false;
  }
};


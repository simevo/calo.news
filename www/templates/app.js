// configuration for jshint
/* jshint browser: true, devel: true */
/* global app, Vue, VueRouter, bus */

var routes = [
  { path: '/', component: NewsGrid },
  { path: '/view/:view', component: NewsList },
  { path: '/feed/:feed', component: NewsList },
  { path: '/author/:author', component: NewsList },
  { path: '/search/:query', component: NewsList },
  { path: '/article/:id', component: Article }
];

var router = new VueRouter({
  mode: 'history',
  scrollBehavior: function(to, fro, savedPosition) {
    // native-like behavior when navigating with back/forward buttons
    if (savedPosition) {
      return savedPosition;
    } else {
      return { x: 0, y: 0 };
    }
  },
  routes: routes
});

var app = new Vue({
  router: router,
  directives: { InfiniteScroll: infiniteScroll },
  el: '#app',
  render: function(_c) {
    with(this){return _c('div',[_c('div',{directives:[{name:"show",rawName:"v-show",value:(showModal),expression:"showModal"}],staticClass:"modal",attrs:{"id":"rating_modal","role":"dialog","tabindex":"-1"},on:{"keyup":function($event){if(!$event.type.indexOf('key')&&_k($event.keyCode,"esc",27,$event.key,["Esc","Escape"]))return null;$event.stopPropagation();showModal = false;}}},[_c('div',{staticClass:"modal-dialog",attrs:{"role":"document"}},[_c('div',{attrs:{"id":"rating_modal_mask"}}),_v(" "),_c('div',{staticClass:"modal-content"},[_c('div',{staticClass:"modal-header"},[_c('strong',{staticClass:"modal-title"},[_v("Valuta l'articolo \""+_s(title)+"\"")]),_v(" "),_c('button',{staticClass:"close",attrs:{"type":"button","aria-label":"Close"},on:{"click":function($event){showModal = false;}}},[_c('span',{attrs:{"aria-hidden":"true"}},[_v("×")])])]),_v(" "),_c('div',{staticClass:"modal-body"},[_c('rating-toolbar',{staticClass:"float-right",attrs:{"id":"rate_control","endpoint":"articles","item":item}})],1)])])]),_v(" "),_c('keep-alive',[_c('router-view')],1)],1)} 
  },
  data: {
    showModal: false,
    index: -1,
    item: {
      title: '',
      title_original: ''
    }
  },
  created: function() {
    var self = this;
    bus.$on('rated', function(id, rating) {
      self.showModal = false;
    });
    bus.$on('rating', function(index, item) {
      console.log('selected article: ', index);
      self.index = index;
      self.item = item;
      self.showModal = true;
      Vue.nextTick(function() { document.getElementById('rating_modal').focus(); });
    });
  },
  computed: {
    title: function() {
      return this.item.title || this.item.title_original || '[Senza titolo]';
    }
  }
});


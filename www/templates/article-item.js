// configuration for jshint
/* jshint browser: true, devel: true */
/* global Vue, getRandomInt, icons, feeds, secondsToString, getParameterByName, api, font_size, storage, $, sendReceiveData */

var url = window.location.href;

var bus = new Vue();

var Article = {
  render: function(_c) {
    with(this){return _c('div',{staticClass:"container"},[(loading)?_c('div',{staticClass:"text-center"},[_m(0)]):_c('div',[_c('div',{staticClass:"row"},[_c('div',{staticClass:"text-center col-md-8 offset-md-2"},[_c('div',[_c('a',{attrs:{"href":'/feed/' + article.feed_id,"title":'vai a tutti gli articoli della fonte ' + article.feed}},[_c('span',{staticClass:"h2"},[(article.premium)?_c('img',{style:('background:url(/' + article.icon + ');background-size:contain;'),attrs:{"src":"images/wreath.png","width":"30px","height":"30px","alt":"premium"}}):_c('img',{attrs:{"width":"30px","height":"30px","src":'/' + article.icon,"alt":"feed logo"}}),_v(" "+_s(article.feed))])])]),_v(" "),_c('div',{staticClass:"text-muted"},[_c('div',{staticClass:"float-left"},[_c('svg',{attrs:{"width":"1em","height":"1em"}},[_c('use',{attrs:{"xlink:href":"#clock"}})]),_v(" "),_c('small',[_v(" Tempo di lettura stimato: "+_s(secondsToString1(60*article.length/6/300))+" ")])]),_v(" "),_c('div',{staticClass:"float-right"},[_c('small',[_v(_s(secondsToString(article.age))+" ("+_s(new Date(article.epoch * 1000).toLocaleString())+")")])])])])]),_v(" "),_c('div',{staticClass:"row"},[_c('div',{staticClass:"col-md-8 offset-md-2 title"},[_c('h1',{staticClass:"float-left font-weight-bold"},[_c('span',{attrs:{"id":"title_tts"}},[(article.language == window.base_language && article.title)?_c('span',{attrs:{"lang":window.base_language}},[_v(_s(article.title))]):_e(),_v(" "),(article.language != window.base_language && article.title_original)?_c('span',{attrs:{"lang":article.language}},[_v("["+_s(article.title_original)+"] ")]):_e(),_v(" "),(!article.title_original && !article.title)?_c('span',{attrs:{"lang":window.base_language}},[_v("[Senza titolo]")]):_e()]),_v(" "),(article.language != window.base_language && article.title)?_c('span',{attrs:{"lang":window.base_language}},[_v(_s(article.title))]):_e()])])]),_v(" "),_c('div',{staticClass:"row"},[_c('div',{staticClass:"col-md-4 offset-md-2 text-muted"},[_c('small',[_v("Articolo originale:")]),_v(" "),_c('a',{attrs:{"target":"_blank","href":article.url}},[_c('small',{staticStyle:{"word-wrap":"break-word"}},[_v(_s(article.url))])]),_v(" "),_c('br')]),_v(" "),_c('div',{staticClass:"text-muted col-md-4"},[_c('small',{staticClass:"float-right"},[_v("di "),_c('a',{attrs:{"href":'/author/' + encodeURIComponent(article.author),"title":'vai a tutti gli articoli con ' + article.author + ' come autore'}},[_c('span',[_v(_s(article.author))])]),_v(" "+_s(article.license))]),_v(" "),_c('br'),_v(" "),_c('actions',{staticClass:"float-right",attrs:{"news":article}}),_v(" "),_c('a',{directives:[{name:"show",rawName:"v-show",value:(article.comments > 0),expression:"article.comments > 0"}],staticClass:"comments float-right",attrs:{"href":"#comment_list","title":article.comments + ' comment' + (article.comments > 1 ? 'i' : 'o')}},[_c('span',{staticClass:"badge badge-pill badge-info"},[_v(_s(article.comments))])])],1)]),_v(" "),_c('div',{staticClass:"row"},[_c('div',{staticClass:"col-md-8 offset-md-2"},[(article.language != window.base_language && !article.content)?_c('button',{staticClass:"btn btn-secondary btn-sm",attrs:{"type":"button","title":"Traduci"},on:{"click":function($event){return translate();}}},[_c('svg',{staticStyle:{"fill":"white"},attrs:{"width":"1em","height":"1em"}},[_c('use',{attrs:{"xlink:href":"#globe"}})])]):_e(),_v(" "),_c('download-send-share',{attrs:{"ids":article.id,"clean":false,"share":true}}),_v(" "),(tts && !tts_open && (paragraphs.length > 0))?_c('button',{staticClass:"btn btn-success btn-sm",attrs:{"type":"button","id":"button-speak","title":"Leggi ad alta voce"},on:{"click":function($event){return tts_speak();}}},[_c('svg',{staticStyle:{"fill":"white"},attrs:{"width":"1em","height":"1em"}},[_c('use',{attrs:{"xlink:href":"#play"}})])]):_e(),_v(" "),(tts && tts_open)?_c('div',{staticClass:"btn-group btn-group-sm",staticStyle:{"position":"fixed","z-index":"1000","bottom":"1em","left":"1em"},attrs:{"id":"button-tts","role":"group","aria-label":"Leggi ad alta voce"}},[_c('button',{staticClass:"btn btn-success",attrs:{"type":"button","id":"button-restart","title":"Dall'inizio"},on:{"click":function($event){return tts_restart();}}},[_c('svg',{staticStyle:{"fill":"white"},attrs:{"width":"1em","height":"1em"}},[_c('use',{attrs:{"xlink:href":"#fast-backward"}})])]),_v(" "),_c('button',{staticClass:"btn btn-success",attrs:{"type":"button","id":"button-back","title":"Indietro","disabled":current_paragraph == 0},on:{"click":function($event){return tts_back();}}},[_c('svg',{staticStyle:{"fill":"white"},attrs:{"width":"1em","height":"1em"}},[_c('use',{attrs:{"xlink:href":"#backward"}})])]),_v(" "),_c('button',{staticClass:"btn btn-success",attrs:{"type":"button","id":"button-stop","title":"Stop","disabled":!speaking},on:{"click":function($event){return tts_stop();}}},[_c('svg',{staticStyle:{"fill":"white"},attrs:{"width":"1em","height":"1em"}},[_c('use',{attrs:{"xlink:href":"#pause"}})])]),_v(" "),_c('button',{staticClass:"btn btn-success",attrs:{"type":"button","id":"button-continue","title":"Continua","disabled":speaking},on:{"click":function($event){return tts_continue();}}},[_c('svg',{staticStyle:{"fill":"white"},attrs:{"width":"1em","height":"1em"}},[_c('use',{attrs:{"xlink:href":"#play"}})])]),_v(" "),_c('button',{staticClass:"btn btn-success",attrs:{"type":"button","id":"button-forward","title":"Avanti","disabled":current_paragraph >= paragraphs.length - 1},on:{"click":function($event){return tts_forward();}}},[_c('svg',{staticStyle:{"fill":"white"},attrs:{"width":"1em","height":"1em"}},[_c('use',{attrs:{"xlink:href":"#forward"}})])]),_v(" "),_c('button',{staticClass:"btn btn-success",attrs:{"type":"button","id":"button-close","title":"Chiudi"},on:{"click":function($event){return tts_close();}}},[_c('svg',{staticStyle:{"fill":"white"},attrs:{"width":"1em","height":"1em"}},[_c('use',{attrs:{"xlink:href":"#stop"}})])])]):_e(),_v(" "),_c('hr')],1)]),_v(" "),_c('div',{staticClass:"row"},[(article.language != window.base_language && article.content)?[_c('div',{class:{ 'col-md-8 offset-md-2': collapsed, 'col-md-6': !collapsed }},[_c('input',{directives:[{name:"model",rawName:"v-model",value:(collapsed),expression:"collapsed"}],staticStyle:{"float":"left"},attrs:{"type":"checkbox","title":"mostra / nascondi testo tradotto"},domProps:{"checked":Array.isArray(collapsed)?_i(collapsed,null)>-1:(collapsed)},on:{"change":function($event){var $a=collapsed,$el=$event.target,$c=$el.checked?(true):(false);if(Array.isArray($a)){var $v=null,$i=_i($a,$v);if($el.checked){$i<0&&(collapsed=$a.concat([$v]))}else{$i>-1&&(collapsed=$a.slice(0,$i).concat($a.slice($i+1)))}}else{collapsed=$c}}}}),_v(" "),_c('div',{staticClass:"content",attrs:{"lang":article.language,"id":"content_tts"},domProps:{"innerHTML":_s(article.content_original)}})]),_v(" "),_c('div',{staticClass:"col-md-6",class:{ 'collapse': collapsed, 'collapse.show': !collapsed }},[_c('div',{staticClass:"content",domProps:{"innerHTML":_s(article.content)}})])]:[_c('div',{staticClass:"content col-md-8 offset-md-2",attrs:{"lang":article.content ? window.base_language : article.language,"id":"content_tts"},domProps:{"innerHTML":_s(article.content || article.content_original)}})]],2),_v(" "),_c('div',{staticClass:"row"},[_c('div',{staticClass:"col-md-8 offset-md-2"},[_c('hr'),_v(" "),_c('h2',{attrs:{"id":"comment_list"}},[_v("Commenti")]),_v(" "),(article.comments > 0)?[_c('p',[(article.comments > 1)?_c('span',[_v("Ci sono")]):_c('span',[_v("C'è")]),_v(" "),_c('a',{staticClass:"comments",attrs:{"href":window.discourse_base_url + '/t/' + article.topic_id}},[_c('span',{staticClass:"badge badge-pill badge-info"},[_v(_s(article.comments))])]),_v("comment"),(article.comments > 1)?_c('span',[_v("i")]):_c('span',[_v("o")]),_v(":\n            ")]),_v(" "),_c('ul',_l((comments),function(c){return _c('li',[_c('img',{staticStyle:{"border-radius":"50%"},attrs:{"alt":"profilo","width":"32","height":"32","src":window.discourse_base_url + c.avatar_template.replace('{size}', '32')}}),_v(" "),_c('span',[_v(_s(c.name))]),_v(": "),_c('span',{domProps:{"innerHTML":_s(c.cooked)}})])}),0),_v(" "),_c('p',[_v("Vai alla "),_c('a',{attrs:{"href":window.discourse_base_url + '/t/' + article.topic_id}},[_v("sezione commenti")]),_v(" per proseguire la conversazione !")])]:[_c('div',{attrs:{"id":"comment_form"}},[_c('p',[_v("Non ci sono ancora commenti, inserisci il primo:")]),_v(" "),_c('textarea',{directives:[{name:"model",rawName:"v-model",value:(comment),expression:"comment"}],staticClass:"form-control",attrs:{"id":"text","name":"text","rows":"2"},domProps:{"value":(comment)},on:{"input":function($event){if($event.target.composing)return;comment=$event.target.value}}}),_v(" "),_c('br'),_v(" "),_c('button',{staticClass:"btn btn-primary",attrs:{"disabled":comment.length == 0},on:{"click":function($event){return send_comment();}}},[_v("Invia commento")])]),_v(" "),_c('div',{staticStyle:{"display":"none"},attrs:{"id":"comment_confirm"}},[_c('p',[_v("Il tuo commento è stato inviato, grazie !")]),_v(" "),_c('p',[_v("Vai alla "),_c('a',{attrs:{"href":window.discourse_base_url + '/t/' + article.topic_id}},[_v("sezione commenti")]),_v(" se vuoi modificarlo.")])])]],2)]),_v(" "),_c('div',{staticClass:"row"},[_c('div',{staticClass:"col-md-8 offset-md-2"},[_c('hr'),_v(" "),_c('h2',{staticClass:"row"},[_v("Articoli simili")]),_v(" "),_c('div',{directives:[{name:"show",rawName:"v-show",value:(similar.length == 0),expression:"similar.length == 0"}]},[_v("Nessun risultato per ora.")]),_v(" "),_l((similar),function(n){return _c('news-item',{key:n.id,attrs:{"news":n}})})],2)])])])} 
  },
  staticRenderFns: [function(_c) {with(this){return _c('div',{staticClass:"spinner-border text-primary m-5",attrs:{"role":"status"}},[_c('span',{staticClass:"sr-only"},[_v("Loading...")])])}},],
  data: function () {
    return {
      active: false,
      article: {},
      comment: '',
      comments: [],
      similar: [],
      busy: false,
      loading: true,
      collapsed: false,
      tts: false,
      tts_open: false,
      paragraphs: [],
      speaking: false,
      current_paragraph: 0,
      stopped: false
    };
  },
  created: function () {
    console.log('Article created');
    this.loadData();
  },
  mounted: function () {
    console.log('Article mounted');
  },
  activated: function() {
    console.log('Article activated');
    var button_tts = document.getElementById('button-tts');
    if (button_tts) button_tts.hidden = true;
    var button_speak = document.getElementById('button-speak');
    if (button_speak) button_speak.hidden = true;
    this.active = true;
  },
  deactivated: function() {
    console.log('Article deactivated');
    this.tts_stop();
    var button_tts = document.getElementById('button-tts');
    if (button_tts) button_tts.hidden = true;
    var button_speak = document.getElementById('button-speak');
    if (button_speak) button_speak.hidden = true;
    this.active = false;
  },
  watch: {
    '$route': function(to, fro) {
      // react to route changes...
      console.log('Article to from ' + JSON.stringify(fro.params) + ' to ' + JSON.stringify(to.params));
      this.loadData();
    }
  },
  methods: {
    translate: function() {
      console.log('translate');
      var self = this;
      self.loading = true;
      sendReceiveData(null, 'GET', "/api/translate/" + self.article.id, function (dataFrom) {
        self.article = dataFrom;
        self.article.cached = Date.now();
        storage.set_element('articles', self.article.id, self.article);
        self.loading = false;
      });
    },
    send_comment: function() {
      console.log('send_comment');
      var self = this;
      var dataTo = {
        "text": document.getElementById("text").value.trim()
      };
      sendReceiveData(dataTo, 'POST', "/api/articles/" + self.article.id + "/comment", function (dataFrom) {
        document.getElementById("comment_form").style.display = "none";
        document.getElementById("comment_confirm").style.display = "block";
        if (dataFrom) {
          self.article.comments = 1;
          self.article.topic_id = dataFrom.topic_id;
          // notify NewsList
          bus.$emit('commented', self.$route.params.id, dataFrom.topic_id);
          // recache article
          self.article.cached = Date.now();
          storage.set_element('articles', self.article.id, self.article);
        }
      });
    },
    loadData: function() {
      var self = this;
      this.loading = true;
      console.log('Article loadData', self.$route.params.id);
      if (!self.$route.params.id)
        return;
      // first fetch article from cache
      var article = (storage.get('articles') || {})[self.$route.params.id];      
      if (article) {
        self.article = article;
        self.loading = false;
        Vue.nextTick(function () {
          setTimeout(function(){
            self.tts_init();
            $('.content a').off('click').on('click', function(e) {
              self.reroute_link(e, this.href);
            });
          }, 2000);
        });
        var similar = (storage.get('similar') || {})[self.$route.params.id];
        if (similar) {
          self.similar = similar;
        }
        var comments = (storage.get('comments') || {})[self.$route.params.id];
        if (comments) {
          self.comments = comments;
        }
      }
      sendReceiveData(null, 'GET', api + '/articles/' + self.$route.params.id, function (dataFrom) {
        bus.$emit('read', self.$route.params.id);
        var article_cached = self.article.cached;
        self.article = dataFrom;
        Vue.nextTick(function () {
          setTimeout(function(){
            self.tts_init();
            $('.content a').off('click').on('click', function(e) {
              self.reroute_link(e, this.href);
          });
          }, 2000);
        });
        self.loading = false;
        self.article.cached = Date.now();
        storage.set_element('articles', self.article.id, self.article);
        // consider cache for similar articles valid only if stale by less than 3600 s
        console.log('article cached = ', article_cached);
        console.log('cache age (s) = ', (Date.now() - article_cached)/1000.0);
        if (!article_cached || (Date.now() - article_cached >= 3.6E6)) {
          sendReceiveData(null, 'GET', api + '/articles/' + self.$route.params.id + '/similar', function (dataFrom) {
            self.similar = dataFrom.articles;
            storage.set_element('similar', self.article.id, self.similar);
          });
          if (self.article.comments > 0) {
            sendReceiveData(null, 'GET', api + '/comments/' + self.article.topic_id, function (dataFrom) {
              self.comments = dataFrom.comments;
              storage.set_element('comments', self.article.id, self.comments);
            });
          } // comments > 0
        }
      });
    },
    tts_init: function() {
      if ('speechSynthesis' in window) {
        console.log('TTS API available');
        var content_tts = document.getElementById('content_tts');
        if (content_tts) {
          lang = content_tts.getAttribute('lang');
          console.log('looking for voices with lang = ' + lang);
          find_voice(voices, lang);
          if (voice) {
            console.log('voice = ', voice);
            var title_tts = document.getElementById('title_tts');
            this.paragraphs = [title_tts].concat(getParagraphs(content_tts));
            this.current_paragraph = 0;
            this.tts = true;
            console.log('ready');
          } else {
            console.log('no voice found !');
            this.tts = false;
          } // voice found
        } else {
          console.log('no content');
          this.tts = false;
        } // voice found
      } else {
        console.log('no TTS API !');
        this.tts = false;
      }
    },
    tts_speak: function() {
      console.log('speak button pressed');
      this.tts_open = true;
      this.current_paragraph = 0;
      this.tts_continue();
    },
    tts_stop: function() {
      this.stopped = true;
      window.speechSynthesis.cancel();
      console.log('Speaker stopped ' + this.current_paragraph);
    },
    tts_continue: function() {
      this.stopped = false;
      this.read_paragraph();
    },
    tts_restart: function() {
      this.stopped = true;
      window.speechSynthesis.cancel();
      this.tts_speak();
      this.tts_continue();
    },
    tts_back: function() {
      this.stopped = true;
      window.speechSynthesis.cancel();
      this.current_paragraph -= 1;
      this.tts_continue();
    },
    tts_forward: function() {
      this.stopped = true;
      window.speechSynthesis.cancel();
      this.current_paragraph += 1;
      this.tts_continue();
    },
    tts_close: function() {
      this.stopped = true;
      window.speechSynthesis.cancel();
      this.tts_open = false;
    },
    read_paragraph: function () {
      var self = this;
      console.log('Reading ' + this.current_paragraph);
      console.log('paragraphs = ' + this.paragraphs);
      if (this.paragraphs.length <= this.current_paragraph) {
        this.current_paragraph = this.paragraphs.length;
        this.tts_open = false;
        return;
      }
      if (this.current_paragraph < 0) {
        this.current_paragraph = 0;
      }
      var paragraph = this.paragraphs[this.current_paragraph];
      if ('innerText' in paragraph) {
        var utterance = new window.SpeechSynthesisUtterance();
        utterance.voice = voice;
        utterance.lang = voice.lang;
        utterance.text = paragraph.innerText;
        utterance.addEventListener('start', function() {
          console.log('Speaker started ' + self.current_paragraph);
          self.speaking = true;
          paragraph.style.backgroundColor = 'lightgray';
          paragraph.scrollIntoView();
        });
        utterance.addEventListener('end', function(e) {
          console.log('Speaker finished ' + self.current_paragraph + ' in ' + e.elapsedTime + ' seconds.');
          self.speaking = false;
          paragraph.style.backgroundColor = 'white';
          if (!self.stopped) {
            self.current_paragraph += 1;
            self.read_paragraph();
          }
        });
        window.speechSynthesis.speak(utterance);
      } else {
        this.current_paragraph += 1;
        this.read_paragraph();
      }
    },
    reroute_link: function(e, url) {
      console.log('reroute_link = ', url);
      if (url.substring(0, 1) == '/') {
        console.log('local link passthrough');
        return true;
      } else if (url.substring(0, aggregator_base_url.length) == aggregator_base_url) {
        console.log(aggregator_base_url + ' link passthrough');
        return true;
      } else if (url.substring(0, discourse_base_url.length) == discourse_base_url) {
        console.log(discourse_base_url + ' link passthrough');
        return true;
      } else {
        e.preventDefault();
        console.log('rerouting link ...');
        var self = this;
        this.loading = true;
        sendReceiveData(null, 'GET', api + '/links/' + encodeURIComponent(url), function (dataFrom) {
          var id = dataFrom.id;
          if (id == -1) {
            self.loading = false;
            Vue.nextTick(function () {
              console.log('opening link in new tab');
              window.open(url, '_blank');
            });
          } else {
            console.log('opening article ', id, ' in this tab');
            var newurl = '/article/' + id;
            self.$router.push(newurl);
            return false;
          }
        }, false);
      }
    }
  }
};

document.addEventListener('DOMContentLoaded', function() {
  console.log("DOM fully loaded and parsed");
  font_size();
});


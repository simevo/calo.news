// configuration for jshint
/* jshint browser: true, devel: true */
/* global app, Vue, bus, api, $, sendReceiveData */

Vue.component('rating-toolbar', {
  props: ['item', 'endpoint'],
  render: function(_c) {
    with(this){return _c('div',{staticClass:"btn-toolbar float-right",attrs:{"role":"toolbar","aria-label":"Toolbar with button groups"}},[_c('div',{staticClass:"btn-group mr-2",attrs:{"role":"group","aria-label":"negative ratings"}},[_c('negative-rating',{attrs:{"endpoint":endpoint,"item":item,"threshold":-4}}),_v(" "),_c('negative-rating',{attrs:{"endpoint":endpoint,"item":item,"threshold":-3}}),_v(" "),_c('negative-rating',{attrs:{"endpoint":endpoint,"item":item,"threshold":-2}}),_v(" "),_c('negative-rating',{attrs:{"endpoint":endpoint,"item":item,"threshold":-1}}),_v(" "),_c('negative-rating',{attrs:{"endpoint":endpoint,"item":item,"threshold":0}})],1),_v(" "),_c('div',{staticClass:"btn-group",attrs:{"role":"group","aria-label":"positive ratings"}},[_c('positive-rating',{attrs:{"endpoint":endpoint,"item":item,"threshold":0}}),_v(" "),_c('positive-rating',{attrs:{"endpoint":endpoint,"item":item,"threshold":1}}),_v(" "),_c('positive-rating',{attrs:{"endpoint":endpoint,"item":item,"threshold":2}}),_v(" "),_c('positive-rating',{attrs:{"endpoint":endpoint,"item":item,"threshold":3}}),_v(" "),_c('positive-rating',{attrs:{"endpoint":endpoint,"item":item,"threshold":4}})],1)])} 
  }
});


// configuration for jshint
/* jshint browser: true, devel: true */
/* global app, Vue, bus, api, $, sendReceiveData */

function set(item, rating, endpoint) {
  if (item.my_rating == rating) {
    rating = 0;
  }
  console.log('set rating for ' + item.id + ' to ' + rating);
  $('div#rate_control button').attr('disabled', 'disabled');
  item.my_rating = rating;
  // update the value server-side
  var dataTo = { rating: rating };
  sendReceiveData(dataTo, 'POST', api + '/' + endpoint + '/' + item.id, function () {
    $('div#rate_control button').removeAttr('disabled');
    bus.$emit('rated', item.id, rating);
  });
}

Vue.component('negative-rating', {
  props: ['item', 'threshold', 'endpoint'],
  render: function(_c) {
    with(this){return _c('button',{staticClass:"btn btn-outline-danger button-narrow",class:{ 'icon-danger': item.my_rating < threshold, 'icon-light': item.my_rating >= threshold },attrs:{"type":"button"},on:{"click":function($event){return set(item, threshold - 1, endpoint)}}},[_c('svg',{attrs:{"width":"1em","height":"1em"}},[_c('use',{attrs:{"xlink:href":"#thumbs-down"}})])])} 
  },
  methods: {
    set: set
  }
});


// configuration for jshint
/* jshint browser: true, devel: true */
/* global Vue */

Vue.component('news-item', {
  props: ['news', 'index'],
  render: function(_c) {
    with(this){return _c('li',{staticClass:"card mb-3",class:{ 'bg-light': news.read, 'border-primary': news.views > 0 && !news.read },staticStyle:{"flex-direction":"row","line-height":"1.1"},attrs:{"id":'news_' + news.id}},[_c('div',{staticClass:"float-left"},[_c('img',{attrs:{"src":'/' + news.icon,"width":"30","height":"30","alt":""}}),_v(" "),_c('span',{directives:[{name:"show",rawName:"v-show",value:(news.author.length > 0),expression:"news.author.length > 0"}]},[_c('span',{staticClass:"text-muted"},[_v(_s(news.author)+" – ")])]),_v(" "),_c('span',{staticClass:"text-muted"},[_v(_s(secondsToString(news.age))+" – ")]),_v(" "),_c('a',{attrs:{"href":'/article.php?id=' + news.id}},[(news.language != window.base_language && news.title)?_c('span',[_v("["+_s(news.title_original)+"] ")]):_e(),_c('span',[_v(_s(news.title || news.title_original))]),(!news.title_original && !news.title)?_c('span',{attrs:{"lang":window.base_language}},[_v("[Senza titolo]")]):_e()])])])} 
  }
});


// configuration for jshint
/* jshint browser: true, devel: true */
/* global Vue */

// global var to store the last received user filter1
var old_filter1 = '';

var NewsList =  {
  render: function(_c) {
    with(this){return _c('div',{directives:[{name:"infinite-scroll",rawName:"v-infinite-scroll",value:(loadMore),expression:"loadMore"}],staticClass:"container-fluid",attrs:{"infinite-scroll-disabled":"busy","infinite-scroll-distance":"100"}},[(feed == -1 && query == '' && author == '')?[_c('div',{staticClass:"row justify-content-md-center",staticStyle:{"padding-bottom":"0.5em"}},[_c('div',{staticClass:"col-md-4"},[_c('select',{directives:[{name:"model",rawName:"v-model",value:(filter1),expression:"filter1"}],staticStyle:{"font-family":"Arial, FontAwesome"},attrs:{"name":"filter1"},on:{"change":function($event){var $selectedVal = Array.prototype.filter.call($event.target.options,function(o){return o.selected}).map(function(o){var val = "_value" in o ? o._value : o.value;return val}); filter1=$event.target.multiple ? $selectedVal : $selectedVal[0]}}},[_c('option',{staticStyle:{"color":"black"},attrs:{"value":"mixed"}},[_v("!   Mix")]),_v(" "),_c('option',{staticStyle:{"color":"green"},attrs:{"value":"filtered"}},[_v("   Filtrati")]),_v(" "),_c('option',{staticStyle:{"color":"red"},attrs:{"value":"latest"}},[_v("   Ultima ora")]),_v(" "),_c('option',{staticStyle:{"color":"black"},attrs:{"value":"suggestions"}},[_v("   Suggeriti")]),_v(" "),_c('option',{staticStyle:{"color":"grey"},attrs:{"value":"read"}},[_v("   Cronologia")]),_v(" "),_c('option',{staticStyle:{"color":"blue"},attrs:{"value":"to_read"}},[_v("   Interessanti")]),_v(" "),_c('option',{staticStyle:{"color":"orange"},attrs:{"value":"keywords"}},[_v("◎   Preferiti")]),_v(" "),_c('option',{staticStyle:{"color":"magenta"},attrs:{"value":"popular"}},[_v("   Popolari")]),_v(" "),_c('option',{staticStyle:{"color":"cyan"},attrs:{"value":"premium"}},[_v("   Premium")]),_v(" "),_c('option',{staticStyle:{"color":"black"},attrs:{"value":"foreign"}},[_v("拼   In lingua")]),_v(" "),_c('option',{staticStyle:{"color":"black"},attrs:{"value":"dismissed"}},[_v("   Nascosti")])]),_v(" "),_c('a',{attrs:{"href":"#","title":'il newsfeed è vecchio di ' + Math.round(cache_age/60.0) + ' minuti: ricaricalo!'},on:{"click":function($event){reset(); loadMore();}}},[_c('svg',{style:({ fill: rgb(cache_age) }),attrs:{"width":"1em","height":"1em"}},[_c('use',{attrs:{"xlink:href":"#sync-alt"}})])])]),_v(" "),_c('div',{staticClass:"col-md-8"},[_c('span',{directives:[{name:"show",rawName:"v-show",value:(description_filter1),expression:"description_filter1"}],staticClass:"text-muted",staticStyle:{"display":"none"}},[_v(_s(description_filter1))])])]),_v(" "),_c('div',{directives:[{name:"show",rawName:"v-show",value:(news.length > 0 && filter1 == 'to_read'),expression:"news.length > 0 && filter1 == 'to_read'"}],staticClass:"row",staticStyle:{"display":"none","padding-bottom":"10px"}},[_c('div',{staticClass:"col-md-4"},[_c('a',{attrs:{"href":"#"},on:{"click":function($event){return unmark_all();}}},[_c('svg',{attrs:{"width":"1em","height":"1em"}},[_c('use',{attrs:{"xlink:href":"#square"}})]),_v(" "),_c('small',[_v(" Svuota la lista degli articoli interessanti")])])]),_v(" "),_c('div',{staticClass:"col-md-4"},[_c('download-send-share',{attrs:{"ids":to_read_ids,"clean":true,"share":false}}),_v(" "),_c('button',{directives:[{name:"show",rawName:"v-show",value:(tts && !tts_open && (to_read_news.length > 0)),expression:"tts && !tts_open && (to_read_news.length > 0)"}],staticClass:"btn btn-success btn-sm",attrs:{"type":"button","id":"button-speak","title":"Leggi tutti gli articoli interessanti ad alta voce"},on:{"click":function($event){return tts_speak();}}},[_c('svg',{staticStyle:{"fill":"white"},attrs:{"width":"1em","height":"1em"}},[_c('use',{attrs:{"xlink:href":"#play"}})])]),_v(" "),_c('div',{directives:[{name:"show",rawName:"v-show",value:(tts && tts_open),expression:"tts && tts_open"}]},[_c('div',[_v("\n              Lettura sequenziale\n            ")]),_v(" "),_c('div',[_c('p',[_v(_s(tts_message))])]),_v(" "),_c('div',{staticClass:"modal-footer"},[_c('button',{staticClass:"modal-default-button",on:{"click":function($event){return tts_stop();}}},[_v("\n                Ferma lettura\n              ")])])])],1),_v(" "),_c('div',{directives:[{name:"show",rawName:"v-show",value:(to_read_news.length < news.length),expression:"to_read_news.length < news.length"}],staticClass:"col-md-4"},[_c('a',{attrs:{"href":"#"},on:{"click":function($event){return mark_all();}}},[_c('svg',{attrs:{"width":"1em","height":"1em"}},[_c('use',{attrs:{"xlink:href":"#check-square"}})]),_v(" "),_c('small',[_v(" Segna tutti come interessanti")])])])])]:_e(),_v(" "),(feed != -1 && selected_feed.title !== '')?[_c('h2',[_v("Risultati della ricerca per fonte: \""+_s(selected_feed.title)+"\"")]),_v(" "),_c('feed-item',{attrs:{"feed":selected_feed,"freeze":freeze,"readonly":false}})]:_e(),_v(" "),(author != '')?[_c('h2',[_v("Risultati della ricerca per autore: \""+_s(author)+"\"")]),_v(" "),_c('div',{staticClass:"pb-3"},[_c('a',{attrs:{"href":'/search/' + author}},[_v("cerca invece \""+_s(author)+"\" a tutto testo")])])]:_e(),_v(" "),(query != '')?[_c('h2',[_v("Risultati della ricerca a tutto testo di \""+_s(query)+"\"")]),_v(" "),_c('div',{staticClass:"pb-3"},[_c('a',{attrs:{"href":'/author/' + query}},[_v("cerca invece \""+_s(query)+"\" per autore")])])]:_e(),_v(" "),(((feed != -1 && selected_feed.title !== '') || (author != '') || (query != '')) && (news.length > 0))?[_c('form',{staticStyle:{"background-color":"lightgrey","padding":"5px","margin-bottom":"1em"}},[_c('div',{staticClass:"row h2"},[_c('div',{staticClass:"col"},[_c('a',{attrs:{"href":"#","title":"ricarica"},on:{"click":function($event){reset(); loadMore();}}},[_c('svg',{attrs:{"width":"1em","height":"1em"}},[_c('use',{attrs:{"xlink:href":"#sync-alt"}})])]),_v(" "),_c('a',{attrs:{"href":"#","title":"carica altri"},on:{"click":function($event){return loadMore();}}},[_m(0)])])]),_v(" "),_c('div',{staticClass:"form-check"},[_c('label',{staticClass:"form-check-label col-md-12"},[_c('input',{directives:[{name:"model",rawName:"v-model",value:(only_read),expression:"only_read"}],staticClass:"form-check-input",attrs:{"type":"checkbox"},domProps:{"checked":Array.isArray(only_read)?_i(only_read,null)>-1:(only_read)},on:{"change":function($event){var $a=only_read,$el=$event.target,$c=$el.checked?(true):(false);if(Array.isArray($a)){var $v=null,$i=_i($a,$v);if($el.checked){$i<0&&(only_read=$a.concat([$v]))}else{$i>-1&&(only_read=$a.slice(0,$i).concat($a.slice($i+1)))}}else{only_read=$c}}}}),_v("\n            mostra solo articoli già letti da te\n          ")])]),_v(" "),_c('div',{directives:[{name:"show",rawName:"v-show",value:(Object.keys(feeds).length > 1),expression:"Object.keys(feeds).length > 1"}],staticClass:"form-check"},[_c('h5',[_v("Filtra per fonte:")]),_v(" "),_c('label',{staticClass:"form-check-label h2 p-1",class:{ 'dimmed': pick_feed != -1 },attrs:{"title":"mostra tutti gli articoli"}},[_c('input',{directives:[{name:"model",rawName:"v-model",value:(pick_feed),expression:"pick_feed"}],staticClass:"form-check-input",attrs:{"type":"radio","value":"-1","hidden":""},domProps:{"checked":_q(pick_feed,"-1")},on:{"change":function($event){pick_feed="-1"}}}),_v(" "),_c('svg',{attrs:{"width":"1em","height":"1em"}},[_c('use',{attrs:{"xlink:href":"#question-circle"}})])]),_v(" "),_l((feeds),function(feed,feed_id){return _c('label',{staticClass:"form-check-label p-1",class:{ 'dimmed': pick_feed != feed_id },attrs:{"title":'mostra solo gli articoli provenienti dalla fonte ' + feed.name}},[_c('input',{directives:[{name:"model",rawName:"v-model",value:(pick_feed),expression:"pick_feed"}],staticClass:"form-check-input",attrs:{"type":"radio","hidden":""},domProps:{"value":feed_id,"checked":_q(pick_feed,feed_id)},on:{"change":function($event){pick_feed=feed_id}}}),_v(" "),(feed.premium)?_c('img',{style:('background:url(/' + feed.icon + ');background-size:contain;'),attrs:{"src":"images/wreath.png","width":"30px","height":"30px","alt":"premium"}}):_c('img',{attrs:{"width":"30px","height":"30px","src":'/' + feed.icon,"alt":"feed logo"}})])})],2)])]:_e(),_v(" "),_c('div',{directives:[{name:"show",rawName:"v-show",value:(retrieved >= 0 && !busy && news.length == 0),expression:"retrieved >= 0 && !busy && news.length == 0"}],staticStyle:{"display":"none"}},[_c('span',[_v("Mi spiace, non trovo niente ...")]),_v(" "),_c('div',{directives:[{name:"show",rawName:"v-show",value:(filter1 === 'keywords' && feed === -1 && author === '' && query === ''),expression:"filter1 === 'keywords' && feed === -1 && author === '' && query === ''"}],staticClass:"alert alert-danger",attrs:{"role":"alert"}},[_v("\n        Affinché gli articoli appaiano qui devi inserire delle parole chiave o degli autori preferiti "),_m(1),_v(" !\n      ")])]),_v(" "),_c('ul',{attrs:{"id":"slippylist"}},_l((news_filtered),function(n,i){return _c('news-item',{key:n.id,attrs:{"news":n,"index":i}})}),1),_v(" "),_c('div',{directives:[{name:"show",rawName:"v-show",value:(busy),expression:"busy"}],staticClass:"mx-auto",staticStyle:{"width":"50px"}},[_m(2)])],2)} 
  },
  staticRenderFns: [function(_c) {with(this){return _c('div',{staticClass:"spinner-static text-primary",attrs:{"role":"status"}},[_c('span',{staticClass:"sr-only"},[_v("Loading...")])])}},function(_c) {with(this){return _c('a',{attrs:{"href":"/settings.php#lists"}},[_v("nella linguetta "),_c('strong',[_v("Vista \"preferiti\"")]),_v(" delle impostazioni")])}},function(_c) {with(this){return _c('div',{staticClass:"spinner-border text-primary",attrs:{"role":"status"}},[_c('span',{staticClass:"sr-only"},[_v("Loading...")])])}},],
  data: function () {
    return {
      dirty: false,
      active: false,
      filter1: 'filtered',
      news: [],
      retrieved: -1,
      busy: false,
      feed: -1,
      query: '',
      author: '',
      cache_age: 0,
      freeze: true,
      pick_feed: -1,
      only_read: false,
      selected_feed: {
        incomplete: true,
        active: true,
        language: "it",
        tags: ['blog'],
        title: "",
        id: 4,
        url: "",
        average_time_from_last_post: 5897,
        last_polled: "2017-11-25T13:37:08.908792+00:00",
        rating: null,
        epoch: 1511617028,
        license: "",
        my_rating: 1,
        salt_url: false,
        views: 1431,
        count: 4254,
        icon: "images/internazionale-icon.png",
        homepage: ""
      },
      tts: true,
      tts_open: false,
      tts_message: ""
    };
  },
  watch: {
    '$route': function(to, fro) {
      console.log('$route go from ' + fro.path + ' to ' + to.path);
      let slug = to.path.split('/')[1];
      let slugs = ['view', 'feed', 'author', 'search'];
      if (slugs.find(s => s === slug)) {
        // react to route changes...
        var old_status = this.status;
        console.log('NewsList go from ' + JSON.stringify(fro.params) + ' to ' + JSON.stringify(to.params) + ' status = ' +  JSON.stringify(old_status));
        this.query = '';
        this.author = '';
        this.feed = -1;
        if (('feed' in to.params) && (to.params.feed != -1))
          this.feed = to.params.feed;
        if (('author' in to.params) && (to.params.author !== ''))
          this.author = to.params.author;
        if (('query' in to.params) && (to.params.query != ''))
          this.query = to.params.query;
        var new_status = this.status;
        var is_same = shallow(old_status, new_status);
        console.log('NewsList new_status = ' +  JSON.stringify(new_status) + ' is_same = ' + (is_same ? 'true' : 'false'));
        if (!is_same) {
          if (this.active) {
            this.reset();
            this.loadMore();
          } else {
            console.log('set dirty');
            this.dirty = true;
          }
        }
      }
    }
  },
  computed: {
    feeds: function() {
      var fs = {};
      this.news.forEach(function (a) {
        if (!(a.feed_id in fs)) {
          fs[a.feed_id] = {'icon': a.icon, 'name': a.feed, 'premium': a.premium};
        }
      });
      return fs;
    },
    news_filtered: function() {
      var self = this;
      var news0 = self.news.filter(function(n) {
        return (self.filter1 == 'dismissed' || !n.dismissed);
      });
      var news1;
      if (self.only_read) {
        news1 = news0.filter(function(n) { return n.read; });
      } else {
        news1 = news0;
      }
      if (self.pick_feed != -1) {
        return news1.filter(function(n) { return n.feed_id == self.pick_feed; });
      } else {
        return news1;
      }
    },
    description_filter1: function() {
      var self = this;
      window.focus();
      if (document.activeElement) {
        document.activeElement.blur();
      }
      if (self.filter1) {
        var new_filter1 = self.filter1;
        console.log('old filter1 = ' + old_filter1 + ' new filter1 = ' + new_filter1);
        var is_same = (new_filter1 == old_filter1);
        if (old_filter1 && !is_same) {
          localStorage.setItem("filter", self.filter1);
          old_filter1 = new_filter1;
          self.empty_list();
          self.loadMore();
        }
        return description(self.filter1);
      } else {
        return '';
      }
    },
    to_read_news: function() {
      return this.news.filter(function(n) { return n.to_read; });
    },
    to_read_ids: function() {
      var ids = [];
      this.to_read_news.forEach(function (a) { ids.push(a.id); });
      return ids.join(',');
    },
    offset: function() {
      return this.news.length;
    },
    status: function() {
      return {login_name: login_name, query: this.query, filter: this.filter1, author: this.author, feed: this.feed, component: 'list'};
    }
  },
  methods: {
    loadFeed: function() {
      var self = this;
      sendReceiveData(null, 'GET', api + '/feeds/' + self.feed, function (dataFrom) {
        self.selected_feed = dataFrom.feeds[0];
      });
    },
    empty_list: function() {
      console.log('empty list');
      this.retrieved = -1;
      this.news = [];
    },
    loadMore: function() {
      if (!this.active)
        return;
      var self = this;
      // console.log('busy = ', self.busy);
      // console.log('retrieved = ', self.retrieved);
      console.log('news.length = ', self.news.length);
      if (self.busy || (self.retrieved === 0))
        return;
      self.busy = true;
      sendReceiveData(null, 'GET', self.endpoint(localStorage.getItem("filter") || 'filtered'), function (dataFrom) {
        var new_articles = dataFrom.articles;
        console.log('received articles: ' + new_articles.length);
        self.news = self.news.concat(new_articles);
        self.save_cache(self.retrieved == -1);
        self.retrieved = new_articles.length;
        self.busy = false;
      });
    },
    email: function(format) {
      var xhr = new XMLHttpRequest();
      var url = '/email.php?format=' + format + '&ids=' + this.to_read_ids;
      xhr.open('GET', url);
      xhr.onload = function () {
        $('#myModal').modal({keyboard: false});
      };
      xhr.send();
    },
    reset: function() {
      console.log('NewsList reset');
      this.retrieved = -1;
      this.cache_age = 0;
      this.news = [];
      this.save_cache(true);
      this.freeze = true;
      this.selected_feed.title = '';
    },
    save_cache: function(fresh) {
      var self = this;
      console.log('entered save_cache');
      if (fresh) {
        storage.set('news_cached', Date.now());
      }
      storage.set('news_status', self.status);
      storage.set('news', self.news);
    },
    endpoint: function(filt) {
      if (Object.keys(login).length === 0) {
        // user is not logged in
        return api + '/articles?offset=' + this.offset;
      } else if (login.login.groups.indexOf("soci") == -1) {
        // user is not active
        return api + '/articles?offset=' + this.offset;
      } else if (this.query !== '') {
        // full-text search query
        return api + '/articles?offset=' + this.offset + '&query=' + encodeURIComponent(this.query);
      } else if (this.author !== '') {
        // author query
        return api + '/articles?offset=' + this.offset + '&author=' + encodeURIComponent(this.author);
      } else if (this.feed != -1) {
        // feed query
        this.loadFeed();
        return api + '/articles?offset=' + this.offset + '&feed=' + this.feed;
      } else {
        return api + '/articles.smart?offset=' + this.offset + '&filter=' + filt;
      }
    },
    unmark_all: function() {
      var self = this;
      sendReceiveData(null, 'DELETE', api + '/articles/to_read', function () {
        self.to_read_news.forEach(function (a) { a.to_read = false; });
        self.save_cache(false);
        self.empty_list();
        self.loadMore();
      });
      return false;
    },
    mark_all: function() {
      this.news.filter(function(n) {
        if (!n.to_read) {
          var dataTo = { to_read: true };
          sendReceiveData(dataTo, 'POST', api + '/articles/' + n.id, function () {
            n.to_read = true;
          });
        }
      });
      this.save_cache(false);
      return false;
    },
    restore_from_cache: function() {
      var self = this;
      var news_cached = storage.get('news_cached');
      var news_status = storage.get('news_status');
      var is_same = shallow(news_status, self.status);
      self.cache_age = (Date.now() - news_cached) / 1000.0; // seconds
      setTimeout(self.update_cache_age, 60*1000.0);
      console.log('news_cached = ' + news_cached);
      console.log('age = ' + self.cache_age);
      if (is_same && (self.cache_age < 3600.0)) {
        var val = storage.get('news');
        val.forEach(function(n) { n.age = n.age + self.cache_age; });
        self.news = val;
        console.log('restored ' + val.length + ' news from cache with age = ' + self.cache_age);
        if (val.length == 0) {
          console.log('force cache outdate');
          self.cache_age = 0;
          storage.clear();
        } else {
          self.retrieved = val.length;
        }
        self.busy = false;
      } else {
        console.log('cache outdated');
        self.cache_age = 0;
        storage.clear();
        self.busy = false;
      }
    },
    update_news: function(id, read, to_read, rating, topic_id, dismissed) {
      console.log('updating news ', id);
      var self = this;
      var index = -1;
      var found = self.news.some(function(n, i, a) {
        if (n.id == id) {
          index = i;
          if (read !== undefined) { n.read = read; n.views =  n.views + 1; }
          if (to_read !== undefined) { n.to_read = to_read; }
          if (dismissed !== undefined) { n.dismissed = dismissed; }
          if (rating !== undefined) { n.my_rating = rating; }
          if (topic_id !== undefined) { n.comments = 1; n.topic_id = topic_id; }
          console.log('updated news item: ', n);
          return true;
        } else {
          return false;
        }
      });
      if (found) {
        console.log('updating news in local storage');
        storage.set('news', self.news);
      }
    },
    rgb: function(age) {
      // returns a CSS rgb color that goes from white when the age is 0
      // to black when the age is equal to or greater than 3600 seconds
      var graylevel = 255 - Math.min(255, Math.round(256.0 * Math.max(age, 0.0) / 3600.0));
      var decColor = (1 + 256 * (1 + 256)) * graylevel;
      return '#' + decColor.toString(16);
    },
    update_cache_age: function() {
      var news_cached = storage.get('news_cached');
      this.cache_age = (Date.now() - news_cached) / 1000.0; // seconds
      setTimeout(this.update_cache_age, 60*1000.0);
    },
    tts_speak: function() {
      var self = this;
      console.log('speak button pressed');
      if ('speechSynthesis' in window) {
        console.log('TTS API available');
          sendReceiveData(null, 'GET', api + '/articles.json?ids=' + this.to_read_ids, function (dataFrom) {
            var articles = dataFrom.articles;
            console.log('received articles: ' + articles.length);
            self.tts_open = true;
            self.tts_read(articles, 0);
          });
      } else {
        console.log('no TTS API !');
        this.tts = false;
      }

    },
    tts_read: function(articles, n) {
      var self = this;
      if (this.tts_open && n < articles.length) {
        console.log('speak ' + n);
        var article = articles[n];
        var text = "";
        var title = "";
        if (article.language == window.base_language) {
          text = article.title + '.' + stripHtml(article.content);
          title = article.title;
        } else {
          text = article.title_original + '.' + stripHtml(article.content_original);
          title = article.title_original;
        }
        this.tts_message = 'leggo "' + title + '" da ' + article.feed + ' del ' + article.stamp;
        console.log('looking for voices with lang = ' + article.language);
        find_voice(voices, article.language);
        if (voice) {
          console.log('voice = ', voice);
          var utterance = new window.SpeechSynthesisUtterance();
          utterance.voice = voice;
          utterance.lang = voice.lang;
          utterance.text = text;
          console.log(text);
          utterance.addEventListener('end', function(e) {
            console.log('Speaker finished in ' + e.elapsedTime + ' seconds.');
            self.tts_read(articles, n + 1);
          });
          window.speechSynthesis.speak(utterance);
        } // voice found
      } else {
        this.tts_open = false;
      }
    },
    tts_stop: function() {
      window.speechSynthesis.cancel();
      this.tts_open = false;
    }
  },
  created: function() {
    var self = this;
    console.log('NewsList created with parameters: ' + JSON.stringify(this.$route.params));
    this.busy = true;
    if (('query' in this.$route.params) && (this.$route.params.query != '')) {
      this.query = this.$route.params.query;
      document.getElementById("query").value = this.query;
    }
    if (('feed' in this.$route.params) && (this.$route.params.feed != -1)) {
      this.feed = this.$route.params.feed;
      this.loadFeed();
    }
    if (('author' in this.$route.params) && (this.$route.params.author !== '')) {
      this.author = this.$route.params.author;
    }
    this.filter1 = old_filter1 = (localStorage.getItem("filter") || 'filtered');
    this.restore_from_cache();
    bus.$on('read', function(id) {
      console.log('read article: ', id);
      self.update_news(id, true, undefined, undefined, undefined, undefined);
    });
    bus.$on('marked', function(id, to_read) {
      console.log('marked article: ', id, ' to read: ', to_read);
      self.update_news(id, undefined, to_read, undefined, undefined, undefined);
    });
    bus.$on('dismissed', function(id, dismissed) {
      console.log('marked article: ', id, ' dismissed: ', dismissed);
      self.update_news(id, undefined, undefined, undefined, undefined, dismissed);
    });
    bus.$on('rated', function(id, rating) {
      console.log('rated article: ', id, ' = ', rating);
      self.update_news(id, undefined, undefined, rating, undefined, undefined);
    });
    bus.$on('commented', function(id, topic_id) {
      console.log('commented article: ', id);
      self.update_news(id, undefined, undefined, undefined, topic_id, undefined);
    });
    bus.$on('unmark', function() {
      self.unmark_all();
    });
  },
  updated: function() {
    console.log('unfreeze');
    this.freeze = false;
    this.dirty = false;
  },
  activated: function() {
    console.log('NewsList activated');
    this.active = true;
    if (this.dirty) {
      this.reset();
      this.loadMore();
    }
  },
  deactivated: function() {
    console.log('NewsList deactivated');
    this.active = false;
  },
  mounted: function() {
    var self = this;
    var list = document.getElementById('slippylist');
    list.addEventListener('slip:swipe', function(e) {
      console.log('delete ', id);
      e.target.style.display = 'none';
      var id = e.target.id.substring(5);
      // update the cached value
      self.update_news(id, undefined, undefined, undefined, undefined, true);
      // update the value server-side
      var dataTo = { dismissed: true };
      sendReceiveData(dataTo, 'POST', api + '/articles/' + id, function () { });
    });
    new Slip(list, {minimumSwipeVelocity: 0.5});
  }
};


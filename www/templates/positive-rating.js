// configuration for jshint
/* jshint browser: true, devel: true */
/* global set, app, Vue, bus, api, $, sendReceiveData */

Vue.component('positive-rating', {
  props: ['item', 'threshold', 'endpoint'],
  render: function(_c) {
    with(this){return _c('button',{staticClass:"btn btn-outline-success button-narrow",class:{ 'icon-success': item.my_rating > threshold, 'icon-light': item.my_rating <= threshold },attrs:{"type":"button"},on:{"click":function($event){return set(item, threshold + 1, endpoint);}}},[_c('svg',{attrs:{"width":"1em","height":"1em"}},[_c('use',{attrs:{"xlink:href":"#star"}})])])}
  },
  methods: {
    set: set
  }
});


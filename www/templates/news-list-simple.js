// configuration for jshint
/* jshint browser: true, devel: true */
/* global Vue */

var NewsList =  {
  render: function(_c) {
    with(this){return _c('div',{directives:[{name:"infinite-scroll",rawName:"v-infinite-scroll",value:(loadMore),expression:"loadMore"}],staticClass:"container-fluid",attrs:{"infinite-scroll-disabled":"busy","infinite-scroll-distance":"100"}},[(author != '')?[_c('h2',[_v("Risultati della ricerca per autore: \""+_s(author)+"\"")]),_v(" "),_c('div',{staticClass:"pb-3"},[_c('a',{attrs:{"href":'/search/' + author}},[_v("cerca invece \""+_s(author)+"\" a tutto testo")])])]:_e(),_v(" "),(query != '')?[_c('h2',[_v("Risultati della ricerca a tutto testo di \""+_s(query)+"\"")]),_v(" "),_c('div',{staticClass:"pb-3"},[_c('a',{attrs:{"href":'/author/' + query}},[_v("cerca invece \""+_s(query)+"\" per autore")])])]:_e(),_v(" "),_c('div',{directives:[{name:"show",rawName:"v-show",value:(non_member),expression:"non_member"}],staticClass:"alert alert-danger",attrs:{"role":"alert"}},[_c('strong',[_v("Attenzione")]),_v(": al momento non sei soci* attivo del GIS (Gruppo di Informazione Solidale) quindi il tuo newsfeed non è più personalizzato; inoltre quando clicchi sugli articoli vieni dirottat* sulla fonte originale, quindi non puoi più leggere le fonti \"premium\" a cui siamo abbonati.\n      "),_c('br'),_c('br'),_v("\n      Per ottenere l'accesso completo a tutte le funzioni della piattaforma, ricarica ora il tuo conto sottoscrivendo un "),_c('strong',[_v("abbonamento digitale")]),_v(" oppure dando una somma a uno degli altri soci che ha il conto in attivo, e registrando la transazione nella "),_c('a',{attrs:{"href":window.discourse_base_url + '/c/' + window.eig_category_name + '/' + window.accounting_category_name}},[_v("categoria "+_s(window.accounting_category_name))]),_v(" !\n    ")]),_v(" "),_c('div',{directives:[{name:"show",rawName:"v-show",value:(retrieved >= 0 && !busy && news.length == 0),expression:"retrieved >= 0 && !busy && news.length == 0"}],staticStyle:{"display":"none"}},[_c('span',[_v("Mi spiace, non trovo niente ...")]),_v(" "),_c('div',{directives:[{name:"show",rawName:"v-show",value:(filter1 === 'keywords' && feed === -1 && author === '' && query === ''),expression:"filter1 === 'keywords' && feed === -1 && author === '' && query === ''"}],staticClass:"alert alert-danger",attrs:{"role":"alert"}},[_v("\n        Affinché gli articoli appaiano qui devi inserire delle parole chiave o degli autori preferiti "),_m(0),_v(" !\n      ")])]),_v(" "),_c('ul',{attrs:{"id":"slippylist"}},_l((news),function(n){return _c('news-item',{key:n.id,attrs:{"news":n}})}),1),_v(" "),_c('div',{directives:[{name:"show",rawName:"v-show",value:(busy),expression:"busy"}],staticClass:"mx-auto",staticStyle:{"width":"50px"}},[_m(1)])],2)} 
  },
  staticRenderFns: [function(_c) {with(this){return _c('a',{attrs:{"href":"/settings.php#lists"}},[_v("nella linguetta "),_c('strong',[_v("Vista \"preferiti\"")]),_v(" delle impostazioni")])}},function(_c) {with(this){return _c('div',{staticClass:"spinner-border text-primary",attrs:{"role":"status"}},[_c('span',{staticClass:"sr-only"},[_v("Loading...")])])}},],
  data: function () {
    return {
      active: false,
      filter1: 'filtered',
      news: [],
      retrieved: -1,
      busy: false,
      feed: -1,
      query: '',
      author: '',
      cache_age: 0,
      freeze: true,
      pick_feed: -1,
      only_read: false
    };
  },
  watch: {
    '$route': function(to, fro) {
      // react to route changes...
      console.log('NewsList go from ' + JSON.stringify(fro.params) + ' to ' + JSON.stringify(to.params));
      var old_status = this.status;
      this.query = '';
      this.author = '';
      this.feed = -1;
      if (('feed' in to.params) && (to.params.feed != -1))
        this.feed = to.params.feed;
      if (('author' in to.params) && (to.params.author !== ''))
        this.author = to.params.author;
      if (('query' in to.params) && (to.params.query != ''))
        this.query = to.params.query;
      var is_same = shallow(old_status, this.status);
      if (!is_same && this.active) {
        this.reset();
        this.loadMore();
      }
    }
  },
  computed: {
    non_member: function() {
      return (Object.keys(login).length > 0) && (login.login.groups.indexOf("soci") == -1);
    },
    offset: function() {
      return this.news.length;
    },
    status: function() {
      return {login_name: login_name};
    }
  },
  methods: {
    empty_list: function() {
      console.log('empty list');
      this.retrieved = -1;
      this.news = [];
    },
    loadMore: function() {
      if (!this.active)
        return;
      var self = this;
      // console.log('busy = ', self.busy);
      // console.log('retrieved = ', self.retrieved);
      console.log('news.length = ', self.news.length);
      if (self.busy || (self.retrieved === 0))
        return;
      self.busy = true;
      sendReceiveData(null, 'GET', self.endpoint(localStorage.getItem("filter") || 'filtered'), function (dataFrom) {
        var new_articles = dataFrom.articles;
        console.log('received articles: ' + new_articles.length);
        self.news = self.news.concat(new_articles);
        self.save_cache(self.retrieved == -1);
        self.retrieved = new_articles.length;
        self.busy = false;
      });
    },
    reset: function() {
      console.log('NewsList reset');
      this.retrieved = -1;
      this.cache_age = 0;
      this.news = [];
      this.save_cache(true);
      this.freeze = true;
    },
    save_cache: function(fresh) {
      var self = this;
      console.log('entered save_cache');
      if (fresh) {
        storage.set('news_cached', Date.now());
      }
      storage.set('news_status', self.status);
      storage.set('news', self.news);
    },
    endpoint: function(filt) {
      if (Object.keys(login).length === 0) {
        // user is not logged in
        return api + '/articles?offset=' + this.offset;
      } else if (login.login.groups.indexOf("soci") == -1) {
        // user is not active
        return api + '/articles?offset=' + this.offset;
      }
    },
    restore_from_cache: function() {
      var self = this;
      var news_cached = storage.get('news_cached');
      var news_status = storage.get('news_status');
      var is_same = shallow(news_status, self.status);
      self.cache_age = (Date.now() - news_cached) / 1000.0; // seconds
      setTimeout(self.update_cache_age, 60*1000.0);
      console.log('news_cached = ' + news_cached);
      console.log('age = ' + self.cache_age);
      if (is_same && (self.cache_age < 3600.0)) {
        var val = storage.get('news');
        val.forEach(function(n) { n.age = n.age + self.cache_age; });
        self.news = val;
        console.log('restored ' + val.length + ' news from cache with age = ' + self.cache_age);
        if (val.length == 0) {
          console.log('force cache outdate');
          self.cache_age = 0;
          storage.clear();
        } else {
          self.retrieved = val.length;
        }
        self.busy = false;
      } else {
        console.log('cache outdated');
        self.cache_age = 0;
        storage.clear();
        self.busy = false;
      }
    },
    update_cache_age: function() {
      var news_cached = storage.get('news_cached');
      this.cache_age = (Date.now() - news_cached) / 1000.0; // seconds
      setTimeout(this.update_cache_age, 60*1000.0);
    }
  },
  created: function() {
    var self = this;
    console.log('NewsList created with parameters: ' + JSON.stringify(this.$route.params));
    this.busy = true;
    if (('query' in this.$route.params) && (this.$route.params.query != '')) {
      this.query = this.$route.params.query;
      document.getElementById("query").value = this.query;
    }
    this.restore_from_cache();
  },
  activated: function() {
    console.log('NewsList activated');
    this.active = true;
  },
  deactivated: function() {
    console.log('NewsList deactivated');
    this.active = false;
  },
  mounted: function() {
    var self = this;
    var list = document.getElementById('slippylist');
    list.addEventListener('slip:swipe', function(e) {
      console.log('delete ', id);
      e.target.style.display = 'none';
      var id = e.target.id.substring(5);
      // update the cached value
      self.update_news(id, undefined, undefined, undefined, undefined, true);
      // update the value server-side
      var dataTo = { dismissed: true };
      sendReceiveData(dataTo, 'POST', api + '/articles/' + id, function () { });
    });
    new Slip(list, {minimumSwipeVelocity: 0.5});
  }
};


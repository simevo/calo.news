<?php
require __DIR__ . '/../vendor/autoload.php';
use \Firebase\JWT\JWT;

// based on:
// - https://gist.github.com/paxmanchris/e93018a3e8fbdfced039
// - https://meta.discourse.org/t/using-discourse-as-a-sso-provider/32974

$dotenv = Dotenv\Dotenv::createImmutable('/srv/calo.news');
$dotenv->load();
$discourse_url = 'https://' . getenv('DISCOURSE_HOSTNAME');

$me = 'https://'.$_SERVER['HTTP_HOST'].$_SERVER['SCRIPT_NAME'];
$aggregator_url = 'https://' . getenv('AGGREGATOR_HOSTNAME');

$sso_secret = file_get_contents('../secrets/sso_secret');

function setToken($value)
{
    $encoded_secret = file_get_contents('../secrets/secret'); // base64 encoded
    $secret = base64_decode(strtr($encoded_secret, '-_', '+/'));
    $now = time();
    $aggregator_url = 'https://' . $_SERVER['HTTP_HOST'];
    $token = array(
      "iss" => $aggregator_url,
      "aud" => $aggregator_url,
      "iat" => $now,
      "exp" => $now + 3600000,
      "nbf" => $now - 1,
      "login" => $value
    );
    // error_log(json_encode($token));
    $jwt = JWT::encode($token, $secret);
    setcookie('token', $jwt, time()+3600, "/", "", true, false);
}

function setNonce($value, $duration) {
    // $duration in seconds
    setcookie('nonce', json_encode($value), time()+$duration, "/", "", true, false);
}

function getKey($key)
{
    if (isset($_COOKIE[$key])) {
        return json_decode($_COOKIE[$key]);
    } else {
        return null;
    }
}

function deleteKey($key)
{
    setcookie($key, "", 1, "/", "", true, false);
}

if (!empty($_GET) and isset($_GET['logout'])) {
    deleteKey('nonce');
    deleteKey('token');
    header("location: $aggregator_url");
    exit();
}

$token = getKey('token');
if ($token) {
    header("location: $aggregator_url");
    exit();
}

if (!empty($_GET) and isset($_GET['sso']) and isset($_GET['sig'])) {
    // response received from Discourse: we've been redirected to the provided RETURN_URL (here)
    // now process the get query string parameters sig and sso
    $sso = $_GET['sso'];
    $sso = urldecode($sso);
    $sig = $_GET['sig'];
    // validate sso
    if (hash_hmac('sha256', $sso, $sso_secret) !== $sig) {
        header("HTTP/1.1 404 Not Found");
        exit();
    }
    $query = [];
    # https://meta.discourse.org/t/using-discourse-as-a-sso-provider/32974/76
    $query_string = strtolower(base64_decode($sso));
    parse_str($query_string, $query);
    $query['groups'] = explode(',', $query['groups']);
    // echo("<pre>" . print_r($query) . '</pre>');
    // verify nonce with generated nonce
    $nonce = getKey('nonce');
    if ($query['nonce'] != $nonce) {
        header("location: $discourse_url");
    }
    // login user
    setToken($query);
    setNonce($nonce, 36000000);
    if (isset($_COOKIE["redirect"])) {
        $redirect = $_COOKIE["redirect"];
        $unquoted = str_replace('"', '', $redirect);
        header("location: $unquoted");
    } else {
        header("location: $aggregator_url");
    }
    exit();
}

// if not logged, attempt login
$nonce = hash('sha512', mt_rand());
setNonce($nonce, 300);
$payload =  base64_encode(
    http_build_query(
        array (
            'nonce' => $nonce,
            'return_sso_url' => $me
        )
    )
);
$request = array(
    'sso' => $payload,
    'sig' => hash_hmac('sha256', $payload, $sso_secret)
);
$query = http_build_query($request);
$url = "$discourse_url/session/sso_provider?$query";
header("location: $url");
exit();
?>

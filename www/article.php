<?php
require 'prolog.php';
$id = $_GET["id"];
$redirect = $_GET["redirect"];

$admin_email = getenv('ADMIN_EMAIL');
$aggregator_hostname = getenv('AGGREGATOR_HOSTNAME');
$discourse_hostname = getenv('DISCOURSE_HOSTNAME');
$eig_category_name = getenv('EIG_CATEGORY_NAME');
$accounting_category_name = getenv('ACCOUNTING_CATEGORY_NAME');
$base_language = getenv('BASE_LANGUAGE');

// based on: http://blog.codelv.com/2011/10/creating-snippet-or-excerpt-of-words.html
function excerpt($text, $words=30, $end='...', $limit=500) {
    // split the string by spaces into an array
    $split = explode(' ', $text);
    if (count($split) > $words) {
        // rebuild the excerpt back into a string
        $text = join(' ',array_slice($split, 0, $words));
    }
    // append the ending, limit it, and return
    return substr($text, 0, $limit - strlen($end)).$end;
}

if ($decoded && in_array('soci', $decoded->login->groups)) {
    // logged in and active member
    $url = '/article/' . $id;
    header("location: " . $url);
    exit();
}

// validate id
if (!is_string($id)) {
    echo "An error occurred while validating (1) the id.\n";
    exit;
}
if (strlen($id) == 0) {
    echo "An error occurred while validating (0) the id.\n";
    exit;
}
if (strlen($id) > 9) {
    echo "An error occurred while validating (2) the id.\n";
    exit;
}
if (preg_match('/^[0-9]*$/', $id) !== 1) {
    echo "An error occurred while validating (3) the id.\n";
    exit;
}

$conn = pg_pconnect("dbname=calonews");
if (!$conn) {
    echo "An error occurred while connecting to the database.\n";
    exit;
}

$query = "SELECT articles.title, title_original, feeds.icon, feeds.title, articles.url, articles.content, articles.content_original, feeds.url FROM articles JOIN feeds ON articles.feed_id = feeds.id WHERE articles.id = $1";

$result = pg_query_params($conn, $query, [$id]);
if (!$result) {
    echo "An error occurred while performing the query.\n";
    exit;
}

$row = pg_fetch_row($result);
if (!$row) {
    echo "An error occurred while retrieving the row.\n";
    exit;
}
$title = $row[0];
$title_original = $row[1];
if (strlen($title_original) > 0) {
  $title_combined = $title_original . ' - ' . $title;
} else {
  $title_combined = $title;
}
$icon = $row[2];
$feed = $row[3];
$url = $row[4];
$content = strip_tags($row[5]);
$content_original = strip_tags($row[6]);
$feed_rss = $row[7];
if (strlen($content_original) > 0) {
    $content_excerpt = excerpt($content_original);
} else {
    $content_excerpt = excerpt($content);
}
setcookie('redirect', $_SERVER['REQUEST_URI'], time()+60, "/", "", true, false);

if ($redirect) {
    // record click and redirect browser to target url
    $query = "INSERT INTO user_articles(user_id, article_id, read, rating) VALUES ($1, $2, TRUE, 0) ON CONFLICT (user_id, article_id) DO UPDATE SET rating = user_articles.rating + 1 WHERE user_articles.user_id = $1 AND user_articles.article_id = $2";
    $result = pg_query_params($conn, $query, [-1, $id]);
    header("location: " . $url);
    exit();
}

if ($decoded) {
    // logged in but not active member
    $msg = <<<EOT
          <strong>Attenzione</strong>: al momento non sei soci* attivo del GIS (Gruppo di Informazione Solidale) quindi non puoi leggere e commentare l'articolo all'interno della piattaforma.
          <br/><br/>
          <a class="btn btn-info" onclick="linkClick()" href="#" title="$url">Leggi sulla fonte originale</a>
          <br/><br/>
          Per ottenere l'accesso completo a tutte le funzioni della piattaforma, ricarica ora il tuo conto abbonandoti/facendo una donazione ad una <strong>testata giornalistica digitale</strong> oppure dando una somma a uno degli altri soci che ha il conto in attivo, e registrando la transazione nella <a href="https://$discourse_hostname/c/$eig_category_name/$accounting_category_name">categoria $accounting_category_name</a> !
EOT;
    $msg_en = <<<EOT
          <strong>Warning</strong>: You are not currently an active member of the Ethical Information Group so you cannot read and comment on the article within the platform.
          <br/><br/>
          <a class="btn btn-success" onclick="linkClick()" href="#" title="$url">Read on the original source</a>
          <br/><br/>
          To gain full access to all of the platform's features, reload now your account by subscribing / donating to a <strong>digital news outlet</strong> or by giving a sum to one of the other members who has the account in surplus, and by recording the transaction in the <a href="https://$discourse_hostname/c/$eig_category_name/$accounting_category_name">accounting category</a> !
EOT;
} else {
    // not logged in
    $msg = <<<EOT
          <strong>Attenzione</strong>: per leggere e commentare l'articolo all'interno della piattaforma del Gruppo di Informazione Solidale <a href="https://$discourse_hostname/login">crea un nuovo account (è gratis per i primi due mesi !) oppure connettiti se l’hai già</a>.
          <br/><br/>
          Per saperne di più sul Gruppo di Informazione Solidale, leggi le <a href="https://calomelano.it/?p=2107">FAQ</a>, il <a href="https://calomelano.it/?p=1521">manifesto</a> o <a href="https://calomelano.it/?p=2114">questa serie di 10 brevi blog post</a> su funzioni e benefici per l’utente dell'aggregatore di notizie,           
          oppure guarda <a href="https://youtu.be/KdryVdVz0MU?list=PLkjc89M0LEXMAnbcioj55Pl-Wp67lFgBi">questi video su youtube</a>.
          <br/><br/>
          <a class="btn btn-info" onclick="linkClick()" href="#" title="$url">Leggi sulla fonte originale</a>
          <a class="btn btn-warning" target="_blank" href="$feed_rss">
            <svg width="1em" height="1em">
              <use xlink:href="#rss-square"></use>
            </svg>&nbsp;Apri il feed RSS della fonte
          </a>
EOT;
    $msg_en = <<<EOT
          <strong>Warning</strong>: to read and comment this article on the Ethical Information Group platform, <a href="https://$discourse_hostname/login">request an account or if you already have one, connect with your username / password</a>.
          <br/><br/>
          To know more about the Ethical Information Group, have a look at <a href="https://calomelano.it/?p=1550">the manifesto</a>.
          <br/><br/>
          <a class="btn btn-success" onclick="linkClick()" href="#" title="$url">Read on the original source</a>
          <a class="btn btn-warning" target="_blank" href="$feed_rss">
            <svg width="1em" height="1em">
              <use xlink:href="#rss-square"></use>
            </svg>&nbsp;Open the source RSS feed
          </a>
EOT;
}
?>

<!DOCTYPE html>
<html lang="<?php echo($base_language) ?>" class="h-100">
  <head>
    <meta charset="UTF-8">
    <title><?php echo htmlspecialchars($feed); ?> - <?php echo htmlspecialchars($title_combined); ?></title>
    <script type="text/javascript" src="/js/jquery.slim.min.js"></script>
    <link rel="stylesheet" href="css/bootstrap.min.css">
    <link rel="stylesheet" href="css/style.css">
    <link rel="apple-touch-icon" sizes="180x180" href="/apple-touch-icon.png">
    <link rel="icon" type="image/png" href="/favicon-32x32.png" sizes="32x32">
    <link rel="icon" type="image/png" href="/favicon-16x16.png" sizes="16x16">
    <link rel="manifest" href="/manifest.json">
    <link rel="mask-icon" href="/safari-pinned-tab.svg" color="#5bbad5">
    <meta name="theme-color" content="#ffffff">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta property="og:title" content="<?php echo htmlspecialchars($feed); ?> - <?php echo htmlspecialchars($title_combined); ?>">
    <meta property="og:description" content="<?php echo htmlspecialchars($content_excerpt); ?>">
    <meta property="og:type" content="article">
    <meta property="og:image" content="https://<?php echo($aggregator_hostname . '/' . htmlspecialchars($icon)); ?>">
    <meta property="og:url" content="https://<?php echo($aggregator_hostname); ?>/article.php?id=<?php echo $id; ?>">
    <meta property="og:site_name" content="Calo.news">
    <script type="text/javascript">
      function linkClick() {
        window.location.href = "<?php print('/article.php?id=' . $id . '&redirect=true'); ?>";
        return false;
      }
    </script>
  </head>
  <body class="d-flex flex-column h-100">
<?php require 'header.php'; ?>
    <main role="main" class="flex-shrink-0">
      <div class="container">
        <div class="row">
          <div class="text-center col-md-8 offset-md-2">
            <span class="h2"><img width="30px" height="30px" src="/<?php echo htmlspecialchars($icon); ?>" alt="feed logo"> <?php echo htmlspecialchars($feed); ?></span>
          </div>
        </div>
        <div class="row">
          <div class="text-center col-md-8 offset-md-2 title">
            <h1 class="font-weight-bold"><?php echo htmlspecialchars($title_combined); ?></h1>
          </div>
        </div>
        
        <div class="row mb-3">
          <div class="col-md-12 text-muted">
            <small>Articolo originale:</small>
            <small style="word-wrap: break-word;"><?php echo $url; ?></small>
          </div>
        </div> <!-- row -->

        <div class="alert alert-info" role="alert">
          <?php echo($msg); ?>
        </div>
        <div class="alert alert-success" role="alert">
          <?php echo($msg_en); ?>
        </div>
      </div> <!-- container -->
    </main>
<?php require 'footer.php'; ?>

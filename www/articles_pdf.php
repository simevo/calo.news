<?php
require 'prolog.php';
if (!$decoded || !in_array('soci', $decoded->login->groups)) {
    header("location: /");
    exit();
}
?>
<?php
require_once 'tcpdf_include.php';
  
  // Extend the TCPDF class to create custom Header and Footer
class MYPDF extends TCPDF
{
    public function Header()
    {
        // No Header
    } 
    public function Footer()
    {
        $image_file = "/images/logo150x150.png";
        $this->Image($image_file, 70, 210-15, 40, '', 'PNG', '', 'T', true, 300, '', false, false, 0, false, false, false);
        //           $file        $x  $y      $w  $h  $type  $link    $resize         $ismask       $border   $hidden $fitonpage
        //                                                               $align     $dpi $palign    $imgmask  $fitbox
        $this->SetY(-15);

        $this->SetFont('helvetica', 'N', 9);

        // http://stackoverflow.com/a/20289096
        $tz = 'Europe/Rome';
        $timestamp = time();
        $dt = new DateTime("now", new DateTimeZone($tz)); // first argument "must" be a string
        $dt->setTimestamp($timestamp); // adjust the object to correct timestamp
        $date = $dt->format('Y-m-d H:i:s');

        $this->Cell(70, 15, $date, "T", false, 'L', 0, '', 0, false, 'T', 'M');
        // Page number
        $this->Cell(70, 15, $this->getAliasNumPage().' / '.$this->getAliasNbPages(), 'T', false, 'R', 0, '', 0, false, 'T', 'M');
    }
}

$pdf = new MYPDF(PDF_PAGE_ORIENTATION, PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false, true);

// set document information
$pdf->SetCreator('PDF_CREATOR');
$pdf->SetAuthor('calo.news');
$pdf->SetTitle('notiziario');

// set default monospaced font
$pdf->SetDefaultMonospacedFont(PDF_FONT_MONOSPACED);

// set margins
$pdf->SetMargins(5, 5, 5); // PDF_MARGIN_LEFT, PDF_MARGIN_TOP, PDF_MARGIN_RIGHT);
$pdf->SetFooterMargin(5); // PDF_MARGIN_FOOTER

// set auto page breaks
$pdf->SetAutoPageBreak(true, 15); // PDF_MARGIN_BOTTOM

// set some language-dependent strings (optional)
if (@file_exists(dirname(__FILE__).'/lang/eng.php')) {
    include_once dirname(__FILE__).'/lang/eng.php';
    $pdf->setLanguageArray($l);
}

// set default font subsetting mode
$pdf->setFontSubsetting(true);

$pdf->SetFont('helvetica', '', 12, '', true);

$pdf->AddPage('P', 'A5');

$ch = curl_init();
// set url
curl_setopt($ch, CURLOPT_URL, "http://localhost:8080/articles.html?ids=".$_GET["ids"]);
// return the transfer as a string
curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
// forward the authentication cookie
$strCookie = 'token=' . $token;
curl_setopt($ch, CURLOPT_COOKIE, $strCookie);
// $html contains the output string
$html = curl_exec($ch);
// close curl resource to free up system resources
curl_close($ch);

// error_reporting(E_ALL);

// debug print:
// echo $html;

// error print:
// echo ob_get_contents();

// Avoid the error "TCPDF ERROR: Some data has already been output, can't send PDF file"
// http://php.net/manual/en/function.ob-end-clean.php
ob_end_clean();

$pdf->writeHTML($html, true, 0, true, true);
$pdf->lastPage();
$pdf->Output('articoli.pdf', 'I');
?>

    <footer class="footer mt-auto py-3 border-top bg-light">
      <div class="container-fluid">
        <div class="row">
          <div class="mx-auto">
            <small class="text-muted">Powered by <a href="https://gitlab.com/simevo/calo.news">calo.news</a> - a news platform with aggregation, ranking and conversations</small>
          </div>
        </div>
      </div>
    </footer>
    <script type="text/javascript" src="/js/font_size.js"></script>
    <script type="text/javascript" src="/js/popper.min.js"></script>
    <script type="text/javascript" src="/js/bootstrap.min.js"></script>
  </body>
<!--
This file is part of calo.news: A news platform

Copyright (C) 2017-2023 Paolo Greppi

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with this program (file LICENSE).
If not, see https://www.gnu.org/licenses/.
-->
</html>

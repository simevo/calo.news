<?php
require 'prolog.php';
if (!$decoded) {
    header("location: /");
    exit();
}
$base_language = getenv('BASE_LANGUAGE');
?>
<!DOCTYPE html>
<html lang="<?php echo($base_language) ?>">
  <head>
    <meta charset="UTF-8">
    <title>calo.news - statistiche sugli utenti</title>
    <script type="text/javascript" src="/js/jquery.slim.min.js"></script>
    <script type="text/javascript" src="/js/vue.min.js"></script>
    <link rel="stylesheet" href="css/bootstrap.min.css">
    <link rel="stylesheet" href="css/style.css">
    <link rel="apple-touch-icon" sizes="180x180" href="/apple-touch-icon.png">
    <link rel="icon" type="image/png" href="/favicon-32x32.png" sizes="32x32">
    <link rel="icon" type="image/png" href="/favicon-16x16.png" sizes="16x16">
    <link rel="manifest" href="/manifest.json">
    <link rel="mask-icon" href="/safari-pinned-tab.svg" color="#5bbad5">
    <meta name="theme-color" content="#ffffff">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  </head>
  <body>
<?php require 'header.php'; ?>
    <main>
      <div class="container" id="stats">
        <h2 class="row">Quanti utenti usano l’aggregatore di notizie ?</h2>
        <p>Per il metodo di calcolo vedi <a href="https://commenti.calomelano.it/t/343/">qui</a>.</p>
        <ul class="nav nav-tabs" role="tablist">
          <li class="nav-item">
            <a class="nav-link active" data-toggle="tab" role="tab" href="#mau">Utenti mensili</a>
          </li>
          <li class="nav-item">
            <a class="nav-link" data-toggle="tab" role="tab" href="#wau">Utenti settimanali</a>
          </li>
          <li class="nav-item">
            <a class="nav-link" data-toggle="tab" role="tab" href="#dau">Utenti giornalieri</a>
          </li>
        </ul>
        <div class="tab-content">
          <div class="tab-pane active" id="mau" role="tabpanel">
            <table class="table">
              <thead>
                <tr>
                  <th>Id</th>
                  <th>Mese</th>
                  <th>Utenti medi</th>
                </tr>
              </thead>
              <tbody>
                <tr v-for="u in mau">
                  <td>{{ u.bucket }}</td>
                  <td>{{ u.m | formatMonth }}</td>
                  <td>{{ u.average_users }}</td>
                </tr> 
              </tbody>
            </table>
          </div> <!-- tab-pane -->
          <div class="tab-pane" id="wau" role="tabpanel">
            <table class="table">
              <thead>
                <tr>
                  <th>Id</th>
                  <th>Settimana</th>
                  <th>Utenti medi</th>
                </tr>
              </thead>
              <tbody>
                <tr v-for="u in wau">
                  <td>{{ u.bucket }}</td>
                  <td>{{ u.m | formatDay }}</td>
                  <td>{{ u.average_users }}</td>
                </tr> 
              </tbody>
            </table>
          </div> <!-- tab-pane -->
          <div class="tab-pane" id="dau" role="tabpanel">
            <table class="table">
              <thead>
                <tr>
                  <th>Id</th>
                  <th>Giorno</th>
                  <th>Utenti medi</th>
                </tr>
              </thead>
              <tbody>
                <tr v-for="u in dau">
                  <td>{{ u.bucket }}</td>
                  <td>{{ u.m | formatDay }}</td>
                  <td>{{ u.average_users }}</td>
                </tr> 
              </tbody>
            </table>
          </div> <!-- tab-pane -->
        </div> <!-- tab-content -->

      </div> <!-- container -->
    </main>
    <script type="text/javascript" src="/js/common.js"></script>
    <script type="text/javascript" src="/js/envsubst.js"></script>
    <script type="text/javascript">
// configuration for jshint
/* jshint browser: true, devel: true */
/* global Vue, api, font_size, login_id */

"use strict";

Vue.filter('formatMonth', function(value) {
  if (value) {
    var date = new Date(value);
    var month = date.getMonth() + 1;
    var year = date.getFullYear();
    return year + '/' + ((month < 10) ? ('0' + month) : month);
  }
});

Vue.filter('formatDay', function(value) {
  if (value) {
    var date = new Date(value);
    var day = date.getDate();
    var month = date.getMonth() + 1;
    var year = date.getFullYear();
    return year + '/' + ((month < 10) ? ('0' + month) : month) + '/' + ((day < 10) ? ('0' + day) : day);
  }
});

var app = new Vue({
  el: '#stats',
  created: function () {
    var xhr = new XMLHttpRequest();
    var self = this;
    xhr.open('GET', api + '/user_stats');
    xhr.onload = function () {
      var data = JSON.parse(xhr.responseText);
      self.mau = data.mau;
      self.wau = data.wau;
      self.dau = data.dau;
    };
    xhr.setRequestHeader('X-Requested-With', 'XMLHttpRequest');
    xhr.setRequestHeader("accept", "application/json");
    xhr.send();
  },
  data: {
    mau: [],
    wau: [],
    dau: []
  }
});

document.addEventListener('DOMContentLoaded', function() {
  console.log("DOM fully loaded and parsed");
  font_size();
});

    </script>
<?php require 'footer.php'; ?>

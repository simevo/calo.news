<?php
require 'prolog.php';
if (!$decoded) {
    header("location: /");
    exit();
}
if (isset($_GET['id'])) {
    $id = $_GET['id'];
    header("location: /feed/$id");
    exit();
}
$base_language = getenv('BASE_LANGUAGE');
?>
<!DOCTYPE html>
<html lang="<?php echo($base_language) ?>" class="h-100">
  <head>
    <meta charset="UTF-8">
    <title>calo.news - fonti</title>
    <script type="text/javascript" src="/js/jquery.slim.min.js"></script>
    <script type="text/javascript" src="/js/vue.min.js"></script>
    <script type="text/javascript" src="/js/vue-select.js"></script>
    <link rel="stylesheet" href="css/bootstrap.min.css">
    <link rel="stylesheet" href="css/style.css">
    <link rel="apple-touch-icon" sizes="180x180" href="/apple-touch-icon.png">
    <link rel="icon" type="image/png" href="/favicon-32x32.png" sizes="32x32">
    <link rel="icon" type="image/png" href="/favicon-16x16.png" sizes="16x16">
    <link rel="manifest" href="/manifest.json">
    <link rel="mask-icon" href="/safari-pinned-tab.svg" color="#5bbad5">
    <meta name="theme-color" content="#ffffff">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  </head>
  <body class="d-flex flex-column h-100">
    <script type="text/javascript" src="/templates/negative-rating.js"></script>
    <script type="text/javascript" src="/templates/positive-rating.js"></script>
    <script type="text/javascript" src="/templates/rating-toolbar.js"></script>
    <script type="text/javascript" src="/templates/feed-item.js"></script>
<?php require 'header.php'; ?>
    <main role="main" class="flex-shrink-0">
      <div class="container" id="feeds">
        <div id="rating_modal" v-show="showModal" class="modal" role="dialog" tabindex="-1" @keyup.esc.stop="showModal = false;" >
          <div class="modal-dialog" role="document">
            <div id="rating_modal_mask"></div>
            <div class="modal-content">
              <div class="modal-header">
                <strong class="modal-title">Valuta la fonte "{{ item.title }}"</strong>
                <button type="button" class="close" aria-label="Close" @click="showModal = false;">
                  <span aria-hidden="true">&times;</span>
                </button>
              </div>
              <div class="modal-body">
                <rating-toolbar id="rate_control" class="float-right" endpoint="feeds" v-bind:item="item"></rating-toolbar>
              </div>
            </div>
          </div> <!-- modal dialog -->
        </div> <!-- rating modal -->
<?php if (!isset($_GET['id'])): ?>
        <h2>Elenco fonti</h2>
        <div class="row" style="padding: 0.5em;">
          <div class="col-md-2">
            <span>Totali: {{ feeds.length }}</span> <span v-if="feeds_filtered.length != feeds.length">Visibili: {{ feeds_filtered.length }}</span>
          </div>
          <div class="col-md-3">
            <div class="form-check">
              <label class="form-check-label">
                <input class="form-check-input" type="checkbox" id="newsletter" v-model="premium">
                PREMIUM <img src="images/wreath.png" width="30px" height="30px">
              </label>
            </div>
          </div>
          <div class="col-md-3">
            <select class="form-control" id="language" v-model="language">
              <option value='' selected>qualsiasi lingua</option>
              <?php require 'languages.php'; ?>
            </select>
          </div>
          <div class="col-md-4">
            <v-select multiple placeholder="qualsiasi tag" :options="['cultura', 'società', 'italia', 'estero', 'scienza_tecnica', 'sport', 'economia', 'blog', 'notiziario', 'rivista']" v-model='tags'></v-select>
          </div>
        </div>
<?php else: ?>
        <h2>Fonte <?php print_r($_GET['id']); ?></h2>
        <div><a href="/feeds.php">Vai all'elenco di tutte le fonti</a>.</div>
<?php endif; ?>
        <feed-item v-for="(f, i) in feeds_filtered" v-bind:feed="f" v-bind:freeze="freeze" v-bind:index="i" :key="f.id" v-bind:readonly="true"></feed-item>
      </div>
    </main>
    <script type="text/javascript" src="/js/common.js"></script>
    <script type="text/javascript" src="/js/envsubst.js"></script>
    <script type="text/javascript" src="/js/feeds.js"></script>
<?php require 'footer.php'; ?>

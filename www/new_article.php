<?php
require 'prolog.php';
if (!$decoded || !in_array('soci', $decoded->login->groups)) {
    header("location: /");
    exit();
}
$base_language = getenv('BASE_LANGUAGE');
?>
<!DOCTYPE html>
<html lang="<?php echo($base_language) ?>" class="h-100">
  <head>
    <meta charset="UTF-8">
    <title>calo.news - segnala notizia</title>
    <script type="text/javascript" src="/js/jquery.slim.min.js"></script>
    <script type="text/javascript" src="/js/franc-min.js"></script>
    <script type="text/javascript" src="/js/vue.min.js"></script>
    <script type="text/javascript" src="/js/vee-validate.min.js"></script>
    <script type="text/javascript" src="/js/locale/it.js"></script>
    <link rel="stylesheet" href="css/bootstrap.min.css">
    <link rel="stylesheet" href="css/style.css">
    <link rel="stylesheet" type="text/css" href="css/trix.css">
    <link rel="apple-touch-icon" sizes="180x180" href="/apple-touch-icon.png">
    <link rel="icon" type="image/png" href="/favicon-32x32.png" sizes="32x32">
    <link rel="icon" type="image/png" href="/favicon-16x16.png" sizes="16x16">
    <link rel="manifest" href="/manifest.json">
    <link rel="mask-icon" href="/safari-pinned-tab.svg" color="#5bbad5">
    <meta name="theme-color" content="#ffffff">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <style>
      trix-toolbar {
        display: none;
      }
    </style>
  </head>
  <body id="page-top" class="d-flex flex-column h-100">
<?php require 'header.php'; ?>
    <main id="new_article" role="main" class="flex-shrink-0">
      <div class="container">
        <h2>Segnala una nuova notizia</h2>
        <form>
          <div class="form-group" :class="{'has-danger': errors.has('url') }">
            <label for="url" v-show="!received && !trying">Copia e incolla il link alla notizia (URL)</label>
            <label style="display: none;" v-show="trying">Attendi qualche istante ...</label>
            <label style="display: none;" v-show="received">Quando ti sembra a posto, premi sul bottone "Invia" per inviare la notizia all'aggregatore</label>
            <input v-validate="'required|url:true'" type="url" class="form-control" id="url" name="url" placeholder="http://..." v-bind:value="article.url" v-on:input="article.url =  $event.target.value" v-on:change="try_it();" autocomplete='off' spellcheck='false' autocorrect='off'>
            <p class="text-danger" v-if="errors.has('url')">{{ errors.first('url') }}</p>
          </div>
          <div style="display: none;" v-show="received">
            <button v-on:click="send();" type="button" class="btn btn-primary col-md-2" title="Invia la notizia all'aggregatore" v-bind:disabled="errors.any() || !article.url || !article.title || !article.content || !article.author">Invia</button>
            <button v-on:click="clean();" type="button" class="btn btn-secondary col-md-2" title="Pulisci tutti i campi">Azzera</button>
            <a target="_blank" href="#" v-bind:href="article.url" role="button" class="btn btn-danger col-md-2" title="Apri il link in un nuovo tag">Apri URL</a>
            <hr/>
            <div class="form-group" :class="{'has-danger': errors.has('titolo') }">
              <label for="title">Titolo della notizia</label>
              <input autocomplete="off" autocorrect="off" autocapitalize="off" spellcheck="false" v-validate="'required'" type="text" class="form-control" id="title" name="titolo" placeholder="titolo" v-model="article.title">
              <button type="button" aria-hidden="true" v-on:click="resetTitle();" class="close" style="left: -18px;position: relative;margin-top: -40px;z-index: 100;">×</button>
              <p class="text-danger" v-if="errors.has('titolo')">{{ errors.first('titolo') }}</p>
            </div> <!-- form-group -->
            <div class="form-group" :class="{'has-danger': errors.has('autore') }">
              <label for="author">Autore</label>
              <input autocomplete="off" autocorrect="off" autocapitalize="off" spellcheck="false" v-validate="'required'" type="text" class="form-control" id="author" name="autore" placeholder="autore" v-model="article.author">
              <button type="button" aria-hidden="true" v-on:click="resetAuthor();" class="close" style="left: -18px;position: relative;margin-top: -40px;z-index: 100;">×</button>
              <p class="text-danger" v-if="errors.has('autore')">{{ errors.first('autore') }}</p>
            </div> <!-- form-group -->
            <div class="form-group" :class="{'has-danger': errors.has('testo') }">
              <label for="text">Testo completo della notizia</label> <input style="float: right;" type="checkbox" v-model="collapsed" title="mostra / nascondi testo completo">
              <div v-bind:class="{ 'collapse': collapsed, 'collapse.show': !collapsed }">
                <input v-validate="'required|min:140'" id="content" type="hidden" name="testo">
                <trix-editor autocomplete="off" autocorrect="off" autocapitalize="off" spellcheck="false" input="content" id="content_editor" placeholder="contenuto"></trix-editor>
              </div> <!-- collapsible -->
              <p class="text-danger" v-if="errors.has('testo')">{{ errors.first('testo') }}</p>
            </div> <!-- form-group -->
            <div class="form-group">
              <label for="language">Lingua</label>
              <select class="form-control" id="language" v-model="article.language">
                <?php require 'languages.php'; ?>
              </select>
            </div> <!-- form-group -->
            <hr/>
            <a href="#page-top">Torna su</a>
          </div>
          <input id="guess_language" type="hidden" v-model="guess_language">
        </form>
      </div>
      <br/>
      <br/>
      <br/>
    </main>
    <script type="text/javascript" src="/js/es6-promise.auto.min.js"></script>
    <script type="text/javascript" src="/js/trix.js"></script>
    <script type="text/javascript" src="/js/Readability.js"></script>
    <script type="text/javascript" src="/js/common.js"></script>
    <script type="text/javascript" src="/js/envsubst.js"></script>
    <script type="text/javascript" src="/js/new_article.js"></script>
<?php require 'footer.php'; ?>

<?php
require 'prolog.php';
if (!$decoded || !in_array('soci', $decoded->login->groups)) {
    header("location: /");
    exit();
}
$base_language = getenv('BASE_LANGUAGE');

$conn = pg_pconnect("dbname=calonews");
if (!$conn) {
    echo "An error occurred while connecting to the database.\n";
    exit;
}

$query = "SELECT
  EXTRACT(year FROM stamp) || '-' || TO_CHAR(stamp, 'Mon') AS month,
  SUM(LENGTH(content_original)+LENGTH(title_original)) AS sum,
  MIN(stamp)
FROM
  articles
  JOIN user_articles ON articles.id = user_articles.article_id
WHERE
  user_articles.read
  AND stamp >= '2017-03-18'
GROUP BY 1
ORDER BY 3;";

$result = pg_query($conn, $query);
if (!$result) {
    echo "An error occurred while performing the query.\n";
    exit;
}

$rows = pg_fetch_all($result);
if (!$rows) {
    echo "An error occurred while retrieving the row.\n";
    exit;
}
?>

<!DOCTYPE html>
<html lang="<?php echo($base_language) ?>">
  <head>
    <meta charset="UTF-8">
    <title>calo.news - traduzioni automatiche</title>
    <script type="text/javascript" src="/js/jquery.slim.min.js"></script>
    <link rel="stylesheet" href="css/bootstrap.min.css">
    <link rel="stylesheet" href="css/style.css">
    <link rel="apple-touch-icon" sizes="180x180" href="/apple-touch-icon.png">
    <link rel="icon" type="image/png" href="/favicon-32x32.png" sizes="32x32">
    <link rel="icon" type="image/png" href="/favicon-16x16.png" sizes="16x16">
    <link rel="manifest" href="/manifest.json">
    <link rel="mask-icon" href="/safari-pinned-tab.svg" color="#5bbad5">
    <meta name="theme-color" content="#ffffff">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="format-detection" content="telephone=no">
  </head>
  <body>
<?php require 'header.php'; ?>
    <main>
      <div class="container">
        <h2>Traduzioni automatiche</h2>
        <table>
          <thead>
            <tr>
              <th>Mese</th>
              <th>Caratteri tradotti</th>
            </tr>
          </thead>
          <tbody>
<?php
foreach($rows as $row)
{
    echo '<tr>
            <td>'. $row['month'].'</td>
            <td>'. $row['sum'].'</td>
          </tr>';
}
?>
          </tbody>
        </table>
      </div> <!-- container -->
    </main>
<?php require 'footer.php'; ?>

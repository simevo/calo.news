<?php
$discourse_hostname = getenv('DISCOURSE_HOSTNAME');
$wordpress_hostname = getenv('WORDPRESS_HOSTNAME');
$additional_links = getenv('ADDITONAL_LINKS');

if ($dummy_sso) {
    $avatar = '/icons/unknown.png';
    $unseen_notifications = 10;
    $username = $login->username;
} else {
    if ($decoded) {
        $login = $decoded->login;
        // stdClass Object ( [username] => paolog [email] => john.doe@example.com [nonce] => ... [name] => paolog [external_id] => 2 [moderator] => true [admin] => true [return_sso_url] => https://notizie.example.com/sso.php )
        $username = $login->username;
        $api_key = file_get_contents('../secrets/api_key');

        $url = "https://$discourse_hostname/users/" . $username . '.json';
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_HTTPHEADER, [
          'Api-Key: ' . $api_key,
          'Api-Username: ' . $username
        ]);
        $result = curl_exec($ch);
        $current_user = json_decode($result, true);
        $avatar_template = $current_user['user']['avatar_template'];
        $avatar = "https://" . $discourse_hostname . str_replace("{size}", "32", $avatar_template);

        $url = "https://$discourse_hostname/notifications.json";
        curl_setopt($ch, CURLOPT_URL, $url);
        $result = curl_exec($ch);
        $notifications = json_decode($result, true);
        $seen_notification_id = $notifications['seen_notification_id'];
        $unseen_notifications = 0;
        foreach ($notifications['notifications'] as $n) {
            if ($n['id'] > $seen_notification_id) {
                $unseen_notifications += 1;
            }
        }
        curl_close($ch);
    }
}
?>
    <?php require 'images/fontawesome.svg'; ?>
    <header>
<?php if ($decoded): ?>
      <nav class="navbar navbar-expand-sm navbar-light bg-primary">
<?php else: ?> <!-- $decoded -->
      <nav class="navbar navbar-expand-xl navbar-light bg-primary">
<?php endif; ?> <!-- $decoded -->
        <div class="navbar-brand">
          <button title="aggregatore" class="btn btn-primary btn-sm dropdown-toggle" type="button" id="dropdownMenuLeft" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
            <img src="/images/logo_calonews_1.png" width="40" height="40" alt="">
          </button>
          <div class="dropdown-menu" aria-labelledby="dropdownMenuLeft">
            <a class="dropdown-item" title="blog calomelanico" href="https://<?php echo($wordpress_hostname); ?>">
              <img src="/images/logo150x150.png" width="40" height="40" alt=""> blog
            </a>
            <a class="dropdown-item" title="conversazioni calomelaniche" href="https://<?php echo($discourse_hostname); ?>">
              <img src="/images/logo_commenti.png" width="40" height="40" alt=""> commenti
            </a>
            <?php echo($additional_links); ?>
          </div>
        </div>
<?php if ($decoded): ?>
        <button class="navbar-toggler navbar-toggler-right" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
          <span class="navbar-toggler-icon"></span>
<?php if ($unseen_notifications > 0): ?>
          <span style="position:absolute;top:0px;right:1em;" class="badge badge-pill badge-info" title="<?php print_r($unseen_notifications); ?> notifiche / messaggi per te"><?php print_r($unseen_notifications); ?></span>
<?php endif; ?> <!-- $unseen_notifications -->
        </button>
<?php endif; ?> <!-- $decoded -->
<?php if ($decoded): ?>
        <script type="text/javascript">
          var login = <?php print_r(json_encode($decoded)); ?>;
          Vue.prototype.$login = login;
          var login_name = "<?php print_r($decoded->login->name); ?>";
          var login_id = "<?php print_r($decoded->login->external_id); ?>";
        </script>
        <div class="collapse navbar-collapse" id="navbarSupportedContent">
          <ul class="navbar-nav mr-auto">
            <li class="nav-item">
              <a class="nav-link active" href="/" title="prima pagina">
                <svg width="1em" height="1em">
                  <use xlink:href="#newspaper"></use>
                </svg>
                <span class="d-sm-none d-md-inline"> prima</span>
              </a>
            </li>
            <li class="nav-item">
              <a class="nav-link active" href="/view/xxx" title="viste personalizzate: cronologia, interessanti ...">
                <svg width="1em" height="1em">
                  <use xlink:href="#eye"></use>
                </svg>
                <span class="d-sm-none d-md-inline"> viste</span>
              </a>
            </li>
<?php if (substr($_SERVER['REQUEST_URI'], -10) != '/feeds.php'): ?>
            <li class="nav-item">
              <a class="nav-link active" href="/feeds.php" title="elenco fonti">
                <svg width="1em" height="1em">
                  <use xlink:href="#rss-square"></use>
                </svg>
                <span class="d-sm-none d-md-inline"> fonti</span>
              </a>
            </li>
<?php if ($decoded && in_array('soci', $decoded->login->groups)): ?>
            <li class="nav-item active">
              <a class="nav-link" href="/new_article.php" title="segnala una nuova notizia">
                <svg width="1em" height="1em">
                  <use xlink:href="#plus-circle"></use>
                </svg>
                <span class="d-sm-none d-md-inline"> nuova</span>
              </a>
            </li>
<?php endif; ?> <!-- $decoded && member -->
<?php endif; ?> <!-- !feeds -->
<?php if ($decoded && in_array('soci', $decoded->login->groups)): ?>
<?php if (substr($_SERVER['PHP_SELF'], 0, 10) == '/feeds.php'): ?>
            <li class="nav-item active">
              <a class="nav-link" href="/new_feed.php" title="aggiungi fonte">
                <svg width="1em" height="1em">
                  <use xlink:href="#plus-circle"></use>
                </svg>
                <span class="d-sm-none d-md-inline"> nuova</span>
              </a>
            </li>
<?php endif; ?> <!-- feeds -->
            <li class="nav-item active">
              <a class="nav-link" href="/settings.php" title="impostazioni">
                <svg width="1em" height="1em">
                  <use xlink:href="#sliders-h"></use>
                </svg>
                <span class="d-sm-none d-md-inline"> impostazioni</span>
              </a>
            </li>
<?php endif; ?> <!-- $decoded && member -->
          </ul>
<?php if ($decoded && in_array('soci', $decoded->login->groups)): ?>
          <div class="form-inline my-2 my-lg-0">
            <input class="form-control mr-sm-2" type="text" placeholder="ricerca" id="query" onkeyup="search1()"  onchange="search()" >
          </div>
          <div aria-hidden="true" class="close" style="left: -40px; position: relative; cursor: pointer; z-index: 100;" onclick="resetSearch();">×</div>
<?php endif; ?> <!-- $decoded && member -->
          <a class="nav-link" href="https://<?php echo($discourse_hostname); ?>">
            <img alt="profilo" width="32" height="32" title="<?php print_r($username); ?>" src="<?php echo($avatar); ?>" style="border-radius: 50%;" />
<?php if ($unseen_notifications > 0): ?>
            <span style="position:relative;top:-9px;left:-9px;" class="badge badge-pill badge-info" title="<?php print_r($unseen_notifications); ?> notifiche / messaggi per te"><?php print_r($unseen_notifications); ?></span>
<?php endif; ?> <!-- $unseen_notifications -->
          </a>
        </div> <!-- navbar-collapse -->
<?php else: ?> <!-- $decoded -->
        <script type="text/javascript">
          var login = {};
          var login_name = "";
        </script>
        <div class="navbar-nav ml-auto">
          <form class="form-inline">
            <button type="button" title="Connettiti per accedere alle funzioni avanzate" class="btn btn-info" onclick="parent.location='/sso.php'">Connetti</button>
          </form>
        </div> <!-- navbar-nav -->
<?php endif; ?> <!-- $decoded -->
      </nav>
    </header>
    <div id='message' style='display:none;'></div>

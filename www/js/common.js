// configuration for jshint
/* jshint browser: true, devel: true */
/* global Vue, $ */

"use strict";

// fuzzily humanize elapsed time
function secondsToString(seconds) {
  // TODO these three should be const (ES6)
  var upper_threshold = 1.5;
  var lower_threshold = 0.9;
  var hour = 3600.0;
  var day = 24.0 * hour;
  var week = 7.0 * day;
  var year = 365.2421 * day;
  var month = year / 12.0;

  var years = seconds / year;
  var months = seconds / month;
  var weeks = seconds / week;
  var days = seconds / day;
  var hours = seconds / hour;
  var minutes = seconds / 60.0;
  if (years > 10.0)
    return "un eternità";
  else if (years > upper_threshold)
    return Math.round(years) + " anni fa";
  else if (years > lower_threshold)
    return "un anno fa";
  else if (months > upper_threshold)
    return Math.round(months) + " mesi fa";
  else if (months > lower_threshold)
    return "un mese fa";
  else if (weeks > upper_threshold)
    return Math.round(weeks) + " settimane fa";
  else if (weeks > lower_threshold)
    return "una settimana fa";
  else if (days > upper_threshold)
    return Math.round(days) + " giorni fa";
  else if (days > lower_threshold)
    return "un giorno fa";
  else if (hours > upper_threshold)
    return Math.round(hours) + " ore fa";
  else if (hours > lower_threshold)
    return "un'ora fa";
  else if (minutes > upper_threshold)
    return Math.round(minutes) + " minuti fa";
  else if (minutes > lower_threshold)
    return "un minuto fa";
  else
    return "or ora";
} // secondsToString

// precisely humanize time interval
function secondsToString1(seconds) {
  // TODO these three should be const (ES6)
  var hour = 3600;
  var day = 24 * hour;
  var week = 7 * day;
  var year = 365.2421 * day;
  var month = year / 12.0;

  var years = Math.round(seconds / year);
  var months = Math.round(seconds / month);
  var weeks = Math.round(seconds / week);
  var days = Math.round(seconds / day);
  var hours = Math.round(seconds / hour);
  var minutes = Math.round(seconds / 60);
  if (years >= 2)
    return years + " anni";
  else if (years === 1)
    return "un anno";
  else if (months >= 2)
    return months + " mesi";
  else if (months === 1)
    return "un mese";
  else if (weeks > 1)
    return weeks + " settimane";
  else if (weeks === 1)
    return "una settimana";
  else if (days >= 2)
    return days + " giorni";
  else if (days === 1)
    return "un giorno";
  else if (hours > 1)
    return hours + " ore";
  else if (hours === 1)
    return "un'ora";
  else if (minutes > 1)
    return minutes + " minuti";
  else if (minutes === 1)
    return "un minuto";
  else if (seconds > 20)
    return "½ minuto";
  else
    return "un attimo";
} // secondsToString1

if (typeof variable !== 'undefined') {
  Vue.prototype.secondsToString = secondsToString;
  Vue.prototype.secondsToString1 = secondsToString1;
}

/**
 * http://stackoverflow.com/a/1527820
 * Returns a random integer between min (inclusive) and max (inclusive)
 * Using Math.round() will give you a non-uniform distribution!
 */
function getRandomInt(min, max) {
  return Math.floor(Math.random() * (max - min + 1)) + min;
}

var api = '/api';

// http://stackoverflow.com/a/901144
function getParameterByName(name, def) {
  var url = window.location.href;
  name = name.replace(/[\[\]]/g, "\\$&");
  var regex = new RegExp("[?&]" + name + "(=([^&#]*)|&|#|$)");
  var results = regex.exec(url);
  if (!results) return def;
  if (!results[2]) return def;
  var q_enc = results[2].replace(/\+/g, " ");
  var q = decodeURIComponent(q_enc);
  return q;
}

// https://gist.github.com/lovasoa/3361645
function array_intersect() {
  var i, all, shortest, nShortest, n, len, ret = [], obj={}, nOthers;
  nOthers = arguments.length-1;
  nShortest = arguments[0].length;
  shortest = 0;
  for (i=0; i<=nOthers; i++){
    n = arguments[i].length;
    if (n<nShortest) {
      shortest = i;
      nShortest = n;
    }
  }

  for (i=0; i<=nOthers; i++) {
    n = (i===shortest)?0:(i||shortest); //Read the shortest array first. Read the first array instead of the shortest
    len = arguments[n].length;
    for (var j=0; j<len; j++) {
        var elem = arguments[n][j];
        if(obj[elem] === i-1) {
          if(i === nOthers) {
            ret.push(elem);
            obj[elem]=0;
          } else {
            obj[elem]=i;
          }
        }else if (i===0) {
          obj[elem]=0;
        }
    }
  }
  return ret;
}

// Array.from polyfill for IE
// https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Array/from
// Production steps of ECMA-262, Edition 6, 22.1.2.1
if (!Array.from) {
  Array.from = (function () {
    var toStr = Object.prototype.toString;
    var isCallable = function (fn) {
      return typeof fn === 'function' || toStr.call(fn) === '[object Function]';
    };
    var toInteger = function (value) {
      var number = Number(value);
      if (isNaN(number)) { return 0; }
      if (number === 0 || !isFinite(number)) { return number; }
      return (number > 0 ? 1 : -1) * Math.floor(Math.abs(number));
    };
    var maxSafeInteger = Math.pow(2, 53) - 1;
    var toLength = function (value) {
      var len = toInteger(value);
      return Math.min(Math.max(len, 0), maxSafeInteger);
    };

    // The length property of the from method is 1.
    return function from(arrayLike/*, mapFn, thisArg */) {
      // 1. Let C be the this value.
      var C = this;

      // 2. Let items be ToObject(arrayLike).
      var items = Object(arrayLike);

      // 3. ReturnIfAbrupt(items).
      if (arrayLike === null) {
        throw new TypeError('Array.from requires an array-like object - not null or undefined');
      }

      // 4. If mapfn is undefined, then let mapping be false.
      var mapFn = arguments.length > 1 ? arguments[1] : void undefined;
      var T;
      if (typeof mapFn !== 'undefined') {
        // 5. else
        // 5. a If IsCallable(mapfn) is false, throw a TypeError exception.
        if (!isCallable(mapFn)) {
          throw new TypeError('Array.from: when provided, the second argument must be a function');
        }

        // 5. b. If thisArg was supplied, let T be thisArg; else let T be undefined.
        if (arguments.length > 2) {
          T = arguments[2];
        }
      }

      // 10. Let lenValue be Get(items, "length").
      // 11. Let len be ToLength(lenValue).
      var len = toLength(items.length);

      // 13. If IsConstructor(C) is true, then
      // 13. a. Let A be the result of calling the [[Construct]] internal method 
      // of C with an argument list containing the single item len.
      // 14. a. Else, Let A be ArrayCreate(len).
      var A = isCallable(C) ? Object(new C(len)) : new Array(len);

      // 16. Let k be 0.
      var k = 0;
      // 17. Repeat, while k < len… (also steps a - h)
      var kValue;
      while (k < len) {
        kValue = items[k];
        if (mapFn) {
          A[k] = typeof T === 'undefined' ? mapFn(kValue, k) : mapFn.call(T, kValue, k);
        } else {
          A[k] = kValue;
        }
        k += 1;
      }
      // 18. Let putStatus be Put(A, "length", len, true).
      A.length = len;
      // 20. Return A.
      return A;
    };
  }());
}

function store_tab() {
  $("a").on("shown.bs.tab", function(e) {
    var id = $(e.target).attr("href").substr(1);
    window.location.hash = id;
  });
}

function show_tab_from_hash() {
  var url = document.location.toString();
  var tab;
  if (url.indexOf("#tab_") != -1) {
    url = url.replace("#tab_", "#");
    tab = url.split('#')[1];
  } else if (url.match(/#.+/)) {
    tab = url.split('#')[1];
  } else {
    tab = 'reset';
  }
  $('.nav-tabs a[href="#' + tab + '"]').tab('show');
}

// trick for module.exports for shallow-equals.js
var module = { exports: {} };

function resetSearch() {
  document.getElementById("query").value = '';
}

function search() {
  var query = document.getElementById("query").value;
  if (query != '') {
    window.location.href = "/search/" + query;
  }
}

function search1() {
  return;
}

function description(filter) {
  if (filter == 'filtered') {
    return 'articoli ancora da leggere, filtrati in base alle tue impostazioni';
  } else if (filter == 'latest') {
    return 'articoli recenti e brevi ancora da leggere, filtrati per lingua';
  } else if (filter == 'suggestions') {
    return 'articoli in italiano ancora da leggere, scelti dall\'algoritmo per te';
  } else if (filter == 'to_read') {
    return 'articoli da te selezionati come interessanti';
  } else if (filter == 'keywords') {
    return 'articoli in italiano ancora da leggere che contengono le parole chiave o che sono stati scritti dagli autori da te selezionati';
  } else if (filter == 'popular') {
    return 'articoli ancora da leggere che gli altri hanno già letto';
  } else if (filter == 'premium') {
    return 'articoli provenienti da fonti premium';
  } else if (filter == 'mixed') {
    return 'mix di 5 viste: filtrati, ultima ora, suggeriti, preferiti e popolari';
  } else if (filter == 'foreign') {
    return 'articoli in lingue diverse dall\'italiano';
  } else if (filter == 'dismissed') {
    return 'articoli nascosti da te';
  } else {
    // filter == read
    return 'gli articoli che hai già letto';
  }
}

var storage = {
  set: function(key, value) {
    window.localStorage.setItem(key, JSON.stringify(value));
  },
  set_element: function(key, index, value) {
    var elements = this.get(key) || {};
    elements[index] = value;
    storage.set(key, elements);
  },
  get: function(key) {
    return JSON.parse(window.localStorage.getItem(key));
  },
  remove: function(key) {
    window.localStorage.removeItem(key);
  },
  clear: function() {
    this.remove('news_cached');
    this.remove('news_status');
    this.remove('news');
    this.remove('articles');
    this.remove('similar');
    this.remove('comments');
  }
};

function showErrorMessage(message) {
  var el = document.getElementById("message");
  el.innerHTML = message;
  setTimeout(function() {
    el.classList.remove('fadeinout');
    el.style.display = "none";
  }, 10000);
  el.style.display = "block";
  el.classList.add('fadeinout');
}

function sendReceiveData(dataTo, verb, url, callback, asy = true) {
  // sends the dataTo to the url with the supplied verb
  // on success executes the callback passing the received data (if any)
  // on error executes the error callback
  // input:
  //   - dataTo, JSON data to send (only POST & PUT)
  //   - verb, string: one of GET, POST, DELETE or PUT
  //   - url, string: the API endpoint
  //   - callback, function: the function to execute on success; the JSON data returned (if any) is forwarded
  // output: on success, whatever the callback returns; on error, nothing
  console.log('sendReceiveData ' + verb + ' ' + url);
  var request = new XMLHttpRequest();
  console.log(verb + ' ' + url);
  request.onload = function() {
    if (request.status >= 200 && request.status < 400) {
      if (request.responseText) {
        console.log('sendReceiveData request success: ' + request.responseText.substring(0, 100) + '...');
        var dataFrom = JSON.parse(request.responseText);
        return callback(dataFrom);
      } else {
        return callback();
      }
    } else if (request.status === 401) {
      // if authorization fails on the API backend, reload the entire page
      window.location.reload(true);
    } else {
      showErrorMessage('Errore del server');
    }
  }; // onload
  request.onerror = function(e) {
    showErrorMessage('Errore di connessione');
  };
  request.open(verb, url, asy);
  request.setRequestHeader('X-Requested-With', 'XMLHttpRequest');
  request.setRequestHeader("accept", "application/json");
  if (verb == 'POST' || verb == 'PUT') {
    var formData = JSON.stringify(dataTo);
    request.setRequestHeader("Content-Type", "application/json; charset=UTF-8");
    request.send(formData);
  } else {
    request.send();
  }
} // sendReceiveData

function stripHtml(html) {
/*
  var div = document.createElement("div");
  div.innerHTML = html;
  return div.textContent || div.innerText || "";
  */
  var doc = new DOMParser().parseFromString(html, 'text/html');
  return doc.body.textContent || "";
}

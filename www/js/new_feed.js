// configuration for jshint
/* jshint browser: true, devel: true */
/* global Vue, api, VeeValidate, $, font_size, sendReceiveData */

"use strict";

var config = { locale: window.base_language };
Vue.use(VeeValidate, config);

var app = new Vue({
  el: '#new_feed',
  data: {
    feed: {
      title: '',
      homepage: '',
      url: '',
      language: 'it',
      license: '',
      topic_id: 0,
      feed_id: 0
    }
  },
  methods: {
    send: function() {
      this.$validator.validateAll();
      if (this.errors.any()) {
        console.log(this.errors.all());
        return;
      }
      var self = this;
      sendReceiveData(self.feed, 'POST', api + '/feeds', function (data) {
        var id = data.id;
        if (id > 0) {
          console.log("feed created with id = " + id);
          self.feed.topic_id = data.topic_id;
          self.feed.feed_id = data.id;
          $('#myModal').modal({keyboard: false});
          $('#myModal').on('hidden.bs.modal', function (e) {
            window.location.href = "/feeds.php";
          });
        } else {
          alert('feed già presente (controlla URL RSS !)');
        }
      });
    }
  }
});

document.addEventListener('DOMContentLoaded', function() {
  console.log("DOM fully loaded and parsed");
  font_size();
});

var element = document.getElementById('language');
element.value = window.base_language;

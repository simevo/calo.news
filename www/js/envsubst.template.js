var aggregator_base_url = 'https://${AGGREGATOR_HOSTNAME}';
var discourse_base_url = 'https://${DISCOURSE_HOSTNAME}';
var eig_category_name = '${EIG_CATEGORY_NAME}';
var accounting_category_name = '${ACCOUNTING_CATEGORY_NAME}';
var feeds_category_name = '${FEEDS_CATEGORY_NAME}';
var base_language = '${BASE_LANGUAGE}';

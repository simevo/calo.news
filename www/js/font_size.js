var min_font_size = 8;
var max_font_size = 32;
var default_font_size = 16;
function get_font_size(size) {
  var size_px = document.documentElement.style.fontSize || default_font_size;
  return parseInt(size_px);
}
function set_font_size(size) {
  document.documentElement.style.fontSize = size + "px";
}
function increase_font_size() {
  console.log("increase_font_size");
  var size = get_font_size();
  console.log("current size = " + size);
  if (size <= max_font_size) {
    size++;
    set_font_size(size);
    localStorage.setItem("size", size);
  }
  return false;
}
function decrease_font_size() {
  console.log("decrease_font_size");
  var size = get_font_size();
  console.log("current size = " + size);
  if (size >= min_font_size) {
    size--;
    set_font_size(size);
    localStorage.setItem("size", size);
  }
  return false;
}
function reset_font_size() {
  console.log("reset_font_size");
  set_font_size(default_font_size);
  localStorage.setItem("size", default_font_size);
}
// call this function after the DOM is fully loaded and parsed
function font_size() {
  var size = localStorage.getItem("size");
  if (size && size <= max_font_size && size >= min_font_size) {
    set_font_size(size);
  }
}

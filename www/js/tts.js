// configuration for jshint
/* jshint browser: true, devel: true */
/* global $ */

"use strict";

// returns the voice
function find_voice(voices, lang) {
  console.log('found ' + voices.length + ' voices');
  var voices_filtered = voices.filter(function(v) {
    console.log('name = ' + v.name + ' lang = ' + v.lang);
    return v.lang == lang;
  });
  if (voices_filtered.length == 0) {
    // try matching only 2-character ISO code
    voices_filtered = voices.filter(function(v) {
      return v.lang.substr(0, 2) == lang;
    });
  }
  console.log('found ' + voices_filtered.length + ' ' + lang + ' voices');
  if (voices_filtered.length == 0) {
    voice = null;
  } else if (voices_filtered.length == 1) {
    voice = voices_filtered[0];
  } else {
    var voices_filtered_default = voices_filtered.filter(function(v) {
      return v.default;
    });
    console.log('found ' + voices_filtered_default.length + ' ' + lang + ' default voices');
    if (voices_filtered_default.length > 0) {
      voice = voices_filtered_default[0];
    } else {
      voice = voices_filtered[0];
    }
  }
} // find_voice

function getParagraphs(element) {
  var ps = [];
  // https://stackoverflow.com/a/24775765
  NodeList.prototype.forEach = Array.prototype.forEach;
  element.childNodes.forEach(function(child) {
    if (child.nodeName == 'P' || child.nodeName == 'H1' || child.nodeName == 'H2' || child.nodeName == 'H3' || child.nodeName == 'H4' || child.nodeName == 'H5') {
      ps = ps.concat([child]);
    } else {
      var sub = getParagraphs(child);
      if (sub.lenght > 0) {
        ps = ps.concat(sub);
      } else {
        ps = ps.concat([child]);
      }
    }
  });
  return ps;
} // getParagraphs

var voice = null;
var lang = '';
var voices = [];

if ('speechSynthesis' in window) {
  voices = window.speechSynthesis.getVoices();
  // Chrome loads voices asynchronously.
  window.speechSynthesis.onvoiceschanged = function() {
    voices = window.speechSynthesis.getVoices();
  };
}

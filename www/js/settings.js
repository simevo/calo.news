// configuration for jshint
/* jshint browser: true, devel: true */
/* global Vue, infiniteScroll, secondsToString, api, VueSelect, show_tab_from_hash, description, font_size, increase_font_size, reset_font_size, decrease_font_size, sendReceiveData */

"use strict";

var url = window.location.href;

Vue.component('v-select', VueSelect.VueSelect);

// global var to store the last received json
var old_json = '';

Vue.filter('currency', function(value) {
  return value.toFixed(2);
});

var app = new Vue({
  el: '#settings',
  created: function () {
    var self = this;
    sendReceiveData(null, 'GET', api + '/settings', function (data) {
      self.settings = data;
      Vue.nextTick(function () {
        old_json = JSON.stringify(self.$data.settings);
        console.log('old json = ' + old_json);
      });
    });    
    sendReceiveData(null, 'GET', api + '/accounts/-1', function (data) {
      self.busy_transactions = false;
      self.transactions = data.transactions;
      sendReceiveData(null, 'GET', api + '/accounts', function (data) {
        self.busy_accounts = false;
        self.accounts = data.accounts;        
      });
    });
  },
  data: {
    settings: [],
    transactions: [],
    busy_transactions: true,
    accounts: [],
    busy_accounts: true,
    dirty: false,
    key: 'count',
    order: 1,
    // TODO: use this instead of naked ISO 3166-1 alpha-2 country codes
    // pending https://trello.com/c/Hk0NmkaQ/15-optionally-return-a-specific-key-from-selected-objects
    langs: [{value: "ar", label: "araba"}, {value: "fr", label: "francese"}, {value: "en", label: "inglese"}, {value: "it", label: "italiana"}, {value: "nl", label: "olandese"}, {value: "pt", label: "portoghese"}, {value: "ru", label: "russa"}, {value: "es", label: "spagnola"}, {value: "de", label: "tedesca"}]
  },
  computed: {
    is_dirty: function(settings) {
      var current_json = JSON.stringify(settings.$data.settings);
      if (old_json) {
        if (current_json !== old_json) {
          this.dirty = true;
        }
      }
      return current_json;
    },
    description_filter2: function() {
      var self = this;
      if (self.settings && self.settings.filter2) {
        return description(self.settings.filter2);
      } else {
        return '';
      }
    },
    sorted_accounts: function() {
      var self = this;
      if (self.key == 'username') {
        return this.accounts.sort(function(a, b) {return a[self.key].toLowerCase() > b[self.key].toLowerCase() ? self.order : -self.order; });
      } else {
        return this.accounts.sort(function(a, b) {return self.order*(a[self.key] - b[self.key]); });
      }
    }
  },
  methods: {
    sortBy: function(key) {
      if (this.key == key) {
        this.order *= -1;
      } else {
        this.key = key;
        this.order = 1;
      }
    }
  }
});

function save(event) {
  event.preventDefault();
  sendReceiveData(app.settings, 'PUT', api + '/settings', function () {
    old_json = JSON.stringify(app.$data.settings);
    app.dirty = false;
  });
}

document.addEventListener('DOMContentLoaded', function() {
  console.log("DOM fully loaded and parsed");
  font_size();
  document.getElementById('fontSizePlus').onclick = increase_font_size;
  document.getElementById('fontReset').onclick = reset_font_size;
  document.getElementById('fontSizeMinus').onclick = decrease_font_size;
});

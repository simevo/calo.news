// configuration for jshint
/* jshint browser: true, devel: true */
/* global Vue, api, VeeValidate, Readability, font_size, franc, sendReceiveData */

"use strict";

Vue.config.ignoredElements = ['trix-editor'];
var config = { locale: window.base_language };
Vue.use(VeeValidate, config);

// ISO 639-3 to ISO 639-1
// https://en.wikipedia.org/wiki/List_of_ISO_639-1_codes
// https://en.wikipedia.org/wiki/List_of_ISO_639-3_codes
function convert(s) {
  if (s == 'arb')
    return 'ar';
  if (s == 'cat')
    return 'ca';
  if (s == 'fra')
    return 'fr';
  if (s == 'eng')
    return 'en';
  if (s == 'nld')
    return 'nl';
  if (s == 'por')
    return 'pt';
  if (s == 'rus')
    return 'ru';
  if (s == 'spa')
    return 'es';
  if (s == 'deu')
    return 'de';
  return window.base_language;
}

function find_candidates(elements) {
  var candidates = [];
  var j;
  for (j = 0; j < elements.length; j++) {
    var e = elements[j];
    candidates = candidates.concat([e.textContent]);
  }
  console.log(candidates);
  // https://stackoverflow.com/a/14438954
  candidates = candidates.filter(function (value, index, self) { 
    return self.indexOf(value) === index;
  });
  return candidates;
}

function guess_author(doc) {
  var i;

  // try first with meta tags
  var metaList = doc.getElementsByTagName("META");
  var candidate;
  for (i = 0; i < metaList.length; i++) {
    var m = metaList[i];
    if (m.getAttribute('property') == 'author') {
      candidate = m.content.replace(/\s+/g, ' ');
      if (candidate) {
        return candidate;
      }
    }
    if (m.getAttribute('property') == 'article:author') {
      candidate = m.content.replace(/\s+/g, ' ');
      if (candidate) {
        return candidate;
      }
    }
    if (m.name == 'author') {
      candidate = m.content.replace(/\s+/g, ' ');
      if (candidate) {
        return candidate;
      }
    }
  }

  // try with the HTML5 Link type "author"
  // <a href="http://johnsplace.com" rel="author">John</a>
  var rel = doc.querySelectorAll('[rel="author"]');
  var c0 = find_candidates(rel);
  if (c0.length == 1) {
    candidate = c0[0].replace(/\s+/g, ' ');
    if (candidate) {
      return candidate;
    }
  }

  // <a ... title="guido smorto" itemprop="author"><strong>guido smorto</strong></a>
  var itemprop = doc.querySelectorAll('[itemprop="author"]');
  var c1 = find_candidates(itemprop);
  if (c1.length == 1) {
    candidate = c1[0].replace(/\s+/g, ' ');
    if (candidate) {
      return candidate;
    }
  }

  // now try with the obvious classes:
  var classnames = ['.autore', '.author', '.auth', '.author-name', '.name-autore', '.item_author', '.bylines'];
  for (i = 0; i < classnames.length; i++) {
    var classname = classnames[i];
    console.log('looking for ', classname);
    var elements = doc.querySelectorAll(classname);
    var c2 = find_candidates(elements);
    if (c2.length == 1) {
      candidate = c2[0].replace(/\s+/g, ' ');
      if (candidate) {
        return candidate;
      }
    }
  }
  
  // default
  return 'anonimo';
}

var app = new Vue({
  el: '#new_article',
  data: {
    article: {
      author: '',
      title: '',
      language: 'it',
      url: '',
      content: ''
    },
    collapsed: false,
    trying: false,
    received: false
  },
  computed: {
    guess_language: function() {
      var self = this;
      var language = franc(self.article.title + '. ' + self.article.content, {whitelist: ['arb', 'fra', 'eng', 'ita', 'nld', 'por', 'rus', 'spa', 'deu']});
      self.article.language = convert(language);
      console.log(language);
      return language;
    }
  },
  methods: {
    try_it: function() {
      if (this.article.url && !this.errors.has('url')) {
        this.trying = true;
        var url = document.getElementById('url');
        url.disabled = 'https://boingboing.net/2018/02/26/adulting-ery.html';
        this.prefill();
      }
    },
    send: function() {
      this.$validator.validateAll();
      if (this.errors.any()) {
        console.log(this.errors.all());
        return;
      }
      sendReceiveData(this.article, 'POST', api + '/articles', function (data) {
        var id = data.id;
        if (id > 0) {
          console.log("article created with id = " + id);
          window.location.href = "/article.php?id=" + id;
        } else {
          var r = confirm('Notizia già presente; se premi OK te la faccio vedere nell\'aggregatore.');
          if (r == true) {
            window.location.href = "/article.php?id=" + (-id);
          }
        }
      });
    },
    clean: function() {
      this.trying = false;
      this.received = false;
      var content_trix = document.getElementById('content_editor');
      content_trix.editor.loadHTML('');
      this.article.content = document.getElementById('content').value;
      this.article.author = '';
      this.article.title = '';
      this.article.language = window.base_language;
      this.article.url = '';
      var url = document.getElementById('url');
      url.removeAttribute('disabled');
    },
    prefill: function() {
      var xhr = new XMLHttpRequest();
      var self = this;
      xhr.open('GET', '/proxy.php?url=' + self.article.url);
      xhr.timeout = 12000; // time in milliseconds
      xhr.onload = function () {
        var parser = document.createElement('a');
        parser.href = self.article.url;
        var doc = document.implementation.createHTMLDocument("");
        doc.documentElement.innerHTML = xhr.responseText;
        self.article.author = guess_author(doc);
        var r = new Readability(doc);
        try {
          var a = r.parse();
          if (a) {
            self.article.title = a.title;
            var content_trix = document.getElementById('content_editor');
            content_trix.editor.loadHTML(a.content);
            self.article.content = a.content;
          }
        } catch (e) {
          alert('impossibile estrarre il contenuto');
        }
        self.trying = false;
        self.received = true;
      };
      xhr.ontimeout = function (e) {
        alert('la pagina non risponde');
        self.trying = false;
        self.received = true;
      };
      xhr.send();
    },
    resetAuthor: function() {
      this.article.author = '';
    },
    resetTitle: function() {
      this.article.title = '';
    }
  }
});

document.addEventListener('DOMContentLoaded', function() {
  console.log("DOM fully loaded and parsed");
  Trix.config.blockAttributes.default.tagName = "p";
  font_size();
  document.getElementById('url').focus();
  document.addEventListener("trix-blur", function(event) {
    app.article.content = document.getElementById('content').value;
  });
});

// configuration for jshint
/* jshint browser: true, devel: true */
/* global Vue, infiniteScroll, secondsToString1, api, VueSelect, array_intersect, getParameterByName, font_size, sendReceiveData */

"use strict";

Vue.component('v-select', VueSelect.VueSelect);

Vue.prototype.secondsToString1 = secondsToString1;

Vue.filter('percent', function(value) {
  if (value) {
    return (value * 100.0).toFixed(0) + " %";
  }
});

var url = window.location.href;

var bus = new Vue();

var app = new Vue({
  el: '#feeds',
  created: function () {
    var endpoint = api + '/feeds';
    var feed_id = getParameterByName('id', -1);
    if (feed_id != -1) {
      // feed query
      endpoint = endpoint + '/' + feed_id;
    }
    var self = this;
    sendReceiveData(null, 'GET', endpoint, function (data) {
      self.feeds = data.feeds;
      bus.$on('rated', function(id, rating) {
        self.showModal = false;
      });
      bus.$on('rating', function(index, item) {
        console.log('selected feed: ', index);
        self.index = index;
        self.item = item;
        self.showModal = true;
        Vue.nextTick(function() { document.getElementById('rating_modal').focus(); });
      });
    });
  },
  updated: function() {
    console.log('unfreeze');
    this.freeze = false;
  },
  data: {
    feeds: [],
    tags: [],
    freeze: true,
    language: '',
    search: '',
    premium: false,
    showModal: false,
    index: -1,
    item: {
      title: ''
    }
  },
  computed: {
    feeds_filtered: function() {
      var self = this;
      if (self.tags.length === 0 && self.search == '' && self.language == '' && !self.premium) {
        return self.feeds;
      } else {
        // console.log(JSON.stringify(self.tags));
        return self.feeds.filter(function(f) {
          // only keep feed which have all the flags
          // console.log(JSON.stringify(f.tags));
          var intersection = array_intersect(f.tags, self.tags);
          // console.log('feed[' + f.title + '] = ' + JSON.stringify(intersection));
          var result = (self.tags.length === intersection.length);
          if (self.search != '') {
            result = result && (f.title.toLowerCase().indexOf(self.search.toLowerCase()) >= 0);
          }
          if (self.language != '') {
            result = result && (f.language == self.language);
          }
          if (self.premium) {
            result = result && f.premium;
          }
          return result;
        });
      }
    }
  }
});

document.addEventListener('DOMContentLoaded', function() {
  console.log("DOM fully loaded and parsed");
  font_size();
});

// override functions from common.js
function resetSearch() {
  document.getElementById("query").value = '';
  app.search = '';
}

function search() {
  app.search = document.getElementById('query').value;
}

function search1() {
  app.search = document.getElementById('query').value;
}

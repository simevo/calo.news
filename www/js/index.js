// configuration for jshint
/* jshint browser: true, devel: true */
/* global render, Vue, infiniteScroll, secondsToString, secondsToString1, getParameterByName, api, login, font_size, $, VueRouter, Article, idbKeyval, VueSelect, shallow, bus, Slip, description, login_name, sendReceiveData, storage, staticRenderFns  */

"use strict";

Vue.component('v-select', VueSelect.VueSelect);

Vue.filter('percent', function(value) {
  if (value) {
    return (value * 100.0).toFixed(0) + " %";
  }
});

document.addEventListener('DOMContentLoaded', function() {
  console.log("DOM fully loaded and parsed");
  font_size();
  $(document).on('hidden.bs.modal', '#modal_email_sent', function (event) {
    var cleanup = document.getElementById('cleanup').checked;
    if (cleanup) {
      console.log('cleanup!');
      bus.$emit('unmark');
    }
    // restore checked state
    document.getElementById('cleanup').checked = true;
  });
});

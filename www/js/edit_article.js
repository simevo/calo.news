// configuration for jshint
/* jshint browser: true, devel: true */
/* global Vue, getParameterByName, api, sendReceiveData, font_size */

"use strict";

Vue.config.ignoredElements = ['trix-editor'];

var url = window.location.href;
var id = getParameterByName('id', 0);

var app = new Vue({
  el: '#new_article',
  data: {
    article: {
      author: '',
      title: '',
      language: 'it',
      url: ''
    },
    collapsed: false,
    collapsed_original: false
  },
 created: function () {
    var self = this;
    sendReceiveData(null, 'GET', api + '/articles/' + id, function (dataFrom) {
      self.article = dataFrom;
      // wait sometime for the trix editors to be ready
      setTimeout(function() {
        if (self.article.content) {
          var content_trix = document.getElementById('content_editor');
          content_trix.editor.insertHTML(self.article.content);
        }
        if (self.article.content_original) {
          var content_original_trix = document.getElementById('content_original_editor');
          content_original_trix.editor.insertHTML(self.article.content_original);
        }
      }, 500);
    });
  },
  methods: {
    send: function() {
      var article = this.article;
      article.content = document.getElementById('content').value;
      article.content_original = document.getElementById('content_original').value;
      sendReceiveData(article, 'PUT', api + '/articles/' + id, function (dataFrom) {
        var id = dataFrom.id;
        if (id > 0) {
          console.log("successfully modified article with id = " + id);
          window.location.href = "/article.php?id=" + id;
        } else {
          alert('errore durante la modifica della notizia');
        }
      });
    }
  }
});

document.addEventListener("DOMContentLoaded", function() {
  console.log("DOM fully loaded and parsed");
  Trix.config.blockAttributes.default.tagName = "p";
  font_size();
});

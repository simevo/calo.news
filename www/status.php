<?php
require __DIR__ . '/../vendor/autoload.php';

function isactive($service) {
    $output = ucfirst(trim(shell_exec("systemctl is-active $service")));
    if ($output == "Active") {
        echo "<td style='color: green;'>Active</td>";
    } 
    else {
        echo "<td style='color: red;'>$output</td>";
    }
}

$dotenv = Dotenv\Dotenv::createImmutable('/srv/calo.news');
$dotenv->load();
$discourse_hostname = getenv('DISCOURSE_HOSTNAME');
$wordpress_hostname = getenv('WORDPRESS_HOSTNAME');
$additional_links = getenv('ADDITONAL_LINKS');
$base_language = getenv('BASE_LANGUAGE');
?>

<!DOCTYPE html>
<html lang="<?php echo($base_language) ?>">
  <head>
    <meta charset="UTF-8">
    <title>calo.news - stato dei servizi</title>
    <script type="text/javascript" src="/js/jquery.slim.min.js"></script>
    <script type="text/javascript" src="/js/vue.min.js"></script>
    <link rel="stylesheet" href="css/bootstrap.min.css">
    <link rel="stylesheet" href="css/style.css">
    <link rel="apple-touch-icon" sizes="180x180" href="/apple-touch-icon.png">
    <link rel="icon" type="image/png" href="/favicon-32x32.png" sizes="32x32">
    <link rel="icon" type="image/png" href="/favicon-16x16.png" sizes="16x16">
    <link rel="manifest" href="/manifest.json">
    <link rel="mask-icon" href="/safari-pinned-tab.svg" color="#5bbad5">
    <meta name="theme-color" content="#ffffff">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <style>
      tr.debug { color: gray; }
      tr.info { color: blue; }
      tr.warning { color: orange; }
      tr.err { color: red; }
      tr.crit { color: white; background-color: magenta; }
    </style>
  </head>
  <body>
    <?php require 'images/fontawesome.svg'; ?>
    <header>
      <nav class="navbar navbar-expand-xl navbar-light bg-primary">
        <div class="navbar-brand">
          <button title="aggregatore" class="btn btn-primary btn-sm dropdown-toggle" type="button" id="dropdownMenuLeft" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
            <img src="/images/logo_calonews_1.png" width="40" height="40" alt="">
          </button>
          <div class="dropdown-menu" aria-labelledby="dropdownMenuLeft">
            <a class="dropdown-item" title="blog calomelanico" href="https://<?php echo($wordpress_hostname); ?>">
              <img src="/images/logo150x150.png" width="40" height="40" alt=""> blog
            </a>
            <a class="dropdown-item" title="conversazioni calomelaniche" href="https://<?php echo($discourse_hostname); ?>">
              <img src="/images/logo_commenti.png" width="40" height="40" alt=""> commenti
            </a>
            <?php echo($additional_links); ?>
          </div>
        </div>
        <script type="text/javascript">
          var login = {};
          var login_name = "";
        </script>
      </nav>
    </header>



    <main>
      <div class="container" id="stats">
        <h1>Stato dei servizi</h1>
        <table class="table">
          <thead>
            <tr>
              <th>Servizio</th>
              <th>Stato</th>
            </tr>
          </thead>
          <tbody>
            <tr>
              <td>readability.service</td>
              <?php isactive('readability.service') ?>
            </tr>
            <tr>
              <td>calonews.service</td>
              <?php isactive('calonews.service') ?>
            </tr>
            <tr>
              <td>docker.service</td>
              <?php isactive('docker.service') ?>
            </tr>
            <tr>
              <td>postgresql.service</td>
              <?php isactive('postgresql.service') ?>
            </tr>
          </tbody>
        </table>

<?php
require 'prolog.php';
if ($decoded): ?>
        <hr>
        <h1>Log</h1>
        <script>

<?php
$rsyslog = file_get_contents('/var/log/calonews.log');

$lines = file('/var/log/calonews.log');
$records = array();
foreach ($lines as $line_num => $line) {
  preg_match('/^(?<stamp>\S{32}) (?<hostname>\S+) local0.(?<channel>\S+) (?<log>\S+): (?<message>.+)$/', $line, $matches);
  $array = \array_diff_key($matches, ['0', '1', '2', '3', '4', '5']);
  array_push($records, $array);
}
echo 'var Items = ' . json_encode($records, JSON_PRETTY_PRINT) . ';';
 ?>
        </script>

        <div id="log">
          <div class="text-center" v-show="busy">
            <div class="spinner-border text-primary m-5" role="status">
              <span class="sr-only">Loading...</span>
            </div>
          </div>
          <div style="display:none;" v-show="!busy">

            Filtra per canale: <select v-model="channel_filter">
              <option v-for="c in channels">{{ c }}</option>
            </select>
            Filtra per log: <select v-model="log_filter">
              <option v-for="l in logs">{{ l }}</option>
            </select>

            <table id="log" class="table">
              <thead>
                <tr>
                  <th @click="sort('stamp')"><svg v-show="currentSort == 'stamp'" width="1em" height="1em"><use v-bind:xlink:href="'#sort-' + currentSortDir"></use></svg> Data e ora</th>
                  <th @click="sort('channel')"><svg v-show="currentSort == 'channel'" width="1em" height="1em"><use v-bind:xlink:href="'#sort-' + currentSortDir"></use></svg> Canale</th>
                  <th @click="sort('log')"><svg v-show="currentSort == 'log'" width="1em" height="1em"><use v-bind:xlink:href="'#sort-' + currentSortDir"></use></svg> Log</th>
                  <th @click="sort('message')"><svg v-show="currentSort == 'message'" width="1em" height="1em"><use v-bind:xlink:href="'#sort-' + currentSortDir"></use></svg> Messaggio</th>
                </tr>
              </thead>
              <tbody>
                <tr :class="i.channel" v-for="i in items_filtered">
                  <td>{{ i.stamp }}</td>
                  <td>{{ i.channel }}</td>
                  <td>{{ i.log }}</td>
                  <td>{{ i.message }}</td>
                </tr> 
              </tbody>
            </table>
          </div>
        </div>

<?php else: ?> <!-- $decoded -->
<script>
var Items = [];
</script>
<pre id="log"></pre>
<?php endif; ?> <!-- $decoded -->
      </div> <!-- container -->
    </main>
    <script type="text/javascript" src="/js/common.js"></script>
    <script type="text/javascript" src="/js/envsubst.js"></script>
    <script type="text/javascript">
// configuration for jshint
/* jshint browser: true, devel: true */
/* global Vue, api, font_size, login_id */

"use strict";

var app = new Vue({
  el: '#log',
  created: function () {
    this.items = Items;
    var self = this;
    Items.forEach(function (i) {
      if (! self.channels.includes(i.channel)) { self.channels.push(i.channel); }
      if (! self.logs.includes(i.log)) { self.logs.push(i.log); }
    });
    this.busy = false;
  },
  data: {
    items: [],
    currentSort: 'stamp',
    currentSortDir: 'down',
    log_filter: '',
    channel_filter: 'err',
    channels: [''],
    logs: [''],
    busy: true
  },
  methods:{
    sort: function(s) {
      // if s == current sort, reverse
      if (s === this.currentSort) {
        this.currentSortDir = this.currentSortDir === 'up' ? 'down' : 'up';
      }
      this.currentSort = s;
    }
  },
  computed: {
    items_filtered: function() {
      var self = this;
      return this.items_sorted.filter(function(i) {
        return (self.channel_filter == '' || self.channel_filter == i.channel) && (self.log_filter == '' || self.log_filter == i.log);
      });

    },
    items_sorted: function() {
      var self = this;
      return this.items.sort(function(a,b) {
        var modifier = 1;
        if (self.currentSortDir === 'down') modifier = -1;
        if (a[self.currentSort] < b[self.currentSort]) return -1 * modifier;
        if (a[self.currentSort] > b[self.currentSort]) return 1 * modifier;
        return 0;
      });
    }
  }
});

document.addEventListener('DOMContentLoaded', function() {
  console.log("DOM fully loaded and parsed");
  font_size();
});

    </script>
<?php require 'footer.php'; ?>


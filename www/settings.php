<?php
require 'prolog.php';
if (!$decoded || !in_array('soci', $decoded->login->groups)) {
    header("location: /");
    exit();
}

$discourse_hostname = getenv('DISCOURSE_HOSTNAME');
$base_language = getenv('BASE_LANGUAGE');
?>
<!DOCTYPE html>
<html lang="<?php echo($base_language) ?>" class="h-100">
  <head>
    <meta charset="UTF-8">
    <title>calo.news - impostazioni</title>
    <script type="text/javascript" src="/js/jquery.slim.min.js"></script>
    <script type="text/javascript" src="/js/vue.min.js"></script>
    <script type="text/javascript" src="/js/vue-select.js"></script>
    <link rel="stylesheet" href="css/bootstrap.min.css">
    <link rel="stylesheet" href="css/style.css">
    <link rel="apple-touch-icon" sizes="180x180" href="/apple-touch-icon.png">
    <link rel="icon" type="image/png" href="/favicon-32x32.png" sizes="32x32">
    <link rel="icon" type="image/png" href="/favicon-16x16.png" sizes="16x16">
    <link rel="manifest" href="/manifest.json">
    <link rel="mask-icon" href="/safari-pinned-tab.svg" color="#5bbad5">
    <meta name="theme-color" content="#ffffff">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  </head>
  <body class="d-flex flex-column h-100">
<?php require 'header.php'; ?>
    <main role="main" class="flex-shrink-0">
      <div class="container" id="settings">
        <h2 class="row">Impostazioni</h2>
        <div class="row">
          <div class="col">
            <strong>Dimensione caratteri: </strong><a href="#" id="fontSizePlus">A+</a> | <a href="#" id="fontReset">reset</a> | <a href="#" id="fontSizeMinus">A-</a>
          </div>
        </div>
        <hr/>
        <ul class="nav nav-tabs" role="tablist">
          <li class="nav-item">
            <a class="nav-link active" data-toggle="tab" role="tab" href="#account">Mio conto</a>
          </li>
          <li class="nav-item">
            <a class="nav-link" data-toggle="tab" role="tab" href="#accounts">Tutti i conti</a>
          </li>
<!--
          <li class="nav-item">
            <a class="nav-link active" data-toggle="tab" role="tab" href="#reset">Azzera dati</a>
          </li>
          <li class="nav-item">
            <a class="nav-link" data-toggle="tab" role="tab" href="#advanced">Avanzate</a>
          </li>
-->
          <li class="nav-item">
            <a class="nav-link" data-toggle="tab" role="tab" href="#news">Newsletter</a>
          </li>
          <li class="nav-item">
            <a class="nav-link" data-toggle="tab" role="tab" href="#filters">Vista "filtrati"</a>
          </li>
          <li class="nav-item">
            <a class="nav-link" data-toggle="tab" role="tab" href="#keywords">Vista "preferiti"</a>
          </li>
          <li class="nav-item">
            <a class="nav-link" data-toggle="tab" role="tab" href="#blacklists">Prima pagina</a>
          </li>
          <li class="nav-item">
            <a class="nav-link" data-toggle="tab" role="tab" href="#stats">Statistiche</a>
          </li>
        </ul>
        <form style="margin-bottom: 2em;">
          <input type="hidden" v-model="is_dirty">
          <div class="tab-content">
            <div class="tab-pane" id="reset" role="tabpanel">
              <p>
                <button disabled type="button" class="btn btn-danger">Azzera tutto</button> Azzera tutti i dati: mappatura e contatori articoli letti, voti e impostazioni
              </p>
              <p>
                <button disabled type="button" class="btn btn-warning">Azzera mappatura</button> Azzera la mappatura dell'utente (cioè i dati di profilazione raccolti per mappare gli interessi) e i contatori degli articoli letti
              </p>
              <p>
                <button disabled type="button" class="btn btn-warning">Azzera ratings</button> Azzera voti (stelline e pollici versi)
              </p>
              <p>
                <button disabled type="button" class="btn btn-warning">Azzera impostazioni</button> Resetta tutte le impostazioni ai valori di difetto
              </p>
            </div> <!-- tab pane -->
            <div class="tab-pane" id="news" role="tabpanel">
              <p>
                <button type="button" class="btn btn-success" v-on:click="window.open('/email.php');">Invia newsletter di prova ora !</button>
              </p>
              <div class="form-check">
                <label class="form-check-label">
                  <input class="form-check-input" type="checkbox" id="newsletter" v-model="settings.newsletter">
                  Inviare la newsletter automaticamente
                </label>
              </div>
              <div class="form-check">
                <label for="list_filter">Tipo di newsletter:</label>
                <select id="list_filter" v-model="settings.filter2" name='filter2' style='font-family: Arial, FontAwesome;' aria-describedby="help_list_filter">
                  <option value='mixed' style='color:black'>! &nbsp; Mix</option>
                  <option value='filtered' style='color:green;'>&#xf0b0; &nbsp; Filtrati</option>
                  <option value='latest' style='color:red;'>&#xf0a1; &nbsp; Ultima ora</option>
                  <option value='suggestions' style='color:black;'>&#xf0eb; &nbsp; Suggeriti</option>
                  <option disabled value='read' style='color:grey;'>&#xf1da; &nbsp; Cronologia</option>
                  <option disabled value='to_read' style='color:blue'>&#xf07a; &nbsp; Interessanti</option>
                  <option value='keywords' style='color:orange'>&#x25ce; &nbsp; Preferiti</option>
                  <option value='popular' style='color:magenta'>&#xf06d; &nbsp; Popolari</option>
                  <option value='premium' style='color:cyan'>&#xf155; &nbsp; Premium</option>
                  <option disabled value='dismissed' style='color:black'>&#xf1f8; &nbsp; Nascosti</option>
                </select>
                <small id="help_list_filter" class="help-block text-muted" style="display: none;" v-show="description_filter2">{{ description_filter2 }}</small>
              </div>
<!--
              <div class="form-check">
                <label class="form-check-label">
                  <input class="form-check-input" type="checkbox" id="list_fulltext" v-model="settings.list_fulltext">
                  Includere il testo completo di tutti gli articoli nella newsletter
                </label>
              </div>
-->
              <div class="form-group">
                <label for="list_email">Indirizzo email alternativo per l'invio della newsletter e degli ebook:</label>
                <input class="form-control" type="email" id="list_email" aria-describedby="help_list_email" v-model="settings.list_email">
                <small id="help_list_email" class="help-block text-muted">utile ad esempio per <a href="https://www.amazon.it/gp/help/customer/display.html?nodeId=201974220" target="_blank">inviare direttamente sul Kindle tramite l'indirizzo e-mail Invia-a-Kindle</a>; se non specificato, newsletter ed ebook saranno inviati all'indirizzo predefinito del profilo</small>
              </div>
<!--
              <div class="form-group">
                <label for="list_frequency">Frequenza della newsletter automatica:</label>
                <select disabled class="form-control" id="list_frequency" v-model="settings.list_frequency">
                  <option value='daily'>giornaliera</option>
                  <option value='weekly'>settimanale (domenica)</option>
                </select>
              </div>
-->
              <div class="form-group">
                <label for="list_frequency">Orario d'invio della newsletter automatica:</label>
                <select v-bind:disabled="!settings.newsletter" class="form-control" id="list_hour" v-model="settings.list_hour">
                  <option value='0'>00</option>
                  <option value='1'>01</option>
                  <option value='2'>02</option>
                  <option value='3'>03</option>
                  <option value='4'>04</option>
                  <option value='5'>05</option>
                  <option value='6'>06</option>
                  <option value='7'>07</option>
                  <option value='8'>08</option>
                  <option value='9'>09</option>
                  <option value='10'>10</option>
                  <option value='11'>11</option>
                  <option value='12'>12</option>
                  <option value='13'>13</option>
                  <option value='14'>14</option>
                  <option value='15'>15</option>
                  <option value='16'>16</option>
                  <option value='17'>17</option>
                  <option value='18'>18</option>
                  <option value='19'>19</option>
                  <option value='20'>20</option>
                  <option value='21'>21</option>
                  <option value='22'>22</option>
                  <option value='23'>23</option>
                </select>
              </div>
              <div class="form-group">
                <label for="list_news">Numero di articoli da includere nella newsletter:</label>
                <input class="form-control" type="number" min="1" max="100" id="list_news" v-model.number="settings.list_news">
              </div>  
              <div class="form-group">
                <span>Formato:</span>
                <select class="custom-select" id="list_format" v-model="settings.list_format">
                  <option value="html">html</option>
                  <option value="epub">epub</option>
                  <option value="mobi">mobi (amazon kindle)</option>
                  <option value="pdf">pdf</option>
                  <option value="txt">testo</option>
                </select>
              </div>
              <button type="submit" class="btn btn-primary" v-bind:disabled="!dirty" onclick="save(event);">Salva</button>
            </div> <!-- tab pane -->
            <div class="tab-pane" id="filters" role="tabpanel">
              <h4>Nella vista "filtrati":</h4>
              
              <h5>Mostrami le notizie provenienti da fonti taggate con:</h5>
              <div class="form-group">
                <v-select multiple placeholder="filtra per tag" :options="['cultura', 'società', 'italia', 'estero', 'scienza_tecnica', 'sport', 'economia', 'blog', 'notiziario', 'rivista']" v-model='settings.tags' aria-describedby="help_tags"></v-select>
              </div>
              <small id="help_tags" class="help-block text-muted">Se non scegli alcuna tag, ti farò vedere tutti gli articoli; basta che una fonte abbia uno di questi tag per venire inclusa.<br/><b>N.B.</b>: alcune fonti sono taggate in modo generico quindi escludere alcuni tag non garantisce che <b>tutti</b> gli articoli su quell'argomento spariranno dal tuo newsfeed !</small>

              <h5>Mostrami solo le notizie in queste lingue:</h5>
              <div class="form-group">
                <v-select multiple placeholder="filtra per lingua" :options="['ar', 'ca', 'fr', 'en', 'it', 'nl', 'pt', 'es', 'de']" v-model='settings.languages'></v-select>
                <small class="text-muted">Se non scegli alcuna lingua, ti farò vedere tutti gli articoli. Se includi l'italiano, ti farò vedere anche gli articoli tradotti.</small>
              </div>
              <button type="submit" class="btn btn-primary" v-bind:disabled="!dirty" onclick="save(event);">Salva</button>
            </div> <!-- tab pane -->
            <div class="tab-pane" id="keywords" role="tabpanel">
              <h4>Nella vista "preferiti":</h4>
              <div class="alert alert-danger" role="alert" v-show="settings.whitelist == '' || settings.whitelist_authors == ''">
                Indica qua sotto quali sono gli <b>argomenti</b> e gli <b>autori</b> che <b>ti interessano</b>, solo in questo modo la vista preferiti sarà personalizzata alle tue esigenze !
              </div>              
              <h5>Mostrami solo le notizie contenenti almeno una tra queste <strong>parole chiave</strong> o provenienti da questi <strong>autori</strong>:</h5>
              <div class="form-group">
                <label for="whitelist">Parole chiave (separate da spazio)</label>
                <textarea placeholder="parola1 parola2 parola3" class="form-control" id="whitelist" rows="2" v-model="settings.whitelist"></textarea>
              </div>
              <div class="form-group">
                <label for="whitelist_authors">Autori (separati da spazio)</label>
                <textarea placeholder="autore1 autore2 autore3" class="form-control" id="whitelist_authors" rows="2" v-model="settings.whitelist_authors"></textarea>
              </div>
              <button type="submit" class="btn btn-primary" v-bind:disabled="!dirty" onclick="save(event);">Salva</button>
            </div>
            <div class="tab-pane" id="blacklists" role="tabpanel">
              <h4>Nella prima pagina:</h4>
              <div class="alert alert-danger" role="alert" v-show="settings.blacklist == ''">
                Indica qua sotto quali sono gli <b>argomenti</b> che <b>non ti interessano</b>, in questo modo potrai bannare nella prima pagina gli articoli indesiderati cliccando sul bottone che appare a sinistra
                <button disabled type="button" class="btn btn-small btn-info">
                  <svg width="1em" height="1em" style="fill: white">
                    <use xlink:href="#ban"></use>
                  </svg>
                </button> !
              </div>
              <div class="form-group">
                <label for="blacklist">Escludi gli articoli che contengono queste parole chiave (separate da spazio)</label>
                <textarea class="form-control" id="blacklist" rows="2" v-model="settings.blacklist" placeholder="parola1 parola2 parola3" ></textarea>
              </div>
              <button type="submit" class="btn btn-primary" v-bind:disabled="!dirty" onclick="save(event);">Salva</button>
            </div>
            <div class="tab-pane" id="advanced" role="tabpanel">
              <div class="form-group">
                <label for="sociality">Socialità (quanto le preferenze altrui contribuiscono alla rilevanza degli articoli che vengono mostrati a me)</label>
                <div class="row">
                  <div class="col-xs-1 no-padding center">
                    <span class="gray">min</span>
                  </div>
                  <div class="col-xs-10">
                    <input disabled type="range" min="-5" max="5" class="form-control" id="sociality" v-model="settings.sociality_weight">
                  </div>
                  <div class="col-xs-1 no-padding center">
                    <span class="gray">max</span>
                  </div>
                </div>
              </div>
              <div class="form-group">
                <label for="gravity">Gravità: esponente che serve ad accelerare (valori più alti) o rallentare (valori più bassi) l’affondamento</label>
                <input disabled class="form-control" type="number" value="2" id="gravity" min="1" max="3" v-model="settings.gravity">
              </div>
              <div class="form-group">
                <label for="comment_factor">Coefficiente d'interesse dei commenti: fattore che serve a dare maggiore (valori più alti) o minore peso (valori più bassi) ai commenti nella stima dell'interesse di un articolo</label>
                <input disabled class="form-control" type="number" value="4" id="comment_factor" min="1" max="10" v-model="settings.comment_factor">
              </div>
              <div class="form-group">
                <label for="age_divider">Divisore di normalizzazione dell'età: coefficiente che serve a dare maggiore (valori alti) o minore (valori bassi) peso ai rating rispetto all'età degli articoli nella determinazione della posizione</label>
                <input disabled class="form-control" type="number" value="100" id="age_divider" min="1" max="1000" v-model="settings.age_divider">
              </div>
              <div class="form-group">
                <label for="feed">Peso del rating personale sul feed sulla rilevanza</label>
                <div class="row">
                  <div class="col-xs-1 no-padding center">
                    <span class="gray">min</span>
                  </div>
                  <div class="col-xs-10">
                    <input disabled type="range" min="-5" max="5" class="form-control" id="feed" v-model="settings.feed_weight">
                  </div>
                  <div class="col-xs-1 no-padding center">
                    <span class="gray">max</span>
                  </div>
                </div>
              </div>
              <div class="form-group">
                <label for="list_weight">Peso delle black/white lists sulla rilevanza</label>
                <div class="row">
                  <div class="col-xs-1 no-padding center">
                    <span class="gray">min</span>
                  </div>
                  <div class="col-xs-10">
                    <input disabled type="range" min="-5" max="5" class="form-control" id="list_weight" v-model="settings.list_weight">
                  </div>
                  <div class="col-xs-1 no-padding center">
                    <span class="gray">max</span>
                  </div>
                </div>
              </div>
              <button type="submit" class="btn btn-primary" v-bind:disabled="!dirty" onclick="save(event);">Salva</button>
            </div> <!-- tab pane -->

            <div class="tab-pane active" id="account" role="tabpanel">
              <div class="mx-auto" style="width: 50px;" v-show="busy_transactions">
                <div class="spinner-border text-primary" role="status">
                  <span class="sr-only">Loading...</span>
                </div>
              </div>
              <table class="table" v-show="!busy_transactions">
                <thead>
                  <tr>
                    <th>Data</th>
                    <th>Transazione, €</th>
                    <th>Saldo, €</th>
                  </tr>
                </thead>
                <tbody>
                  <tr v-for="t in transactions">
                    <td>{{ t.date }}</td>
                    <td>{{ t.amount | currency }}</td>
                    <td>{{ t.balance | currency }}</td>
                  </tr> 
                </tbody>
              </table>
            </div> <!-- tab pane -->

            <div class="tab-pane" id="accounts" role="tabpanel">
              <div class="mx-auto" style="width: 50px;" v-show="busy_accounts">
                <div class="spinner-border text-primary" role="status">
                  <span class="sr-only">Loading...</span>
                </div>
              </div>
              <table class="table" v-show="!busy_accounts">
                <thead>
                  <tr>
                    <th v-on:click="sortBy('count')">Numero</th>
                    <th v-on:click="sortBy('username')">Socio</th>
                    <th v-on:click="sortBy('balance')">Saldo, €</th>
                  </tr>
                </thead>
                <tbody>
                  <tr v-for="a in sorted_accounts" v-bind:class="{ 'table-primary': a.balance > -10 }">
                    <td>{{ a.count }}</td>
                    <td><a href="#" v-bind:href="'https://<?php echo($discourse_hostname); ?>/u/' + a.username + '/summary'">{{ a.username }}</a></td>
                    <td>{{ a.balance | currency }}</td>
                  </tr> 
                </tbody>
              </table>
            </div> <!-- tab pane -->

            <div class="tab-pane" id="stats" role="tabpanel">
              <ul>
                <li><a target="_blank" href="/status.php">Stato dei servizi</a></li>
                <li><a target="_blank" href="/stats.php">Classifica delle fonti più lette</a></li>
                <li><a target="_blank" href="/user_stats.php">Quanti utenti usano l’aggregatore di notizie ?</a></li>
                <li><a target="_blank" href="/api/feed_activity">Grafico con l'attività mensile dei feed</a> (lento !)</li>
                <li><a target="_blank" href="/translation_stats.php">Traduzioni automatiche</a></li>
                <li><a target="_blank" href="/feed_stats.php">Statistiche sulle fonti</a></li>
              </ul>
            </div> <!-- tab pane -->

          </div> <!-- tab content -->
        </form>
      </div> <!-- container -->
    </main>
    <script type="text/javascript" src="/js/common.js"></script>
    <script type="text/javascript" src="/js/envsubst.js"></script>
    <script type="text/javascript" src="/js/settings.js"></script>
<?php require 'footer.php'; ?>
    <script type="text/javascript">
      // run this after jquery is up & running
      show_tab_from_hash();
      store_tab();
    </script>

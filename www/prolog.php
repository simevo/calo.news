<?php
require __DIR__ . '/../vendor/autoload.php';
use \Firebase\JWT\JWT;
$dotenv = Dotenv\Dotenv::createImmutable('/srv/calo.news');
$dotenv->load();

$dummy_sso = getenv('DUMMY_SSO') ? true : false;

function deleteKey($key)
{
    setcookie($key, "", 1, "/", "", true, false);
}

if ($dummy_sso) {
    $decoded = (object) array(
        'login' => (object) array(
            'external_id' => 2,
            'username' => 'john.doe',
            'groups' => array('soci', 'staff')
        )
    );
} else {
    $decoded = null;
    if (isset($_COOKIE["token"])) {
        $encoded_secret = file_get_contents('../secrets/secret'); // base64 encoded
        $secret = base64_decode(strtr($encoded_secret, '-_', '+/'));
        try {
            $token = $_COOKIE["token"];
            $decoded = JWT::decode($token, $secret, array('HS256'));
            deleteKey('redirect');
        } catch (Exception $e) {
            error_log('error while decoding');
        }
    } else {
        if (isset($_COOKIE["nonce"])) {
            setcookie('redirect', $_SERVER['REQUEST_URI'], time()+60, "/", "", true, false);
            header("location: /sso.php");
            exit();
        }
    }
}

?>

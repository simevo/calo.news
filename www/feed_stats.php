<?php
require 'prolog.php';
if (!$decoded || !in_array('soci', $decoded->login->groups)) {
    header("location: /");
    exit();
}

$base_language = getenv('BASE_LANGUAGE');

$conn = pg_pconnect("dbname=calonews");
if (!$conn) {
    echo "An error occurred while connecting to the database.\n";
    exit;
}

$query = <<<SQL
WITH
  day AS (SELECT feed_id, COUNT(*) FROM articles WHERE stamp > NOW() - INTERVAL '1 DAY' group by feed_id),
  week AS (SELECT feed_id, COUNT(*) FROM articles WHERE stamp > NOW() - INTERVAL '1 WEEK' group by feed_id), 
  month AS (SELECT feed_id, COUNT(*) FROM articles WHERE stamp > NOW() - INTERVAL '1 MONTH' group by feed_id)
  SELECT
    feeds.id,
    feeds.title,
    feeds.icon,
    last_polled_epoch,
    article_count,
    views,
    day.count as day,
    week.count as week,
    month.count as month
  FROM
    feeds
    LEFT JOIN feeds_data ON feeds.id=feed_id
    LEFT JOIN day ON feeds.id=day.feed_id
    LEFT JOIN week ON feeds.id=week.feed_id
    LEFT JOIN month ON feeds.id=month.feed_id
  ORDER BY id
SQL;
$result = pg_query($conn, $query);
if (!$result) {
    echo "An error occurred while performing the query.\n";
    exit;
}

$rows = pg_fetch_all($result);
if (!$rows) {
    echo "An error occurred while retrieving the row.\n";
    exit;
}
?>

<!DOCTYPE html>
<html lang="<?php echo($base_language) ?>">
  <head>
    <meta charset="UTF-8">
    <title>calo.news - statistiche sulle fonti</title>
    <script type="text/javascript" src="/js/jquery.slim.min.js"></script>
    <link rel="stylesheet" href="css/bootstrap.min.css">
    <link rel="stylesheet" href="css/style.css">
    <link rel="apple-touch-icon" sizes="180x180" href="/apple-touch-icon.png">
    <link rel="icon" type="image/png" href="/favicon-32x32.png" sizes="32x32">
    <link rel="icon" type="image/png" href="/favicon-16x16.png" sizes="16x16">
    <link rel="manifest" href="/manifest.json">
    <link rel="mask-icon" href="/safari-pinned-tab.svg" color="#5bbad5">
    <meta name="theme-color" content="#ffffff">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="format-detection" content="telephone=no">
  </head>
  <body>
<?php require 'header.php'; ?>
    <main>
      <div class="container">
        <h2>Statistiche sulle fonti</h2>
        <table class="table">
          <thead>
            <tr>
              <th rowspan="2">id</th>
              <th rowspan="2">icon</th>
              <th rowspan="2">title</th>
              <th colspan="4">articoli aggregati</th>
              <th rowspan="2">last scanned</th>
              <th rowspan="2">read articles</th>
            </tr>
            <tr>
              <th>oggi</th>
              <th>ultimi 7 giorni</th>
              <th>ultimi 30 giorni</th>
              <th>totali</th>
            </tr>
          </thead>
          <tbody>
<?php
foreach($rows as $row)
{
    $id = $row['id'];
    $link = "/feed/" . $id;
    $icon = $row['icon'];
    $title = $row['title'];
    $article_count = $row['article_count'];
    $views = $row['views'];
    $day = $row['day'];
    $week = $row['week'];
    $month = $row['month'];
    $last_polled = date("Y-m-d H:i:s", $row['last_polled_epoch']);
    echo "            <tr>";
    echo "              <td>$id</td>";
    echo "              <td><img width='30px' height='30px' src='$icon' alt='feed logo'></td>";
    echo "              <td><a href='$link'>$title</td>";
    echo "              <td>$day</td>";
    echo "              <td>$week</td>";
    echo "              <td>$month</td>";
    echo "              <td>$article_count</td>";
    echo "              <td>$last_polled</td>";
    echo "              <td>$views</td>";
    echo "            </tr>";
}
?>
          </tbody>
        </table>
      </div> <!-- container -->
    </main>
<?php require 'footer.php'; ?>

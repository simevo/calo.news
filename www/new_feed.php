<?php
require 'prolog.php';
if (!$decoded || !in_array('soci', $decoded->login->groups)) {
    header("location: /");
    exit();
}
$base_language = getenv('BASE_LANGUAGE');
?>
<!DOCTYPE html>
<html lang="<?php echo($base_language) ?>" class="h-100">
  <head>
    <meta charset="UTF-8">
    <title>calo.news - aggungi fonte</title>
    <script type="text/javascript" src="/js/jquery.slim.min.js"></script>
    <script type="text/javascript" src="/js/vue.min.js"></script>
    <script type="text/javascript" src="/js/vee-validate.min.js"></script>
    <script type="text/javascript" src="/js/locale/it.js"></script>
    <link rel="stylesheet" href="css/bootstrap.min.css">
    <link rel="stylesheet" href="css/style.css">
    <link rel="apple-touch-icon" sizes="180x180" href="/apple-touch-icon.png">
    <link rel="icon" type="image/png" href="/favicon-32x32.png" sizes="32x32">
    <link rel="icon" type="image/png" href="/favicon-16x16.png" sizes="16x16">
    <link rel="manifest" href="/manifest.json">
    <link rel="mask-icon" href="/safari-pinned-tab.svg" color="#5bbad5">
    <meta name="theme-color" content="#ffffff">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  </head>
  <body class="d-flex flex-column h-100">
<?php require 'header.php'; ?>
    <main id="new_feed" role="main" class="flex-shrink-0">
      <div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="Fonte aggiunta" aria-hidden="true">
        <div class="modal-dialog" role="document">
          <div class="modal-content">
            <div class="modal-header">
              <h5 class="modal-title">Fonte aggiunta !</h5>
              <button type="button" class="close" data-dismiss="modal" aria-label="Chiudi">
                <span aria-hidden="true">&times;</span>
              </button>
            </div>
            <div class="modal-body">
              <p>Hai appena aggiunto la fonte <strong>{{ feed.title }}</strong> all'aggregatore. Clicca <a v-bind:href="'/feed/' + feed.feed_id">qui</a> per visualizzarla direttamente. Ho anche pubblicato <a href="#" v-bind:href="window.discourse_base_url + '/t/' + feed.topic_id">una notifica nella categoria <b>fonti aggiunte</b></a></p>
            </div>
            <div class="modal-footer">
              <button type="button" class="btn btn-secondary" data-dismiss="modal">OK</button>
            </div>
          </div>
        </div>
      </div>
      <div class="container">
        <h2>Aggiungi una nuova fonte</h2>
        <p>Per richiedere l'aggiunta di una nuova fonte (anche se a pagamento), crea un argomento nella <a v-bind:href="window.discourse_base_url + '/c/' + window.feeds_category_name">categoria {{ window.feeds_category_name }} della piattforma di commento</a>.</p>
        
        <p>Per creare direttamente una <strong>fonte gratuita</strong> compila il form qua sotto e premi <strong>Crea fonte</strong> in fondo.</p>
        <form>
          <div class="form-group" :class="{'has-danger': errors.has('title') }">
            <label for="title">Nome della fonte</label>
            <input v-validate="'required|min:2'" type="text" class="form-control" id="title" name="title" placeholder="nome breve" v-model="feed.title">
            <p class="text-danger" v-if="errors.has('title')">{{ errors.first('title') }}</p>
          </div>
          <div class="form-group" :class="{'has-danger': errors.has('homepage') }">
            <label for="homepage">
              <svg width="1em" height="1em">
                <use xlink:href="#home"></use>
              </svg>
              <span> <a target="_blank" v-bind:href="feed.homepage">Link</a> alla homepage (URL)</span>
            </label>
            <input v-validate="'required|url:true'" type="url" class="form-control" id="homepage" name="homepage" placeholder="http://..." v-model="feed.homepage">
            <p class="text-danger" v-if="errors.has('homepage')">{{ errors.first('homepage') }}</p>
          </div>
          <div class="form-group" :class="{'has-danger': errors.has('url') }">
            <label for="url">
              <svg width="1em" height="1em">
                <use xlink:href="#rss-square"></use>
              </svg>
              <span> <a target="_blank" v-bind:href="feed.url">Link</a> al feed <a href="https://it.wikipedia.org/wiki/RSS" target="_blank">RSS</a> (RSS URL)</span>
            </label>
            <input v-validate="'required|url:true'" type="url" class="form-control" id="url" name="url" placeholder="http://.../rss2"  v-model="feed.url">
            <p class="text-danger" v-if="errors.has('url')">{{ errors.first('url') }}</p>
          </div>
          <div class="form-group">
            <label for="language">Lingua</label>
            <select class="form-control" id="language" v-model="feed.language">
              <?php require 'languages.php'; ?>
            </select>
          </div>
          <div class="form-group" :class="{'has-danger': errors.has('license') }">
            <label for="license">Licenza</label>
            <input v-validate="'required|min:10'" type="text" class="form-control" id="license" name="license" placeholder="Tutti i diritti riservati / Creative Commons ...">
            <p class="text-danger" v-if="errors.has('license')">{{ errors.first('license') }}</p>
          </div>
          <button v-on:click="send();" type="button" class="btn btn-primary">Crea fonte</button>
        </form>
      </div>
    </main>
    <script type="text/javascript" src="/js/es6-promise.auto.min.js"></script>
    <script type="text/javascript" src="/js/common.js"></script>
    <script type="text/javascript" src="/js/envsubst.js"></script>
    <script type="text/javascript" src="/js/new_feed.js"></script>
<?php include 'footer.php'; ?>

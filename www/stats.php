<?php
require 'prolog.php';
if (!$decoded) {
    header("location: /");
    exit();
}
$base_language = getenv('BASE_LANGUAGE');
?>
<!DOCTYPE html>
<html lang="<?php echo($base_language) ?>">
  <head>
    <meta charset="UTF-8">
    <title>calo.news - statistiche sulle fonti</title>
    <script type="text/javascript" src="/js/jquery.slim.min.js"></script>
    <script type="text/javascript" src="/js/vue.min.js"></script>
    <link rel="stylesheet" href="css/bootstrap.min.css">
    <link rel="stylesheet" href="css/style.css">
    <link rel="apple-touch-icon" sizes="180x180" href="/apple-touch-icon.png">
    <link rel="icon" type="image/png" href="/favicon-32x32.png" sizes="32x32">
    <link rel="icon" type="image/png" href="/favicon-16x16.png" sizes="16x16">
    <link rel="manifest" href="/manifest.json">
    <link rel="mask-icon" href="/safari-pinned-tab.svg" color="#5bbad5">
    <meta name="theme-color" content="#ffffff">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  </head>
  <body>
<?php require 'header.php'; ?>
    <main>
      <div class="container" id="stats">
        <h2 class="row">Classifica delle fonti più lette</h2>
        <form>
          <div>
            <label for="party">Data d'inizio:</label>
            <input type="date" id="start" name="start" min="2004-07-15" max="<?php echo(date('Y-m-d')); ?>" value="2004-07-15" onchange="app.clear(); app.load();">
          </div>
        </form>
        <p>Calcolato <a href="https://en.wikipedia.org/wiki/Borda_count">secondo Borda</a> con <a href="https://gitlab.com/simevo/calo.news/blob/master/py/borda.py">questo algoritmo</a></p>
        <ul class="nav nav-tabs" role="tablist">
          <li class="nav-item">
            <a class="nav-link active" data-toggle="tab" role="tab" href="#all">Da tutti gli utenti</a>
          </li>
          <li class="nav-item">
            <a class="nav-link" data-toggle="tab" role="tab" href="#you">Solo da te</a>
          </li>
        </ul>
        <div class="tab-content">
          <div class="tab-pane active" id="all" role="tabpanel">
            <table class="table">
              <thead>
                <tr>
                  <th>Fonte</th>
                  <th>Punti</th>
                </tr>
              </thead>
              <tbody>
                <tr v-for="v in sorted_votes">
                  <td>{{ v[0] }}</td>
                  <td>{{ v[1] }}</td>
                </tr> 
              </tbody>
            </table>
          </div> <!-- tab-pane -->
          <div class="tab-pane" id="you" role="tabpanel">
            <table class="table">
              <thead>
                <tr>
                  <th>Fonte</th>
                  <th>Punti</th>
                </tr>
              </thead>
              <tbody>
                <tr v-for="v in user_votes[login_id]">
                  <td>{{ v[0] }}</td>
                  <td>{{ v[1] }}</td>
                </tr> 
              </tbody>
            </table>
          </div> <!-- tab-pane -->
        </div> <!-- tab-content -->
      </div> <!-- container -->
    </main>
    <script type="text/javascript" src="/js/common.js"></script>
    <script type="text/javascript" src="/js/envsubst.js"></script>
    <script type="text/javascript">
// configuration for jshint
/* jshint browser: true, devel: true */
/* global Vue, api, font_size, login_id */

"use strict";

var app = new Vue({
  el: '#stats',
  created: function () {
    this.load();
  },
  data: {
    user_votes: [],
    sorted_votes: []
  },
  methods: {
    load: function() {
      var xhr = new XMLHttpRequest();
      var self = this;
      var start = document.getElementById('start').value;
      console.log(start);
      if (self.isValidDate(start)) {
        xhr.open('GET', api + '/borda/' + start);
        xhr.onload = function () {
          var data = JSON.parse(xhr.responseText);
          self.user_votes = data.user_votes;
          self.sorted_votes = data.sorted_votes;
        };
        xhr.setRequestHeader('X-Requested-With', 'XMLHttpRequest');
        xhr.setRequestHeader("accept", "application/json");
        xhr.send();
      }
    },
    clear: function() {
      this.user_votes = [];
      this.sorted_votes = [];
    },
    isValidDate: function(dateString) {
      // based on: https://stackoverflow.com/a/6178341
      // First check for the pattern
      if(!/^\d{4}-\d{2}-\d{2}$/.test(dateString)) {
        return false;
      }

      // Parse the date parts to integers
      var parts = dateString.split("-");
      var year = parseInt(parts[0], 10);
      var month = parseInt(parts[1], 10);
      var day = parseInt(parts[2], 10);

      // Check the ranges of month and year
      if(year < 1000 || year > 3000 || month == 0 || month > 12) {
        return false;
      }

      var monthLength = [ 31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31 ];

      // Adjust for leap years
      if(year % 400 == 0 || (year % 100 != 0 && year % 4 == 0)) {
        monthLength[1] = 29;
      }

      // Check the range of the day
      return day > 0 && day <= monthLength[month - 1];
    }
  }
});

document.addEventListener('DOMContentLoaded', function() {
  console.log("DOM fully loaded and parsed");
  font_size();
});

    </script>
<?php require 'footer.php'; ?>

<?php
require 'prolog.php';
if (!$decoded || !in_array('soci', $decoded->login->groups)) {
    header("location: /");
    exit();
}

$admin_email = getenv('ADMIN_EMAIL');
$aggregator_hostname = getenv('AGGREGATOR_HOSTNAME');

$id = $decoded->login->external_id;

$conn = pg_pconnect("dbname=calonews");
if (!$conn) {
    echo "An error occurred while connecting to the database.\n";
    exit;
}
$query = "SELECT list_email,list_format,list_fulltext FROM users WHERE id = $1";
$result = pg_query_params($conn, $query, [$id]);
if (!$result) {
    echo "An error occurred while performing the query.\n";
    exit;
}
$row = pg_fetch_row($result);
if (!$row) {
    echo "An error occurred while retrieving the row.\n";
    exit;
}
$to = $row[0];
if ($to == '') {
    $to = $decoded->login->email;
}
$format = $row[1];
if (isset($_GET["format"])) {
    $format = $_GET["format"];
}
$fulltext = $row[2];

// http://stackoverflow.com/a/20289096
$tz = 'Europe/Rome';
$timestamp = time();
$dt = new DateTime("now", new DateTimeZone($tz)); // first argument "must" be a string
$dt->setTimestamp($timestamp); // adjust the object to correct timestamp
$date = $dt->format('YmdHis');
$filename = 'estratto_' . $date;

$ch = curl_init();
if ($format == 'html') {
    $endpoint = "http://localhost:8080/articles.html";
} else if ($format == 'txt') {
    $endpoint = "http://localhost:8080/articles.txt";
} else if ($format == 'pdf') {
    $endpoint = "https://$aggregator_hostname/articles_pdf.php";
} else if ($format == 'mobi') {
    $endpoint = "http://localhost:8080/articles.mobi";
} else if ($format == 'epub') {
    $endpoint = "http://localhost:8080/articles.epub";
} else {
    echo("Formato sconosciuto" . $format);
    exit();
}
if (isset($_GET["ids"])) {
    $title = 'selezione articoli';
    $endpoint = $endpoint."?ids=".$_GET["ids"];
} else {
    $title = 'newsletter';
}
curl_setopt($ch, CURLOPT_URL, $endpoint);
curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
$strCookie = 'token=' . $token;
curl_setopt($ch, CURLOPT_COOKIE, $strCookie);
$articles = curl_exec($ch);
curl_close($ch);

$mail = new PHPMailer\PHPMailer\PHPMailer();
$mail->CharSet = 'UTF-8';
$mail->setFrom($admin_email, 'Calomelano Social Club');
$mail->Subject = $title . ' in formato ' . $format;
$mail->addAddress($to);

if ($format == 'html') {
    $mail->msgHtml($articles);
    $mail->AltBody = "La tua $title in formato HTML è in allegato.\n\nPer modificare le impostazioni relative alla newsletter visita: https://$aggregator_hostname/settings.php#news\n\nCiao !";
} else if ($format == 'txt') {
    $mail->Body      = $articles;
} else if ($format == 'pdf') {
    $mail->Body      = "La tua $title in formato PDF è in allegato.\n\nPer modificare le impostazioni relative alla newsletter visita: https://$aggregator_hostname/settings.php#news\n\nCiao !";
    $mail->AddStringAttachment($articles, $filename . '.pdf');
} else if ($format == 'mobi') {
    $mail->Body      = "La tua $title in formato Amazon Kindle (mobi) è in allegato.\n\nPer modificare le impostazioni relative alla newsletter visita: https://$aggregator_hostname/settings.php#news\n\nCiao !";
    $mail->AddStringAttachment($articles, $filename . '.mobi');
} else if ($format == 'epub') {
    $mail->Body      = "La tua $title in formato epub è in allegato.\n\nPer modificare le impostazioni della tua newsletter visita: https://$aggregator_hostname/settings.php#news\n\nCiao !";
    $mail->addStringAttachment($articles, $filename . '.epub');
} else {
    echo("Formato sconosciuto" . $format);
    exit();
}

$result = $mail->send();

if ($result) {
    echo("Ho inviato una mail con gli articoli in formato " . $format . " a: " . $to);
} else {
    echo("Errore durante l'invio degli articoli in formato " . $format . " a: " . $to);
    echo("Errore di PHPMailer: " . $mail->ErrorInfo);
}
?>

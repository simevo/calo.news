-- SQL create the feeds_data view
--
-- This file is part of calo.news: A news platform
--
-- Copyright (C) 2017-2023 Paolo Greppi
--
-- This program is free software: you can redistribute it and/or modify
-- it under the terms of the GNU Affero General Public License as published by
-- the Free Software Foundation, either version 3 of the License, or
-- (at your option) any later version.
--
-- This program is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
-- GNU Affero General Public License for more details.
-- You should have received a copy of the GNU Affero General Public License
-- along with this program (file LICENSE).
-- If not, see <https://www.gnu.org/licenses/>.

BEGIN;

DROP MATERIALIZED VIEW IF EXISTS feeds_data;

CREATE MATERIALIZED VIEW feeds_data AS
  WITH aa AS (
    SELECT
      articles.id,
      articles.feed_id,
      SUM(COALESCE(CASE WHEN user_articles.read THEN 1 END, 0)) AS views
    FROM
      articles
      LEFT OUTER JOIN user_articles ON articles.id = user_articles.article_id
    GROUP BY articles.id),
  aaa AS (
    SELECT
      feeds.id,
      EXTRACT(EPOCH FROM feeds.last_polled) AS last_polled_epoch,
      COUNT(aa) AS article_count,
      SUM(aa.views) AS views
    FROM feeds
      LEFT OUTER JOIN aa ON feeds.id = aa.feed_id
    GROUP BY feeds.id),
  diffs as (
    SELECT
      feed_id,
      stamp - LAG(stamp, 1) over (PARTITION BY feed_id ORDER BY stamp) as time_from_last_post
    FROM
      articles
    WHERE
      EXTRACT(EPOCH FROM (NOW() - stamp)) < 3600*24*365
  ),
  tflp AS (SELECT
    feed_id,
    EXTRACT(EPOCH FROM (AVG(time_from_last_post)))::INTEGER as average_time_from_last_post
  FROM
    diffs
  GROUP BY feed_id)
  SELECT * FROM aaa JOIN tflp ON aaa.id = tflp.feed_id ORDER BY aaa.id;

GRANT SELECT ON feeds_data to "www-data";

COMMIT;

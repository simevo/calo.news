-- SQL create the users table
--
-- This file is part of calo.news: A news platform
--
-- Copyright (C) 2017-2023 Paolo Greppi
--
-- This program is free software: you can redistribute it and/or modify
-- it under the terms of the GNU Affero General Public License as published by
-- the Free Software Foundation, either version 3 of the License, or
-- (at your option) any later version.
--
-- This program is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
-- GNU Affero General Public License for more details.
-- You should have received a copy of the GNU Affero General Public License
-- along with this program (file LICENSE).
-- If not, see <https://www.gnu.org/licenses/>.

BEGIN;

CREATE TABLE IF NOT EXISTS users (
  id INTEGER PRIMARY KEY NOT NULL,  -- matches the external_id field in the discourse auth token
  newsletter BOOLEAN DEFAULT FALSE,
  list_email TEXT DEFAULT '',
  list_frequency TEXT DEFAULT 'daily',
  list_news INTEGER DEFAULT 10,
  list_format TEXT DEFAULT 'pdf',
  whitelist TEXT DEFAULT '',
  whitelist_authors TEXT DEFAULT '',
  blacklist TEXT DEFAULT '',
  blacklist_authors TEXT DEFAULT '',
  sociality_weight INTEGER DEFAULT 0,
  gravity FLOAT DEFAULT 2.0,
  comment_factor FLOAT DEFAULT 4.0,
  age_divider FLOAT DEFAULT 100.0,
  feed_weight INTEGER DEFAULT 0,
  list_weight INTEGER DEFAULT 0,
  last_access TIMESTAMP WITH TIME ZONE,
  only_new BOOLEAN DEFAULT TRUE,
  bow JSONB  -- bag-of-words counters for all read articles
);

INSERT INTO users (id) SELECT
    -1
    WHERE NOT EXISTS (SELECT id FROM users WHERE id = -1);

CREATE TABLE IF NOT EXISTS user_articles (
  user_id INTEGER REFERENCES users(id),
  article_id INTEGER REFERENCES articles(id),
  read BOOLEAN DEFAULT FALSE,
  rating INTEGER DEFAULT 0,
  PRIMARY KEY(user_id, article_id)
);

CREATE TABLE IF NOT EXISTS user_feeds (
  user_id INTEGER REFERENCES users(id),
  feed_id INTEGER REFERENCES feeds(id),
  rating INTEGER,
  PRIMARY KEY(user_id, feed_id)
);

-- from https://www.depesz.com/2008/06/18/conditional-ddl/
CREATE OR REPLACE FUNCTION execute(text)
RETURNS void AS $$
   BEGIN EXECUTE $1; END;
$$ LANGUAGE plpgsql STRICT;

CREATE OR REPLACE FUNCTION column_exists(text, text, text)
RETURNS bool AS $$
   SELECT exists(SELECT 1 FROM information_schema.columns WHERE (table_schema, table_name, column_name) = ($1, $2, $3));
$$ language sql STRICT;

SELECT execute($$
  INSERT INTO user_articles (user_id, article_id, read, rating)
  SELECT -1, id, views > 0, rating
  FROM articles $$)
WHERE column_exists(current_schema, 'articles', 'views');

ALTER TABLE articles DROP COLUMN IF EXISTS views;

ALTER TABLE articles DROP COLUMN IF EXISTS rating;

ALTER TABLE feeds DROP COLUMN IF EXISTS rating;

CREATE OR REPLACE FUNCTION view_or_table_exists(text, text)
RETURNS bool AS $$
  SELECT
    exists(SELECT 1 from information_schema.views where (table_schema, table_name) = ($1, $2))
    OR exists(SELECT 1 from information_schema.tables where (table_schema, table_name) = ($1, $2));
$$ language sql STRICT;

SELECT execute($$
  CREATE VIEW articles_data AS
    SELECT
      articles.id,
      EXTRACT(EPOCH FROM (NOW() - articles.stamp)) AS age,
      EXTRACT(EPOCH FROM articles.stamp) AS epoch,
      COALESCE(COUNT(CASE WHEN user_articles.read THEN 1 END), 0) AS views,
      COALESCE(AVG(user_articles.rating), 0) AS rating
    FROM
      articles LEFT OUTER JOIN user_articles ON articles.id = user_articles.article_id
    GROUP BY articles.id $$)
WHERE NOT view_or_table_exists(current_schema, 'articles_data');

DROP FUNCTION column_exists(text, text, text);
DROP FUNCTION view_or_table_exists(text, text);
DROP FUNCTION execute(text);

COMMIT;

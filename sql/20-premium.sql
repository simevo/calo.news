-- add premium flag to feeds
--
-- This file is part of calo.news: A news platform
--
-- Copyright (C) 2017-2023 Paolo Greppi
--
-- This program is free software: you can redistribute it and/or modify
-- it under the terms of the GNU Affero General Public License as published by
-- the Free Software Foundation, either version 3 of the License, or
-- (at your option) any later version.
--
-- This program is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
-- GNU Affero General Public License for more details.
-- You should have received a copy of the GNU Affero General Public License
-- along with this program (file LICENSE).
-- If not, see <https://www.gnu.org/licenses/>.

BEGIN;

-- from https://www.depesz.com/2008/06/18/conditional-ddl/
CREATE OR REPLACE FUNCTION execute(text)
RETURNS void AS $$
   BEGIN EXECUTE $1; END;
$$ LANGUAGE plpgsql STRICT;

CREATE OR REPLACE FUNCTION column_exists(text, text, text)
RETURNS bool AS $$
   SELECT exists(SELECT 1 FROM information_schema.columns WHERE (table_schema, table_name, column_name) = ($1, $2, $3));
$$ language sql STRICT;

SELECT execute($$
   ALTER TABLE feeds ADD COLUMN premium BOOLEAN DEFAULT FALSE;
   $$)
   WHERE NOT column_exists(current_schema, 'feeds', 'premium');

INSERT INTO feeds (id, homepage, url, language, title, icon, active, license, premium) SELECT
  90, 'https://www.ilfattoquotidiano.it/premium/', '-', 'it', 'Il Fatto Quotidiano Premium', 'images/FQ.jpg', false, 'Copyright © Il Fatto Quotidiano', TRUE
  WHERE NOT EXISTS (SELECT id FROM feeds WHERE id = 90);

UPDATE feeds SET premium=TRUE WHERE id=84;

DROP FUNCTION column_exists(text, text, text);
DROP FUNCTION execute(text);

COMMIT; 

-- SQL code to create the tables
--
-- This file is part of calo.news: A news platform
--
-- Copyright (C) 2017-2023 Paolo Greppi
--
-- This program is free software: you can redistribute it and/or modify
-- it under the terms of the GNU Affero General Public License as published by
-- the Free Software Foundation, either version 3 of the License, or
-- (at your option) any later version.
--
-- This program is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
-- GNU Affero General Public License for more details.
-- You should have received a copy of the GNU Affero General Public License
-- along with this program (file LICENSE).
-- If not, see <https://www.gnu.org/licenses/>.

BEGIN;

CREATE TABLE IF NOT EXISTS feeds (
  id INTEGER PRIMARY KEY NOT NULL,
  homepage TEXT NOT NULL,
  url TEXT NOT NULL,
  language TEXT NOT NULL,
  title TEXT NOT NULL,
  rating INTEGER DEFAULT 0,
  license TEXT,
  icon TEXT NOT NULL,
  active BOOLEAN DEFAULT FALSE,
  last_polled TIMESTAMP WITH TIME ZONE
);

CREATE TABLE IF NOT EXISTS articles (
  id SERIAL PRIMARY KEY,
  stamp TIMESTAMP WITH TIME ZONE DEFAULT now() NOT NULL,
  time_from_last_post INTEGER, -- time in seconds elapsed from the previous post
  author TEXT,
  title_original TEXT,         -- title in the original language if language is not it
  title TEXT,                  -- title in it language
  content_original TEXT,       -- content in the original language if language is not it
  content TEXT,                -- content in it language
  language TEXT,               -- it, en ...
  url TEXT,
  comments INTEGER DEFAULT 0,
  rating INTEGER DEFAULT 0,
  views INTEGER DEFAULT 0,
  feed_id INTEGER REFERENCES feeds(id),
  tsv TSVECTOR
);

DROP TRIGGER IF EXISTS tsvectorupdate ON articles;

CREATE TRIGGER tsvectorupdate BEFORE INSERT OR UPDATE
ON articles FOR EACH ROW EXECUTE PROCEDURE
tsvector_update_trigger(tsv, 'pg_catalog.italian', title, content);

-- SELECT url, title FROM articles WHERE tsv @@ to_tsquery('giornali');

COMMIT;

-- #39: fulltext search for foreign language articles
--
-- This file is part of calo.news: A news platform
--
-- Copyright (C) 2017-2023 Paolo Greppi
--
-- This program is free software: you can redistribute it and/or modify
-- it under the terms of the GNU Affero General Public License as published by
-- the Free Software Foundation, either version 3 of the License, or
-- (at your option) any later version.
--
-- This program is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
-- GNU Affero General Public License for more details.
-- You should have received a copy of the GNU Affero General Public License
-- along with this program (file LICENSE).
-- If not, see <https://www.gnu.org/licenses/>.

BEGIN;

-- from https://www.depesz.com/2008/06/18/conditional-ddl/
CREATE OR REPLACE FUNCTION execute(text)
RETURNS void AS $$
   BEGIN EXECUTE $1; END;
$$ LANGUAGE plpgsql STRICT;

CREATE OR REPLACE FUNCTION column_exists(text, text, text)
RETURNS bool AS $$
   SELECT exists(SELECT 1 FROM information_schema.columns WHERE (table_schema, table_name, column_name) = ($1, $2, $3));
$$ language sql STRICT;

SELECT execute($$
   ALTER TABLE articles ADD COLUMN tsv_simple TSVECTOR;
   $$)
   WHERE NOT column_exists(current_schema, 'articles', 'tsv_simple');

DROP FUNCTION column_exists(text, text, text);
DROP FUNCTION execute(text);

DROP TRIGGER IF EXISTS tsvectorsimpleupdate ON articles;

CREATE TRIGGER tsvectorsimpleupdate BEFORE INSERT OR UPDATE
ON articles FOR EACH ROW EXECUTE PROCEDURE
tsvector_update_trigger(tsv_simple, 'pg_catalog.simple', title_original, content_original);
-- to force the update on existing rows:
--   UPDATE articles set title_original=title_original;

CREATE INDEX IF NOT EXISTS search_simple_idx ON articles USING GIN(tsv_simple);

COMMIT; 

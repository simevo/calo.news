-- add a couple of feeds
--
-- This file is part of calo.news: A news platform
--
-- Copyright (C) 2017-2023 Paolo Greppi
--
-- This program is free software: you can redistribute it and/or modify
-- it under the terms of the GNU Affero General Public License as published by
-- the Free Software Foundation, either version 3 of the License, or
-- (at your option) any later version.
--
-- This program is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
-- GNU Affero General Public License for more details.
-- You should have received a copy of the GNU Affero General Public License
-- along with this program (file LICENSE).
-- If not, see <https://www.gnu.org/licenses/>.

BEGIN;

INSERT INTO feeds (id, homepage, url, language, title, icon, active) SELECT
  0, 'https://notizie.calomelano.it/', 'https://notizie.calomelano.it/api/feeds/0/rss', 'it', 'AAA - articoli inseriti a mano', 'icons/manual.png', false
  WHERE NOT EXISTS (SELECT id FROM feeds WHERE id = 0);

INSERT INTO feeds (id, homepage, url, language, title, icon, active, license) SELECT
  1, 'http://calomelano.it', 'http://calomelano.it/?feed=rss2', 'it', 'calomelano', 'icons/unknown.png', true, 'Copyright © gli autori, Creative Commons Attribution 3.0 License'
  WHERE NOT EXISTS (SELECT id FROM feeds WHERE id = 1);

INSERT INTO feeds (id, homepage, url, language, title, icon, active) SELECT
  2, 'http://www.paolonori.it', 'http://www.paolonori.it/feed/', 'it', 'Paolo Nori', 'icons/unknown.png', true
  WHERE NOT EXISTS (SELECT id FROM feeds WHERE id = 2);

INSERT INTO feeds (id, homepage, url, language, title, icon, active, license) SELECT
  3, 'https://theintercept.com/', 'https://theintercept.com/feed/?lang=en', 'en', 'The Intercept', 'icons/unknown.png', true, 'Copyright © First Look Media. All rights reserved'
  WHERE NOT EXISTS (SELECT id FROM feeds WHERE id = 9);

INSERT INTO feeds (id, homepage, url, language, title, icon, active, license) SELECT
  4, 'http://www.iltascabile.com/', 'http://www.iltascabile.com/feed/', 'it', 'Il Tascabile', 'icons/unknown.png', true, 'Copyright © TRECCANI'
  WHERE NOT EXISTS (SELECT id FROM feeds WHERE id = 20);

INSERT INTO feeds (id, homepage, url, language, title, icon, active, license) SELECT
  5, 'http://www.valigiablu.it/', 'http://www.valigiablu.it/feed/', 'it', 'Valigia Blu', 'icons/unknown.png', true, 'Copyright © Valigia Blu - i contenuti di questo sito sono utilizzabili sotto licenza cc-by-nc-nd'
  WHERE NOT EXISTS (SELECT id FROM feeds WHERE id = 21);

COMMIT;

-- add a field to mark feeds for which we want to download the fulltext
--
-- This file is part of calo.news: A news platform
--
-- Copyright (C) 2017-2023 Paolo Greppi
--
-- This program is free software: you can redistribute it and/or modify
-- it under the terms of the GNU Affero General Public License as published by
-- the Free Software Foundation, either version 3 of the License, or
-- (at your option) any later version.
--
-- This program is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
-- GNU Affero General Public License for more details.
-- You should have received a copy of the GNU Affero General Public License
-- along with this program (file LICENSE).
-- If not, see <https://www.gnu.org/licenses/>.

BEGIN;

ALTER TABLE feeds
  ADD COLUMN IF NOT EXISTS incomplete BOOLEAN DEFAULT FALSE;

UPDATE feeds set incomplete=TRUE WHERE ID=10;
UPDATE feeds set incomplete=TRUE WHERE ID=18;
UPDATE feeds set incomplete=TRUE WHERE ID=23;
  
COMMIT;

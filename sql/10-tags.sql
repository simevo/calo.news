-- add columns required to store tags
--
-- This file is part of calo.news: A news platform
--
-- Copyright (C) 2017-2023 Paolo Greppi
--
-- This program is free software: you can redistribute it and/or modify
-- it under the terms of the GNU Affero General Public License as published by
-- the Free Software Foundation, either version 3 of the License, or
-- (at your option) any later version.
--
-- This program is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
-- GNU Affero General Public License for more details.
-- You should have received a copy of the GNU Affero General Public License
-- along with this program (file LICENSE).
-- If not, see <https://www.gnu.org/licenses/>.

BEGIN;

-- from https://www.depesz.com/2008/06/18/conditional-ddl/
CREATE OR REPLACE FUNCTION execute(text)
RETURNS void AS $$
   BEGIN EXECUTE $1; END;
$$ LANGUAGE plpgsql STRICT;

CREATE OR REPLACE FUNCTION column_exists(text, text, text)
RETURNS bool AS $$
   SELECT exists(SELECT 1 FROM information_schema.columns WHERE (table_schema, table_name, column_name) = ($1, $2, $3));
$$ language sql STRICT;

SELECT execute($$
   ALTER TABLE feeds ADD COLUMN tags TEXT[] default ARRAY[]::TEXT[];
   $$)
   WHERE NOT column_exists(current_schema, 'feeds', 'tags');

SELECT execute($$
   ALTER TABLE users ADD COLUMN tags TEXT[];
   $$)
   WHERE NOT column_exists(current_schema, 'users', 'tags');

SELECT execute($$
   ALTER TABLE users DROP COLUMN fr;
   ALTER TABLE users DROP COLUMN en;
   ALTER TABLE users DROP COLUMN nl;
   ALTER TABLE users DROP COLUMN pt;
   ALTER TABLE users DROP COLUMN es;
   ALTER TABLE users DROP COLUMN de;
   $$)
   WHERE column_exists(current_schema, 'users', 'fr');

SELECT execute($$
   ALTER TABLE users DROP COLUMN only_new;
   $$)
   WHERE column_exists(current_schema, 'users', 'only_new');

SELECT execute($$
   ALTER TABLE users ADD COLUMN languages TEXT[];
   $$)
   WHERE NOT column_exists(current_schema, 'users', 'languages');

SELECT execute($$
   ALTER TABLE users ADD COLUMN filter1 TEXT;
   $$)
   WHERE NOT column_exists(current_schema, 'users', 'filter1');

DROP FUNCTION column_exists(text, text, text);
DROP FUNCTION execute(text);

COMMIT; 

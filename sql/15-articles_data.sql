-- SQL create the articles_data pseudo-materialized view as a table
--
-- This file is part of calo.news: A news platform
--
-- Copyright (C) 2017-2023 Paolo Greppi
--
-- This program is free software: you can redistribute it and/or modify
-- it under the terms of the GNU Affero General Public License as published by
-- the Free Software Foundation, either version 3 of the License, or
-- (at your option) any later version.
--
-- This program is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
-- GNU Affero General Public License for more details.
-- You should have received a copy of the GNU Affero General Public License
-- along with this program (file LICENSE).
-- If not, see <https://www.gnu.org/licenses/>.

BEGIN;

DROP VIEW IF EXISTS articles_data_view;

CREATE VIEW articles_data_view AS
  SELECT
    articles.id,
    articles.feed_id,
    EXTRACT(EPOCH FROM articles.stamp) AS epoch,
    COALESCE(COUNT(CASE WHEN user_articles.read THEN 1 END), 0) AS views,
    COALESCE(AVG(user_articles.rating), 0) AS rating,
    COALESCE(COUNT(CASE WHEN user_articles.to_read THEN 1 END), 0) AS to_reads,
    COALESCE(LENGTH(articles.content), LENGTH(articles.content_original)) as length
  FROM
    articles LEFT OUTER JOIN user_articles ON articles.id = user_articles.article_id
  GROUP BY articles.id;

-- from: https://stackoverflow.com/a/463314
CREATE OR REPLACE FUNCTION delete_table_or_view(objectName TEXT) RETURNS INTEGER AS $$
DECLARE
  isTable INTEGER;
  isView INTEGER;
BEGIN
  SELECT INTO isTable COUNT(*) FROM pg_tables WHERE tablename=objectName;
  SELECT INTO isView COUNT(*) FROM pg_views WHERE viewname=objectName;
  IF isTable = 1 THEN
    execute 'DROP TABLE ' || objectName;
    RETURN 1;
  END IF;
  IF isView = 1 THEN
    execute 'DROP VIEW ' || objectName;
    RETURN 2;
  END IF;
  RETURN 0;
END;
$$ LANGUAGE plpgsql;

SELECT delete_table_or_view('articles_data');
DROP FUNCTION delete_table_or_view(TEXT);

CREATE TABLE IF NOT EXISTS articles_data AS
SELECT * FROM articles_data_view;

ALTER TABLE articles_data ADD CONSTRAINT unique_id UNIQUE (id);

COMMIT;

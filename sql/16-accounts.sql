-- add column required to store the virtual account balance
--
-- This file is part of calo.news: A news platform
--
-- Copyright (C) 2017-2023 Paolo Greppi
--
-- This program is free software: you can redistribute it and/or modify
-- it under the terms of the GNU Affero General Public License as published by
-- the Free Software Foundation, either version 3 of the License, or
-- (at your option) any later version.
--
-- This program is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
-- GNU Affero General Public License for more details.
-- You should have received a copy of the GNU Affero General Public License
-- along with this program (file LICENSE).
-- If not, see <https://www.gnu.org/licenses/>.

BEGIN;

ALTER TABLE users DROP COLUMN IF EXISTS balance;

CREATE TABLE IF NOT EXISTS user_transactions (
  user_id INTEGER REFERENCES users(id),
  date DATE DEFAULT now(),
  amount DOUBLE PRECISION,
  PRIMARY KEY(user_id, date)
);

GRANT SELECT ON user_transactions to "www-data";

COMMIT; 

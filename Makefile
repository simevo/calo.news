all:
	cp node_modules/vue/dist/vue.js www/js/.
	cp node_modules/vue/dist/vue.min.js www/js/.
	cp node_modules/vue/dist/vue.runtime.js www/js/.
	cp node_modules/vue/dist/vue.runtime.min.js www/js/.
	cp node_modules/jquery/dist/jquery.slim.min.js www/js/.
	cp node_modules/jquery/dist/jquery.slim.min.map www/js/.
	cp node_modules/bootstrap/dist/js/bootstrap.min.js www/js/.
	cp node_modules/bootstrap/dist/js/bootstrap.min.js.map www/js/.
	cp node_modules/popper.js/dist/umd/popper.min.js www/js/.
	cp node_modules/popper.js/dist/umd/popper.min.js.map www/js/.

	cp node_modules/bootstrap/dist/css/bootstrap.min.css www/css/.
	cp node_modules/bootstrap/dist/css/bootstrap.min.css.map www/css/.

	cp node_modules/trix/dist/trix.js www/js/.
	cp node_modules/trix/dist/trix.css www/css/.

	cp node_modules/vee-validate/dist/vee-validate.min.js www/js/.
	cp "node_modules/vee-validate/dist/locale/${BASE_LANGUAGE}.js" www/js/locale/.

	cp node_modules/vue-select/dist/vue-select.js www/js/.
	cp node_modules/vue-select/dist/vue-select.js.map www/js/.

	cp node_modules/vue-router/dist/vue-router.min.js www/js/.

	cp node_modules/es6-promise/dist/es6-promise.auto.min.js www/js/.
	cp node_modules/es6-promise/dist/es6-promise.auto.min.map www/js/.

	cp ./node_modules/shallow-equals/index.js www/js/shallow-equals.js

	cp ./node_modules/slipjs/slip.js www/js/.

	webpack --entry franc-min --output-filename www/js/franc-min.js --output-library franc

clean:
	rm www/js/vue.js
	rm www/js/vue.min.js 
	rm www/js/vue.runtime.js
	rm www/js/vue.runtime.min.js
	rm www/js/jquery.slim.min.js
	rm www/js/jquery.slim.min.map
	rm www/js/bootstrap.min.js 
	rm www/js/bootstrap.min.js.map
	rm www/js/popper.min.js
	rm www/js/popper.min.js.map

	rm www/css/bootstrap.min.css 
	rm www/css/bootstrap.min.css.map 

	rm www/js/trix.js
	rm www/css/trix.css

	rm www/js/vee-validate.min.js 
	rm "www/js/locale/${BASE_LANGUAGE}.js"

	rm www/js/vue-select.js
	rm www/js/vue-select.js.map

	rm www/js/vue-router.min.js 

	rm www/js/es6-promise.auto.min.js 
	rm www/js/es6-promise.auto.min.map

	rm www/js/shallow-equals.js

	rm www/js/slip.js 

	rm www/js/franc-min.js


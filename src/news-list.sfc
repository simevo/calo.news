<template>
  <div class="container-fluid" v-infinite-scroll="loadMore" infinite-scroll-disabled="busy" infinite-scroll-distance="100">
    <template v-if="feed == -1 && query == '' && author == ''">
      <div class="row justify-content-md-center" style="padding-bottom: 0.5em;">
        <div class="col-md-4">
          <select v-model="filter1" name='filter1' style='font-family: Arial, FontAwesome;'>
            <option value='mixed' style='color:black'>! &nbsp; Mix</option>
            <option value='filtered' style='color:green;'>&#xf0b0; &nbsp; Filtrati</option>
            <option value='latest' style='color:red;'>&#xf0a1; &nbsp; Ultima ora</option>
            <option value='suggestions' style='color:black;'>&#xf0eb; &nbsp; Suggeriti</option>
            <option value='read' style='color:grey;'>&#xf1da; &nbsp; Cronologia</option>
            <option value='to_read' style='color:blue'>&#xf07a; &nbsp; Interessanti</option>
            <option value='keywords' style='color:orange'>&#x25ce; &nbsp; Preferiti</option>
            <option value='popular' style='color:magenta'>&#xf06d; &nbsp; Popolari</option>
            <option value='premium' style='color:cyan'>&#xf155; &nbsp; Premium</option>
            <option value='foreign' style='color:black'>拼 &nbsp; In lingua</option>
            <option value='dismissed' style='color:black'>&#xf1f8; &nbsp; Nascosti</option>
          </select>
          <a href="#" v-on:click="reset(); loadMore();" v-bind:title="'il newsfeed è vecchio di ' + Math.round(cache_age/60.0) + ' minuti: ricaricalo!'">
            <svg width="1em" height="1em" v-bind:style="{ fill: rgb(cache_age) }">
              <use xlink:href="#sync-alt"></use>
            </svg>
          </a>
        </div> <!-- col-md-4 -->
        <div class="col-md-8">
          <span class="text-muted" style="display: none;" v-show="description_filter1">{{ description_filter1 }}</span>
        </div> <!-- col-md-8 -->
      </div> <!-- row -->
      <div class="row" style="display: none; padding-bottom: 10px;" v-show="news.length > 0 && filter1 == 'to_read'">
        <div class="col-md-4">
          <a href="#" v-on:click="unmark_all();">
            <svg width="1em" height="1em">
              <use xlink:href="#square"></use>
            </svg>
            <small> Svuota la lista degli articoli interessanti</small>
          </a>
        </div>
        <div class="col-md-4">
          <download-send-share v-bind:ids="to_read_ids" v-bind:clean="true" v-bind:share="false"></download-send-share>
          <button type="button" id="button-speak" class="btn btn-success btn-sm" title="Leggi tutti gli articoli interessanti ad alta voce" v-show="tts && !tts_open && (to_read_news.length > 0)" v-on:click="tts_speak();">
            <svg width="1em" height="1em" style="fill: white">
              <use xlink:href="#play"></use>
            </svg>
          </button>

          <div v-show="tts && tts_open">
            <div>
              Lettura sequenziale
            </div>
            <div>
              <p>{{ tts_message }}</p>
            </div>
            <div class="modal-footer">
              <button class="modal-default-button" v-on:click="tts_stop();">
                Ferma lettura
              </button>
            </div>
          </div>

        </div>
        <div class="col-md-4" v-show="to_read_news.length < news.length">
          <a href="#" v-on:click="mark_all();">
            <svg width="1em" height="1em">
              <use xlink:href="#check-square"></use>
            </svg>
            <small> Segna tutti come interessanti</small></a>
        </div>
      </div>
    </template>
    <template v-if="feed != -1 && selected_feed.title !== ''">
      <h2>Risultati della ricerca per fonte: "{{ selected_feed.title }}"</h2>
      <feed-item v-bind:feed="selected_feed" v-bind:freeze="freeze" v-bind:readonly="false"></feed-item>
    </template>
    <template v-if="author != ''">
      <h2>Risultati della ricerca per autore: "{{ author }}"</h2>
      <div class="pb-3"><a v-bind:href="'/search/' + author">cerca invece "{{ author }}" a tutto testo</a></div>
    </template>
    <template v-if="query != ''">
      <h2>Risultati della ricerca a tutto testo di "{{ query }}"</h2>
      <div class="pb-3"><a v-bind:href="'/author/' + query">cerca invece "{{ query }}" per autore</a></div>
    </template>
    <template v-if="((feed != -1 && selected_feed.title !== '') || (author != '') || (query != '')) && (news.length > 0)">
      <form style="background-color: lightgrey; padding: 5px background-color: lightgrey;padding: 5px;margin-bottom: 1em;">
        <div class="row h2">
          <div class="col">
            <a href="#" v-on:click="reset(); loadMore();" title="ricarica">
              <svg width="1em" height="1em">
                <use xlink:href="#sync-alt"></use>
              </svg>
            </a>
            <a href="#" v-on:click="loadMore();" title="carica altri">
              <div class="spinner-static text-primary" role="status">
                <span class="sr-only">Loading...</span>
              </div>
            </a>
          </div>
        </div>
        <div class="form-check">
          <label class="form-check-label col-md-12">
            <input class="form-check-input" type="checkbox" v-model="only_read">
            mostra solo articoli già letti da te
          </label>
        </div>
        <div class="form-check" v-show="Object.keys(feeds).length > 1">
          <h5>Filtra per fonte:</h5>
          <label class="form-check-label h2 p-1" title="mostra tutti gli articoli" v-bind:class="{ 'dimmed': pick_feed != -1 }">
            <input class="form-check-input" type="radio" v-model="pick_feed" value="-1" hidden>
            <svg width="1em" height="1em">
              <use xlink:href="#question-circle"></use>
            </svg>
          </label>
          <label class="form-check-label p-1" v-for="(feed, feed_id) in feeds" v-bind:title="'mostra solo gli articoli provenienti dalla fonte ' + feed.name" v-bind:class="{ 'dimmed': pick_feed != feed_id }">
            <input class="form-check-input" type="radio" v-model="pick_feed" :value="feed_id" hidden>
            <img v-if='feed.premium' v-bind:style="'background:url(/' + feed.icon + ');background-size:contain;'" src="images/wreath.png" width="30px" height="30px" alt="premium">
            <img v-else width="30px" height="30px" v-bind:src="'/' + feed.icon" alt="feed logo">
          </label>
        </div>
      </form>
    </template>
    <div style="display:none;" v-show="retrieved >= 0 && !busy && news.length == 0">
      <span>Mi spiace, non trovo niente ...</span>
      <div class="alert alert-danger" role="alert" v-show="filter1 === 'keywords' && feed === -1 && author === '' && query === ''">
        Affinché gli articoli appaiano qui devi inserire delle parole chiave o degli autori preferiti <a href="/settings.php#lists">nella linguetta <strong>Vista "preferiti"</strong> delle impostazioni</a> !
      </div>
    </div>
    <ul id="slippylist">
      <news-item v-for="(n, i) in news_filtered" v-bind:news="n" v-bind:index="i" :key="n.id"></news-item>
    </ul>
    <div class="mx-auto" style="width: 50px;" v-show="busy">
      <div class="spinner-border text-primary" role="status">
        <span class="sr-only">Loading...</span>
      </div>
    </div>
  </div>
</template>
// configuration for jshint
/* jshint browser: true, devel: true */
/* global Vue */

// global var to store the last received user filter1
var old_filter1 = '';

var NewsList =  {
  render: function(_c) {
    ### 
  },
  staticRenderFns: $$$,
  data: function () {
    return {
      dirty: false,
      active: false,
      filter1: 'filtered',
      news: [],
      retrieved: -1,
      busy: false,
      feed: -1,
      query: '',
      author: '',
      cache_age: 0,
      freeze: true,
      pick_feed: -1,
      only_read: false,
      selected_feed: {
        incomplete: true,
        active: true,
        language: "it",
        tags: ['blog'],
        title: "",
        id: 4,
        url: "",
        average_time_from_last_post: 5897,
        last_polled: "2017-11-25T13:37:08.908792+00:00",
        rating: null,
        epoch: 1511617028,
        license: "",
        my_rating: 1,
        salt_url: false,
        views: 1431,
        count: 4254,
        icon: "images/internazionale-icon.png",
        homepage: ""
      },
      tts: true,
      tts_open: false,
      tts_message: ""
    };
  },
  watch: {
    '$route': function(to, fro) {
      console.log('$route go from ' + fro.path + ' to ' + to.path);
      let slug = to.path.split('/')[1];
      let slugs = ['view', 'feed', 'author', 'search'];
      if (slugs.find(s => s === slug)) {
        // react to route changes...
        var old_status = this.status;
        console.log('NewsList go from ' + JSON.stringify(fro.params) + ' to ' + JSON.stringify(to.params) + ' status = ' +  JSON.stringify(old_status));
        this.query = '';
        this.author = '';
        this.feed = -1;
        if (('feed' in to.params) && (to.params.feed != -1))
          this.feed = to.params.feed;
        if (('author' in to.params) && (to.params.author !== ''))
          this.author = to.params.author;
        if (('query' in to.params) && (to.params.query != ''))
          this.query = to.params.query;
        var new_status = this.status;
        var is_same = shallow(old_status, new_status);
        console.log('NewsList new_status = ' +  JSON.stringify(new_status) + ' is_same = ' + (is_same ? 'true' : 'false'));
        if (!is_same) {
          if (this.active) {
            this.reset();
            this.loadMore();
          } else {
            console.log('set dirty');
            this.dirty = true;
          }
        }
      }
    }
  },
  computed: {
    feeds: function() {
      var fs = {};
      this.news.forEach(function (a) {
        if (!(a.feed_id in fs)) {
          fs[a.feed_id] = {'icon': a.icon, 'name': a.feed, 'premium': a.premium};
        }
      });
      return fs;
    },
    news_filtered: function() {
      var self = this;
      var news0 = self.news.filter(function(n) {
        return (self.filter1 == 'dismissed' || !n.dismissed);
      });
      var news1;
      if (self.only_read) {
        news1 = news0.filter(function(n) { return n.read; });
      } else {
        news1 = news0;
      }
      if (self.pick_feed != -1) {
        return news1.filter(function(n) { return n.feed_id == self.pick_feed; });
      } else {
        return news1;
      }
    },
    description_filter1: function() {
      var self = this;
      window.focus();
      if (document.activeElement) {
        document.activeElement.blur();
      }
      if (self.filter1) {
        var new_filter1 = self.filter1;
        console.log('old filter1 = ' + old_filter1 + ' new filter1 = ' + new_filter1);
        var is_same = (new_filter1 == old_filter1);
        if (old_filter1 && !is_same) {
          localStorage.setItem("filter", self.filter1);
          old_filter1 = new_filter1;
          self.empty_list();
          self.loadMore();
        }
        return description(self.filter1);
      } else {
        return '';
      }
    },
    to_read_news: function() {
      return this.news.filter(function(n) { return n.to_read; });
    },
    to_read_ids: function() {
      var ids = [];
      this.to_read_news.forEach(function (a) { ids.push(a.id); });
      return ids.join(',');
    },
    offset: function() {
      return this.news.length;
    },
    status: function() {
      return {login_name: login_name, query: this.query, filter: this.filter1, author: this.author, feed: this.feed, component: 'list'};
    }
  },
  methods: {
    loadFeed: function() {
      var self = this;
      sendReceiveData(null, 'GET', api + '/feeds/' + self.feed, function (dataFrom) {
        self.selected_feed = dataFrom.feeds[0];
      });
    },
    empty_list: function() {
      console.log('empty list');
      this.retrieved = -1;
      this.news = [];
    },
    loadMore: function() {
      if (!this.active)
        return;
      var self = this;
      // console.log('busy = ', self.busy);
      // console.log('retrieved = ', self.retrieved);
      console.log('news.length = ', self.news.length);
      if (self.busy || (self.retrieved === 0))
        return;
      self.busy = true;
      sendReceiveData(null, 'GET', self.endpoint(localStorage.getItem("filter") || 'filtered'), function (dataFrom) {
        var new_articles = dataFrom.articles;
        console.log('received articles: ' + new_articles.length);
        self.news = self.news.concat(new_articles);
        self.save_cache(self.retrieved == -1);
        self.retrieved = new_articles.length;
        self.busy = false;
      });
    },
    email: function(format) {
      var xhr = new XMLHttpRequest();
      var url = '/email.php?format=' + format + '&ids=' + this.to_read_ids;
      xhr.open('GET', url);
      xhr.onload = function () {
        $('#myModal').modal({keyboard: false});
      };
      xhr.send();
    },
    reset: function() {
      console.log('NewsList reset');
      this.retrieved = -1;
      this.cache_age = 0;
      this.news = [];
      this.save_cache(true);
      this.freeze = true;
      this.selected_feed.title = '';
    },
    save_cache: function(fresh) {
      var self = this;
      console.log('entered save_cache');
      if (fresh) {
        storage.set('news_cached', Date.now());
      }
      storage.set('news_status', self.status);
      storage.set('news', self.news);
    },
    endpoint: function(filt) {
      if (Object.keys(login).length === 0) {
        // user is not logged in
        return api + '/articles?offset=' + this.offset;
      } else if (login.login.groups.indexOf("soci") == -1) {
        // user is not active
        return api + '/articles?offset=' + this.offset;
      } else if (this.query !== '') {
        // full-text search query
        return api + '/articles?offset=' + this.offset + '&query=' + encodeURIComponent(this.query);
      } else if (this.author !== '') {
        // author query
        return api + '/articles?offset=' + this.offset + '&author=' + encodeURIComponent(this.author);
      } else if (this.feed != -1) {
        // feed query
        this.loadFeed();
        return api + '/articles?offset=' + this.offset + '&feed=' + this.feed;
      } else {
        return api + '/articles.smart?offset=' + this.offset + '&filter=' + filt;
      }
    },
    unmark_all: function() {
      var self = this;
      sendReceiveData(null, 'DELETE', api + '/articles/to_read', function () {
        self.to_read_news.forEach(function (a) { a.to_read = false; });
        self.save_cache(false);
        self.empty_list();
        self.loadMore();
      });
      return false;
    },
    mark_all: function() {
      this.news.filter(function(n) {
        if (!n.to_read) {
          var dataTo = { to_read: true };
          sendReceiveData(dataTo, 'POST', api + '/articles/' + n.id, function () {
            n.to_read = true;
          });
        }
      });
      this.save_cache(false);
      return false;
    },
    restore_from_cache: function() {
      var self = this;
      var news_cached = storage.get('news_cached');
      var news_status = storage.get('news_status');
      var is_same = shallow(news_status, self.status);
      self.cache_age = (Date.now() - news_cached) / 1000.0; // seconds
      setTimeout(self.update_cache_age, 60*1000.0);
      console.log('news_cached = ' + news_cached);
      console.log('age = ' + self.cache_age);
      if (is_same && (self.cache_age < 3600.0)) {
        var val = storage.get('news');
        val.forEach(function(n) { n.age = n.age + self.cache_age; });
        self.news = val;
        console.log('restored ' + val.length + ' news from cache with age = ' + self.cache_age);
        if (val.length == 0) {
          console.log('force cache outdate');
          self.cache_age = 0;
          storage.clear();
        } else {
          self.retrieved = val.length;
        }
        self.busy = false;
      } else {
        console.log('cache outdated');
        self.cache_age = 0;
        storage.clear();
        self.busy = false;
      }
    },
    update_news: function(id, read, to_read, rating, topic_id, dismissed) {
      console.log('updating news ', id);
      var self = this;
      var index = -1;
      var found = self.news.some(function(n, i, a) {
        if (n.id == id) {
          index = i;
          if (read !== undefined) { n.read = read; n.views =  n.views + 1; }
          if (to_read !== undefined) { n.to_read = to_read; }
          if (dismissed !== undefined) { n.dismissed = dismissed; }
          if (rating !== undefined) { n.my_rating = rating; }
          if (topic_id !== undefined) { n.comments = 1; n.topic_id = topic_id; }
          console.log('updated news item: ', n);
          return true;
        } else {
          return false;
        }
      });
      if (found) {
        console.log('updating news in local storage');
        storage.set('news', self.news);
      }
    },
    rgb: function(age) {
      // returns a CSS rgb color that goes from white when the age is 0
      // to black when the age is equal to or greater than 3600 seconds
      var graylevel = 255 - Math.min(255, Math.round(256.0 * Math.max(age, 0.0) / 3600.0));
      var decColor = (1 + 256 * (1 + 256)) * graylevel;
      return '#' + decColor.toString(16);
    },
    update_cache_age: function() {
      var news_cached = storage.get('news_cached');
      this.cache_age = (Date.now() - news_cached) / 1000.0; // seconds
      setTimeout(this.update_cache_age, 60*1000.0);
    },
    tts_speak: function() {
      var self = this;
      console.log('speak button pressed');
      if ('speechSynthesis' in window) {
        console.log('TTS API available');
          sendReceiveData(null, 'GET', api + '/articles.json?ids=' + this.to_read_ids, function (dataFrom) {
            var articles = dataFrom.articles;
            console.log('received articles: ' + articles.length);
            self.tts_open = true;
            self.tts_read(articles, 0);
          });
      } else {
        console.log('no TTS API !');
        this.tts = false;
      }

    },
    tts_read: function(articles, n) {
      var self = this;
      if (this.tts_open && n < articles.length) {
        console.log('speak ' + n);
        var article = articles[n];
        var text = "";
        var title = "";
        if (article.language == window.base_language) {
          text = article.title + '.' + stripHtml(article.content);
          title = article.title;
        } else {
          text = article.title_original + '.' + stripHtml(article.content_original);
          title = article.title_original;
        }
        this.tts_message = 'leggo "' + title + '" da ' + article.feed + ' del ' + article.stamp;
        console.log('looking for voices with lang = ' + article.language);
        find_voice(voices, article.language);
        if (voice) {
          console.log('voice = ', voice);
          var utterance = new window.SpeechSynthesisUtterance();
          utterance.voice = voice;
          utterance.lang = voice.lang;
          utterance.text = text;
          console.log(text);
          utterance.addEventListener('end', function(e) {
            console.log('Speaker finished in ' + e.elapsedTime + ' seconds.');
            self.tts_read(articles, n + 1);
          });
          window.speechSynthesis.speak(utterance);
        } // voice found
      } else {
        this.tts_open = false;
      }
    },
    tts_stop: function() {
      window.speechSynthesis.cancel();
      this.tts_open = false;
    }
  },
  created: function() {
    var self = this;
    console.log('NewsList created with parameters: ' + JSON.stringify(this.$route.params));
    this.busy = true;
    if (('query' in this.$route.params) && (this.$route.params.query != '')) {
      this.query = this.$route.params.query;
      document.getElementById("query").value = this.query;
    }
    if (('feed' in this.$route.params) && (this.$route.params.feed != -1)) {
      this.feed = this.$route.params.feed;
      this.loadFeed();
    }
    if (('author' in this.$route.params) && (this.$route.params.author !== '')) {
      this.author = this.$route.params.author;
    }
    this.filter1 = old_filter1 = (localStorage.getItem("filter") || 'filtered');
    this.restore_from_cache();
    bus.$on('read', function(id) {
      console.log('read article: ', id);
      self.update_news(id, true, undefined, undefined, undefined, undefined);
    });
    bus.$on('marked', function(id, to_read) {
      console.log('marked article: ', id, ' to read: ', to_read);
      self.update_news(id, undefined, to_read, undefined, undefined, undefined);
    });
    bus.$on('dismissed', function(id, dismissed) {
      console.log('marked article: ', id, ' dismissed: ', dismissed);
      self.update_news(id, undefined, undefined, undefined, undefined, dismissed);
    });
    bus.$on('rated', function(id, rating) {
      console.log('rated article: ', id, ' = ', rating);
      self.update_news(id, undefined, undefined, rating, undefined, undefined);
    });
    bus.$on('commented', function(id, topic_id) {
      console.log('commented article: ', id);
      self.update_news(id, undefined, undefined, undefined, topic_id, undefined);
    });
    bus.$on('unmark', function() {
      self.unmark_all();
    });
  },
  updated: function() {
    console.log('unfreeze');
    this.freeze = false;
    this.dirty = false;
  },
  activated: function() {
    console.log('NewsList activated');
    this.active = true;
    if (this.dirty) {
      this.reset();
      this.loadMore();
    }
  },
  deactivated: function() {
    console.log('NewsList deactivated');
    this.active = false;
  },
  mounted: function() {
    var self = this;
    var list = document.getElementById('slippylist');
    list.addEventListener('slip:swipe', function(e) {
      console.log('delete ', id);
      e.target.style.display = 'none';
      var id = e.target.id.substring(5);
      // update the cached value
      self.update_news(id, undefined, undefined, undefined, undefined, true);
      // update the value server-side
      var dataTo = { dismissed: true };
      sendReceiveData(dataTo, 'POST', api + '/articles/' + id, function () { });
    });
    new Slip(list, {minimumSwipeVelocity: 0.5});
  }
};

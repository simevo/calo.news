#!/usr/bin/env python3
# coding=utf-8
#
# This file is part of calo.news: A news platform
#
# Copyright (C) 2017-2023 Paolo Greppi
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
# You should have received a copy of the GNU Affero General Public License
# along with this program (file LICENSE).
# If not, see <https://www.gnu.org/licenses/>.

import os
import re
import syslog
from xml.etree import ElementTree

import bottle
import common
import database as db
import requests

base_language = os.environ["BASE_LANGUAGE"]


def secret(path):
    if secret.cached.get(path, "") == "":
        try:
            with open(path, "r") as secret_file:
                encoded_secret = secret_file.read()
        except EnvironmentError:
            raise bottle.HTTPError(status=520, body="internal error")
        secret.cached[path] = encoded_secret
    return secret.cached[path]


secret.cached = {}


def get_token():
    # Get the access token from ADM, token is good for 10 minutes
    key = secret("secrets/key")
    url = (
        "https://api.cognitive.microsoft.com/sts/v1.0/issueToken?Subscription-Key=%s"
        % key
    )
    # print(url)
    try:
        access_token = requests.post(url, data="")
        if access_token.status_code == 200:
            # print(access_token.text)
            token = "Bearer " + access_token.text
        else:
            common.log_error(
                "error while getting token ! status code = %s, status message = %s"
                % (access_token.status_code, access_token.text)
            )
            token = ""
    except OSError:
        common.log_error("exception while getting token")
        token = ""
    return token


def translate(token, fromLangCode, title, content):
    # sample usage:
    # (title, content) = translate(token, 'en', 'the apple is ripe', 'most apples are brightly colored when they are ripe')
    # print(title)
    # print(content)
    title_translated = ""
    content_translated = ""
    split = split_content(content, 8000)
    for s in split:
        xml = """<?xml version="1.0"?>
            <TranslateArrayRequest>
                <AppId/>
                <From>%s</From>
                <Options>
                    <Category xmlns="http://schemas.datacontract.org/2004/07/Microsoft.MT.Web.Service.V2" />
                    <ContentType xmlns="http://schemas.datacontract.org/2004/07/Microsoft.MT.Web.Service.V2">text/html</ContentType>
                    <ReservedFlags xmlns="http://schemas.datacontract.org/2004/07/Microsoft.MT.Web.Service.V2" />
                    <State xmlns="http://schemas.datacontract.org/2004/07/Microsoft.MT.Web.Service.V2" />
                    <Uri xmlns="http://schemas.datacontract.org/2004/07/Microsoft.MT.Web.Service.V2" />
                    <User xmlns="http://schemas.datacontract.org/2004/07/Microsoft.MT.Web.Service.V2" />
                </Options>
                <Texts>
                    <string xmlns="http://schemas.microsoft.com/2003/10/Serialization/Arrays"><![CDATA[%s]]></string>
                    <string xmlns="http://schemas.microsoft.com/2003/10/Serialization/Arrays"><![CDATA[%s]]></string>
                </Texts>
                <To>%s</To>
            </TranslateArrayRequest>""" % (
            fromLangCode,
            title,
            s,
            base_language,
        )
        # print(xml)
        headers = {
            "Authorization ": token,
            "Content-Type": "application/xml; charset=utf-8",
        }
        # http://docs.microsofttranslator.com/text-translate.html#!/default/post_TranslateArray
        url = "http://api.microsofttranslator.com/v2/Http.svc/TranslateArray"
        try:
            translationData = requests.post(
                url, headers=headers, data=xml.encode("utf-8")
            )  # make request
            if translationData.status_code == 200:
                # print(translationData.text)
                translation = ElementTree.fromstring(
                    translationData.text.encode("utf-8")
                )  # parse xml return values
                title_translated = translation[0][3].text
                if len(translation) > 1:
                    if translation[1][3].text:
                        content_translated += translation[1][3].text
            else:
                common.log_error(
                    "error ! status code = %s, status message = %s"
                    % (translationData.status_code, translationData.text)
                )
                return (None, None)
        except OSError:
            return (None, None)
    return (title_translated, content_translated)


def get_languages(token):
    # https://docs.microsofttranslator.com/text-translate.html#!/default/get_GetLanguagesForTranslate
    # af,ar,bg,bs-Latn,ca,cs,cy,da,de,el,en,es,et,fa,fi,fil,fj,fr,he,hi,hr,ht,hu,id,it,ja,ko,lt,lv, mg,ms,mt,mww,nl
    # no,otq,pl,pt,ro,ru,sk,sl,sm,sr-Cyrl,srLatn,sv,sw,th,tlh,tlh-Qaak,to,tr,ty,uk,ur,vi,yua,yue,zh-CHS,zh-CHT
    headers = {
        "Authorization ": token,
        "Content-Type": "application/xml; charset=utf-8",
    }
    # http://docs.microsofttranslator.com/text-translate.html#!/default/post_TranslateArray
    url = "http://api.microsofttranslator.com/v2/Http.svc/GetLanguagesForTranslate "
    try:
        translationData = requests.get(url, headers=headers)  # make request
        print(translationData.text)
    except OSError:
        pass


def split_content_naive(content, max_length):
    # splits the content in an array of strings
    # with length less than or equal to max_length
    split = [content[i : i + max_length] for i in range(0, len(content), max_length)]
    return split


def split_content(content, max_length):
    # splits the content in an array of strings
    # with length approximately less than or equal to max_length
    # taking care not to break up the tags
    open_tag = re.compile(r"<")
    close_tag = re.compile(r">")
    split = []
    while True:
        s = content[:max_length]
        content = content[max_length:]
        o = len(open_tag.findall(s))
        c = len(close_tag.findall(s))
        # print '%d %d' % (o, c)
        if o > c:
            pos = content.find(">")
            if pos >= 0:
                s = s + content[: pos + 1]
                content = content[pos + 1 :]
        if o < c:
            pos = s.rfind(">")
            if pos >= 0:
                s = s[: pos - 1]
                content = s[pos - 1 :] + content
        split.append(s)
        if len(content) == 0:
            break
    return split


if __name__ == "__main__":
    syslog.openlog("translate_py", 0, syslog.LOG_LOCAL0)
    common.log_info("entering")
    with db.connect() as conn, conn.cursor() as cur:
        cur.execute(
            """
            SELECT
              id,
              language,
              content_original,
              title_original,
              SUM(COALESCE(CASE WHEN user_articles.read THEN 1 END, 0)) AS views,
              SUM(COALESCE(CASE WHEN user_articles.rating > 0 THEN 1 END, 0)) AS positive_ratings,
              comments
            FROM
              articles JOIN user_articles ON articles.id = user_articles.article_id
            WHERE
              (language <> %(base_language)s) AND
              (content IS NULL OR TRIM(content) = '') AND
              user_articles.read
            GROUP BY id""",
            {"base_language": base_language},
        )
        articles = cur.fetchall()
        if len(articles) > 0:
            token = get_token()
            if token != "":
                common.log_info(
                    "pondering the translation of %d articles" % len(articles)
                )
                for a in articles:
                    if a[4] + a[5] + a[6] > 1:
                        common.log_info("translating article %d" % a[0])
                        language_original = a[1]
                        content_original = a[2]
                        title_original = a[3]
                        (title, content) = translate(
                            token, language_original, title_original, content_original
                        )
                        data = {"title": title, "content": content, "id": a[0]}
                        cur.execute(
                            "UPDATE articles SET content=%(content)s, title=%(title)s WHERE id = %(id)s",
                            data,
                        )
                    # else:
                    #     print('%s - skipping article %d' % (time.strftime(time_format, time.gmtime()), a[0]))

    common.log_info("done")
    syslog.closelog()

#!/usr/bin/env python3
# coding=utf-8
# calonews RESTful API server using cherrypy
#
# This file is part of calo.news: A news platform
#
# Copyright (C) 2017-2023 Paolo Greppi
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
# You should have received a copy of the GNU Affero General Public License
# along with this program (file LICENSE).
# If not, see <https://www.gnu.org/licenses/>.

import argparse
import datetime
import hashlib
import hmac
import io
import json
import os
import syslog

import account
import borda
import bottle
import common
import database as db
import discourse
import ebook
import feed_activity
import feed_detail
import jwt_token
import normalize
import poller  # for store_article
import precompute
import pytz
import translate
from bs4 import BeautifulSoup
from PIL import Image, ImageOps
from psycopg2 import extras


def users(cur, period):
    cur.execute(
        """
      WITH
        l AS (
          SELECT
            TIMESTAMPTZ '2017-02-01 00:00' AS min,
            DATE_TRUNC('{}', NOW()) + INTERVAL '1 {}' - INTERVAL '1 SECOND' AS max),
        u AS (
          SELECT
            WIDTH_BUCKET(
              articles.stamp,
              ARRAY(SELECT GENERATE_SERIES(l.min, l.max, '1 {}'))
            ) AS bucket,
            COUNT(DISTINCT user_articles.user_id) AS average_users
          FROM
            l,
            user_articles JOIN articles ON user_articles.article_id = articles.id
          WHERE read
          GROUP BY bucket
          ORDER BY bucket DESC),
        t AS (
          SELECT
            bucket,
            DATE_TRUNC('{}', l.min + (bucket - 1) * INTERVAL '1 {}') AS m,
            average_users
          FROM l, u)
        SELECT
          COALESCE(JSON_AGG(t), '[]'::json) FROM t""".format(
            period, period, period, period, period
        )
    )
    return cur.fetchone()[0]


@bottle.route("/user_stats")
@jwt_token.require_token()
def user_stats():
    with db.connect() as conn, conn.cursor() as cur:
        mau = users(cur, "MONTH")
        wau = users(cur, "WEEK")
        dau = users(cur, "DAY")
    return common.render_json({"mau": mau, "wau": wau, "dau": dau}, 200)


@bottle.route("/borda")
@jwt_token.require_token()
def borda_get():
    return borda.calculate("2004-07-15")


@bottle.route("/borda/<startdate>")
@jwt_token.require_token()
def borda_get1(startdate):
    return borda.calculate(startdate)


@bottle.route("/feeds")
@jwt_token.require_token()
def all_feeds():
    return feeds(-1)


@bottle.route("/feeds/<id>/rss")
@bottle.jinja2_view("feed.tpl", template_lookup=["views"])
def feeds_rss(id):
    f = feeds_gen(id, -1)
    a = articles_chrono_gen(feed=id)
    a["aggregator_hostname"] = os.environ["AGGREGATOR_HOSTNAME"]
    bottle.response.set_header("Content-Type", "application/rss+xml; charset=UTF8")
    return {**f, **a}


@bottle.route("/feeds/<id>")
@jwt_token.require_token()
def feeds(id):
    user_id = jwt_token.user_id()
    feeds = feeds_gen(id, user_id)
    return common.render_json(feeds, 200)


def feeds_gen(id, user_id):
    with db.connect() as conn, conn.cursor() as cur:
        cur.execute(
            """
            WITH f AS (
              SELECT
                feeds.id,
                COALESCE(AVG(user_feeds.rating)::INTEGER, COALESCE(feeds.rating, 0)) AS my_rating
              FROM
                feeds
                LEFT OUTER JOIN user_feeds ON feeds.id = user_feeds.feed_id AND user_id = %(user_id)s
              GROUP BY feeds.id),
            c AS (
              SELECT
                feeds.id,
                feeds.homepage,
                feeds.url,
                feeds.language,
                feeds.title,
                feeds.license,
                feeds.icon,
                feeds.active,
                feeds.last_polled,
                feeds.incomplete,
                feeds.tags,
                feeds.salt_url,
                feeds.rating,
                feeds.premium,
                feeds.script,
                feeds.frequency,
                feeds.cookies,
                feeds.exclude,
                feeds.main,
                feeds.tor,
                feeds.asy,
                feeds_data.*,
                f.*
              FROM feeds
                LEFT OUTER JOIN f ON feeds.id = f.id
                LEFT OUTER JOIN feeds_data ON feeds.id = feeds_data.id
              WHERE
                %(feed_id)s = -1 OR feeds.id = %(feed_id)s
              ORDER BY title)
            SELECT JSON_AGG(ROW_TO_JSON(c)) FROM c""",
            {"user_id": user_id, "feed_id": id},
        )
        res = cur.fetchone()
        if res and res[0] is not None:
            feeds = {"feeds": res[0]}
        else:
            feeds = {"feeds": []}
    return feeds


@bottle.route("/feeds/<id>", method="PUT")
@jwt_token.require_token()
def feed_put(id):
    """update the feed (add/remove tags)

    sample call:
    curl -k -i -X PUT -H 'Content-Type: application/json' -d '{"tags": ["italia", "società"]}' http://localhost:8080/feeds/1
    """
    groups = jwt_token.user_groups()
    if "staff" not in groups:
        common.log_error("non-staff user tried to access PUT /feeds/<id> endpoint")
        return bottle.HTTPResponse(
            status=403, body="you must be member of staff group to do this"
        )

    data = bottle.request.json
    data["id"] = id
    with db.connect() as conn, conn.cursor() as cur:
        cur.execute(
            """
          UPDATE
            feeds
          SET
            tags=COALESCE(%(tags)s, feeds.tags),
            active=COALESCE(%(active)s, feeds.active),
            premium=COALESCE(%(premium)s, feeds.premium),
            incomplete=COALESCE(%(incomplete)s, feeds.incomplete),
            main=COALESCE(%(main)s, feeds.main),
            exclude=COALESCE(%(exclude)s, feeds.exclude),
            salt_url=COALESCE(%(salt_url)s, feeds.salt_url),
            cookies=COALESCE(%(cookies)s, feeds.cookies),
            title=COALESCE(%(title)s, feeds.title),
            homepage=COALESCE(%(homepage)s, feeds.homepage),
            url=COALESCE(%(url)s, feeds.url),
            language=COALESCE(%(language)s, feeds.language),
            script=COALESCE(%(script)s, feeds.script),
            frequency=COALESCE(%(frequency)s, feeds.frequency),
            license=COALESCE(%(license)s, feeds.license)
          WHERE id = %(id)s""",
            data,
        )
    return common.render_json({"id": id}, 200)


@bottle.route("/feeds/<id>/poll", method="POST")
@jwt_token.require_token()
def feed_poll(id):
    """force polling the feed

    sample call:
    curl -k -i -X POST http://localhost:8080/feeds/132/poll
    """
    groups = jwt_token.user_groups()
    if "staff" not in groups:
        common.log_error(
            "non-staff user tried to access POST /feeds/<id>/poll endpoint"
        )
        return bottle.HTTPResponse(
            status=403, body="you must be member of staff group to do this"
        )

    with db.connect() as conn, conn.cursor(cursor_factory=extras.RealDictCursor) as cur:
        cur.execute(
            "SELECT id, language, url, incomplete, salt_url, exclude, asy, cookies, tor, script, active, frequency FROM feeds WHERE %(feed_id)s = id",
            {"feed_id": id},
        )
        result = cur.fetchone()
    # ignore active and frequency settings
    result["active"] = True
    result["frequency"] = ""
    p = poller.Poller(result)
    p.poll()
    return p.__dict__


@bottle.route("/feeds/<id>/icon", method="POST")
@jwt_token.require_token()
def feed_put_icon(id):
    """update the feed icon

    sample call:
    curl -k -i -X POST -F 'image=@cern.png' http://localhost:8080/feeds/132/icon
    """
    groups = jwt_token.user_groups()
    if "staff" not in groups:
        common.log_error(
            "non-staff user tried to access POST /feeds/<id>/icon endpoint"
        )
        return bottle.HTTPResponse(
            status=403, body="you must be member of staff group to do this"
        )

    f = bottle.request.files.get("image")
    if f.content_type == "image/x-png" or f.content_type == "image/png":
        ext = "png"
    elif f.content_type == "image/jpeg":
        ext = "jpeg"
    elif f.content_type == "image/gif":
        ext = "gif"
    elif f.content_type == "image/svg+xml":
        ext = "svg"
    else:
        common.log_error("unsupported image format POSTed to /feeds/<id>/icon endpoint")
        return bottle.HTTPResponse(
            status=400,
            body="only gif, jpeg, png or svg image types accepted, %s received"
            % f.content_type,
        )
    icon = "icons/%s.%s" % (id, ext)

    if ext == "svg":
        f.file.seek(0)
        content = f.file.read()
    else:
        # resize and scale raster images
        img = Image.open(f.file)
        s = max(img.size[0], img.size[1])
        square = ImageOps.pad(img, (s, s), color=1)
        resized = square.resize((200, 200))
        img = io.BytesIO()
        resized.save(img, format=ext)
        img.seek(0)
        content = img.read()

    if len(content) < 50 * 1024:
        with db.connect() as conn, conn.cursor() as cur:
            cur.execute(
                "UPDATE feeds SET icon = %s, iconblob = %s WHERE id = %s",
                (icon, content, id),
            )

        common.log_info("dumping icon for feed %s: %s" % (id, icon))
        with open("www/" + icon, "wb") as f:
            f.write(content)
            f.close()

        return common.render_json({"id": id}, 200)
    else:
        common.log_error("content too long POSTed to /feeds/<id>/icon endpoint")
        return bottle.HTTPResponse(
            status=400, body="%d B exceeds maximum size accepted: 50 kB" % len(content)
        )


@bottle.route("/translate/<id>", method="GET")
@jwt_token.require_token()
def translate_article(id):
    with db.connect() as conn, conn.cursor() as cur:
        data = {"id": id}
        cur.execute(
            """
            SELECT
              id,
              language,
              content_original,
              title_original
            FROM
              articles
            WHERE
              id = %(id)s""",
            data,
        )
        res = cur.fetchone()
        if res and res[0] is not None:
            token = translate.get_token()
            if token != "":
                language_original = res[1]
                content_original = res[2]
                title_original = res[3]
                (title, content) = translate.translate(
                    token, language_original, title_original, content_original
                )
                data = {"title": title, "content": content, "id": id}
                cur.execute(
                    "UPDATE articles SET content=%(content)s, title=%(title)s WHERE id = %(id)s",
                    data,
                )
    return article(id)


def log_user_access(cur):
    user_id = jwt_token.user_id()
    data = {
        "user_id": user_id,
        "user_email": jwt_token.user_email(),
        "user_name": jwt_token.user_name(),
    }
    if user_id is not None:
        cur.execute(
            """
            INSERT
              INTO users (id, username, email, last_access)
              VALUES (%(user_id)s, %(user_name)s, %(user_email)s, NOW())
            ON CONFLICT (id)
              DO UPDATE SET username = %(user_name)s, email = %(user_email)s, last_access = NOW()
              WHERE users.id = %(user_id)s""",
            data,
        )
    return data


@bottle.route("/articles/<id>", method="POST")
@jwt_token.require_token()
def article_post(id):
    """set the article rating, to_read and dismissed fields

    sample call:
    curl -k -i -X POST -H 'Content-Type: application/json' -d '{"rating": 4, "to_read": true, 'dismissed': false}' http://localhost:8080/articles/1
    """
    data = {
        "id": id,
        "user_id": jwt_token.user_id(),
        "rating": None,
        "to_read": None,
        "dismissed": None,
    }
    data.update(bottle.request.json)
    # print(data)
    with db.connect() as conn, conn.cursor() as cur:
        log_user_access(cur)
        cur.execute(
            """
            INSERT INTO user_articles(user_id, article_id, rating, to_read, dismissed)
              VALUES (%(user_id)s, %(id)s, %(rating)s, %(to_read)s, %(dismissed)s)
            ON CONFLICT (user_id, article_id)
              DO UPDATE SET
                rating = COALESCE(%(rating)s, user_articles.rating),
                to_read = COALESCE(%(to_read)s, user_articles.to_read),
                dismissed = COALESCE(%(dismissed)s, user_articles.dismissed)
              WHERE user_articles.user_id = %(user_id)s AND user_articles.article_id = %(id)s""",
            data,
        )
        cur.execute(
            """
            INSERT INTO articles_data
              SELECT articles_data_view.*
              FROM articles_data_view
              WHERE id = %(id)s
            ON CONFLICT (id) DO UPDATE
              SET
                rating = EXCLUDED.rating,
                to_reads = EXCLUDED.to_reads""",
            {"id": id},
        )
    return 0


@bottle.route("/articles/<id>", method="PUT")
@jwt_token.require_token()
def article_put(id):
    """update the article

    sample call:
    curl -k -i -X PUT -H 'Content-Type: application/json' -d '{"title": "aaa", "content": "bbb"}' http://localhost:8080/articles/1
    """
    data = bottle.request.json
    data["id"] = id
    data["user_id"] = jwt_token.user_id()
    if "content" in data:
        data["content"] = normalize.normalize(data["content"])
    if "content_original" in data:
        data["content_original"] = normalize.normalize(data["content_original"])
    with db.connect() as conn, conn.cursor() as cur:
        cur.execute(
            """
            UPDATE articles SET
                title=COALESCE(%(title)s,articles.title),
                title_original=COALESCE(%(title_original)s,articles.title_original),
                language=COALESCE(%(language)s,articles.language),
                url=COALESCE(%(url)s,articles.url),
                author=COALESCE(%(author)s,articles.author),
                content=COALESCE(%(content)s,articles.content),
                content_original=COALESCE(%(content_original)s,articles.content_original)
            WHERE id = %(id)s""",
            data,
        )
        cur.execute(
            """
            INSERT INTO articles_data
              SELECT articles_data_view.*
              FROM articles_data_view
              WHERE id = %(id)s
            ON CONFLICT (id) DO UPDATE
              SET
                length = EXCLUDED.length""",
            {"id": id},
        )
    return common.render_json({"id": id}, 200)


@bottle.route("/feeds/<id>", method="POST")
@jwt_token.require_token()
def feed_rating_post(id):
    """update the feed rating

    sample call:
    curl -k -i -X POST -H 'Content-Type: application/json' -d '{"rating": 4}' http://localhost:8080/feeds/1
    """
    data = bottle.request.json
    data["id"] = id
    data["user_id"] = jwt_token.user_id()
    with db.connect() as conn, conn.cursor() as cur:
        log_user_access(cur)
        cur.execute(
            """
            INSERT INTO user_feeds(user_id, feed_id, rating)
              VALUES (%(user_id)s, %(id)s, %(rating)s)
            ON CONFLICT (user_id, feed_id)
              DO UPDATE SET rating = %(rating)s
              WHERE user_feeds.user_id = %(user_id)s AND user_feeds.feed_id = %(id)s""",
            data,
        )
    return 0


@bottle.route("/articles")
def articles():
    """filter articles in reverse chronological order

    sample calls:
    curl -k -i -X GET http://localhost:8080/articles
    curl -k -i -X GET http://localhost:8080/articles?author=paolog
    curl -k -i -X GET http://localhost:8080/articles?feed=0
    curl -k -i -X GET http://localhost:8080/articles?query=Renzi
    curl -k -i -X GET http://localhost:8080/articles?read=true
    curl -k -i -X GET http://localhost:8080/articles?to_read=true
    """
    user_id = jwt_token.user_id()
    params = bottle.request.query.decode()
    offset = params.get("offset", 0)
    feed = params.get("feed", -1)
    author = params.get("author", "")
    query = params.get("query", "")
    read = params.get("read", "false")
    to_read = params.get("to_read", "false")

    articles = articles_chrono_gen(offset, feed, author, query, user_id, read, to_read)
    return common.render_json(articles, 200)


def articles_chrono_gen(
    offset=0, feed=-1, author="", query="", user_id=-1, read="false", to_read="false"
):
    if author:
        author = "%" + author + "%"
    if not user_id or user_id == -1:
        query1 = """
            WITH a AS (
              SELECT DISTINCT
                articles.id,
                articles.stamp,
                articles.feed_id,
                articles.language,
                articles.author,
                articles.title,
                articles.title_original,
                articles.url,
                articles.topic_id,
                articles.comments,
                EXTRACT(EPOCH FROM (NOW() - articles.stamp)) AS age,
                feeds.icon,
                feeds.premium,
                feeds.title AS feed,
                0 AS views,
                0 AS rating,
                0 AS to_reads,
                1000 AS length,
                COALESCE(user_articles.read, FALSE) AS read
              FROM articles
                INNER JOIN feeds ON articles.feed_id = feeds.id
                LEFT OUTER JOIN user_articles ON articles.id = user_articles.article_id
              WHERE read
              ORDER BY stamp DESC, articles.id DESC
              LIMIT 50
              OFFSET %(offset)s)
            SELECT
              JSON_AGG(ROW_TO_JSON(a)) FROM a"""
    else:
        query1 = """
            WITH a AS (
              SELECT
                articles.id,
                articles.feed_id,
                articles.language,
                articles.author,
                articles.title,
                articles.title_original,
                articles.url,
                articles.topic_id,
                articles.comments,
                articles.stamp,
                feeds.icon,
                feeds.premium,
                feeds.rating,
                feeds.title AS feed
              FROM articles
                INNER JOIN feeds ON articles.feed_id = feeds.id
              WHERE
                (%(feed)s = -1 OR feed_id = %(feed)s)
                AND (%(author)s = '' OR articles.author ILIKE %(author)s)
                AND (%(query)s = ''
                  OR articles.tsv @@ plainto_tsquery('pg_catalog.italian', %(query)s)
                  OR articles.tsv_simple @@ plainto_tsquery('pg_catalog.simple', %(query)s))),
            aa AS (
              SELECT
                articles_data.*,
                a.*,
                EXTRACT(EPOCH FROM (NOW() - a.stamp)) AS age,
                CASE WHEN feeds.premium THEN 2 ELSE 0 END + COALESCE(user_feeds.rating::INTEGER, COALESCE(a.rating, 0)) AS my_feed_rating,
                COALESCE(user_articles.rating::INTEGER, 0) AS my_rating,
                COALESCE(user_articles.to_read, FALSE) AS to_read,
                COALESCE(user_articles.dismissed, FALSE) AS dismissed,
                COALESCE(user_articles.read, FALSE) AS read
              FROM
                articles_data
                INNER JOIN a ON a.id = articles_data.id
                INNER JOIN feeds ON articles_data.feed_id = feeds.id
                LEFT OUTER JOIN user_feeds ON articles_data.feed_id = user_feeds.feed_id AND user_feeds.user_id = %(user_id)s
                LEFT OUTER JOIN user_articles ON articles_data.id = user_articles.article_id AND user_articles.user_id = %(user_id)s
              WHERE
                (%(feed)s != -1 OR %(author)s != '' OR %(query)s != '' OR %(read)s = 'true' OR %(to_read)s = 'true' OR COALESCE(user_articles.read, FALSE) = FALSE)
                AND (%(to_read)s = 'false' OR COALESCE(user_articles.to_read, FALSE))
                AND (%(read)s = 'false' OR COALESCE(user_articles.read, FALSE))
              ORDER BY stamp DESC, a.id DESC
              LIMIT 50
              OFFSET %(offset)s)
            SELECT
              JSON_AGG(ROW_TO_JSON(aa)) FROM aa"""
    data = {
        "offset": offset,
        "feed": feed,
        "author": author,
        "query": query,
        "user_id": user_id,
        "read": read,
        "to_read": to_read,
    }
    with db.connect() as conn, conn.cursor() as cur:
        cur.execute(query1, data)
        res = cur.fetchone()
        if res and res[0] is not None:
            articles = {"articles": res[0]}
        else:
            articles = {"articles": []}
    return articles


@bottle.route("/articles_new")
@jwt_token.require_token()
def articles_new():
    """filtered view with excerpts

    sample calls:
    curl -k -i -X GET http://localhost:8080/articles_new
    """
    user_id = jwt_token.user_id()
    offset = bottle.request.query.get("offset", 0)
    articles = {"articles": get_merged_articles(user_id, offset, 100, False, True)}
    return common.render_json(articles, 200)


@bottle.route("/articles", method="POST")
@jwt_token.require_token()
def articles_post():
    """creates a new article by manual submission
    returns -id if the article is already present (same url) or the id of the new article

    sample call:
    curl -k -i -X POST -H 'Content-Type: application/json' -d '{"author": "a", "title": "b", "content": "c", "language": "de", "url": "f"}' http://localhost:8080/articles
    """
    data = bottle.request.json
    # print(data)
    article_id = poller.store_article(
        author=data["author"],
        title=data["title"],
        url=data["url"],
        content=data["content"],
        feed_id=0,
        language=data["language"],
    )
    if article_id == 0:
        with db.connect() as conn, conn.cursor() as cur:
            cur.execute("SELECT id FROM articles WHERE url = %(url)s", data)
            res = cur.fetchone()
            if res and res[0] is not None:
                article_id = -res[0]
    return common.render_json({"id": article_id}, 201)


def get_articles_json(data, full=True, read=False, excerpts=False):
    if full:
        additional_fields = "articles.content, articles.content_original,"
    elif excerpts:
        additional_fields = "SUBSTRING((CASE WHEN LENGTH(TRIM(articles.content))>0 THEN articles.content ELSE articles.content_original END) FOR 400) AS excerpt,"
    else:
        additional_fields = ""
    with db.connect() as conn, conn.cursor() as cur:
        cur.execute(
            """
            WITH ad AS (
              SELECT
                articles_data.*,
                COALESCE(user_articles.rating::INTEGER, 0) AS my_rating,
                COALESCE(user_articles.to_read, FALSE) AS to_read,
                COALESCE(user_articles.read, FALSE) AS read,
                COALESCE(user_articles.dismissed, FALSE) AS dismissed
              FROM
                articles_data
                LEFT OUTER JOIN user_articles ON articles_data.id = user_articles.article_id AND (%(user_id)s IS NULL OR user_articles.user_id = %(user_id)s)
              WHERE
                articles_data.id = ANY(%(ids)s)),
            a AS (
              SELECT
                ad.*,
                articles.stamp,
                articles.feed_id,
                articles.language,
                articles.author,
                articles.title,
                articles.title_original,
                {}
                articles.url,
                articles.comments,
                articles.topic_id,
                EXTRACT(EPOCH FROM (NOW() - articles.stamp)) AS age,
                feeds.icon,
                feeds.premium,
                feeds.license,
                feeds.title AS feed,
                feeds.id AS feed_id
              FROM
                ad
                JOIN articles ON ad.id = articles.id
                JOIN feeds ON ad.feed_id = feeds.id
                JOIN UNNEST(%(ids)s::INT[]) WITH ORDINALITY t(id, ord) ON ad.id = t.id
              ORDER BY t.ord)
            SELECT
              COALESCE(JSON_AGG(a), '[]'::json) FROM a""".format(
                additional_fields
            ),
            data,
        )
        res = cur.fetchall()[0][0]
        log_user_access(cur)
        if read:
            cur.execute(
                """
                INSERT INTO user_articles(user_id, article_id, read)
                  VALUES (%(user_id)s, UNNEST(%(ids)s::INT[]), TRUE)
                ON CONFLICT (user_id, article_id)
                  DO UPDATE SET read = TRUE
                  WHERE user_articles.user_id = %(user_id)s AND user_articles.article_id = ANY(%(ids)s)""",
                data,
            )
            cur.execute(
                """
                INSERT INTO articles_data
                  SELECT articles_data_view.*
                  FROM articles_data_view
                  WHERE id = ANY(%(ids)s)
                ON CONFLICT (id) DO UPDATE
                  SET
                    views = EXCLUDED.views""",
                data,
            )

    # print(res)
    return res


@bottle.route("/articles/to_read", method="DELETE")
@jwt_token.require_token()
def articles_mark_all():
    data = {"user_id": jwt_token.user_id()}
    with db.connect() as conn, conn.cursor() as cur:
        cur.execute(
            """
            UPDATE articles_data
            SET to_reads = to_reads - 1
            FROM (SELECT article_id FROM user_articles WHERE user_articles.user_id = %(user_id)s AND user_articles.to_read) AS t
            WHERE id = article_id""",
            data,
        )
        cur.execute(
            """
            UPDATE user_articles
            SET to_read = FALSE
            WHERE user_articles.user_id = %(user_id)s AND user_articles.to_read""",
            data,
        )


@bottle.route("/articles/json", method="GET")
@jwt_token.require_token()
def articles_json():
    """returns an antology of articles in JSON format

    sample call:
    curl -k -i -X GET -H 'Content-Type: application/json' http://localhost:8080/articles/json
    """
    data = bottle.request.json
    return common.render_json(
        {"articles": get_articles_json(data, full=True, read=True)}, 200
    )

    # @bottle.route('/articles_score', method='GET')
    # @jwt_token.require_token()
    # @bottle.jinja2_view('score.tpl', template_lookup=['views'])
    # def articles_score():
    """ sample call:
    curl -k -i -X GET https://notizie.example.com/api/articles_score
    """


#    user_id = jwt_token.user_id()
#    articles = get_precomputed_articles('filtered', user_id, 0, 100, False)
#    time_format = "%Y-%m-%d %H:%M"
#    cet_tz = pytz.timezone('CET')
#    utc_tz = pytz.utc
#    localized = utc_tz.localize(datetime.datetime.utcnow()).astimezone(cet_tz)
#    return {'articles': articles, 'user_id': user_id, 'stamp': localized.strftime(time_format)}


@bottle.route("/articles_score/<id>", method="GET")
@jwt_token.require_token()
@bottle.jinja2_view("score.tpl", template_lookup=["views"])
def articles_score_id(id):
    """sample call:
    curl -k -i -X GET https://notizie.example.com/api/articles_score
    """
    articles = get_precomputed_articles("filtered", id, 0, 100, False)
    time_format = "%Y-%m-%d %H:%M"
    cet_tz = pytz.timezone("CET")
    utc_tz = pytz.utc
    localized = utc_tz.localize(datetime.datetime.utcnow()).astimezone(cet_tz)
    return {
        "articles": articles,
        "user_id": id,
        "stamp": localized.strftime(time_format),
    }


@bottle.route("/articles.txt", method="GET")
@jwt_token.require_token()
@bottle.jinja2_view("txt.tpl", template_lookup=["views"])
def articles_txt():
    """returns an antology of articles in TEXT format

    sample call:
    curl -k -i -X GET http://localhost:8080/articles.txt?ids=200,9
    """
    user_id = jwt_token.user_id()
    data = {"user_id": user_id}
    ids = "[" + bottle.request.query.get("ids", "") + "]"
    if ids != "[]":
        data["ids"] = json.loads(ids)
        articles = get_articles_json(data, full=True, read=True)
    else:
        articles = get_newsletter_json(data)
    for a in articles:
        if "content" in a and a["content"]:
            soup = BeautifulSoup(a["content"], "lxml")
            a["content"] = soup.get_text(strip=True)
        if "content_original" in a and a["content_original"]:
            soup = BeautifulSoup(a["content_original"], "lxml")
            a["content_original"] = soup.get_text()
    return {
        "articles": articles,
        "aggregator_hostname": os.environ["AGGREGATOR_HOSTNAME"],
    }


def get_newsletter_json(data):
    with db.connect() as conn, conn.cursor() as cur:
        cur.execute(
            """
            SELECT
              COALESCE(filter2, 'filtered') as filter2,
              list_news,
              list_fulltext
            FROM
              users
            WHERE
              id=%(user_id)s""",
            data,
        )
        res = cur.fetchone()
        if res:
            filter2 = res[0]
            list_news = res[1]
            list_fulltext = res[2]
    return articles_smart_filter(data["user_id"], 0, list_news, filter2, list_fulltext)


@bottle.route("/articles.html", method="GET")
@jwt_token.require_token()
@bottle.jinja2_view("html.tpl", template_lookup=["views"])
def articles_html():
    """returns an antology of articles in basic HTML format

    sample call:
    curl -k -i -X GET http://localhost:8080/articles.html?ids=200,9
    """
    user_id = jwt_token.user_id()
    data = {"user_id": user_id}
    ids = "[" + bottle.request.query.get("ids", "") + "]"
    if ids != "[]":
        data["ids"] = json.loads(ids)
        articles = get_articles_json(data, full=True, read=True)
    else:
        articles = get_newsletter_json(data)
    return {
        "articles": articles,
        "aggregator_hostname": os.environ["AGGREGATOR_HOSTNAME"],
    }


@bottle.route("/articles.json", method="GET")
@jwt_token.require_token()
def articles_json1():
    """returns an antology of articles in JSON format

    sample call:
    curl -k -i -X GET http://localhost:8080/articles.json?ids=200,9
    """
    user_id = jwt_token.user_id()
    data = {"user_id": user_id}
    ids = "[" + bottle.request.query.get("ids", "") + "]"
    if ids != "[]":
        data["ids"] = json.loads(ids)
        articles = get_articles_json(data, full=True, read=True)
    else:
        articles = get_newsletter_json(data)
    return {"articles": articles}


@bottle.route("/articles.epub", method="GET")
@jwt_token.require_token()
def articles_epub():
    """returns an antology of articles in epub format

    sample call:
    curl -k -i -X GET http://localhost:8080/articles.epub?ids=200,9
    """
    user_id = jwt_token.user_id()
    data = {"user_id": user_id}
    ids = "[" + bottle.request.query.get("ids", "") + "]"
    if ids != "[]":
        data["ids"] = json.loads(ids)
        articles = get_articles_json(data, full=True, read=True)
    else:
        articles = get_newsletter_json(data)
    book = ebook.export_epub(articles)
    mimetype = "application/epub+zip"
    return bottle.HTTPResponse(
        status=200, body=book, headers={"content-type": mimetype}
    )


@bottle.route("/articles.mobi", method="GET")
@jwt_token.require_token()
def articles_mobi():
    """returns an antology of articles in Amazon Kindle mobi format

    sample call:
    curl -k -i -X GET http://localhost:8080/articles.mobi?ids=200,9
    """
    user_id = jwt_token.user_id()
    data = {"user_id": user_id}
    ids = "[" + bottle.request.query.get("ids", "") + "]"
    if ids != "[]":
        data["ids"] = json.loads(ids)
        articles = get_articles_json(data, full=True, read=True)
    else:
        articles = get_newsletter_json(data)
    book = ebook.export_mobi(articles)
    mimetype = "application/vnd.amazon.mobi7-ebook"
    return bottle.HTTPResponse(
        status=200, body=book, headers={"content-type": mimetype}
    )


@bottle.route("/articles/<id>")
@jwt_token.require_token()
def article(id):
    user_id = jwt_token.user_id()
    data = {"user_id": user_id, "ids": [int(id)]}
    articles = get_articles_json(data, full=True, read=True)
    article = articles[0]
    return common.render_json(article, 200)


@bottle.route("/articles/<id>/similar")
@jwt_token.require_token()
def articles_similar(id):
    # return articles similar to id
    # based on: http://domas.monkus.lt/posts/2009-12-03-document-similarity-in-postgresql/
    with db.connect() as conn, conn.cursor() as cur:
        cur.execute(
            """
          WITH a AS (
            SELECT
              id,
              tsv
            FROM
              articles
            ORDER BY articles.id DESC
            LIMIT 10000),
          s AS (
            SELECT
              a.id,
              TS_RANK(a.tsv, REPLACE(STRIP(original.tsv)::text, ' ', ' | ')::TSQUERY)/(1 + LENGTH(a.tsv) + LENGTH(original.tsv)) AS similarity
            FROM
              a,
              (SELECT tsv, id FROM articles WHERE id = %(id)s) AS original
            WHERE a.id != original.id
            ORDER BY similarity DESC LIMIT 10)
          SELECT
            JSON_AGG(id) FROM s""",
            {"id": id},
        )
        res = cur.fetchone()
        if res and res[0] is not None:
            data = {"ids": res[0]}
        else:
            data = {"ids": []}
    user_id = jwt_token.user_id()
    data["user_id"] = user_id
    return common.render_json(
        {"articles": get_articles_json(data, full=False, read=False)}, 200
    )


def webhook_secret():
    if webhook_secret.cached == b"":
        try:
            with open("secrets/webhook_secret", "r") as secret_file:
                webhook_secret.cached = secret_file.read().encode("utf-8")
        except EnvironmentError:
            common.log_error("error while retrieving webhook_secret")
            raise bottle.HTTPError(status=520, body="internal error")
    # print('[%s]' % webhook_secret.cached.decode())
    return webhook_secret.cached


webhook_secret.cached = b""


@bottle.route("/comment", method="POST")
def comment_post():
    """POST receive for Discourse webhook"""
    discourse_event_signature = bottle.request.get_header("X-Discourse-Event-Signature")
    raw = bottle.request.body.read()
    digest = hmac.new(webhook_secret(), msg=raw, digestmod=hashlib.sha256).hexdigest()
    if discourse_event_signature[7:] != digest:
        common.log_error("signature mismatch in POST /comment endpoint")
        return bottle.HTTPResponse(status=401, body="signature mismatch")
    data = bottle.request.json
    discourse_event_type = bottle.request.get_header("X-Discourse-Event-Type")
    discourse_event = bottle.request.get_header("X-Discourse-Event")
    if discourse_event_type == "post" and discourse_event == "post_created":
        username = data["post"]["username"]
        if username == os.environ["DISCOURSE_USER"]:
            common.log_info("this is the initial post: skip")
            pass
        else:
            topic_id = data["post"]["topic_id"]
            common.log_info("adding one comment to topic_id = %s" % topic_id)
            data = {"topic_id": topic_id}
            with db.connect() as conn, conn.cursor() as cur:
                cur.execute(
                    """
                    UPDATE articles SET
                        comments=comments+1
                    WHERE topic_id = %(topic_id)s""",
                    data,
                )


@bottle.route("/user_event", method="POST")
def user_event():
    """POST receive for Discourse webhook"""
    discourse_event_signature = bottle.request.get_header("X-Discourse-Event-Signature")
    raw = bottle.request.body.read()
    digest = hmac.new(webhook_secret(), msg=raw, digestmod=hashlib.sha256).hexdigest()
    if discourse_event_signature[7:] != digest:
        common.log_error("signature mismatch in POST /user_event endpoint")
        return bottle.HTTPResponse(status=401, body="signature mismatch")
    data = bottle.request.json
    discourse_event_type = bottle.request.get_header("X-Discourse-Event-Type")
    discourse_event = bottle.request.get_header("X-Discourse-Event")
    if discourse_event_type == "user" and discourse_event == "user_approved":
        username = data["user"]["username"]
        # add user to Soci group
        if not discourse.add_user(os.environ["MEMBERS_GROUP_ID"], username):
            common.log_error("error while trying to add user to Soci group")
            return bottle.HTTPResponse(
                status=500, body="impossible to add user to Soci group"
            )
        user_id = data["user"]["id"]
        limit = 100
        with db.connect() as conn, conn.cursor() as cur:
            log_user_access(cur)
        common.log_info("first-time precompute for user: %s" % user_id)
        precompute.articles("filtered", user_id, limit)
        precompute.articles("keywords", user_id, limit)
        precompute.articles("latest", user_id, limit)
        precompute.articles("suggestions", user_id, limit)
        precompute.articles("popular", user_id, limit)


@bottle.route("/articles/<id>/comment", method="POST")
@jwt_token.require_token()
def articles_comment(id):
    """
    send the 1st comment to the article to Discourse
    sample call:
    curl -k -i -X POST -H 'Content-Type: application/json' -d '{"text": "mi piace !"}' http://localhost:8080/articles/1/comment
    """
    data = bottle.request.json
    user_name = jwt_token.user_name()
    with db.connect() as conn, conn.cursor() as cur:
        cur.execute(
            """
            SELECT
              COALESCE(title, ''),
              COALESCE(title_original, ''),
              COALESCE(content, ''),
              COALESCE(content_original, ''),
              language
            FROM articles
            WHERE id = %(id)s""",
            {"id": id},
        )
        res = cur.fetchone()
        title = res[0]
        title_original = res[1]
        content = res[2]
        content_original = res[3]
        language = res[4]
    if language != os.environ["BASE_LANGUAGE"]:
        if title != "":
            article_title = "[%s] %s" % (title_original, title)
            article_content = content
        else:
            article_title = title_original
            article_content = content_original
    else:
        article_title = title
        article_content = content
    common.log_info(
        "title = %s, user name = %s, text = %s"
        % (article_title, user_name, data["text"])
    )
    topic_id = discourse.new_comment(
        id, article_title, article_content, user_name, data["text"]
    )
    if topic_id < 0:
        common.log_error("negative topic id")
        return bottle.HTTPResponse(status=500, body="internal error")
    else:
        common.log_info("adding topic_id = %d to article_id = %s" % (topic_id, id))
        data = {"topic_id": topic_id, "article_id": id}
        with db.connect() as conn, conn.cursor() as cur:
            cur.execute(
                """
                UPDATE articles SET
                    topic_id=%(topic_id)s
                WHERE id = %(article_id)s""",
                data,
            )
        return common.render_json(data, 200)


@bottle.route("/feeds", method="POST")
@jwt_token.require_token()
def feed_post():
    """creates a new (initially) private feed
    returns 0 if the feed is already present (same RSS url) or the id of the new feed

    sample call:
    curl -i -X POST -H 'Content-Type: application/json' -d '{"title": "a", "homepage": "http://b", "url": "http://c", "language": "de", "license": "f"}' https://notizie.example.com/api/feeds
    """
    data = bottle.request.json
    user_name = jwt_token.user_name()
    # print(data)
    with db.connect() as conn, conn.cursor() as cur:
        cur.execute(
            """
            WITH maxid AS (SELECT MAX(id) AS id FROM feeds)
            INSERT INTO feeds (id, title, homepage, url, language, license, icon, rating, active)
            SELECT maxid.id+1, %(title)s, %(homepage)s, %(url)s, %(language)s, %(license)s, 'icons/unknown.png', 0, TRUE FROM maxid
            WHERE NOT EXISTS (SELECT url FROM feeds WHERE url = %(url)s)
            RETURNING ID""",
            data,
        )
        res = cur.fetchone()
        # print(res)
        feed_id = res[0] if res else 0
    topic_id = 0
    if feed_id > 0:
        data["feed_id"] = feed_id
        data["base_url"] = "https://%s" % os.environ["AGGREGATOR_HOSTNAME"]
        topic_id = discourse.new_topic(
            "nuova fonte: %(title)s" % data,
            """Ho inserito ["%(title)s"](%(base_url)s/feed/%(feed_id)s), l'aggregatore comincerà presto a caricare notizie anche da lì.

Se vi interessa potete "abbonarvi" (basta assegnargli qualche stellina :star2:) — in questo modo queste notizie vi verranno mostrate con maggiore evidenza nel **newsfeed** !"""
            % data,
            user_name,
            os.environ["FEEDS_CATEGORY_ID"],
        )
    return common.render_json({"id": feed_id, "topic_id": topic_id}, 201)


@bottle.route("/feed_activity")
@jwt_token.require_token()
def feedactivity():
    """create the feed activity graph and return it as SVG

    sample call:
    curl -i -x GET https://notizie.example.com/api/feed_activity
    """
    headers = dict()
    headers["Content-Type"] = "image/svg+xml"
    return bottle.HTTPResponse(feed_activity.get_plot(), **headers)


@bottle.route("/feed_detail/<id>")
@jwt_token.require_token()
def feeddetail(id):
    """create the feed detail graph and return it as SVG
    optional parameters:
    - start (default: two months ago; the start date is rounded to the preceding month start)
    - weeks (number of weeks in each bin; default: 1)
    end is the end of the current month

    sample call:
    curl -i -x GET https://notizie.example.com/api/feed_detail/0?start=2017-12-01&delta=1
    """
    start = bottle.request.query.get("start", None)
    weeks = bottle.request.query.get("weeks", 1)
    headers = dict()
    # return bottle.HTTPResponse(json.dumps(feed_detail.get_data(id, start, weeks), indent=2, default=str), **headers)
    headers["Content-Type"] = "image/svg+xml"
    return bottle.HTTPResponse(feed_detail.get_plot(id, start, weeks), **headers)


@bottle.route("/settings")
@jwt_token.require_token()
def get_settings():
    with db.connect() as conn, conn.cursor() as cur:
        data = log_user_access(cur)
        cur.execute(
            """
          WITH ua AS (
            SELECT
              SUM(COALESCE(CASE WHEN to_read THEN 1 END, 0)) AS to_read
            FROM
              user_articles
            WHERE
              user_id = %(user_id)s),
          s AS (
            SELECT
              %(user_id)s AS id,
              newsletter,
              list_email,
              list_fulltext,
              list_frequency,
              list_news,
              list_format,
              whitelist,
              whitelist_authors,
              blacklist,
              blacklist_authors,
              sociality_weight,
              gravity,
              comment_factor,
              age_divider,
              feed_weight,
              list_weight,
              '' AS filter1,
              COALESCE(filter2, 'filtered') as filter2,
              list_hour,
              COALESCE(languages, array[]::text[]) AS languages,
              COALESCE(tags, array[]::text[]) AS tags,
              ua.to_read
            FROM
              users,
              ua
            WHERE
              id = %(user_id)s)
          SELECT ROW_TO_JSON(s) FROM s""",
            data,
        )
        res = cur.fetchone()
        if res and res[0] is not None:
            settings = res[0]
        else:
            settings = {}
    return common.render_json(settings, 200)


@bottle.route("/settings", method="PUT")
@jwt_token.require_token()
def settings_put():
    """update the user settings"""
    data = dict.fromkeys(
        [
            "languages",
            "tags",
            "filter2",
            "list_email",
            "list_news",
            "newsletter",
            "list_frequency",
            "list_format",
            "list_fulltext",
            "whitelist",
            "list_hour",
            "whitelist_authors",
        ]
    )
    data.update(bottle.request.json)
    data["user_id"] = jwt_token.user_id()
    # print(data)
    with db.connect() as conn, conn.cursor() as cur:
        cur.execute(
            """
          UPDATE users SET
            whitelist_authors=COALESCE(%(whitelist_authors)s, users.whitelist_authors),
            whitelist=COALESCE(%(whitelist)s, users.whitelist),
            blacklist=COALESCE(%(blacklist)s, users.blacklist),
            languages=COALESCE(%(languages)s, users.languages),
            tags=COALESCE(%(tags)s, users.tags),
            filter2=COALESCE(%(filter2)s, users.filter2),
            list_email=COALESCE(%(list_email)s, users.list_email),
            list_news=COALESCE(%(list_news)s, users.list_news),
            newsletter=COALESCE(%(newsletter)s, users.newsletter),
            list_frequency=COALESCE(%(list_frequency)s, users.list_frequency),
            list_format=COALESCE(%(list_format)s, users.list_format),
            list_fulltext=COALESCE(%(list_fulltext)s, users.list_fulltext),
            list_hour=COALESCE(%(list_hour)s, users.list_hour)
          WHERE id = %(user_id)s""",
            data,
        )
    return 0


def articles_chrono(user_id, offset, limit, full, condition1, condition2="TRUE"):
    """return articles in reverse chronological order filtered according to the supplied conditions"""
    if full:
        additional_fields = "articles.content, articles.content_original,"
    else:
        additional_fields = ""
    query = """WITH u AS (
                  SELECT
                    whitelist,
                    whitelist_authors
                  FROM
                    users
                  WHERE
                    id=%(user_id)s),
                aa AS (
                  SELECT
                    articles_data.*,
                    user_feeds.rating::INTEGER AS my_feed_rating,
                    COALESCE(user_articles.rating::INTEGER, 0) AS my_rating,
                    COALESCE(user_articles.to_read, FALSE) AS to_read,
                    COALESCE(user_articles.read, FALSE) AS read,
                    COALESCE(user_articles.dismissed, FALSE) AS dismissed
                  FROM
                    articles_data
                    LEFT OUTER JOIN user_feeds ON articles_data.feed_id = user_feeds.feed_id AND (%(user_id)s IS NULL OR user_feeds.user_id = %(user_id)s)
                    LEFT OUTER JOIN user_articles ON articles_data.id = user_articles.article_id AND (%(user_id)s IS NULL OR user_articles.user_id = %(user_id)s)
                  WHERE
                    {}
                  ORDER BY id DESC
                  LIMIT 20000),
                a AS (
                  SELECT
                    articles.stamp,
                    feeds.license,
                    articles.feed_id,
                    articles.language,
                    articles.author,
                    articles.title,
                    articles.title_original,
                    {}
                    articles.url,
                    articles.topic_id,
                    articles.comments,
                    EXTRACT(EPOCH FROM (NOW() - articles.stamp)) AS age,
                    aa.*,
                    feeds.icon,
                    feeds.premium,
                    feeds.title AS feed
                  FROM
                    u, articles
                    INNER JOIN aa ON aa.id = articles.id
                    INNER JOIN feeds ON articles.feed_id = feeds.id
                  WHERE
                    {}
                  ORDER BY stamp DESC
                  LIMIT %(limit)s
                  OFFSET %(offset)s)
                SELECT
                  JSON_AGG(ROW_TO_JSON(a)) FROM a""".format(
        condition1, additional_fields, condition2
    )
    with db.connect() as conn, conn.cursor() as cur:
        cur.execute(query, {"offset": offset, "user_id": user_id, "limit": limit})
        res = cur.fetchone()
        if res and res[0] is not None:
            return res[0]
        else:
            return []


def get_precomputed_ids(view, user_id, offset, limit):
    with db.connect() as conn, conn.cursor() as cur:
        cur.execute(
            "SELECT articles FROM precomputed WHERE user_id = %(user_id)s and view=%(view)s LIMIT %(limit)s OFFSET %(offset)s",
            {"limit": limit, "offset": offset, "user_id": user_id, "view": view},
        )
        res = cur.fetchone()
        if res and res[0] is not None:
            return res[0]
        else:
            return []


def get_merged_articles(user_id, offset, limit, full, excerpts=False):
    # merge views into one with the Reißverschlussverfahren (https://de.wikipedia.org/wiki/Rei%C3%9Fverschlussverfahren)
    views = [
        {"view": get_precomputed_ids("filtered", user_id, 0, 100), "weight": 2.0},
        {"view": get_precomputed_ids("keywords", user_id, 0, 100), "weight": 1.5},
        {"view": get_precomputed_ids("suggestions", user_id, 0, 100), "weight": 1.0},
        {"view": get_precomputed_ids("popular", user_id, 0, 100), "weight": 1.0},
        {"view": get_precomputed_ids("latest", user_id, 0, 100), "weight": 0.5},
    ]
    # print('filtered', views[0])
    # print('keywords', views[1])
    # print('suggestions', views[2])
    # print('popular', views[3])
    # print('latest', views[4])
    combined = []
    for v in views:
        w = v["weight"]
        for i, e in enumerate(v["view"]):
            combined.append({"id": e, "rank": float(1 + i) / w})
    combined.sort(key=lambda x: x["rank"])
    ids = [e["id"] for e in combined]
    # print('ids', ids)
    # uniquify perserving order with algo f12 from https://www.peterbe.com/plog/fastest-way-to-uniquify-a-list-in-python-3.6
    uniq = list(dict.fromkeys(ids))
    # print('uniq', uniq)
    data = {"ids": uniq[int(offset) : int(offset) + int(limit)], "user_id": user_id}
    return get_articles_json(data, full=full, read=False, excerpts=excerpts)


def get_precomputed_articles(view, user_id, offset, limit, full, excerpts=False):
    ids = get_precomputed_ids(view, user_id, offset, limit)
    data = {"ids": ids, "user_id": user_id}
    return get_articles_json(data, full=full, read=False, excerpts=excerpts)


def articles_smart_filter(user_id, offset, limit, filt, full):
    if filt == "read":
        return articles_chrono(user_id, offset, limit, full, "user_articles.read")
    elif filt == "premium":
        return articles_chrono(user_id, offset, limit, full, "TRUE", "feeds.premium")
    elif filt == "to_read":
        return articles_chrono(user_id, offset, limit, full, "user_articles.to_read")
    elif filt == "foreign":
        return articles_chrono(
            user_id,
            offset,
            limit,
            full,
            "TRUE",
            "articles.language != '%s'" % os.environ["BASE_LANGUAGE"],
        )
    elif filt == "dismissed":
        return articles_chrono(user_id, offset, limit, full, "user_articles.dismissed")
    elif filt == "mixed":
        return get_merged_articles(user_id, offset, limit, False, True)
    else:
        # filtered, keywords, latest, popular, suggestions
        return get_precomputed_articles(filt, user_id, offset, limit, full)


def get_blacklist(user_id):
    with db.connect() as conn, conn.cursor() as cur:
        cur.execute(
            "SELECT COALESCE(blacklist, '') FROM users WHERE id = %(user_id)s",
            {"user_id": user_id},
        )
        res = cur.fetchone()
        return res[0].strip()


@bottle.route("/articles.smart")
@jwt_token.require_token()
def articles_smart():
    """return list of articles depending on filter

    sample calls:
    curl -k -i -X GET http://localhost:8080/articles.smart
    """
    user_id = jwt_token.user_id()
    offset = bottle.request.query.get("offset", 0)
    filt = bottle.request.query.get("filter", "filtered")
    articles = {
        "articles": articles_smart_filter(user_id, offset, 50, filt, False),
        "blacklist": get_blacklist(user_id),
    }
    return common.render_json(articles, 200)


@bottle.route("/comments/<topic_id>")
@jwt_token.require_token()
def comments(topic_id):
    user_name = jwt_token.user_name()
    data = {"comments": discourse.get_posts(topic_id, user_name)}
    return common.render_json(data, 200)


dummy_sso = True if "DUMMY_SSO" in os.environ else False


@bottle.route("/account")
@jwt_token.require_token()
def get_account():
    if dummy_sso:
        return []
    else:
        # return the current users account balance
        user_id = int(jwt_token.user_id())
        # print('user_id = %d' % user_id)
        return get_transactions(user_id)


@bottle.route("/accounts")
@jwt_token.require_token()
def get_accounts():
    # return the account balance for all users
    with db.connect() as conn, conn.cursor() as cur:
        cur.execute(
            "WITH a AS (SELECT id AS user_id, ROW_NUMBER() over (ORDER BY balance DESC) as count, balance, username FROM users WHERE NOT inactive AND username IS NOT NULL) SELECT COALESCE(JSON_AGG(a), '[]'::json) FROM a"
        )
        res = cur.fetchone()
    return common.render_json({"accounts": res[0]}, 200)


@bottle.route("/accounts/<user_id>")
@jwt_token.require_token()
def get_transactions(user_id):
    if user_id == "-1":
        user_id = jwt_token.user_id()
    if dummy_sso:
        data = {"transactions": []}
    else:
        data = {"transactions": account.transactions(user_id)}
    return common.render_json(data, 200)


@bottle.route("/test_reminder", method="GET")
@jwt_token.require_token()
@bottle.jinja2_view("reminder.tpl", template_lookup=["views"])
def test_reminder():
    data = {
        "username": "username",
        "account": -5.0,
        "final_threshold": account.final_threshold,
        "daily_fee": account.daily_fee,
        "days_to_go": 3,
        "end_date": "2018-02-09",
        "aggregator_hostname": os.environ["AGGREGATOR_HOSTNAME"],
        "discourse_hostname": os.environ["DISCOURSE_HOSTNAME"],
        "eig_category_name": os.environ["EIG_CATEGORY_NAME"],
        "accounting_category_name": os.environ["ACCOUNTING_CATEGORY_NAME"],
        "feeds_category_name": os.environ["FEEDS_CATEGORY_NAME"],
    }
    return data


@bottle.route("/test_farewell", method="GET")
@jwt_token.require_token()
@bottle.jinja2_view("farewell.tpl", template_lookup=["views"])
def test_farewell():
    data = {
        "username": "username",
        "account": -9.99999999,
        "final_threshold": account.final_threshold,
        "daily_fee": account.daily_fee,
        "end_date": "2018-02-09",
    }
    return data


@bottle.route("/links/<url:path>", method="GET")
@jwt_token.require_token()
def check_link(url):
    # url decode strips the second forward slash
    url = url.replace("http:/", "http://")
    url = url.replace("https:/", "https://")
    common.log_info("check_link(%s)" % url)
    with db.connect() as conn, conn.cursor() as cur:
        data = {"url": url, "len": len(url)}
        # print(cur.mogrify("SELECT id FROM articles WHERE SUBSTRING(url FOR %(len)s) = %(url)s", data))
        cur.execute(
            "SELECT id FROM articles WHERE SUBSTRING(url FOR %(len)s) = %(url)s", data
        )
        res = cur.fetchone()
        if res and res[0] is not None:
            id = res[0]
        else:
            id = -1
    return common.render_json({"id": id}, 200)


if __name__ == "__main__":
    syslog.openlog("server_py", 0, syslog.LOG_LOCAL0)

    parser = argparse.ArgumentParser()
    parser.add_argument(
        "-d", "--debug", help="enable debugging and autoreloading", action="store_true"
    )
    args = parser.parse_args()

    bottle.BaseRequest.MEMFILE_MAX = 1024 * 1024

    common.log_info(
        "calo.news service started for %s" % os.environ["AGGREGATOR_HOSTNAME"]
    )
    if args.debug:
        bottle.debug(True)
        common.logger.setLevel(10)
        bottle.run(host="0.0.0.0", port=8080, reloader=True)  # , server='cherrypy')
    else:
        bottle.run(host="127.0.0.1", port=8080, reloader=False)  # , server='cherrypy')

    common.log_info(
        "calo.news service stopped for %s" % os.environ["AGGREGATOR_HOSTNAME"]
    )
    syslog.closelog()

#!/usr/bin/env python3
# coding=utf-8
#
# This file is part of calo.news: A news platform
#
# Copyright (C) 2017-2023 Paolo Greppi
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
# You should have received a copy of the GNU Affero General Public License
# along with this program (file LICENSE).
# If not, see <https://www.gnu.org/licenses/>.

import random
import sys
import syslog
import time

import common
import database as db
import dateparser
import poller
import requests
from bs4 import BeautifulSoup

syslog.openlog("poll_189_py", 0, syslog.LOG_LOCAL0)
common.log_info("entering")

headers = {
    "User-Agent": "Mozilla/5.0 (X11; Linux x86_64; rv:52.0) Gecko/20100101 Firefox/52.0"
}

base_url = "https://pagellapolitica.it"
feed_id = 189

html = requests.get(base_url, headers=headers).text
soup = BeautifulSoup(html, "lxml")
entries = []

for a in soup.select("article a"):
    url = a.get("href")
    if len(url) > 0:
        if url[0] == "/":
            url = base_url + url
        entries.append(url)
unique_entries = set(entries)
entries = sorted(list(unique_entries))

data = {}
data["feed_id"] = feed_id
data["count"] = len(entries)

with db.connect() as conn, conn.cursor() as cur:
    # http://stackoverflow.com/a/7573706
    # To remove elements from a list while iterating over it, you need to go backwards
    for e in entries[-1::-1]:
        cur.execute("SELECT id FROM articles WHERE url = %(url)s", {"url": e})
        res = cur.fetchone()
        if res:
            common.log_info("article %s already retrieved with id = %s" % (e, res))
            entries.remove(e)

data["to_retrieve"] = len(entries)
data["non_articles"] = 0
data["retrieved"] = 0
data["failed"] = 0
data["stored"] = 0
for e in entries:
    common.log_info("to retrieve: %s" % e)
    print(e)
    html = requests.get(e, headers=headers).text
    soup = BeautifulSoup(html, "lxml")
    stamps = soup.select("div.post-date > span")
    if len(stamps) == 1:
        stamp = dateparser.parse(stamps[0].get_text())
        title = soup.select(".post-title")[0].get_text()
        author = soup.select("div.post-author")[0].get_text()
        content = soup.select(".editor")[0].prettify()
        if isinstance(content, bytes):
            content = content.decode()
        res = poller.store_article(
            author=author,
            title=title,
            url=e,
            content=content,
            feed_id=feed_id,
            language="it",
            stamp=stamp,
        )
        data["retrieved"] += 1
        if res > 0:
            common.log_info("  new article %s stored with id %d" % (e, res))
        for i in range(random.randint(5, 20)):
            sys.stdout.write(".")
            sys.stdout.flush()
            time.sleep(1)
        sys.stdout.write("\n")
with db.connect() as conn, conn.cursor() as cur:
    cur.execute(
        "UPDATE feeds SET last_polled = NOW() WHERE id = %(feed_id)s",
        {"feed_id": feed_id},
    )

# poller.send_email(data)

common.log_info("done")
print("%d %d %d" % (data["retrieved"], data["failed"], data["stored"]))

syslog.closelog()

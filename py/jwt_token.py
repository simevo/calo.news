#!/usr/bin/env python3
# coding=utf-8
#
# JWT token management
#
# This file is part of calo.news: A news platform
#
# Copyright (C) 2017-2023 Paolo Greppi
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
# You should have received a copy of the GNU Affero General Public License
# along with this program (file LICENSE).
# If not, see <https://www.gnu.org/licenses/>.

import base64
import os
import time

import bottle
import jwt
import wrapt

aggregator_url = "https://%s" % os.environ["AGGREGATOR_HOSTNAME"]
dummy_sso = True if "DUMMY_SSO" in os.environ else False


def token_add(token_data):
    duration = 3600  # seconds
    new_token = stamp(token_data, duration)
    encoded = sign(token_data)
    bottle.response.set_cookie(
        "token", encoded, httponly="on", secure="on", max_age=duration, path="/"
    )
    return new_token


def stamp(token_data, duration):
    # update the iat and exp fields
    now = int(time.time())
    token_data.update(dict(iat=now, exp=now + duration))
    return token_data


def sign(token_data):
    s = secret()
    return jwt.encode(token_data, s, algorithm="HS256")


def token_keepalive():
    token_data = token_get()
    return token_add(token_data)


def secret():
    if secret.cached == "":
        try:
            with open("secrets/secret", "r") as secret_file:
                encoded_secret = secret_file.read()
        except EnvironmentError:
            raise bottle.HTTPError(status=520, body="internal error")
        secret.cached = base64.b64decode(encoded_secret)
    # print('[%s]' % secret.cached)
    return secret.cached


secret.cached = ""


def local():
    """returns true if the request is local"""
    return (bottle.request.get_header("Host") == "127.0.0.1:8080") and (
        bottle.request.get_header("X-Forwarded-For") is None
    )


def token_get(encoded=None):
    # {
    #   "nbf": 1523294070,
    #   "exp": 1523297672,
    #   "aud": "https://notizie.example.com",
    #   "iss": "https://notizie.example.com",
    #   "iat": 1523294072,
    #   "login": {
    #     "admin": "false",
    #     "external_id": "111",
    #     "username": "username",
    #     "groups": ['trust_level_0', 'trust_level_1', 'trust_level_2', 'moderatori', 'staff', 'Soci'],
    #     "email": "user.name@example.com",
    #     "nonce": "1564...",
    #     "name": "user name",
    #     "return_sso_url": "https://notizie.example.com/sso.php",
    #     "moderator": "true"
    #   }
    # }
    if not encoded:
        encoded = bottle.request.get_cookie("token")
    if not encoded:
        return None
    token_data = decode(encoded)
    return token_data


def decode(encoded):
    s = secret()
    try:
        d = jwt.decode(encoded, s, algorithms=["HS256"], audience=aggregator_url)
    except Exception:
        raise bottle.HTTPError(status=401, body="authentication error")
    return d


# public functions:


def user_id():
    if dummy_sso:
        return 1
    else:
        token_data = token_get()
        return token_data["login"]["external_id"] if token_data else None


def user_name():
    if dummy_sso:
        return "john"
    else:
        token_data = token_get()
        return token_data["login"]["username"] if token_data else None


def user_email():
    if dummy_sso:
        return "john.doe@example.com"
    else:
        token_data = token_get()
        return token_data["login"]["email"] if token_data else None


def user_groups():
    if dummy_sso:
        return ["soci", "staff"]
    else:
        token_data = token_get()
        return token_data["login"]["groups"] if token_data else []


def require_token():
    @wrapt.decorator
    def require_wrapper(wrapped, instance, args, kwargs):
        if dummy_sso or local():
            return wrapped(*args, **kwargs)
        else:
            token_data = token_get()
            if not token_data:
                raise bottle.HTTPError(status=401, body="login failure")
            token_data = token_keepalive()
            return wrapped(*args, **kwargs)

    return require_wrapper

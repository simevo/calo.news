#!/usr/bin/env python3
# coding=utf-8
#
# precompute cache
# design basis: 32000 articles / month = 8000 articles / week
#
# return articles filtered (by tags and languages) and ranked by score
# - filtered: 8000
# return articles in reverse chronological order filtered according user-selected keywords and authors
# - keywords: 16000
# return latest short articles
# - latest: 2000
# extract a list of suggested, unread articles
# - suggestions: 8000
#
# sample invocation
#   ./py/precompute.py
#
# This file is part of calo.news: A news platform
#
# Copyright (C) 2017-2023 Paolo Greppi
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
# You should have received a copy of the GNU Affero General Public License
# along with this program (file LICENSE).
# If not, see <https://www.gnu.org/licenses/>.

import argparse
import os
import syslog

import common
import database as db
import discourse
from psycopg2 import extras

base_language = os.environ["BASE_LANGUAGE"]
dummy_sso = True if "DUMMY_SSO" in os.environ else False

queries = {
    "filtered": """
        WITH u AS (
          SELECT
            tags AS my_tags,
            languages AS my_languages
          FROM
            users
          WHERE
            id=%(user_id)s),
        uf AS (
          SELECT
            feed_id,
            rating
          FROM
            user_feeds
          WHERE
            user_id=%(user_id)s),
        f AS (
          SELECT
            id AS feed_id,
            uf.rating AS my_feed_rating
          FROM
            u,
            feeds LEFT JOIN uf ON feeds.id = uf.feed_id
          WHERE
            u.my_tags IS NULL
            OR u.my_tags = array[]::text[]
            OR feeds.tags && u.my_tags),
        aaa AS (
          SELECT
            articles.id,
            articles.stamp,
            articles.comments,
            f.*
          FROM
            u,
            f INNER JOIN articles ON articles.feed_id = f.feed_id
          WHERE
            u.my_languages IS NULL
            OR u.my_languages = array[]::text[]
            OR articles.language = ANY(u.my_languages)
            OR (%(base_language)s = ANY(u.my_languages) AND articles.title IS NOT NULL)
          ORDER BY id DESC
          LIMIT 8000),
        ua AS (
          SELECT
            article_id,
            rating,
            read
          FROM
            user_articles
          WHERE
            user_id=%(user_id)s),
        aa AS (
          SELECT
            aaa.*,
            COALESCE(ua.rating, 0) AS my_rating,
            COALESCE(ua.read, FALSE) AS read
          FROM
            aaa
            LEFT JOIN ua ON aaa.id = ua.article_id),
        a AS (
          SELECT
            aa.id,
            aa.stamp,
            (
              1 +
              %(sociality_weight)s * (articles_data.views + articles_data.to_reads + aa.comments + GREATEST(0, articles_data.rating)) +
              %(my_weight)s * (GREATEST(0, aa.my_rating + CASE WHEN feeds.premium THEN 2 ELSE 0 END + COALESCE(my_feed_rating, COALESCE(feeds.rating, 0))))
            ) / POWER(GREATEST(1.0, EXTRACT(EPOCH FROM (NOW() - aa.stamp))) / (%(age_divider)s * SQRT(1 + feeds_data.average_time_from_last_post)) - LEAST(0, %(sociality_weight)s * (articles_data.rating + CASE WHEN feeds.premium THEN 2 ELSE 0 END) + %(my_weight)s * (aa.my_rating + COALESCE(my_feed_rating, COALESCE(feeds.rating, 0)))), 1.5) AS score
          FROM
            aa
            INNER JOIN articles_data ON articles_data.id = aa.id
            INNER JOIN feeds ON aa.feed_id = feeds.id
            INNER JOIN feeds_data ON feeds.id = feeds_data.id
          WHERE
            NOT read
          ORDER BY score DESC, stamp DESC
          LIMIT %(limit)s)
        SELECT
          JSON_AGG(ROW_TO_JSON(a)) FROM a""",
    "keywords": """
        WITH u AS (
          SELECT
            whitelist,
            whitelist_authors
          FROM
            users
          WHERE
            id=%(user_id)s),
        aa AS (
          SELECT
            articles.id,
            articles.tsv,
            articles.author,
            articles.stamp
          FROM
            articles
            LEFT OUTER JOIN user_articles ON
              articles.id = user_articles.article_id
              AND (%(user_id)s IS NULL OR user_articles.user_id = %(user_id)s)
          WHERE
            NOT COALESCE(user_articles.read, FALSE)
          ORDER BY id DESC
          LIMIT 16000),
        a AS (
          SELECT
            aa.id
          FROM
            u,
            aa
          WHERE
            tsv @@ to_tsquery('pg_catalog.italian', REPLACE(TRIM(u.whitelist), ' ', '|')) OR author ~* ('.*(' || REPLACE(TRIM(u.whitelist_authors), ' ', '|') || ').*')
          ORDER BY stamp DESC
          LIMIT %(limit)s)
        SELECT
          JSON_AGG(ROW_TO_JSON(a)) FROM a""",
    "latest": """
        WITH u AS (
          SELECT
            languages AS my_languages
          FROM
            users
          WHERE
            id=%(user_id)s),
        f AS (
          SELECT
            id AS feed_id
          FROM
            feeds
          WHERE
            'notiziario' = ANY(feeds.tags)),
        aaa AS (
          SELECT
            articles.id,
            f.feed_id
          FROM
            u,
            f INNER JOIN articles ON articles.feed_id = f.feed_id
          WHERE
            u.my_languages IS NULL
            OR u.my_languages = array[]::text[]
            OR articles.language = ANY(u.my_languages)
            OR (%(base_language)s = ANY(u.my_languages) AND articles.title IS NOT NULL)
          ORDER BY id DESC
          LIMIT 2000),
        aa AS (
          SELECT
            aaa.id,
            AVG(user_feeds.rating)::INTEGER AS my_feed_rating,
            COALESCE(AVG(user_articles.rating)::INTEGER, 0) AS my_rating,
            COALESCE(COUNT(CASE WHEN user_articles.read THEN 1 END) > 0, FALSE) AS read
          FROM
            aaa
            LEFT OUTER JOIN user_feeds ON aaa.feed_id = user_feeds.feed_id AND user_feeds.user_id = %(user_id)s
            LEFT OUTER JOIN user_articles ON aaa.id = user_articles.article_id AND user_articles.user_id = %(user_id)s
          GROUP BY aaa.id),
        a AS (
          SELECT
            aa.id,
            ((articles_data.views + articles.comments + articles_data.to_reads + aa.my_rating) * 2 + 1) / POWER(GREATEST(1.0, EXTRACT(EPOCH FROM (NOW() - articles.stamp)) / 100 - CASE WHEN feeds.premium THEN 2 ELSE 0 END - COALESCE(my_feed_rating, COALESCE(feeds.rating, 0)) - articles_data.rating), 2) AS score
          FROM
            articles
            INNER JOIN aa ON aa.id = articles.id
            INNER JOIN articles_data ON articles_data.id = articles.id
            INNER JOIN feeds ON articles.feed_id = feeds.id
          WHERE
            NOT read
            AND COALESCE(LENGTH(articles.content), LENGTH(articles.content_original)) < 5000
          ORDER BY score DESC
          LIMIT %(limit)s)
        SELECT
          JSON_AGG(ROW_TO_JSON(a)) FROM a""",
    "popular": """
        WITH a AS (
          SELECT
            articles_data.id AS id
          FROM
            articles_data
            LEFT OUTER JOIN user_articles
            ON articles_data.id = user_articles.article_id AND user_articles.user_id = %(user_id)s
          WHERE
            articles_data.views > 0 AND NOT COALESCE(user_articles.read, FALSE)
          ORDER BY id DESC
          LIMIT %(limit)s)
        SELECT
          JSON_AGG(ROW_TO_JSON(a)) FROM a""",
    "suggestions": """
        WITH bb AS (
          SELECT
            STRING_AGG(q, '|') AS query
          FROM
            users,
            JSONB_ARRAY_ELEMENTS_TEXT(bow) q
          WHERE
            id=%(user_id)s),
        ua AS (
          SELECT
            article_id,
            read
          FROM
            user_articles
          WHERE
            user_id=%(user_id)s),
        aa AS (
          SELECT
            articles.id,
            articles.tsv
          FROM
            articles
            LEFT JOIN ua ON articles.id = ua.article_id
          WHERE
            title IS NOT NULL
            AND NOT COALESCE(ua.read, FALSE)
          ORDER BY
            articles.id DESC
          LIMIT 8000),
        a AS (
          SELECT
            aa.id,
            TS_RANK(aa.tsv, bb.query::tsquery)/(10+LENGTH(aa.tsv)) AS score
          FROM
            aa,
            bb
          ORDER BY score DESC
          LIMIT %(limit)s)
        SELECT
          JSON_AGG(ROW_TO_JSON(a)) FROM a""",
}


def get_users():
    with db.connect() as conn, conn.cursor(cursor_factory=extras.RealDictCursor) as cur:
        cur.execute(
            "SELECT id, username FROM users WHERE id > 0 AND NOT inactive AND username IS NOT NULL ORDER BY id"
        )
        return cur.fetchall()


def articles(view, user_id, limit):
    common.log_info("precomputing view %s" % view)
    with db.connect() as conn, conn.cursor() as cur:
        cur.execute(
            queries[view],
            {
                "user_id": user_id,
                "limit": limit,
                "age_divider": 1000,
                "my_weight": 10,
                "sociality_weight": 10,
                "base_language": base_language,
            },
        )
        res = cur.fetchone()
        if res and res[0] is not None:
            ids = [i["id"] for i in res[0]]
            cur.execute(
                """
                INSERT
                  INTO precomputed (user_id, view, articles)
                  VALUES (%(user_id)s, %(view)s, %(ids)s)
                ON CONFLICT (user_id, view) DO
                  UPDATE
                    SET articles = %(ids)s
                    WHERE precomputed.user_id = %(user_id)s AND precomputed.view = %(view)s""",
                {"user_id": user_id, "view": view, "ids": ids},
            )


if __name__ == "__main__":
    syslog.openlog("precompute_py", 0, syslog.LOG_LOCAL0)
    common.log_info("entering")
    parser = argparse.ArgumentParser(description="Precompute cache")
    parser.add_argument(
        "view",
        help="name of the view, one of: all, filtered, keywords, latest, popular or suggestions",
    )
    args = parser.parse_args()
    limit = 100
    if dummy_sso:
        users = get_users()
    else:
        # get list of users who are members of the group "soci"
        users = discourse.get_active_users()
    print(users)
    if args.view == "all":
        for u in users:
            common.log_info(
                "looking at user: %s with id: %d" % (u["username"], u["id"])
            )
            articles("filtered", u["id"], limit)
            articles("keywords", u["id"], limit)
            articles("latest", u["id"], limit)
            articles("suggestions", u["id"], limit)
            articles("popular", u["id"], limit)
    else:
        for u in users:
            common.log_info(
                "looking at user: %s with id: %d" % (u["username"], u["id"])
            )
            articles(args.view, u["id"], limit)

    common.log_info("done")
    syslog.closelog()

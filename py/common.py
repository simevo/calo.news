#!/usr/bin/env python3
# coding=utf-8
#
# common utilities for route definition """
#
# This file is part of calo.news: A news platform
#
# Copyright (C) 2017-2023 Paolo Greppi
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
# You should have received a copy of the GNU Affero General Public License
# along with this program (file LICENSE).
# If not, see <https://www.gnu.org/licenses/>.

import json
import logging
import syslog

import bottle
import wrapt

logger = logging.getLogger("calonews")


def render_json(d, code):
    """sends the supplied JSON object as response
    if the request header accepts application/json, the JSON is dumped in compact form,
    else it is prettyprinted and wrapped in a <pre> HTML element
    """
    accept = bottle.request.headers.get("accept")
    bottle.response.status = code
    if accept.find("application/json") >= 0:
        bottle.response.set_header("content-type", "application/json")
        return json.dumps(d)
    else:
        return "<pre>" + json.dumps(d, indent=2) + "</pre>\n"


@wrapt.decorator
def logme(wrapped, instance, args, kwargs):
    fname = wrapped.__name__
    log_debug("entering %s" % fname)
    r = wrapped(*args, **kwargs)
    log_debug("done %s" % fname)
    return r


def log_debug(message):
    syslog.syslog(syslog.LOG_DEBUG, message)
    logger.debug(message)


def log_info(message):
    syslog.syslog(syslog.LOG_INFO, message)
    logger.info(message)


def log_warning(message):
    syslog.syslog(syslog.LOG_WARNING, message)
    logger.warning(message)


def log_error(message):
    syslog.syslog(syslog.LOG_ERR, message)
    logger.error(message)


def log_critical(message):
    syslog.syslog(syslog.LOG_CRIT, message)
    logger.critical(message)

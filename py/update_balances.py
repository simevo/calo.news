#!/usr/bin/env python3
# coding=utf-8
#
# update balances:
# - for all users who are members of the "soci" group, deduct the daily fee
# - process all posts in the "contabiltà" subcategory which happened yesterday
#
# invoke this script only once per day !
#
# sample invocation
#   ./update_balances.py
#
#
# This file is part of calo.news: A news platform
#
# Copyright (C) 2017-2023 Paolo Greppi
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
# You should have received a copy of the GNU Affero General Public License
# along with this program (file LICENSE).
# If not, see <https://www.gnu.org/licenses/>.

import syslog

import account
import common
import database as db
import discourse


def add_amount(cur, amount, user_id):
    data = {"amount": amount, "user_id": user_id}
    cur.execute(
        """
        INSERT
          INTO users(id)
          VALUES (%(user_id)s)
        ON CONFLICT (id) DO NOTHING""",
        data,
    )
    cur.execute(
        """
        INSERT
          INTO user_transactions(user_id, date, amount)
          VALUES (%(user_id)s, LOCALTIMESTAMP::DATE, %(amount)s)
        ON CONFLICT (user_id, date) DO NOTHING""",
        data,
    )


if __name__ == "__main__":
    syslog.openlog("update_balances_py", 0, syslog.LOG_LOCAL0)
    common.log_info("entering")
    # get list of users who are members of the group "soci"
    users = discourse.get_active_users()
    with db.connect() as conn, conn.cursor() as cur:
        cur.execute(
            """
          SELECT
            MAX(date),
            NOW(),
            NOW()::DATE,
            LOCALTIMESTAMP,
            LOCALTIMESTAMP::DATE
          FROM
            user_transactions"""
        )
        res = cur.fetchone()
        common.log_info(
            "max date = %s\nnow = %s\nnow date = %s\nlocal time stamp = %s\nlocal time stamp date = %s"
            % (res[0], res[1], res[2], res[3], res[4])
        )
        for u in users:
            common.log_info(
                "looking at user: %s with id: %d" % (u["username"], u["id"])
            )
            add_amount(cur, -account.daily_fee, u["id"])

    accounts = account.accounts(email=True, username=True)
    with db.connect() as conn, conn.cursor() as cur:
        for a in accounts:
            cur.execute(
                """
              UPDATE
                users
              SET
                balance=%(balance)s,
                email=%(email)s,
                username=%(username)s
              WHERE id = %(user_id)s""",
                a,
            )

    common.log_info("done")
    syslog.closelog()

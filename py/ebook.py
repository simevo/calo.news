#!/usr/bin/env python3
# coding=utf-8
#
# calonews epub writer
#
# This file is part of calo.news: A news platform
#
# Copyright (C) 2017-2023 Paolo Greppi
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
# You should have received a copy of the GNU Affero General Public License
# along with this program (file LICENSE).
# If not, see <https://www.gnu.org/licenses/>.

import os
import subprocess
import tempfile
import time
import uuid

import jinja2
from ebooklib import epub

aggregator_hostname = os.environ["AGGREGATOR_HOSTNAME"]
base_language = os.environ["BASE_LANGUAGE"]


def export_epub(articles):
    book = epub.EpubBook()

    # set metadata
    book.set_identifier("id123456")
    book.set_title(
        "%s %s"
        % (aggregator_hostname, time.strftime("%Y-%m-%dT%H:%M:%S", time.gmtime()))
    )
    book.set_language(base_language)

    book.add_author("calomelano")

    # create chapters
    cs = []  # chapters
    fs = {}  # chapters grouped by feed_id
    fn = {}  # feed name for each feed_id
    spine = ["nav"]
    i = 1
    for data in articles:
        print("adding article %(title)s to feed %(feed)s" % data)
        data["minutes"] = int(data["length"] / 6 / 300)

        if not data["title"] and not data["title_original"]:
            title_combined = "[Senza titolo]"
        elif data["language"] == base_language:
            title_combined = "%(title)s" % data
        else:
            if data["title"]:
                title_combined = "[%(title_original)s] %(title)s" % data
            else:
                title_combined = "%(title_original)s" % data
        data["title_combined"] = title_combined

        if data["language"] == base_language:
            content_combined = "<div>%(content)s</div>" % data
        elif data["content"]:
            content_combined = (
                """<h3>Testo tradotto</h3>
<a href="#native_%(id)s">Vai al testo in lingua originale</a>
<div>%(content)s</div>
<br/>
<h3 id="native_%(id)s">Testo in lingua originale</h3>
<div>%(content_original)s</div>"""
                % data
            )
        else:
            content_combined = "<div>%(content_original)s</div>" % data
        data["content_combined"] = content_combined
        data["aggregator_hostname"] = aggregator_hostname

        c = epub.EpubHtml(
            title=title_combined, file_name="chap_%d.xhtml" % i, lang=base_language
        )
        i += 1
        c.content = (
            """<h1>%(title_combined)s</h1>
<p><strong>Pubblicato</strong>: %(stamp)s</p>
<p><strong>Di</strong>: %(author)s %(license)s</p>
<p><strong>Da</strong>: %(feed)s</p>
<p><strong>Tempo di lettura stimato</strong>: %(minutes)d minuti</p>
%(content_combined)s
<p><strong>Commenta</strong>: <a href="https://%(aggregator_hostname)s/article/%(id)s">https://%(aggregator_hostname)s/article/%(id)s</a></p>
<p><strong>Vai all'articolo originale</strong>: <a href="%(url)s">%(url)s</a></p>"""
            % data
        )
        cs.append(c)
        feed_id = data["feed_id"]
        if feed_id in fs:
            fs[feed_id] = fs[feed_id] + (c,)
        else:
            fs[feed_id] = (c,)
            fn[feed_id] = data["feed"]
        # add chapter
        book.add_item(c)
        # add chapter to spine
        spine.append(c)

    # define Table Of Contents
    toc = ()
    for f in list(fs.keys()):
        print("adding feed %s" % fn[f])
        toc = toc + ((epub.Section(fn[f]), fs[f]),)
    book.toc = toc

    # add default NCX and Nav file
    book.add_item(epub.EpubNcx())
    book.add_item(epub.EpubNav())

    # define CSS style
    style = "BODY {color: white;}"
    nav_css = epub.EpubItem(
        uid="style_nav", file_name="style/nav.css", media_type="text/css", content=style
    )

    # add CSS file
    book.add_item(nav_css)

    # basic spine
    book.spine = spine

    f = tempfile.NamedTemporaryFile(delete=True)
    f.close()

    # write to the file
    epub.write_epub(f.name, book, {})
    f = open(f.name, mode="rb")
    book = f.read()
    f.close()
    os.unlink(f.name)
    return book


def process_template(jinja_environment, template_file, data, filename):
    template = jinja_environment.get_template(template_file)
    output = template.render(data)
    f = open(filename, "w+")
    f.write(output)
    f.close()


def export_mobi(articles):
    f = tempfile.NamedTemporaryFile(delete=True)
    f.close()
    f_html = f.name + ".html"
    f_toc_nav = f.name + "_toc_nav.xhtml"
    f_opf = f.name + ".opf"
    f_svg = f.name + ".svg"
    f_png = f.name + ".png"
    f_mobi = f.name + ".mobi"
    # kindlegen wants the output file to not contain the directory path
    f_mobi_stripped = os.path.basename(f_mobi)
    f_html_stripped = os.path.basename(f_html)
    now = time.gmtime()
    data = {
        "articles": articles,
        "title": "Newsletter %s" % time.strftime("%Y-%m-%dT%H:%M:%SZ", now),
        "uuid": uuid.uuid4(),
        "description": "",
        "date": time.strftime("%Y-%m-%d", now),
        "time": time.strftime("%H:%M:%SZ", now),
        "cover": f_png,
        "toc_nav": f_toc_nav,
        "html": f_html,
        "html_stripped": f_html_stripped,
        "aggregator_hostname": os.environ["AGGREGATOR_HOSTNAME"],
    }
    template_dir = "views"
    loader = jinja2.FileSystemLoader(template_dir)
    environment = jinja2.Environment(loader=loader)
    process_template(environment, "html.tpl", data, f_html)
    process_template(environment, "toc_nav.tpl", data, f_toc_nav)
    process_template(environment, "opf.tpl", data, f_opf)
    process_template(environment, "cover.tpl", data, f_svg)
    f = open(f_png, "wb")
    subprocess.call(["rsvg-convert", f_svg], stdout=f)
    f.close()
    subprocess.call(["/opt/kindlegen/kindlegen", f_opf, "-o", f_mobi_stripped])
    f = open(f_mobi, "rb")
    book = f.read()
    f.close()
    # os.unlink(f_mobi)
    return book

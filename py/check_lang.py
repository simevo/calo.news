#!/usr/bin/env python3
#
# check language with https://github.com/saffsd/langid.py
#
# first time:
#   python3 -m venv venv --system-site-packages
#   source ./venv/bin/activate
#
# next time:
#   ./venv/bin/pip3 install --upgrade langid
#   ./py/check_lang.py
#
# This file is part of calo.news: A news platform
#
# Copyright (C) 2017-2023 Paolo Greppi
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
# You should have received a copy of the GNU Affero General Public License
# along with this program (file LICENSE).
# If not, see <https://www.gnu.org/licenses/>.

import os
import re

import database as db
import langid

base_language = os.environ["BASE_LANGUAGE"]

with db.connect() as conn, conn.cursor("my_cursor") as cur:
    cur.execute(
        """
        SELECT
          id,
          title,
          content,
          feed_id
        FROM articles
        WHERE language = %(base_language)s
        ORDER BY id""",
        {"base_language": base_language},
    )
    for article in cur:
        id = article[0]
        title = article[1]
        content = article[2]
        feed_id = article[3]
        stripped_content = re.sub(r"[\s]+", " ", re.sub(r"<.+?>", "", content))
        trimmed_content = " ".join(stripped_content.split())
        if len(trimmed_content) > 0:
            text = title + ". " + trimmed_content
            lang = langid.classify(text)
            if lang[0] != base_language:
                print("%d %d [%s]: %s" % (feed_id, id, title, lang[0]))

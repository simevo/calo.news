#!/usr/bin/env python3
# coding=utf-8
#
# calonews RSS asynchronous feed poller
#
# This file is part of calo.news: A news platform
#
# Copyright (C) 2017-2023 Paolo Greppi
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
# You should have received a copy of the GNU Affero General Public License
# along with this program (file LICENSE).
# If not, see <https://www.gnu.org/licenses/>.

import asyncio
import datetime
import html.parser
import http.cookiejar
import json
import os
import subprocess
import syslog
import time
import urllib

import aiohttp
import common
import database as db
import email_local
import feedparser
import lxml.html
import normalize
import requests
import tor
from bs4 import BeautifulSoup
from jinja2 import Environment, FileSystemLoader

time_format = "%Y-%m-%dT%H:%M:%SZ"
base_language = os.environ["BASE_LANGUAGE"]


def clean(s):
    t = html.unescape(s)
    u = lxml.html.fromstring(t)
    return u.text_content()


# https://stackoverflow.com/a/13565185
def last_day_of_month(any_day):
    next_month = any_day.replace(day=28) + datetime.timedelta(
        days=4
    )  # this will never fail
    return next_month - datetime.timedelta(days=next_month.day)


# returns:
# - 0 if no article is inserted (duplicate url)
# - -1 on failure
# - a strictly positive integer (the id of the inserted article) on success
def store_article(
    author, title, url, content, feed_id, language, stamp=0, salt_url=False, exclude=""
):
    parsed_url = urllib.parse.urlparse(url)
    base_url = urllib.parse.urlunsplit(
        (parsed_url.scheme, parsed_url.netloc, "", "", "")
    )
    content = normalize.normalize(content, base_url, exclude)
    if author:
        author = clean(author)
    if title:
        title = clean(title)
    content_field = "content" if language == base_language else "content_original"
    title_field = "title" if language == base_language else "title_original"
    if stamp == 0:
        stamp = time.strftime(time_format, time.gmtime())
    data = {
        "author": author,
        "title": title,
        "stamp": stamp,
        "url": url,
        "content": content,
        "feed_id": feed_id,
        "language": language,
    }
    with db.connect() as conn, conn.cursor() as cur:
        try:
            if salt_url:
                cur.execute(
                    """
                    INSERT INTO articles
                        (author, {}, stamp, url, {}, feed_id, language)
                    SELECT
                        %(author)s, %(title)s, LEAST(NOW(), %(stamp)s), %(url)s, %(content)s, %(feed_id)s, %(language)s
                    WHERE NOT EXISTS (
                        SELECT url
                        FROM articles
                        WHERE
                        feed_id=%(feed_id)s AND
                            (url = %(url)s AND STRPOS(%(url)s, '#') = 0) OR
                            (
                            regexp_replace(url, '#[0-9]*', '') = regexp_replace(%(url)s, '#[0-9]*', '')
                            AND EXTRACT(EPOCH FROM NOW() - stamp) < 24*3600*7
                            )
                        )
                    RETURNING id""".format(
                        title_field, content_field
                    ),
                    data,
                )
            else:
                cur.execute(
                    """
                    INSERT INTO articles
                        (author, {}, stamp, url, {}, feed_id, language)
                    SELECT
                        %(author)s, %(title)s, LEAST(NOW(), %(stamp)s), %(url)s, %(content)s, %(feed_id)s, %(language)s
                    WHERE NOT EXISTS (
                        SELECT url
                        FROM articles
                        WHERE url = %(url)s
                        )
                    RETURNING id""".format(
                        title_field, content_field
                    ),
                    data,
                )
        except Exception:
            common.log_error("INSERT exception")
            return -1
        res = cur.fetchone()
        id = res[0] if res else 0
        if id > 0:
            cur.execute(
                """
                INSERT INTO articles_data
                  SELECT articles_data_view.*
                  FROM articles_data_view
                  WHERE id = %(id)s
                ON CONFLICT (id) DO UPDATE
                  SET
                    epoch = EXCLUDED.epoch,
                    views = EXCLUDED.views,
                    rating = EXCLUDED.rating,
                    to_reads = EXCLUDED.to_reads,
                    length = EXCLUDED.length""",
                {"id": id},
            )
    return id


async def retrieve_asy(client, e, args, verbose):
    retrieved = 0
    failed = 0
    stored = 0
    content = ""
    url = e["link"]
    if "suffix" in args:
        suffix = args["suffix"]
    else:
        suffix = ""
    if args["incomplete"] and e.link:
        # use readability service to extract the content
        if args["tor"]:
            # Tor uses the 9050 port as the default socks port
            proxy = "socks5://127.0.0.1:9050"
        else:
            proxy = None
        try:
            async with client.get(url=e.link + suffix, proxy=proxy) as response:
                if verbose:
                    common.log_debug("queueing %s" % e.link)
                html = await response.read()
                if response.status == 200:
                    retrieved += 1
                    html = html.decode("utf-8", "ignore")
                    soup = BeautifulSoup(html, "lxml")
                    for tag in soup.find_all(True):
                        if tag.name in ["style", "script", "svg", "picture"]:
                            tag.extract()
                    if args["exclude"] and args["exclude"] != "":
                        for element in soup.select(args["exclude"]):
                            element.extract()
                    content_sanitized = soup.prettify()
                    headers = {"Content-Type": "text/html; charset=UTF-8"}
                    r = requests.post(
                        "http://localhost:8081?href=" + e.link,
                        headers=headers,
                        data=content_sanitized.encode("utf-8"),
                    )
                    # r.encoding = r.apparent_encoding
                    content = r.content
                else:
                    failed += 1
                    common.log_error(
                        "url %s returned error status %d " % (e.link, response.status)
                    )
                    if verbose:
                        common.log_debug("content: %s" % html)
        except Exception:
            common.log_error("exception while asynchronously retrieving %s" % e.link)
            failed += 1
    elif "content" in e:
        for c in e["content"]:
            content += c.value
    elif "summary" in e:
        content = e["summary"]
    else:
        failed += 1
        common.log_error("url %s has no content and no summary" % (e.link))

    if isinstance(content, bytes):
        content = content.decode()

    if args["salt_url"]:
        # add some cruft to the urls so that they are unique
        url = "%s#%d" % (url, int(time.time()))
    if len(content) > 0:
        id = store_article(
            author=e.get("author", ""),
            title=e.get("title", ""),
            url=url,
            content=content,
            feed_id=args["id"],
            language=args["language"],
            stamp=e.get("stamp", 0),
            salt_url=args["salt_url"],
            exclude=args["exclude"],
        )
        if id > 0:
            stored += 1
            if verbose:
                common.log_info(
                    "new article %s asynchronously stored with id %d" % (e["link"], id)
                )
        elif id < 0:
            failed += 1
    return (retrieved, failed, stored)


async def main(loop, entries, args, verbose):
    cookies = {}
    if args["cookies"] and args["cookies"] != "":
        cj = http.cookiejar.MozillaCookieJar(args["cookies"])
        cj.load()
        for c in cj:
            cookies[c.name] = c.value
    headers = {
        "User-Agent": "Mozilla/5.0 (X11; Linux x86_64; rv:52.0) Gecko/20100101 Firefox/52.0"
    }
    async with aiohttp.ClientSession(
        loop=loop, cookies=cookies, headers=headers
    ) as client:
        return await asyncio.gather(
            *[retrieve_asy(client, e, args, verbose) for e in entries]
        )


class Poller(object):
    verbose = False
    in_feed_total = 0
    to_retrieve_total = 0
    stored_total = 0
    retrieved_total = 0
    failed_total = 0
    already_retrieved_total = 0

    def __init__(self, args):
        self.in_feed = 0
        self.to_retrieve = 0
        self.stored = 0
        self.retrieved = 0
        self.failed = 0
        self.already_retrieved = 0
        self.args = args
        syslog.openlog("poller_py", 0, syslog.LOG_LOCAL0)
        if self.verbose:
            common.log_debug(json.dumps(args))

    @common.logme
    def invoke(self, script):
        p = subprocess.Popen(
            script,
            stderr=subprocess.STDOUT,
            stdout=subprocess.PIPE,
            stdin=subprocess.PIPE,
        )
        try:
            output = p.communicate(timeout=200)
        except subprocess.TimeoutExpired:
            p.kill()
            common.log_error("timeout while invoking script %s" % script)
            return (0, 0, 0)
        if p.returncode != 0:
            common.log_error("code %d returned by script %s" % (p.returncode, script))
            return (0, 0, 0)
        lines = output[0].decode().split("\n")
        last_line = lines[-1] if lines[-1] else lines[-2]
        return last_line.split(" ")

    @common.logme
    def poll(self):
        if not self.args["active"]:
            common.log_info("skipping feed %d because non-active" % self.args["id"])
            return

        if self.args["frequency"]:
            # polling will happen only if the current UTC date and time satisfy all conditions
            # frequency is in JSON format: { "day": [1, 2, 3], "hour": [12, 13], "weekday": [7] })
            # hours are 0-24 in UTC timezone
            # days are the day in the month (a negative number counts from the end)
            # weekdays are the day of the week, where Monday is 1 and Sunday is 7
            frequency = json.loads(self.args["frequency"])
            now = datetime.datetime.utcnow()
            hour = now.hour
            day = now.day
            weekday = (
                now.weekday() + 1
            )  # https://docs.python.org/3.7/library/datetime.html#datetime.datetime.weekday returns Monday as 0 and Sunday as 6
            if "hour" in frequency and hour not in frequency["hour"]:
                common.log_info(
                    "skipping feed %d because frequency = %s and hour = %d"
                    % (self.args["id"], self.args["frequency"], hour)
                )
                return
            if "weekday" in frequency and weekday not in frequency["weekday"]:
                common.log_info(
                    "skipping feed %d because frequency = %s and weekday = %d"
                    % (self.args["id"], self.args["frequency"], weekday)
                )
                return
            if "day" in frequency:
                ldom = last_day_of_month(now).day
                for i, d in enumerate(frequency["day"]):
                    if d < 0:
                        frequency["day"][i] = d + ldom + 1
                if day not in frequency["day"]:
                    common.log_info(
                        "skipping feed %d because frequency = %s and day = %d"
                        % (self.args["id"], self.args["frequency"], day)
                    )
                    return

        common.log_info("polling feed %d" % self.args["id"])

        if self.args["script"]:
            common.log_info("using script %s" % self.args["script"])
            results = self.invoke(self.args["script"])
            if results and len(results) == 3:
                self.retrieved += int(results[0])
                self.failed += int(results[1])
                self.stored += int(results[2])
        else:
            feed = feedparser.parse(self.args["url"])
            self.in_feed = len(feed["entries"])
            if self.args["incomplete"]:
                # prune from the feed the articles we already pulled
                with db.connect() as conn, conn.cursor() as cur:
                    # http://stackoverflow.com/a/7573706
                    # To remove elements from a list while iterating over it, you need to go backwards
                    for e in feed["entries"][-1::-1]:
                        cur.execute(
                            "SELECT id FROM articles WHERE url = %(url)s",
                            {"url": e["link"]},
                        )
                        res = cur.fetchone()
                        if res:
                            if self.verbose:
                                common.log_debug(
                                    "article %s already retrieved with id = %s"
                                    % (e["link"], res)
                                )
                            feed["entries"].remove(e)
                            self.already_retrieved += 1
                if self.args["tor"]:
                    tor.renew_connection()

            self.to_retrieve = len(feed["entries"])
            if self.verbose:
                for e in feed["entries"]:
                    common.log_info("to retrieve: %s" % e["link"])

            for e in feed["entries"]:
                if "published_parsed" in e and e["published_parsed"]:
                    if e["published_parsed"] > time.gmtime():
                        e["stamp"] = time.strftime(time_format, time.gmtime())
                    else:
                        e["stamp"] = time.strftime(time_format, e["published_parsed"])
                else:
                    e["stamp"] = time.strftime(time_format, time.gmtime())

            entries = sorted(feed["entries"], key=lambda e: e["stamp"])
            loop = asyncio.new_event_loop()
            results = loop.run_until_complete(
                main(loop, entries, self.args, self.verbose)
            )
            for r in results:
                self.retrieved += r[0]
                self.failed += r[1]
                self.stored += r[2]

        with db.connect() as conn, conn.cursor() as cur:
            cur.execute(
                "UPDATE feeds SET last_polled = NOW() WHERE id = %(feed_id)s",
                {"feed_id": self.args["id"]},
            )

        Poller.in_feed_total += self.in_feed
        Poller.already_retrieved_total += self.already_retrieved
        Poller.to_retrieve_total += self.to_retrieve
        Poller.failed_total += self.failed
        Poller.retrieved_total += self.retrieved
        Poller.stored_total += self.stored


def send_email(data):
    env = Environment(loader=FileSystemLoader("views"))
    template = env.get_template("poller.tpl")
    email_content = template.render(data)
    subject = "[calomelano social club] updated feed %d" % data["feed_id"]
    email_local.send_email(
        to=os.environ["ADMIN_EMAIL"], subject=subject, content=email_content
    )

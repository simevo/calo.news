#!/usr/bin/env python3
# coding=utf-8
#
# calonews article normalization
# sample invocation:
#   ./normalize.py filename.html http://example.com
#
# This file is part of calo.news: A news platform
#
# Copyright (C) 2017-2023 Paolo Greppi
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
# You should have received a copy of the GNU Affero General Public License
# along with this program (file LICENSE).
# If not, see <https://www.gnu.org/licenses/>.

import copy
import urllib

from bs4 import BeautifulSoup, Comment, NavigableString, Tag


def normalize(content, base_url=None, exclude=None):
    html = " ".join(line.strip() for line in content.split("\n"))
    soup = BeautifulSoup(html, "lxml")

    # remove blacklisted tags
    for name in [
        "img",
        "figcaption",
        "figure",
        "hr",
        "source",
        "object",
        "video",
        "audio",
        "track",
        "embed",
        "param",
        "map",
        "area",
        "form",
        "input",
        "button",
        "canvas",
        "style",
        "script",
        "svg",
        "picture",
    ]:
        for element in soup.find_all(name):
            element.extract()

    # remove excluded tags
    if exclude and exclude != "":
        for element in soup.select(exclude):
            element.extract()

    # turn divs into paragraphs
    for div in soup.find_all("div"):
        div.name = "p"

    # unpack graylisted tags
    for tag in soup.find_all(True):
        if tag.name not in [
            "a",
            "p",
            "i",
            "strong",
            "b",
            "br",
            "table",
            "tr",
            "th",
            "td",
            "h1",
            "h2",
            "h3",
            "h4",
            "h5",
            "h6",
            "pre",
            "hr",
            "blockquote",
            "ul",
            "ol",
            "li",
            "dl",
            "dt",
            "dd",
            "em",
            "small",
            "s",
            "cite",
            "code",
            "sub",
            "sup",
            "span",
            "tbody",
            "thead",
            "tfoot",
        ]:
            tag.replaceWithChildren()

    # normalize links
    if base_url:
        for a in soup.find_all("a", href=True):
            a["target"] = "_blank"
            try:
                parsed_link = urllib.parse.urlparse(a["href"])
                if parsed_link.scheme == "":
                    a["href"] = urllib.parse.urljoin(base_url, a["href"])
            except Exception:
                pass

    # remove duplicate br tags
    for br in soup.find_all("br"):
        if isinstance(br.next_sibling, Tag) and br.next_sibling.name.lower() == "br":
            br.extract()

    # remove comments
    comments = soup.findAll(text=lambda text: isinstance(text, Comment))
    [comment.extract() for comment in comments]

    # print('initial: ', soup)

    # identify top-level element
    if soup.body:
        top = soup.body
    elif soup.html:
        top = soup.html
    else:
        top = soup

    # normalize paragraphs
    new_soup = BeautifulSoup("", "lxml")
    current_paragraph = new_soup.new_tag("p")
    new_soup.append(current_paragraph)
    inline_tags = [
        "a",
        "b",
        "cite",
        "code",
        "em",
        "i",
        "s",
        "small",
        "span",
        "strong",
        "sub",
        "sup",
    ]
    for s in top.contents:
        # print('looking at %s: %s' % (s.name, s))
        if isinstance(s, NavigableString) or s.name in inline_tags:
            current_paragraph.append(copy.copy(s))
        elif s.name == "p":
            current_paragraph = copy.copy(s)
            new_soup.append(current_paragraph)
        else:
            current_paragraph = new_soup.new_tag("p")
            t = copy.copy(s)
            new_soup.append(t)
            t.wrap(current_paragraph)
    # print('normalized: ', new_soup)

    # get rid of br's
    fragment = "".join(str(c) for c in new_soup.contents)
    html = fragment.replace("<br/>", "</p><p>")
    soup = BeautifulSoup(html, "lxml")

    # remove empty paragraphs
    for p in soup.find_all("p"):
        if len(p.get_text(strip=True)) == 0:
            p.extract()

    # flatten
    soup.html.body.unwrap()
    soup.html.unwrap()
    pretty = soup.prettify(formatter="html5")
    # get rid of nbsp
    return pretty.replace("&nbsp;", " ")

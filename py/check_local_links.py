#!/usr/bin/env python3
# coding=utf-8
#
# Check that all links of an article are local
#
# sample invocation:
#   py/check_local_links.py 10824
#
# This file is part of calo.news: A news platform
#
# Copyright (C) 2017-2023 Paolo Greppi
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
# You should have received a copy of the GNU Affero General Public License
# along with this program (file LICENSE).
# If not, see <https://www.gnu.org/licenses/>.

import argparse

import database as db
from bs4 import BeautifulSoup

if __name__ == "__main__":
    parser = argparse.ArgumentParser(
        description="Check that all links of an article are local"
    )
    parser.add_argument("id", type=int, help="article id")
    args = parser.parse_args()
    with db.connect() as conn, conn.cursor() as cur:
        cur.execute("SELECT content FROM articles WHERE id=%d" % args.id)
        res = cur.fetchone()
        html = res[0]
        soup = BeautifulSoup(html, "lxml")
        for a in soup.select("a"):
            url = a.get("href")
            cur.execute("SELECT id FROM articles WHERE url='%s'" % url)
            res = cur.fetchone()
            if res:
                id = res[0]
                print("url %s found with id %d" % (url, id))
            else:
                print("url %s not found" % url)

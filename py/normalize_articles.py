#!/usr/bin/env python3
# coding=utf-8
#
# calonews article normalization
# sample invocation:
#   ./normalize_articles.py 999999 1111111
#
# This file is part of calo.news: A news platform
#
# Copyright (C) 2017-2023 Paolo Greppi
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
# You should have received a copy of the GNU Affero General Public License
# along with this program (file LICENSE).
# If not, see <https://www.gnu.org/licenses/>.

import argparse

import database as db
import normalize

if __name__ == "__main__":
    parser = argparse.ArgumentParser(description="Normalize articles")
    parser.add_argument("ids", type=int, nargs="+", help="one or more article ids")
    args = parser.parse_args()
    with db.connect() as conn, conn.cursor() as cur:
        for id in args.ids:
            print("normalizing %d" % id)
            data = {"id": id}
            cur.execute(
                "SELECT content, content_original FROM articles WHERE id=%(id)s", data
            )
            res = cur.fetchone()
            content = res[0]
            if content:
                data["content"] = normalize.normalize(content)
                cur.execute(
                    "UPDATE articles SET content=%(content)s WHERE id=%(id)s", data
                )
            content_original = res[1]
            if content_original:
                data["content_original"] = normalize.normalize(content_original)
                cur.execute(
                    "UPDATE articles SET content_original=%(content_original)s WHERE id=%(id)s",
                    data,
                )

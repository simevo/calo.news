#!/usr/bin/env python3
# coding=utf-8
# Rewrites all external urls found in content and content_original that exist as articles in the aggregator to internal links
#
# sample invocation
#   ./rewire.py 1143355
#
# This file is part of calo.news: A news platform
#
# Copyright (C) 2017-2023 Paolo Greppi
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
# You should have received a copy of the GNU Affero General Public License
# along with this program (file LICENSE).
# If not, see <https://www.gnu.org/licenses/>.

""" change the language of an article """

import argparse

import common
import database as db
from bs4 import BeautifulSoup


def lookup(url):
    with db.connect() as conn, conn.cursor() as cur:
        cur.execute("SELECT id FROM articles WHERE url = %(url)s", {"url": url})
        res = cur.fetchone()
        id = res[0] if res else None
    return id


def rewire(txt):
    soup = BeautifulSoup(txt, "lxml")
    for a in soup.find_all("a", href=True):
        url = a["href"]
        id = lookup(url)
        if id:
            new_url = "/article/%d" % id
            common.log_info("rewiring link %s to %s" % (url, new_url))
            a["href"] = new_url
    return soup.prettify(formatter="html5")


def rewire_article(id):
    common.log_info("rewiring article %d" % id)
    data = {"id": id}
    with db.connect() as conn, conn.cursor() as cur:
        cur.execute(
            """
            SELECT
              content,
              content_original
            FROM
              articles
            WHERE
              id = %(id)s""",
            data,
        )
        res = cur.fetchone()
        content = res[0]
        content_original = res[1]
        if content:
            data["content"] = rewire(content)
            cur.execute("UPDATE articles SET content=%(content)s WHERE id=%(id)s", data)
        if content_original:
            data["content_original"] = rewire(content_original)
            cur.execute(
                "UPDATE articles SET content_original=%(content_original)s WHERE id=%(id)s",
                data,
            )


if __name__ == "__main__":
    parser = argparse.ArgumentParser(
        description="Rewrite urls as internal links if possible"
    )
    parser.add_argument("id", type=int, help="article id")
    args = parser.parse_args()
    rewire_article(args.id)

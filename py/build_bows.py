#!/usr/bin/env python3
# -*- coding: utf-8 -*-
#
# build bow (bag-of-words) for article suggestion
#
# This file is part of calo.news: A news platform
#
# Copyright (C) 2017-2023 Paolo Greppi
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
# You should have received a copy of the GNU Affero General Public License
# along with this program (file LICENSE).
# If not, see <https://www.gnu.org/licenses/>.

import datetime
import json
import operator
import re

import database as db
import discourse

_digits = re.compile(r"\d")
_junk = re.compile(r"[%/()=\.]")


def contains_digits(d):
    return bool(_digits.search(d))


def contains_junk(d):
    return bool(_junk.search(d))


def frequency(cur, user_id):
    # calculates the term frequency in documents read by the user, with a bonus for starred documents and excluding "thumbed-down" documents
    cur.execute(
        """
      WITH t AS (
        SELECT
          (UNNEST(tsv)).*,
          COALESCE(user_articles.rating, 0) AS rating
        FROM
          articles
          JOIN user_articles ON articles.id = user_articles.article_id
        WHERE
          user_articles.user_id = %(user_id)s
      )
      SELECT
        lexeme,
        ARRAY_LENGTH(positions,1) AS count,
        rating
      FROM
        t
      WHERE
        LENGTH(lexeme) > 2
      ORDER BY count DESC""",
        {"user_id": user_id},
    )
    res = cur.fetchall()
    df = {}
    for t in res:
        if t[2] >= 0 and not contains_digits(t[0]) and not contains_junk(t[0]):
            if t[0] in df:
                df[t[0]] += t[1] + t[2]
            else:
                df[t[0]] = t[1] + t[2]
    return df


def document_frequency(cur):
    # returns the document term frequency
    cur.execute(
        """
      WITH t AS (
        SELECT (UNNEST(tsv)).*
        FROM
          articles
          LEFT JOIN user_articles ON articles.id = user_articles.article_id
        WHERE
          user_articles.user_id IS NOT NULL
        )
      SELECT
        lexeme,
        ARRAY_LENGTH(positions,1) AS count
      FROM t
      WHERE LENGTH(lexeme) > 2
      ORDER BY count DESC"""
    )
    res = cur.fetchall()
    df = {}
    for t in res:
        if not contains_digits(t[0]) and not contains_junk(t[0]):
            if t[0] in df:
                df[t[0]] += t[1]
            else:
                df[t[0]] = t[1]
    return df


with db.connect() as conn, conn.cursor() as cur:
    print("%s - build BOWs" % datetime.datetime.now().isoformat())
    df = document_frequency(cur)
    # get list of users who are members of the group "soci"
    users = discourse.get_active_users()
    for u in users:
        print("looking at user: %s with id: %d" % (u["username"], u["id"]))
        id = u["id"]
        if id >= 0:
            tf = frequency(cur, id)
            if len(tf.keys()) > 0:
                max_frequency = max(tf.items(), key=operator.itemgetter(1))[1]
                frequency_threshold = max_frequency / 1000.0
                print(
                    "  has %d keys and max_frequency = %d"
                    % (len(tf.keys()), max_frequency)
                )
                tf = {t: tf[t] for t in tf if tf[t] > frequency_threshold}
                print("  reduced to %d keys" % len(tf.keys()))
                tfidf = {}
                for t in tf.keys():
                    tfidf[t] = float(tf[t]) / float(df[t])
                max_tfidf = max(tfidf.items(), key=operator.itemgetter(1))[1]
                tfidf_threshold = max_tfidf / 10.0
                print("  max_tfidf = %d" % max_tfidf)
                bow = [k for k in tfidf if tfidf[k] > tfidf_threshold]
                print("  bow has %d keys" % len(bow))
                cur.execute(
                    "UPDATE users SET bow=%(bow)s WHERE id = %(id)s",
                    {"bow": json.dumps(bow), "id": id},
                )

#!/usr/bin/env python3
# coding=utf-8
#
# run this script every hour to send the newsletters to all subscribed users
#
# This file is part of calo.news: A news platform
#
# Copyright (C) 2017-2023 Paolo Greppi
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
# You should have received a copy of the GNU Affero General Public License
# along with this program (file LICENSE).
# If not, see <https://www.gnu.org/licenses/>.

import datetime
import locale
import os
import re
import syslog
import time

import account
import common
import database as db
import email_local
import pytz
import server
from jinja2 import Environment, FileSystemLoader

my_locale = os.environ["LOCALE"]


def filter_blacklist(articles, blacklist):
    regexp = "(" + blacklist.strip().lower().replace(" ", "|") + ")"
    return filter(
        lambda n: blacklist == ""
        or re.search(regexp, (f"{n['title']} {n['title_original']}").lower()) is None,
        articles,
    )
    # return filter(lambda n: blacklist == '' or re.search(regexp, (f"{n['title']} {n['title_original']} {n['excerpt']}").lower()) is None, articles)


if __name__ == "__main__":
    syslog.openlog("send_newsletters_py", 0, syslog.LOG_LOCAL0)
    cet_tz = pytz.timezone("CET")
    utc_tz = pytz.utc
    current_hour = utc_tz.localize(datetime.datetime.utcnow()).astimezone(cet_tz).hour
    common.log_info("entering - current_hour = %d" % current_hour)
    env = Environment(loader=FileSystemLoader("views"))
    locale.setlocale(locale.LC_ALL, my_locale)
    with db.connect() as conn, conn.cursor() as cur:
        cur.execute(
            """
            SELECT
              id,
              newsletter,
              list_email,
              list_frequency,
              list_format,
              list_hour,
              COALESCE(blacklist, ''),
              balance,
              username,
              email
            FROM users
            WHERE
              username IS NOT NULL
              AND NOT inactive"""
        )
        users = cur.fetchall()
    for u in users:
        common.log_info("looking at user %s" % u[8])
        if u[1]:
            if u[3] == "daily":
                if u[5] == current_hour:
                    if u[7] > account.final_threshold:
                        data = {"user_id": u[0]}
                        j = server.get_newsletter_json(data)
                        articles = {
                            "articles": filter_blacklist(j, u[6]),
                            "username": u[8],
                            "aggregator_hostname": os.environ["AGGREGATOR_HOSTNAME"],
                        }

                        template = env.get_template("newsletter.tpl")
                        email_content = template.render(articles)
                        subject = "[calomelano social club] la tua newsletter di oggi"
                        email = u[2] if u[2] != "" else u[9]

                        common.log_info("send newsletter to %s" % u[8])

                        # PRODUCTION:
                        email_local.send_email(
                            to=email, subject=subject, content=email_content
                        )
                        time.sleep(10)

                        # TEST:
                        # email_local.send_email(to='john.doe@example.com', subject=subject, content=email_content)

                        # DEBUG:
                        # print("To: %s" % email)
                        # print("Subject: %s" % subject)
                        # print("%s" % email_content)
                        # print("=" * 80)
                    else:
                        common.log_debug(
                            "not sending newsletter to %s because balance setting is %f <= %f"
                            % (u[8], u[7], account.final_threshold)
                        )
                else:
                    common.log_debug(
                        "not sending newsletter to %s because list_hour setting is %d"
                        % (u[8], u[5])
                    )
            else:
                common.log_debug(
                    "not sending newsletter to %s because list_frequency setting is %s"
                    % (u[8], u[3])
                )
        else:
            common.log_debug(
                "not sending newsletter to %s because newsletter setting is False"
                % u[8]
            )

    common.log_info("done")
    syslog.closelog()

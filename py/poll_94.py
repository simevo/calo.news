#!/usr/bin/env python3
# coding=utf-8
#
# This file is part of calo.news: A news platform
#
# Copyright (C) 2017-2023 Paolo Greppi
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
# You should have received a copy of the GNU Affero General Public License
# along with this program (file LICENSE).
# If not, see <https://www.gnu.org/licenses/>.

import syslog
import time

import common
import database as db
import poller
import requests


def secret():
    if secret.cached == "":
        try:
            with open("secrets/94.jwt", "r") as secret_file:
                secret.cached = secret_file.read().strip("\n")
        except EnvironmentError:
            return ""
    return secret.cached


secret.cached = ""

syslog.openlog("poll_94_py", 0, syslog.LOG_LOCAL0)
common.log_info("entering")

headers = {
    "User-Agent": "Mozilla/5.0 (X11; Linux x86_64; rv:52.0) Gecko/20100101 Firefox/52.0",
    "Accept": "application/json, text/plain, */*",
    "Accept-Language": "en-US,en;q=0.5",
    "Referer": "https://app.goodmorningitalia.it/briefing/",
    "Authorization": "Bearer %s" % secret(),
    "Origin": "https://app.goodmorningitalia.it",
}

feed_id = 94
length_threshold = 2000  # minimum acceptable length for an article
language = "it"

date = time.strftime("%Y-%m-%d", time.gmtime())
url = (
    "https://api.goodmorningitalia.it/sync-news/?date_published[$gt]=%s%%2000:00:00%%20&date_published[$lt]=%s%%2023:59:59"
    % (date, date)
)
json = requests.get(url, headers=headers).json()
url = json["data"]["url"]
title = json["data"]["title"]
content = json["data"]["content_html"]
data = {
    "feed_id": feed_id,
    "count": 1,
    "to_retrieve": "?",
    "non_articles": 0,
    "retrieved": 0,
    "failed": 0,
    "stored": 0,
}
if len(content) > length_threshold:
    data["retrieved"] = 1
    res = poller.store_article(
        author="redazione",
        title=title,
        url=url,
        content=content,
        feed_id=feed_id,
        language=language,
    )
    if res > 0:
        common.log_info("new article %s stored with id %d" % (url, res))
        data["stored"] = 1
    else:
        data["failed"] = 1
else:
    data["failed"] = 1
    common.log_error("skipping because lenght = %d" % len(content))
with db.connect() as conn, conn.cursor() as cur:
    cur.execute(
        "UPDATE feeds SET last_polled = NOW() WHERE id = %(feed_id)s",
        {"feed_id": feed_id},
    )

# poller.send_email(data)

common.log_info("done")
print("%d %d %d" % (data["retrieved"], data["failed"], data["stored"]))

syslog.closelog()

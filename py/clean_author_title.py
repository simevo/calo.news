#!/usr/bin/env python3
#
# clean author and title from HTML numeric codes and HTML tags
#
# This file is part of calo.news: A news platform
#
# Copyright (C) 2017-2023 Paolo Greppi
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
# You should have received a copy of the GNU Affero General Public License
# along with this program (file LICENSE).
# If not, see <https://www.gnu.org/licenses/>.

import database as db
import poller

with db.connect() as conn, conn.cursor() as cur:
    cur.execute(
        """
          SELECT
            id,
            title,
            title_original,
            author
          FROM articles
          WHERE
            title LIKE '%<%'
            OR title_original LIKE '%<%'
            OR author LIKE '%<%'
            OR title LIKE '%&#%'
            OR title LIKE '%&amp;#%'
            OR title_original LIKE '%&#%'
            OR author LIKE '%&#%'
          ORDER BY id"""
    )
    res = cur.fetchall()
    for article in res:
        id = article[0]
        title = article[1]
        title_original = article[2]
        author = article[3]
        old_data = {
            "id": id,
            "title": title,
            "title_original": title_original,
            "author": author,
        }
        # print(old_data)
        new_data = {
            "id": id,
            "title": title if not title else poller.clean(title),
            "title_original": title_original
            if not title_original
            else poller.clean(title_original),
            "author": author if not author else poller.clean(author),
        }
        # print(new_data)
        cur.execute(
            "UPDATE articles SET title = %(title)s, title_original = %(title_original)s, author = %(author)s WHERE id = %(id)s",
            new_data,
        )

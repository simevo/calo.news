#!/usr/bin/env python3
# coding=utf-8
#
# This file is part of calo.news: A news platform
#
# Copyright (C) 2017-2023 Paolo Greppi
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
# You should have received a copy of the GNU Affero General Public License
# along with this program (file LICENSE).
# If not, see <https://www.gnu.org/licenses/>.

import datetime
import http.cookiejar
import locale
import random
import sys
import syslog
import time

import common
import database as db
import poller
import requests
import rewire
from bs4 import BeautifulSoup, NavigableString

syslog.openlog("poll_119_py", 0, syslog.LOG_LOCAL0)
common.log_info("entering")

cj = http.cookiejar.MozillaCookieJar("cookies_119.txt")
cj.load()
headers = {
    "User-Agent": "Mozilla/5.0 (X11; Linux x86_64; rv:60.0) Gecko/20100101 Firefox/60.0",
    "Accept": "text/html,application/xhtml+xml,aplication/xml;q=0.9,*/*;q=0.8",
}

feed_id = 119
loc = locale.getlocale()
locale.setlocale(locale.LC_TIME, "it_IT.utf8")
base_url = "https://www.lindiceonline.com"
url0 = "%s/l-indice/sommario/%s/" % (
    base_url,
    datetime.datetime.utcnow().strftime("%B-%Y"),
)
common.log_info("url0 = %s" % url0)
locale.setlocale(locale.LC_TIME, loc)

html = requests.get(url0, cookies=cj, headers=headers).text
soup = BeautifulSoup(html, "lxml")
entries = [url0]

for a in soup.select("div.post-content > .p2 a"):
    url = a.get("href")
    if url[0] == "/":
        url = base_url + url
    entries.append(url)
unique_entries = set(entries)
entries = sorted(list(unique_entries))

data = {}
data["feed_id"] = feed_id
data["count"] = len(entries)

with db.connect() as conn, conn.cursor() as cur:
    # http://stackoverflow.com/a/7573706
    # To remove elements from a list while iterating over it, you need to go backwards
    for e in entries[-1::-1]:
        cur.execute("SELECT id FROM articles WHERE url = %(url)s", {"url": e})
        res = cur.fetchone()
        if res:
            common.log_info("article %s already retrieved with id = %s" % (e, res))
            entries.remove(e)

data["to_retrieve"] = len(entries)
data["non_articles"] = 0
data["retrieved"] = 0
data["failed"] = 0
data["stored"] = 0
for e in entries:
    common.log_info("to retrieve: %s" % e)
    html = requests.get(e, cookies=cj, headers=headers).text

    soup = BeautifulSoup(html, "lxml")
    # print(soup.prettify())

    try:
        stamp = soup.select('meta[property="article:published_time"]')[0].get("content")
        common.log_info("stamp = %s" % stamp)
        title = soup.select("h1.post-title")[0].get_text().strip()
        common.log_info("title = %s" % title)
        try:
            author = (
                soup.select(
                    "div.post-container.cf > div > div > p:nth-of-type(2) > strong"
                )[0]
                .get_text()
                .strip()
            )
            # better query would be: div.post-container.cf > div > div > p[style*="text-align: right;"] > strong
        except IndexError:
            author = "Redazione"
        common.log_info("author = %s" % author)
        contents = soup.select("div[itemprop=articleBody]")[0].contents[6:]
        content = ""
        for c in contents:
            if isinstance(c, NavigableString):
                c1 = c
            else:
                c1 = c.prettify()
            if isinstance(c1, bytes):
                c1 = c1.decode()
            content += c1
        common.log_info(content[:100])

        if len(content) > 50:
            res = poller.store_article(
                author=author,
                title=title,
                url=e,
                content=content,
                feed_id=feed_id,
                language="it",
                stamp=stamp,
            )
            data["retrieved"] += 1
            if res > 0:
                common.log_info("  new article %s stored with id %d" % (e, res))
                data["stored"] = +1
            else:
                data["failed"] = +1
            for i in range(random.randint(5, 20)):
                sys.stdout.write(".")
                sys.stdout.flush()
                time.sleep(1)
            sys.stdout.write("\n")
        else:
            common.log_info("   skipping because length = %d" % len(content))
            data["failed"] += 1
    except IndexError as e:
        common.log_error("   skipping because could not find required keys: %s" % e)
        data["failed"] += 1
    except Exception:
        common.log_error("   skipping because of exception: %s" % e)
        data["failed"] += 1

id0 = rewire.lookup(url0)
rewire.rewire_article(id0)

with db.connect() as conn, conn.cursor() as cur:
    cur.execute(
        "UPDATE feeds SET last_polled = NOW() WHERE id = %(feed_id)s",
        {"feed_id": feed_id},
    )

# poller.send_email(data)

common.log_info("done")
print("%d %d %d" % (data["retrieved"], data["failed"], data["stored"]))

syslog.closelog()

#!/usr/bin/env python3
# -*- coding: utf-8 -*-
#
# create new topic / post on discourse
# https://meta.discourse.org/t/create-new-topic-via-sql/47571/21?u=simevo
# http://docs.discourse.org/#tag/Posts%2Fpaths%2F~1posts%2Fpost
#
# This file is part of calo.news: A news platform
#
# Copyright (C) 2017-2023 Paolo Greppi
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
# You should have received a copy of the GNU Affero General Public License
# along with this program (file LICENSE).
# If not, see <https://www.gnu.org/licenses/>.

import os
import re

import bottle
import common
import requests

discourse_url = "https://%s" % os.environ["DISCOURSE_HOSTNAME"]
discourse_user = os.environ["DISCOURSE_USER"]
aggregator_hostname = os.environ["AGGREGATOR_HOSTNAME"]
news_category_id = os.environ["NEWS_CATEGORY_ID"]


def api_key():
    if api_key.cached == "":
        try:
            with open("secrets/api_key", "r") as secret_file:
                api_key.cached = secret_file.read()
        except EnvironmentError:
            common.log_error("error while retrieving discourse api key")
            raise bottle.HTTPError(status=520, body="internal error")
    # print('[%s]' % api_key.cached.decode())
    return api_key.cached


api_key.cached = ""


def discourse_request(verb, endpoint, data, username):
    headers = {"Api-Key": api_key(), "Api-Username": username}
    url = discourse_url + endpoint
    # print('url = ', url)
    s = requests.Session()
    req = requests.Request(verb, url, data=data, headers=headers)
    prepped = s.prepare_request(req)
    # print(prepped.__dict__)
    return s.send(prepped)


def handle_discourse_error(x):
    common.log_error("discourse says: [%s]" % x.text)


# this should be the Notizie category; find out the category ID from https://commenti.example.com/categories.json
@common.logme
def new_topic(title, content, username=discourse_user, category=news_category_id):
    # create a new discourse topic
    endpoint = "/posts.json"
    params = {
        "title": title,
        "category": category,
        "raw": content,
        "skip_validations": "true",
    }
    x = discourse_request("POST", endpoint, params, username)
    if x.status_code != 200:
        handle_discourse_error(x)
        return -1
    else:
        topic_id = x.json()["topic_id"]
        return topic_id


@common.logme
def new_post(topic_id, content, username):
    # add a post to the supplied topic_id
    endpoint = "/posts.json"
    params = {"topic_id": topic_id, "raw": content, "skip_validations": "true"}
    x = discourse_request("POST", endpoint, params, username)
    if x.status_code != 200:
        handle_discourse_error(x)
        return -1
    else:
        post_number = x.json()["post_number"]
        return post_number


@common.logme
def new_comment(article_id, article_title, article_content, username, comment):
    stripped_article_content = re.sub(
        r"[\s]+", " ", re.sub(r"<.+?>", "", article_content)
    )
    extract = stripped_article_content[:400]
    topic_content = (
        """<p>%s …</p><a href="https://%s/article/%s">Vai all'articolo completo …</a>"""
        % (extract, aggregator_hostname, article_id)
    )
    topic_id = new_topic(article_title, topic_content)
    common.log_info("topic_id = %d" % topic_id)
    if topic_id > 0:
        post_number = new_post(topic_id, comment, username)
        common.log_info("post_number = %d" % post_number)
        if post_number < 0:
            return post_number
    return topic_id


def get_posts(topic_id, username):
    endpoint = "/t/%d.json" % int(topic_id)
    x = discourse_request("GET", endpoint, {}, username)
    if x.status_code != 200:
        handle_discourse_error(x)
        return []
    else:
        posts = x.json()["post_stream"]["posts"][1:]
        return posts


# get all topics in subcategory within category
def get_topics(category_id, subcategory_id):
    # https://meta.discourse.org/t/calls-to-apis-of-type-t-id-json-do-not-work-for-subcategories/67391
    topics = []
    page = 0
    while True:
        endpoint = "/c/%s/%s.json?page=%d" % (category_id, subcategory_id, page)
        x = discourse_request("GET", endpoint, {}, discourse_user)
        t = []
        if x.status_code == 200:
            t = x.json()["topic_list"]["topics"]
        if len(t) == 0:
            break
        topics += t
        page += 1
    return topics


def get_active_users():
    endpoint = "/groups/soci/members.json"
    x = discourse_request("GET", endpoint, {}, discourse_user)
    if x.status_code != 200:
        handle_discourse_error(x)
        return []
    else:
        users = x.json()["members"]
        return users


def get_first_post(topic_id):
    endpoint = "/t/%d.json" % topic_id
    x = discourse_request("GET", endpoint, {}, discourse_user)
    if x.status_code != 200:
        handle_discourse_error(x)
        return []
    else:
        post = x.json()["post_stream"]["posts"][0]
        return post


def get_users():
    endpoint = "/admin/users/list/active.json?show_emails=true"
    x = discourse_request("GET", endpoint, {}, discourse_user)
    if x.status_code != 200:
        handle_discourse_error(x)
        return []
    else:
        users = x.json()
        return users


@common.logme
def delete_user(group_id, user_id):
    # deletes user from group
    # http://docs.discourse.org/#tag/Groups%2Fpaths%2F~1groups~1%7Bgroup_id%7D~1members.json%2Fdelete
    endpoint = "/groups/%d/members.json" % int(group_id)
    params = {"user_id": user_id}
    x = discourse_request("DELETE", endpoint, params, discourse_user)
    if x.status_code != 200:
        handle_discourse_error(x)
        return False
    else:
        return True


@common.logme
def add_user(group_id, user_username):
    # adds user to group
    # https://docs.discourse.org/#tag/Admin%2Fpaths%2F~1groups~1%7Bgroup_id%7D~1members.json%2Fput
    endpoint = "/groups/%d/members.json" % int(group_id)
    params = {"usernames": user_username}
    x = discourse_request("PUT", endpoint, params, discourse_user)
    if x.status_code != 200:
        handle_discourse_error(x)
        return False
    else:
        return True

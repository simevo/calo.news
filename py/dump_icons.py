#!/usr/bin/env python3
# coding=utf-8
# utility to dump iconblobs to icon files
#
# This file is part of calo.news: A news platform
#
# Copyright (C) 2017-2023 Paolo Greppi
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
# You should have received a copy of the GNU Affero General Public License
# along with this program (file LICENSE).
# If not, see <https://www.gnu.org/licenses/>.

import database as db

with db.connect() as conn, conn.cursor() as cur:
    cur.execute("SELECT id, icon, iconblob FROM feeds ORDER BY id")
    res = cur.fetchall()

    for r in res:
        print("dumping icon for feed %d: %s" % (r[0], r[1]))
        f = open("www/" + r[1], "wb")
        f.write(r[2])
        f.close()

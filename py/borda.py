#!/usr/bin/env python3
# coding=utf-8
#
# borda voting
# https://en.wikipedia.org/wiki/Borda_count
#
# This file is part of calo.news: A news platform
#
# Copyright (C) 2017-2023 Paolo Greppi
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
# You should have received a copy of the GNU Affero General Public License
# along with this program (file LICENSE).
# If not, see <https://www.gnu.org/licenses/>.

import datetime

import database as db


def calculate(startdate):
    startstamp = datetime.datetime.strptime(startdate, "%Y-%m-%d").timestamp()
    votes = {}
    user_votes = {}
    with db.connect() as conn, conn.cursor() as cur:
        # The maximum vote each user can assign to a feed
        # is equal to the number of feeds (excluding feed 0 = articoli inseriti a mano)
        cur.execute("SELECT COUNT(*) FROM FEEDS WHERE id != 0")
        max_vote = cur.fetchone()[0]

        cur.execute("SELECT id FROM users WHERE id > 1 ORDER BY id")
        user_ids = cur.fetchall()
        for user_id in user_ids:
            cur.execute(
                """
                WITH my_feeds AS (
                  SELECT
                    feeds.id,
                    SUM(COALESCE(CASE WHEN user_articles.read THEN 1 END, 0) + COALESCE(CASE WHEN user_articles.to_read THEN 1 END, 0)) AS views
                  FROM
                    articles
                    LEFT OUTER JOIN user_articles ON articles.id = user_articles.article_id
                    LEFT OUTER JOIN feeds ON feeds.id = articles.feed_id
                  WHERE user_id = %(user_id)s AND EXTRACT(EPOCH FROM articles.stamp) > %(startstamp)s
                  GROUP BY feeds.id
                ),
                all_feeds AS (
                  SELECT SUM(COALESCE(CASE WHEN user_articles.read THEN 1 END, 0)) + SUM(COALESCE(CASE WHEN user_articles.to_read THEN 1 END, 0)) as views
                  FROM user_articles
                  WHERE user_id = %(user_id)s
                )
                SELECT
                  feeds.id,
                  feeds.title,
                  COALESCE(my_feeds.views, 0) AS views,
                  100.0 * my_feeds.views / all_feeds.views AS percent
                FROM
                  all_feeds,
                  feeds
                  LEFT OUTER JOIN my_feeds ON feeds.id = my_feeds.id
                ORDER BY views DESC""",
                {"user_id": user_id, "startstamp": startstamp},
            )
            # retrieve the list of feeds where the user with id user_id has read or intends to read an article
            # sorted by descending number of views
            feeds = cur.fetchall()
            v = max_vote
            user_votes[user_id[0]] = []
            for f in feeds:
                if f[0] != 0 and f[2] > 0:
                    user_votes[user_id[0]].append((f[1], v))
                    if f[1] in votes:
                        votes[f[1]] += v
                    else:
                        votes[f[1]] = v
                    v -= 1
    sorted_votes = sorted(votes, key=votes.get, reverse=True)
    # for u in user_votes:
    #     print('=== Preferenze per l\'utente %s' % u)
    #     for f in user_votes[u]:
    #         print('%s: %d' % (f[0], f[1]))
    # for v in sorted_votes:
    #     print('%s: %d' % (v, votes[v]))
    return {
        "sorted_votes": [(v, votes[v]) for v in sorted_votes],
        "user_votes": user_votes,
    }

#!/usr/bin/env python3
# coding=utf-8
#
# merge two views into one with the Reißverschlussverfahren
# https://de.wikipedia.org/wiki/Rei%C3%9Fverschlussverfahren
#
# sample invocation
#   ./py/test_reissverschluss.py
#
# This file is part of calo.news: A news platform
#
# Copyright (C) 2017-2023 Paolo Greppi
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
# You should have received a copy of the GNU Affero General Public License
# along with this program (file LICENSE).
# If not, see <https://www.gnu.org/licenses/>.

n = 20
views = [
    {"view": ["a%d" % (n - i) for i in range(n)], "weight": 2.0},
    {"view": ["b%d" % i for i in range(n)], "weight": 1.5},
    {"view": ["c%d" % i for i in range(n)], "weight": 1.0},
    {"view": ["d%d" % i for i in range(n)], "weight": 0.8},
    {"view": ["e%d" % i for i in range(n)], "weight": 0.5},
]
combined = []
for v in views:
    w = v["weight"]
    for i, e in enumerate(v["view"]):
        combined.append({"id": e, "rank": float(1 + i) / w})
combined.sort(key=lambda x: x["rank"])
ids = [e["id"] for e in combined]
# uniquify (only works on python 3.6)
# see https://www.peterbe.com/plog/fastest-way-to-uniquify-a-list-in-python-3.6
uniq = list(dict.fromkeys(ids))
print(uniq)

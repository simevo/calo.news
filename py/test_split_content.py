#!/usr/bin/env python3
# coding=utf-8
#
# launch tests with:
#   py.test py/test_split_content.py -vvv
#
# This file is part of calo.news: A news platform
#
# Copyright (C) 2017-2023 Paolo Greppi
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
# You should have received a copy of the GNU Affero General Public License
# along with this program (file LICENSE).
# If not, see <https://www.gnu.org/licenses/>.

import re

import translate

open_tag = re.compile(r"<")
close_tag = re.compile(r">")


def paranoia(s, low, high):
    for i in range(low, high):
        split = translate.split_content(s, i)
        assert s == "".join(split)
        for s in split:
            o = len(open_tag.findall(s))
            c = len(close_tag.findall(s))
            assert o == c


def test1():
    s = "a<a>b<bbc><cd>dde"
    #    01234567890123456
    paranoia(s, 1, len(s) - 1)


def test2():
    s = '<p>aaaaa<a href="aaaa">uuuuuuuuu</a>uuuuuuuuuu</p>'
    paranoia(s, 1, len(s) - 1)

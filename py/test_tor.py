#!/usr/bin/env python3
# coding=utf-8
# refresh tor exit IP
#
# This file is part of calo.news: A news platform
#
# Copyright (C) 2017-2023 Paolo Greppi
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
# You should have received a copy of the GNU Affero General Public License
# along with this program (file LICENSE).
# If not, see <https://www.gnu.org/licenses/>.

import requests
from stem import Signal
from stem.control import Controller


# signal TOR for a new connection
# http://stackoverflow.com/questions/30286293/make-requests-using-python-over-tor
def renew_connection():
    with Controller.from_port(port=9051) as controller:
        controller.authenticate(password="kcjli<useIiu6sjhsbt7pvcjz,ksduc")
        controller.signal(Signal.NEWNYM)
        print("tor connection renewed")


# renew_connection()
session = requests.session()
# Tor uses the 9050 port as the default socks port
session.proxies = {
    "http": "socks5://127.0.0.1:9050",
    "https": "socks5://127.0.0.1:9050",
}
# Make a request through the Tor connection
# IP visible through Tor
print(session.get("http://httpbin.org/ip").text)
# Following prints your normal public IP
print(requests.get("http://httpbin.org/ip").text)

#!/usr/bin/env python3
# coding=utf-8
#
# calonews feed activity report
#
# This file is part of calo.news: A news platform
#
# Copyright (C) 2017-2023 Paolo Greppi
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
# You should have received a copy of the GNU Affero General Public License
# along with this program (file LICENSE).
# If not, see <https://www.gnu.org/licenses/>.

import datetime
import io
import itertools

import database as db
import matplotlib

# https://matplotlib.org/2.0.2/faq/usage_faq.html#what-is-a-backend
matplotlib.use("svg")
import matplotlib.cm
import matplotlib.dates
import matplotlib.figure
import matplotlib.pyplot


def get_data(days):
    # return user activity data with buckets of days each
    with db.connect() as conn, conn.cursor() as cur:
        cur.execute(
            """
          WITH
            l AS (
              SELECT
                EXTRACT(EPOCH FROM TIMESTAMPTZ '2017-01-01 0:00') AS min,
                EXTRACT(EPOCH FROM DATE_TRUNC('WEEK', NOW()) + INTERVAL '7 DAYS' - INTERVAL '1 SECOND') AS max),
            h AS (
              SELECT
                feed_id,
                width_bucket(extract(epoch from stamp), l.min, l.max, ROUND((l.max - l.min)/%(seconds)s)::INTEGER) as bucket,
                count(*)
              FROM
                articles, l
              GROUP BY feed_id, bucket)
          SELECT
            feeds.title,
            h.bucket,
            min+(h.bucket-1)*%(seconds)s as low,
            min+h.bucket*%(seconds)s as high,
            h.count::INTEGER
          FROM
            h JOIN feeds ON feeds.id=h.feed_id, l
          ORDER BY h.bucket, title""",
            {"seconds": 86400 * days},
        )
        rows = cur.fetchall()
        feeds = {}
        max_bucket = 0
        min_timestamp = 1e10
        max_timestamp = 0
        for row in rows:
            feeds[row[0]] = {}
            max_bucket = max(max_bucket, int(row[1]))
            min_timestamp = min(min_timestamp, int(row[2]))
            max_timestamp = max(max_timestamp, int(row[3]))
        start_timestamp = 1483228800  # 2017-01-01
        x = list(
            datetime.datetime.fromtimestamp(start_timestamp + i * 86400 * days)
            for i in range(max_bucket)
        )
        for feed in feeds:
            feeds[feed] = list(itertools.repeat(0, max_bucket))
        for row in rows:
            feeds[row[0]][row[1] - 1] = row[4]
        return x, feeds


def ellipsis(s, le):
    return (s[:le] + "…") if len(s) > le else s


def get_plot():
    x, feeds = get_data(365.2421 / 12.0)
    # print x
    # print feeds

    matplotlib.pyplot.rcParams["font.family"] = "Liberation Sans"

    w, h = matplotlib.figure.figaspect(1.0 / 3.0)
    fig = matplotlib.pyplot.figure(figsize=(w, h))
    ax = fig.add_subplot(111)
    hfmt = matplotlib.dates.DateFormatter("%Y %b")
    ax.xaxis.set_major_formatter(hfmt)

    bottom = list(itertools.repeat(0, len(x)))
    i = 0
    rects = []
    for feed in feeds:
        color = matplotlib.cm.jet(i / len(feeds))
        i += 1
        feed_short = ellipsis(feed, 15)
        if sum(bottom) == 0:
            rects += ax.bar(
                x, feeds[feed], width=15, color=color, edgecolor=color, label=feed_short
            )
        else:
            rects += ax.bar(
                x,
                feeds[feed],
                width=15,
                color=color,
                edgecolor=color,
                bottom=bottom,
                label=feed_short,
            )
        bottom = list(map(sum, list(zip(bottom, feeds[feed]))))

    matplotlib.pyplot.xlabel("Mese")
    matplotlib.pyplot.ylabel("Numero di articoli aggregati / mese")
    matplotlib.pyplot.title("Attività mensile dei feed")
    ax.set_xticks(x)
    # ax.legend(loc='right', bbox_to_anchor=(1.21, 0.5))  # , prop={'size': 8})
    fig.autofmt_xdate()

    img = io.BytesIO()
    matplotlib.pyplot.savefig(
        img, transparent=False, bbox_inches="tight", pad_inches=0.5
    )
    img.seek(0)
    return img

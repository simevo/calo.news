#!/usr/bin/env python3
# coding=utf-8
#
# calonews article normalization
# sample invocation:
#   ./normalize_file.py https://example.com/link
#
# This file is part of calo.news: A news platform
#
# Copyright (C) 2017-2023 Paolo Greppi
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
# You should have received a copy of the GNU Affero General Public License
# along with this program (file LICENSE).
# If not, see <https://www.gnu.org/licenses/>.

import argparse

import normalize

if __name__ == "__main__":
    parser = argparse.ArgumentParser(description="Normalize file")
    parser.add_argument("filename", type=str, help="filename")
    args = parser.parse_args()
    with open(args.filename, "r") as file:
        content = file.read()
    print(normalize.normalize(content, "/"))

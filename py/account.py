#!/usr/bin/env python3
# -*- coding: utf-8 -*-
#
# calculate expenses
#
# This file is part of calo.news: A news platform
#
# Copyright (C) 2017-2023 Paolo Greppi
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
# You should have received a copy of the GNU Affero General Public License
# along with this program (file LICENSE).
# If not, see <https://www.gnu.org/licenses/>.

import datetime
import os
import re

import common
import database as db
import discourse
from psycopg2 import extras

# regular expression to extract the amount from a properly formatted topic title
# https://pythex.org/?regex=%5E%5B%5E%3A%5D%2B%3A%20*(-%3F%5B0-9%5D%2B%2C%3F%5B0-9%5D*)%20*%E2%82%AC%3F%24&test_string=Xxxx%3A-20%2C3%0A&ignorecase=1&multiline=1&dotall=1&verbose=0
r = re.compile(r"^[^:]+: *(-?[0-9]+,?[0-9]*) *€?$")

threshold = -5  # €
final_threshold = -10  # €
daily_fee = 0.15  # €


@common.logme
def expenses():
    result = []
    # get all topics in the subcategory "contabilita" of category "gruppo-di-informazione"
    topics = discourse.get_topics(
        os.environ["EIG_CATEGORY_ID"], os.environ["ACCOUNTING_CATEGORY_ID"]
    )
    for t in topics:
        # caveat: https://stackoverflow.com/a/30696682
        created_at = datetime.datetime.strptime(
            t["created_at"], "%Y-%m-%dT%H:%M:%S.%fZ"
        )
        delta = datetime.datetime.now() - created_at
        days_ago = delta.days
        user_id = -1
        for p in t["posters"]:
            if "Autore iniziale" in p["description"]:
                user_id = p["user_id"]
                break
        common.log_info(
            "looking at topic [%s] created %d days ago by user id: %s"
            % (t["title"], days_ago, user_id)
        )
        if user_id != 1:
            matches = r.findall(t["title"])
            if len(matches) != 1:
                common.log_error("could not find amount!")
            else:
                amount = float(matches[0].replace(",", "."))
                result.append(
                    {
                        "stamp": created_at.timestamp(),
                        "date": created_at.strftime("%Y-%m-%d"),
                        "user_id": user_id,
                        "amount": amount,
                    }
                )
    return result


@common.logme
def transactions(user_id):
    # return the account balance and details for the users with the supplied id
    with db.connect() as conn, conn.cursor(cursor_factory=extras.DictCursor) as cur:
        cur.execute(
            """
            SELECT
                EXTRACT(EPOCH FROM date) AS stamp,
                user_id,
                TO_CHAR(date, 'YYYY-MM-DD') AS date,
                amount
            FROM user_transactions
            WHERE user_id = %(user_id)s"""
            % {"user_id": user_id}
        )
        transactions = [dict(record) for record in cur]
    es = expenses()
    for e in es:
        # print('e[user_id] %s == user_id %s' % (type(e['user_id']), type(user_id)))
        if e["user_id"] == int(user_id):
            transactions.append(e)
    transactions = sorted(transactions, key=lambda t: t["stamp"])
    balance = 0.0
    for t in transactions:
        t["stamp"] = int(t["stamp"])
        t["amount"] = float(t["amount"])
        balance += t["amount"]
        t["balance"] = balance
    transactions.sort(key=lambda t: t["stamp"], reverse=True)
    return transactions


@common.logme
def accounts(email, username):
    with db.connect() as conn, conn.cursor() as cur:
        cur.execute(
            """
            SELECT
                user_id,
                SUM(amount),
                email,
                username
            FROM
              user_transactions
              JOIN users ON user_id = users.id
            WHERE NOT users.inactive
            GROUP BY user_id, users.email, users.username"""
        )
        revenues = cur.fetchall()
    balances = {}
    emails = {}
    usernames = {}
    for r in revenues:
        balances[r[0]] = r[1]
        emails[r[0]] = r[2]
        usernames[r[0]] = r[3]
    es = expenses()
    for e in es:
        if e["user_id"] in balances:
            balances[e["user_id"]] += e["amount"]
        else:
            balances[e["user_id"]] = e["amount"]
    accounts = []
    i = 1
    for user_id in balances.keys():
        account = {"user_id": user_id, "balance": balances[user_id], "count": i}
        if email:
            account["email"] = emails.get(user_id, "")
        if username:
            account["username"] = usernames.get(user_id, "")
        accounts.append(account)
        i += 1
    return accounts

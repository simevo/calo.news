#!/usr/bin/env python3
# coding=utf-8
#
# email connector to allow sending emails from the localhost
# the localhost MTA should be configured, which is done by the ansible role mta
#
# This file is part of calo.news: A news platform
#
# Copyright (C) 2017-2023 Paolo Greppi
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
# You should have received a copy of the GNU Affero General Public License
# along with this program (file LICENSE).
# If not, see <https://www.gnu.org/licenses/>.

import email.charset
import email.mime.multipart
import email.mime.text
import email.utils
import os
import smtplib


def send_email(to, subject, content):
    """send mail with unicode content"""
    sender = os.environ["ADMIN_EMAIL"]
    text_subtype = "plain"
    text_charset = "utf-8"
    # the following line is to force Content-Transfer-Encoding: 8bit for maximum compatibility
    # see: http://bugs.python.org/issue12552
    email.charset.add_charset("utf-8", email.charset.SHORTEST)
    destination = [to]  # only one address at a time !
    date = email.utils.formatdate(localtime=True)

    msg = email.mime.multipart.MIMEMultipart()
    msg.attach(email.mime.text.MIMEText(content, text_subtype, text_charset))
    msg["Subject"] = subject
    msg["To"] = destination[0]
    msg["From"] = sender
    msg["Date"] = date

    conn = smtplib.SMTP("localhost")
    conn.set_debuglevel(False)
    # print(msg.as_string())
    try:
        conn.sendmail(sender, destination, msg.as_string().encode("utf-8"))
    finally:
        conn.quit()

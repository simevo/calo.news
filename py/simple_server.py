#!/usr/bin/env python3
# coding=utf-8
# calonews RESTful API server using cherrypy
#
# This file is part of calo.news: A news platform
#
# Copyright (C) 2017-2023 Paolo Greppi
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
# You should have received a copy of the GNU Affero General Public License
# along with this program (file LICENSE).
# If not, see <https://www.gnu.org/licenses/>.

import argparse
import json
import os

import bottle
import common
import database as db


def articles():
    user_id = -1
    params = bottle.request.query.decode()
    offset = params.get("offset", 0)
    feed = params.get("feed", -1)
    author = params.get("author", "")
    query = params.get("query", "")
    read = params.get("read", "false")
    to_read = params.get("to_read", "false")

    if author:
        author = "%" + author + "%"
    with db.connect() as conn, conn.cursor() as cur:
        cur.execute(
            """
            WITH aa AS (
               SELECT
                 articles_data.*,
                 CASE WHEN feeds.premium THEN 2 ELSE 0 END + COALESCE(user_feeds.rating::INTEGER, COALESCE(feeds.rating, 0)) AS my_feed_rating,
                 COALESCE(user_articles.rating::INTEGER, 0) AS my_rating,
                 COALESCE(user_articles.to_read, FALSE) AS to_read,
                 COALESCE(user_articles.dismissed, FALSE) AS dismissed,
                 COALESCE(user_articles.read, FALSE) AS read
               FROM
                 articles_data
                 LEFT OUTER JOIN user_feeds ON articles_data.feed_id = user_feeds.feed_id AND %(user_id)s IS NOT NULL AND user_feeds.user_id = %(user_id)s
                 LEFT OUTER JOIN user_articles ON articles_data.id = user_articles.article_id AND %(user_id)s IS NOT NULL AND user_articles.user_id = %(user_id)s
                 JOIN feeds ON articles_data.feed_id = feeds.id
               WHERE
                (%(feed)s = -1 OR articles_data.feed_id = %(feed)s)
                AND (%(to_read)s = 'false' OR user_articles.to_read)
                AND (%(read)s = 'false' OR user_articles.read)),
            a AS (
              SELECT
                articles.feed_id,
                articles.language,
                articles.author,
                articles.title,
                articles.title_original,
                articles.url,
                articles.topic_id,
                articles.comments,
                EXTRACT(EPOCH FROM (NOW() - articles.stamp)) AS age,
                aa.*,
                feeds.icon,
                feeds.premium,
                feeds.title AS feed
              FROM articles
                INNER JOIN aa ON aa.id = articles.id
                INNER JOIN feeds ON articles.feed_id = feeds.id
              WHERE
                (%(feed)s != -1 OR %(author)s != '' OR %(query)s != '' OR %(read)s = 'true' OR %(to_read)s = 'true' OR read = FALSE)
                AND (%(author)s = '' OR articles.author ILIKE %(author)s)
                AND (%(query)s = ''
                  OR articles.tsv @@ plainto_tsquery('pg_catalog.italian', %(query)s)
                  OR articles.tsv_simple @@ plainto_tsquery('pg_catalog.simple', %(query)s))
              ORDER BY stamp DESC
              LIMIT 50
              OFFSET %(offset)s)
            SELECT
              JSON_AGG(ROW_TO_JSON(a)) FROM a""",
            {
                "offset": offset,
                "feed": feed,
                "author": author,
                "query": query,
                "user_id": user_id,
                "read": read,
                "to_read": to_read,
            },
        )
        res = cur.fetchone()
        if res and res[0] is not None:
            articles = {"articles": res[0]}
        else:
            articles = {"articles": []}
    return articles


@bottle.route("/articles")
def articles_json():
    """filter articles in reverse chronological order

    sample calls:
    curl -k -i -X GET http://localhost:8080/articles
    curl -k -i -X GET http://localhost:8080/articles?author=paolog
    curl -k -i -X GET http://localhost:8080/articles?feed=0
    curl -k -i -X GET http://localhost:8080/articles?query=Renzi
    curl -k -i -X GET http://localhost:8080/articles?read=true
    curl -k -i -X GET http://localhost:8080/articles?to_read=true
    """
    return common.render_json(articles(), 200)


def get_articles_json(data, full=True, read=False, excerpts=False):
    if full:
        additional_fields = "articles.content, articles.content_original,"
    elif excerpts:
        additional_fields = "SUBSTRING((CASE WHEN LENGTH(TRIM(articles.content))>0 THEN articles.content ELSE articles.content_original END) FOR 400) AS excerpt,"
    else:
        additional_fields = ""
    with db.connect() as conn, conn.cursor() as cur:
        cur.execute(
            """
            WITH ad AS (
              SELECT
                articles_data.*,
                COALESCE(user_articles.rating::INTEGER, 0) AS my_rating,
                COALESCE(user_articles.to_read, FALSE) AS to_read,
                COALESCE(user_articles.read, FALSE) AS read,
                COALESCE(user_articles.dismissed, FALSE) AS dismissed
              FROM
                articles_data
                LEFT OUTER JOIN user_articles ON articles_data.id = user_articles.article_id AND (%(user_id)s IS NULL OR user_articles.user_id = %(user_id)s)
              WHERE
                articles_data.id = ANY(%(ids)s)),
            a AS (
              SELECT
                ad.*,
                articles.stamp,
                articles.feed_id,
                articles.language,
                articles.author,
                articles.title,
                articles.title_original,
                {}
                articles.url,
                articles.comments,
                articles.topic_id,
                EXTRACT(EPOCH FROM (NOW() - articles.stamp)) AS age,
                feeds.icon,
                feeds.premium,
                feeds.license,
                feeds.title AS feed,
                feeds.id AS feed_id
              FROM
                ad
                JOIN articles ON ad.id = articles.id
                JOIN feeds ON ad.feed_id = feeds.id
                JOIN UNNEST(%(ids)s::INT[]) WITH ORDINALITY t(id, ord) ON ad.id = t.id
              ORDER BY t.ord)
            SELECT
              COALESCE(JSON_AGG(a), '[]'::json) FROM a""".format(
                additional_fields
            ),
            data,
        )
        res = cur.fetchall()[0][0]

    # print(res)
    return res


@bottle.route("/articles.html", method="GET")
@bottle.jinja2_view("html.tpl", template_lookup=["views"])
def articles_html():
    """returns an antology of articles in basic HTML format

    sample call:
    curl -k -i -X GET http://localhost:8080/articles.html?ids=200,9
    """
    user_id = -1
    data = {"user_id": user_id}
    ids = "[" + bottle.request.query.get("ids", "") + "]"
    if ids != "[]":
        data["ids"] = json.loads(ids)
    articles = get_articles_json(data, full=True, read=True)
    return {
        "articles": articles,
        "aggregator_hostname": os.environ["AGGREGATOR_HOSTNAME"],
    }


@bottle.route("/", method="GET")
@bottle.jinja2_view("simple.tpl", template_lookup=["views"])
def articles_list():
    """returns a simple list of all articles in DB in basic HTML format

    sample call:
    curl -k -i -X GET http://localhost:8080/
    """
    return articles()


@bottle.route("/articles/<id>")
def article(id):
    user_id = -1
    data = {"user_id": user_id, "ids": [int(id)]}
    articles = get_articles_json(data, full=True, read=True)
    article = articles[0]
    return common.render_json(article, 200)


if __name__ == "__main__":
    parser = argparse.ArgumentParser()
    parser.add_argument(
        "-d", "--debug", help="enable debugging and autoreloading", action="store_true"
    )
    args = parser.parse_args()

    bottle.BaseRequest.MEMFILE_MAX = 1024 * 1024

    if args.debug:
        bottle.debug(True)
        bottle.run(host="127.0.0.1", port=8080, reloader=True, server="cherrypy")
    else:
        bottle.run(host="127.0.0.1", port=8080, reloader=False, server="cherrypy")

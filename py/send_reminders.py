#!/usr/bin/env python3
# coding=utf-8
#
# run this script to send reminders to all users whose account balance is below certain thresholds
#
# This file is part of calo.news: A news platform
#
# Copyright (C) 2017-2023 Paolo Greppi
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
# You should have received a copy of the GNU Affero General Public License
# along with this program (file LICENSE).
# If not, see <https://www.gnu.org/licenses/>.

import datetime
import locale
import os
import syslog
import time

import account
import common
import discourse
import email_local
from jinja2 import Environment, FileSystemLoader

my_locale = os.environ["LOCALE"]


def near(reference, value, tolerance):
    return value <= reference and value > (reference - tolerance)


def send_mail(template, subject, data):
    email_content = template.render(data)
    email_local.send_email(to=a["email"], subject=subject, content=email_content)
    time.sleep(10)


if __name__ == "__main__":
    syslog.openlog("send_reminders_py", 0, syslog.LOG_LOCAL0)
    common.log_info("entering")

    env = Environment(loader=FileSystemLoader("views"))
    locale.setlocale(locale.LC_ALL, my_locale)
    accounts = account.accounts(email=True, username=True)
    users = discourse.get_active_users()
    for a in accounts:
        active = next((u for u in users if u["username"] == a["username"]), None)
        common.log_info(
            "%s [%s]: %g"
            % (a["username"], "active" if active else "inactive", a["balance"])
        )
        if a["balance"] <= account.threshold and active is not None:
            common.log_info("%s: %g" % (a["username"], a["balance"]))
            days_to_go = (a["balance"] - account.final_threshold) / account.daily_fee
            last_day = datetime.datetime.now() + datetime.timedelta(days=days_to_go)
            data = {
                "username": a["username"],
                "account": a["balance"],
                "final_threshold": account.final_threshold,
                "end_date": last_day.strftime("%d %B %Y"),
                "daily_fee": account.daily_fee,
                "days_to_go": days_to_go,
                "aggregator_hostname": os.environ["AGGREGATOR_HOSTNAME"],
                "discourse_hostname": os.environ["DISCOURSE_HOSTNAME"],
                "eig_category_name": os.environ["EIG_CATEGORY_NAME"],
                "accounting_category_name": os.environ["ACCOUNTING_CATEGORY_NAME"],
                "feeds_category_name": os.environ["FEEDS_CATEGORY_NAME"],
            }
            # print('final_threshold = ', account.final_threshold)
            # print('balance = ', a['balance'])
            # print('daily_fee = ', account.daily_fee)
            if a["balance"] <= account.final_threshold:
                template = env.get_template("farewell.tpl")
                discourse.delete_user(
                    os.environ["MEMBERS_GROUP_ID"], a["user_id"]
                )  # remove user from Soci group
                subject = "[calomelano social club] Disattivazione account"
                common.log_info("farewell %s" % a["username"])
                send_mail(template, subject, data)
            elif near(round(a["balance"]), a["balance"], account.daily_fee):
                template = env.get_template("reminder.tpl")
                subject = "[calomelano social club] Attenzione il tuo conto virtuale è in rosso !"
                common.log_info("remind %s" % a["username"])
                send_mail(template, subject, data)
        if a["balance"] > account.final_threshold and not active and a["username"]:
            discourse.add_user(os.environ["MEMBERS_GROUP_ID"], a["username"])
            template = env.get_template("reactivation.tpl")
            subject = "[calomelano social club] Riattivazione account"
            common.log_info("reactivate %s" % a["username"])
            data = {
                "username": a["username"],
                "final_threshold": account.final_threshold,
                "aggregator_hostname": os.environ["AGGREGATOR_HOSTNAME"],
            }
            send_mail(template, subject, data)

    common.log_info("done")
    syslog.closelog()

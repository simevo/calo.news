#!/usr/bin/env python3
# coding=utf-8
#
# credits: https://intoli.com/blog/running-selenium-with-headless-chrome/
#
# patch for debian stretch:
#
#   sudo ln -s /usr/bin/chromedriver /usr/lib/chromium/chromedriver
#
# This file is part of calo.news: A news platform
#
# Copyright (C) 2017-2023 Paolo Greppi
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
# You should have received a copy of the GNU Affero General Public License
# along with this program (file LICENSE).
# If not, see <https://www.gnu.org/licenses/>.

import time

from selenium import webdriver

options = webdriver.ChromeOptions()
options.add_argument("headless")

# set the window size
options.add_argument("window-size=1200x600")

# initialize the driver
driver = webdriver.Chrome(chrome_options=options)
driver.get("http://www.repubblica.it/")

# http://selenium-python.readthedocs.io/waits.html
# wait up to 10 seconds for the elements to become available
driver.implicitly_wait(10)
# scroll to the bottom of the page
driver.execute_script("window.scrollTo(0, document.body.scrollHeight);")
time.sleep(5)

# use css selectors to grab the login inputs
# http://selenium-python.readthedocs.io/locating-elements.html
links = driver.find_elements_by_xpath("//article//a")

# https://stackoverflow.com/a/45282406
width = driver.execute_script(
    "return Math.max(document.body.scrollWidth, document.body.offsetWidth, document.documentElement.clientWidth, document.documentElement.scrollWidth, document.documentElement.offsetWidth);"
)
height = driver.execute_script(
    "return Math.max(document.body.scrollHeight, document.body.offsetHeight, document.documentElement.clientHeight, document.documentElement.scrollHeight, document.documentElement.offsetHeight);"
)
# driver.set_window_size(width + 100, height + 100)
driver.set_window_size(width, height)
driver.save_screenshot("screenshot.png")

print('<img src="screenshot.png" usemap="#linkmap"><map name="linkmap">')
for li in links:
    location = li.location
    size = li.size
    href = li.get_attribute("href")
    print(
        '<area shape="rect" coords="%d,%d,%d,%d" href="%s">'
        % (
            location["x"],
            location["y"],
            location["x"] + size["width"],
            location["y"] + size["height"],
            href,
        )
    )
print("</map>")

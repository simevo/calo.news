#!/usr/bin/env python3
# coding=utf-8
#
# calonews poll all feeds
# sample invocation:
#   ./py/poll.py
#
# only poll feed wth id = 108:
#   ./py/poll.py 108
#
# This file is part of calo.news: A news platform
#
# Copyright (C) 2017-2023 Paolo Greppi
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
# You should have received a copy of the GNU Affero General Public License
# along with this program (file LICENSE).
# If not, see <https://www.gnu.org/licenses/>.

import argparse

import database as db
import poller
from psycopg2 import extras

parser = argparse.ArgumentParser()
parser.add_argument("feed_id", nargs="?", default=-1)
args = parser.parse_args()

with db.connect() as conn, conn.cursor(cursor_factory=extras.RealDictCursor) as cur:
    cur.execute(
        "SELECT id, language, url, incomplete, salt_url, exclude, asy, cookies, tor, script, active, frequency FROM feeds WHERE (%(feed_id)s = -1 OR %(feed_id)s = id) ORDER BY id",
        {"feed_id": args.feed_id},
    )
    res = cur.fetchall()

for r in res:
    if len(res) == 1:
        # if the script is invoked for a specific feed, ignore active and frequency settings
        r["active"] = True
        r["frequency"] = ""
    p = poller.Poller(r)
    p.poll()
    print(p.__dict__)

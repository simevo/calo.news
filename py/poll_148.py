#!/usr/bin/env python3
# coding=utf-8
#
# calonews poll feed 148
# sample invocation:
#   ./py/poll_148.py
#
# This file is part of calo.news: A news platform
#
# Copyright (C) 2017-2023 Paolo Greppi
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
# You should have received a copy of the GNU Affero General Public License
# along with this program (file LICENSE).
# If not, see <https://www.gnu.org/licenses/>.

import database as db
import poller
from psycopg2 import extras

with db.connect() as conn, conn.cursor(cursor_factory=extras.RealDictCursor) as cur:
    cur.execute(
        "SELECT id, language, url, incomplete, salt_url, exclude, asy, cookies, tor, script, active, frequency FROM feeds WHERE (%(feed_id)s = -1 OR %(feed_id)s = id) ORDER BY id",
        {"feed_id": 148},
    )
    r = cur.fetchall()[0]
    r["active"] = True
    r["frequency"] = ""
    r["suffix"] = "?underPaywall=true&paywall_canRead=true"
p = poller.Poller(r)
p.poll()
print(p.__dict__)

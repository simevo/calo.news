#!/usr/bin/env python3
# coding=utf-8
#
# calonews feed detail report
#
# This file is part of calo.news: A news platform
#
# Copyright (C) 2017-2023 Paolo Greppi
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
# You should have received a copy of the GNU Affero General Public License
# along with this program (file LICENSE).
# If not, see <https://www.gnu.org/licenses/>.

import datetime
import io

import database as db
import matplotlib
from dateutil.relativedelta import relativedelta

# https://matplotlib.org/2.0.2/faq/usage_faq.html#what-is-a-backend
matplotlib.use("svg")
import matplotlib.cm
import matplotlib.dates
import matplotlib.figure
import matplotlib.pyplot


def get_data(feed_id, start, weeks):
    start = (
        start
        if start
        else (datetime.date.today() - relativedelta(months=+2)).isoformat()
    )
    # return feed detail data
    with db.connect() as conn, conn.cursor() as cur:
        cur.execute(
            """
          WITH
            limits AS (
              SELECT
                TIMESTAMPTZ %(start)s AS min,
                NOW() AS max,
                INTERVAL %(delta)s AS delta)
            SELECT
              limits.min + limits.delta * WIDTH_BUCKET(stamp, (SELECT ARRAY_AGG(t) AS result FROM GENERATE_SERIES(limits.min, limits.max, limits.delta) t)) as low,
              COUNT(*)
            FROM
              articles, limits
            WHERE %(feed_id)s = -1 OR feed_id = %(feed_id)s
            GROUP BY low""",
            {"feed_id": feed_id, "start": start, "delta": "%s WEEK" % weeks},
        )
        res = cur.fetchall()

    data1 = {}
    for r in res:
        data1[r[0].strftime(r"%Y-%m-%d")] = r[1]

    def daterange(start_date, end_date, step):
        for n in range(int((end_date - start_date).days / step)):
            yield start_date + datetime.timedelta(n * step)

    data2 = {}
    for d in daterange(
        datetime.datetime.strptime(start, r"%Y-%m-%d").date(),
        datetime.date.today() + datetime.timedelta(7 * int(weeks) + 1),
        7 * int(weeks),
    ):
        df = d.strftime(r"%Y-%m-%d")
        data2[df] = data1[df] if df in data1 else 0

    data3 = []
    for k, v in data2.items():
        data3.append([datetime.datetime.strptime(k, r"%Y-%m-%d").date(), v])

    return data3


def get_plot(feed_id, start, weeks):
    data = get_data(feed_id, start, weeks)[1:]
    # print(data)

    matplotlib.pyplot.rcParams["font.family"] = "Liberation Sans"

    w, h = matplotlib.figure.figaspect(1.0 / 3.0)
    fig = matplotlib.pyplot.figure(figsize=(w, h))
    ax = fig.add_subplot(111)
    hfmt = matplotlib.dates.DateFormatter("%Y %b")
    ax.xaxis.set_major_formatter(hfmt)

    x = []
    y = []
    for d in data:
        x.append(d[0])
        y.append(d[1])

    matplotlib.pyplot.xlabel("Periodo")
    matplotlib.pyplot.ylabel("Numero di articoli aggregati / %s settimane" % weeks)
    matplotlib.pyplot.title("Dettaglio attività del feed %s" % feed_id)
    ax.plot(x, y)
    fig.autofmt_xdate()

    img = io.BytesIO()
    matplotlib.pyplot.savefig(
        img, transparent=False, bbox_inches="tight", pad_inches=0.5
    )
    img.seek(0)
    return img

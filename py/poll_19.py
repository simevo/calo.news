#!/usr/bin/env python3
# coding=utf-8
#
# This file is part of calo.news: A news platform
#
# Copyright (C) 2017-2023 Paolo Greppi
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
# You should have received a copy of the GNU Affero General Public License
# along with this program (file LICENSE).
# If not, see <https://www.gnu.org/licenses/>.

import syslog

import common
import database as db
import poller
from psycopg2 import extras

urls = [
    "http://www.piemonteparchi.it/cms/index.php/parchi-piemontesi?format=feed",
    "http://www.piemonteparchi.it/cms/index.php/parchi-altrove?format=feed",
    "http://www.piemonteparchi.it/cms/index.php/territorio?format=feed",
    "http://www.piemonteparchi.it/cms/index.php/natura?format=feed",
    "http://www.piemonteparchi.it/cms/index.php/ambiente?format=feed",
    "http://www.piemonteparchi.it/cms/index.php/altri-argomenti?format=feed",
    "http://www.piemonteparchi.it/cms/cms/index.php/rubriche?format=feed",
    "http://www.piemonteparchi.it/cms/index.php/news?format=feed",
]
retrieved = 0
failed = 0
stored = 0

syslog.openlog("poll_19_py", 0, syslog.LOG_LOCAL0)
common.log_info("entering")

with db.connect() as conn, conn.cursor(cursor_factory=extras.RealDictCursor) as cur:
    cur.execute(
        "SELECT id, language, url, incomplete, salt_url, exclude, asy, cookies, tor, script, active, frequency FROM feeds WHERE 19 = id ORDER BY id"
    )
    result = cur.fetchone()

for url in urls:
    common.log_info("processing url %s" % url)
    result["url"] = url
    result["script"] = ""
    p = poller.Poller(result)
    p.poll()
    retrieved += p.__dict__["retrieved"]
    failed += p.__dict__["failed"]
    stored += p.__dict__["stored"]

# poller.send_email(data)

common.log_info("done")
print("%d %d %d" % (retrieved, failed, stored))

syslog.closelog()

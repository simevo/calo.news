#!/usr/bin/env python3
# coding=utf-8
#
# This file is part of calo.news: A news platform
#
# Copyright (C) 2017-2023 Paolo Greppi
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
# You should have received a copy of the GNU Affero General Public License
# along with this program (file LICENSE).
# If not, see <https://www.gnu.org/licenses/>.

import datetime
import html
import http.cookiejar
import random
import sys
import syslog
import time

import common
import database as db
import poller
import requests
from bs4 import BeautifulSoup

time_format = "%Y-%m-%dT%H:%M:%SZ"

syslog.openlog("poll_96_py", 0, syslog.LOG_LOCAL0)
common.log_info("entering")

base_url = "https://rep.repubblica.it/ws/cover.json"
feed_id = 96
length_threshold = 500  # minimum acceptable length for an article
language = "it"

cj = http.cookiejar.MozillaCookieJar("cookies_%s.txt" % feed_id)
cj.load()
headers = {
    "User-Agent": "Mozilla/5.0 (X11; Linux x86_64; rv:52.0) Gecko/20100101 Firefox/52.0"
}

json = requests.get(base_url, headers=headers).json()
updated = json["feed"]["updated"]
updated_dt = datetime.datetime.strptime(updated, time_format)  # 2019-05-12T21:56:20Z
entries = []
for z in json["feed"]["zones"]:
    for b in z["blocks"]:
        for e in b["entries"]:
            if "links" in e:
                for li in e["links"]:
                    if li["rel"] == "target-alternate-amp":
                        url = li["href"]
                author = e["author"] if "author" in e else "anonimo"
                if author[:3] == "di ":
                    author = author[3:]
                entries.append(
                    {"author": author, "title": html.unescape(e["title"]), "url": url}
                )
common.log_info("%d candidates" % len(entries))
data = {}
data["feed_id"] = feed_id
data["count"] = len(entries)
with db.connect() as conn, conn.cursor() as cur:
    # http://stackoverflow.com/a/7573706
    # To remove elements from a list while iterating over it, you need to go backwards
    for e in entries[-1::-1]:
        cur.execute("SELECT id FROM articles WHERE url = %(url)s", {"url": e["url"]})
        res = cur.fetchone()
        if res:
            common.log_info(
                "article %s already retrieved with id = %s" % (e["url"], res)
            )
            entries.remove(e)
data["to_retrieve"] = len(entries)
data["non_articles"] = 0
data["retrieved"] = 0
data["failed"] = 0
data["stored"] = 0
for e in entries:
    common.log_info("retrieving: %s" % e["url"])
    html = requests.get(e["url"], cookies=cj, headers=headers).text
    soup = BeautifulSoup(html, "lxml")
    # remove unwanted stuff
    for el in soup.select("amp-analytics"):
        el.extract()
    for el in soup.select(".detail-tag_container"):
        el.extract()
    for el in soup.select("#detail-values_big"):
        el.extract()
    for el in soup.select(".detail-comment_big"):
        el.extract()
    for el in soup.select(".detail-problem"):
        el.extract()
    content = ""
    article_body = soup.select(".paywall")
    for b in article_body:
        content += b.prettify()
    if isinstance(content, bytes):
        content = content.decode()
    if len(content) > length_threshold:
        updated_dt += datetime.timedelta(seconds=1)
        res = poller.store_article(
            author=e["author"],
            title=e["title"],
            url=e["url"],
            content=content,
            feed_id=feed_id,
            language=language,
            stamp=updated_dt.strftime(time_format),
        )
        data["retrieved"] += 1
        if res > 0:
            common.log_info("  new article %s stored with id %d" % (e, res))
            data["stored"] = +1
        else:
            data["failed"] = +1
        for i in range(random.randint(5, 20)):
            sys.stdout.write(".")
            sys.stdout.flush()
            time.sleep(1)
        sys.stdout.write("\n")
    else:
        common.log_error("  skipping because lenght = %d" % len(content))
        data["failed"] += 1
with db.connect() as conn, conn.cursor() as cur:
    cur.execute(
        "UPDATE feeds SET last_polled = NOW() WHERE id = %(feed_id)s",
        {"feed_id": feed_id},
    )

# poller.send_email(data)

common.log_info("done")
print("%d %d %d" % (data["retrieved"], data["failed"], data["stored"]))

syslog.closelog()

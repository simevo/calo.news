#!/usr/bin/env python3
# coding=utf-8
#
# sample invocation
#   ./change_language.py 10824 it en
#
# This file is part of calo.news: A news platform
#
# Copyright (C) 2017-2023 Paolo Greppi
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
# You should have received a copy of the GNU Affero General Public License
# along with this program (file LICENSE).
# If not, see <https://www.gnu.org/licenses/>.

""" change the language of an article """

import argparse
import os
import sys
import time

import database as db

time_format = "%Y-%m-%dT%H:%M:%SZ"
base_language = os.environ["BASE_LANGUAGE"]

if __name__ == "__main__":
    parser = argparse.ArgumentParser(description="Change the language of an article")
    parser.add_argument("id", type=int, help="article id")
    parser.add_argument("current", help="current language")
    parser.add_argument("new", help="destination language")
    args = parser.parse_args()
    if args.current == args.new:
        print(
            "%s - current language %s can not be the same as the destination language %s"
            % (time.strftime(time_format, time.gmtime()), args.current, args.new)
        )
        sys.exit(-1)
    print(
        "%s - changing language for article %d from %s to %s"
        % (time.strftime(time_format, time.gmtime()), args.id, args.current, args.new)
    )
    data = {"id": args.id, "current": args.current, "new": args.new}
    with db.connect() as conn, conn.cursor() as cur:
        if args.current == base_language:
            cur.execute(
                """
              UPDATE articles SET
                language=%(new)s,
                content_original=content,
                title_original=title,
                title=NULL,
                content=NULL
              WHERE
                id=%(id)s AND language=%(current)s
              RETURNING id""",
                data,
            )
        elif args.new == base_language:
            cur.execute(
                """
              UPDATE articles SET
                language=%(new)s,
                content=content_original,
                title=title_original,
                title_original=NULL,
                content_original=NULL
              WHERE
                id=%(id)s AND language=%(current)s
              RETURNING id""",
                data,
            )
        else:
            # reset translated title and content so that it gets re-translated
            cur.execute(
                """
              UPDATE articles SET
                language=%(new)s,
                title=NULL,
                content=NULL
              WHERE
                id=%(id)s AND language=%(current)s
              RETURNING id""",
                data,
            )
        res = cur.fetchone()
        id = res[0] if res else -1
        if id < 0:
            print(
                "%s - article %d not found"
                % (time.strftime(time_format, time.gmtime()), args.id)
            )
            sys.exit(-2)

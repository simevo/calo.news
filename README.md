# Calo.news

**calo.news** is an **open-source news platform** with aggregation, ranking and conversations, designed to be **self-hosted** and managed by a **community** of members that share a preferred language and common interests.

It has been in development since 2017 and there is one [live instance](https://notizie.calomelano.it/) up and running since then, aggregating about 50000 articles/week from 130+ RSS feeds, for a total of over one million articles and 10 GB of data.

The official project repository is at: https://gitlab.com/simevo/calo.news.

Mandatory screen-cast of aggregator home page as it looks like for a logged-in member of the community:

![newsfeed infinite scroll](doc/images/5-0_infinite.gif)

## Functionalities

The **platform**:

- **aggregates** the full-text of articles and posts (but without multimedia items) from selected sources (including professional sources with a subscription fee)

- **calculates** the relevance of the news for each member with transparent and user-tunable algorithms

- ranks, sorts, filters and translates the articles to offer to each member a **personalized newsfeed** accessible via a modern, responsive web application

The **members** can:

  - access the articles in a variety of ways: read online, download, receive as a newsletter or have them read aloud

  - rate articles and feeds

  - tip new articles and feeds

  - comment on the articles using the integrated private social discussion system

  - share links via social media

  - produce more meditated content and publish it using the integrated blog

All **content** can only be accessed by the members, except for:

  - the blog, and the social media posts which are set to be public

  - a stripped-down version of the aggregated news stream (without full text)

## Documentation

- [Manifesto of the Ethical Information Group](https://calomelano.it/?p=1550): background, objectives and ethical code

- [User guide](https://gitlab.com/simevo/calo.news/-/blob/master/doc/USER.md): how to use the platform

- [System administration guide](https://gitlab.com/simevo/calo.news/-/blob/master/doc/ADMIN.md): how to install and operate the platform

- [Development guide](https://gitlab.com/simevo/calo.news/-/blob/master/doc/DEVELOPER.md): how to hack and contribute back to this project

## License

**calo.news** an open-source news platform with aggregation, ranking and conversations

Copyright (C) 2017-2023 Paolo Greppi

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with this program (file [LICENSE](/LICENSE)).
If not, see <https://www.gnu.org/licenses/>.

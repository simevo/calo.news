# User guide for the calo.news platform

[[_TOC_]]

## Introduction

**calo.news** is an **open-source news platform** with aggregation, ranking and conversations, designed to be **self-hosted** and managed by a **community** of members that share a preferred language and common interests.

If is made up by four integrated components:

1. **news aggregation** engine

2. **news reading** web application

3. a forum for private social **conversations**

4. a public **blog**

Read the [manifesto of the Ethical Information Group](https://calomelano.it/?p=1550) for background, objectives and ethical code.

For more technical information, see the [**project repository**](https://gitlab.com/simevo/calo.news).

## First page and full text search

**RSS feeds** are the standard format used by almost all websites to publish updates.

An [**RSS feed aggregator**](https://en.wikipedia.org/wiki/News_aggregator) according to Wikipedia is _"[a] client software or a web application which aggregates syndicated web content such as online newspapers, blogs, podcasts, and video blogs (vlogs) in one location for easy viewing_".

The **news-aggregation engine component** of the calo.news platform automatically collects articles and posts from selected, high-quality sources chosen by the members of the community (henceforth named the **Ethical Information Group** or **EIG** in short).

The **news-reading web application component** presents a [first page](https://notizie.calomelano.it) which to an anonymous user appears as a very simple, chronologically sorted list of articles (the newsfeed); clicking on an article (e.g. https://notizie.calomelano.it/article/1156390) shows a redirection page that with an additional click takes the anonymous user to the original source where she can read the full text:

![newfeed and article detail page for anonymous users](images/1-0_anonymus_newsfeed.gif)

To EIG members, on the other hand, after logging in, the newsfeed appears as a grid of small panels, with a preview of a few lines of text from each article.

The order in which the articles appear in the newsfeed for a logged-in member is non-chronological and personalized for her; also the top menu has a few additional items (views, feeds, new and settings) and a full-text search box.

Let's use the latter to find the previous article with the title "SBB broadens horizons". Searching for example for "Deutsche Bahn" (note: words not present in the title but in the text of the article) you will find it, but this time clicking on it we can see the full text without leaving the platform, without leaving traces on the net, without advertising etc..:

![newfeed and article detail page for members](images/1-1_loggedin_search_fulltext.gif)

The first page can be quickly filtered using the two small floating buttons in the top left.

To exclude articles containing certain **undesired keywords**, first specify the words that should be blacklisted (in settings -> first page), then click on the blue button with the **ban** icon; the other yellow button with the **atom** icon excludes older articles; here "old" means more or less since your third last visit (the timings of your visit are recorded on the device and not on the server), as shown in this video:

![newfeed ban](images/ban.gif)

So the first and foremost feature of the EIG aggregator is that it aggregates (copies) the **full text** of all articles; in this way it acts as an archive (against censorship, the rot link, etc.), it protect members from tracking, and it can offer several useful functions the first one being full text search.

## Normalized text and text-to-speech

The second function is text normalization.

During the aggregation, the platform cleans up the text of the articles, removing images, videos, suspicious HTML tags (`script`, `form`, `object` ...), dividing it into paragraphs and setting a standardized format and layout, with 6 basic pieces information:

1. original feed with its square logo

2. reading time (estimated on the basis of a standard reading speed of 300 words per minute)

3. date and time of publication

4. title

5. link to the original source

6. author (if available)

Thanks to this normalization, all articles (from whatever source they may come from) are homogeneous, and reading is more focused, without distractions from changing text and background colors, sidebars, pop-ups and multimedia content such as photographs and videos ([iconoclasm !](https://en.wikipedia.org/wiki/Iconoclasm)).

In addition, for the visually impaired you can set the font size once and for all (this setting is recorded on the device and does not depend on the account you are using), as shown in this video:

![setting font size](images/2-1_zoom.gif)

Another useful function is **text-to-speech** (TTS). This assistive technology is available on almost all devices (smartphones, tablets and computers), and operating systems (iOS, Android and Windows 10). In the following video, text-to-speech is demonstrated on a tablet, for two articles - one in Italian and one in English:

![text-to-speech demo](images/out.mov)

You may like or dislike these synthetic voices. Some people use them as a background while reading, to concentrate better (to be able to follow the voice, during the reading the paragraphs are highlighted in gray as they are read). For others it might be enough for a first reading of the longer articles while kneading pizza on Sunday afternoon or during a long drive.

**Note**: For TTS, EIG uses the voices installed on the device, so the read texts are not revealed to any external service providers. Some voices may be "_cloud-based_" though: check the documentation of your device.

## Automatic machine translations

Each calo.news instance has a **base language** setting, which should be the preferred language spoken by its community of members. For the [calomelano.it live instance](https://notizie.calomelano.it/) this base language is Italian.

But the platform may also aggregate news in other languages (such as: English, Spanish, Russian, Arabic, Catalan, Portuguese, German, Dutch and French).

Thanks to an interface with machine translation services (at the moment we use the Microsoft Translator Text API), articles that have been read in the original language by at least two people, or by only one who has commented or given a positive vote, are automatically translated into the base language.

Alternatively, the user can request the on-the-fly translation of a certain article, as shown in this video:

![translation requested on-the-fly](images/3_translate.gif)

The original and the translated text are shown side by side, but you can hide the translation if you wish.

As we saw in the second video of the [previous section](#normalized-text-and-text-to-speech), if you choose to have the text of an international article read aloud, the original language text would be read.

## Offline reading and newsletters

The full text of the articles can be downloaded for offline reading in one of 5 formats:

1. **html**: for reading on smartphone or computer

2. **pdf**: great for saving and reading on tablet

3. **epub**: to read on tablet or ebook reader (except Amazon Kindle)

4. **mobi**: to read on Amazon Kindle ebook reader

5. **txt**: to read also on Commodore 64.

You can download single articles, for example in html format:

![downloading article in HTML format](images/4-1_html.gif)

or in pdf format:

![downloading article in PDF format](images/4-3_pdf.gif)

You can can also download a group of articles, but we will see this in the [next section](#algorithms) when describing the "interesting" articles view.

As an alternative to the download it is possible to have the file sent to you by email; the email address provided at registration will be used, unless in the settings page you provide an alternative email address to send the newsletter and ebooks:

![email settings](images/4-4_email.png)

The alternative email address is useful for example to send directly to the Kindle via the email address [Send to Kindle](https://www.amazon.it/gp/help/customer/display.html?nodeId=201974220) (typically name.surname@kindle.com). In this way, ebooks in mobi format arrive directly on the ebook reader via wireless network, without the hassle of connecting the device to the computer with a USB cable.

So in order to protect registered users from tracking and offer a better service, the calo.news platform **must** make copies of articles ("_journalistic publications_"), but only for private use (the copies are not publicly accessible, can only be accessed by logged-in members), for ethical reasons (mainly to comply with objective number 3 of the manifesto: "_to avoid tracking and to protect data and metadata produced by users_"), and above all without commercial purposes, and with the intention of rewarding those who produce quality information.

This type of online use qualifies as private, non-commercial use of journalistic publications within the meaning of Article 15 of [Directive (EU) 2019/790 of the European Parliament and of the Council of 17 April 2019 on copyright and related rights in the digital single market and amending Directives 96/9/EC and 2001/29/EC ("Directive on copyright in the digital single market")](https://eur-lex.europa.eu/legal-content/EN/TXT/HTML/?uri=CELEX:32019L0790&from=EN).

## Algorithms

We are all overwhelmed by the news: 50,000 articles per month enter our aggregator instance alone, while professional aggregators such as the [Europe Media Monitor](https://ec.europa.eu/jrc/en/publication/european-media-monitor) of the European Commission or the [IJS newsfeed](http://newsfeed.ijs.si/) of the Jozef Stefan Institute in Ljubljana travel on the 28,000 and 1500,000 articles per day. Dominating this amount of information would be impossible, without some help from **information technology**. It is enough not to be afraid of the algorithm, we need to control it and make it work at our service, and not to become its slaves.

Let's get back to the home page of the web application: to the logged-in members it appears as a grid of small panels (each of which presents a preview with a few lines of text from each article), and can be infinitely scrolled:

![newsfeed infinite scroll](images/5-0_infinite.gif)

This first page is as easy to use (just scroll and occasionally click ...) as it is complex and powerful; it presents to each member a personalized newsfeed, which is not in chronological order of publication, but is generated by an algorithm, like a cocktail obtained by mixing and shaking five different ingredients (views):

1. **filtered** (4 parts)

2. **keywords** (3 parts)

3. **suggested** (2 parts)

4. **popular** (2 parts)

5. **latest** (1 part)

Let's see these 5 views in more detail.

The **filtered** view is not chronological, and uses [a ranking algorithm similar to those of hackernews and reddit](https://calomelano.it/?p=1483), which tries to keep the articles it thinks you might be interested in visible for longer; this view can be influenced mainly through its settings on the dedicated page:

![settings for filtered view](images/5-1_filtered.png)

In particular:

- you can only ask for articles from sources tagged with at least one of the selected tags (such as: culture, society, Italy, foreign, science_tech, sport, economy, blog, news, magazine); if no tag is chosen, it will show all the articles;

- you can list the languages in which the articles must be shown; if no language is chosen it will show all the articles, and if the base language is included it will show also the translated articles.

Another way to influence the "filtered" algorithm is to rate the sources (positive: 1 to 5 stars, or negative: 1 to 5 thumbs-down) on the feed list page:

![rating feeds](images/5-0_rate_feeds.gif)

In the case of the example above the "filtered" view will try to show more prominetly the articles from "_Al Jazeera_" and "_Alphaville_", and to make those from "_Ansa_" and "_Articolo 21_" get quickly out of sight.

The **keywords** view instead is chronological and shows only the articles that contain the keywords indicated in the text, or have been written by the selected authors in the dedicated settings page:

![settings for keywords view](images/5-2_preferiti.png)

By entering here the keywords you want to keep an eye on (in the example: "_mastodon blockchain m5s eilish_") and your favorite authors (in the example: "_grillo travaglio_") the "keywords" view behaves like a reliable personalized, thematic press review !

The **suggested** view is the one that might surprise you, proposing out of the blue articles that you may find interesting. The algorithm finds them by looking at which words appear in the articles you have already read and/or voted positively: "potentially interesting" are the articles where these words appear with significant frequency.

The **popular** view is chronological and shows what other registered users are reading (collaborative discovery mechanism). Note: for this view and the previous one we do not make use of the social graph (as do social networks that promote posts seen by your friends or that they liked).

The **latest** view only shows articles from sources tagged as "news", it is not chronological, and uses the hackernews-type ranking algorithm trying to keep shorter articles visible for a longer time.

For those who suffer algorithms paranoia, the good news is that as we will see in the [last section](#open-source-and-replicability), the platform is all open-source so the algorithms are perfectly transparent.
So those who can read Python can go and see [the code behind it](https://gitlab.com/simevo/calo.news/blob/master/py/precompute.py).

If you are not into code, you can to see the algorithms in action, by looking separately at the 5 views, selecting "views" from the top bar, and then changing view with the drop-down menu:

![alternative views of the newsfeed](images/5-4_views.gif)

By the way in this dropdown, in addition to the 5 views that contribute to the first page, there are 5 addtional views:

6. **history**: the articles already read by the user

7. **premium**: we will talk about it in the [premium feeds section](#premium-feeds)

8. **foreign**: only articles in languages other than the base language, in chronological order

9. **hidden**: collects the articles that the user for her own reasons does not want to read or see

10. **interesting**: collects the articles that the user has marked as interesting from the newsfeed, from the search page or from the detail page of the article, by clicking on the button with the shopping cart icon ![carrello.png](images/carrello.png)

Here is how the **hidden** view works; articles can be hidden with a side-swipe in the lists, or by clicking on the button with the recycle bin icon:

![hidden view](images/5-5_nascosti.gif)

this function does not imply any negative rating, and has no effect on other users (it is useful for example to get rid of things that the algorithm proposes too insistently).

The **interesting** view collects all the items that you have previously marked as such by clicking on the "shopping cart" button ![carrello.png](images/carrello.png). This function is very useful if you browse the newsfeed while waiting for the bus, and you see an articles that you would like to read but is too long or too demanding: in that case, just click on the shopping cart button. The list of interesting articles is saved in the cloud (!) so articles marked as interesting on your smartphone or computer during the week can be read on your tablet or ebook reader during the weekend.

In practice, this is how it works:

![interesting view](images/5-6_interessanti.gif)

Finally, in the [previous section](#offline-reading-and-newsletters) we saw that you can download (or request to be emailed) individual articles; this is also possible for a group of articles, using the "interesting" view. Here is a demo showing how to download an extract with four articles in pdf format:

![downloading a group of articles in PDF format](images/5-7_interessanti-pdf.gif)

## Tips and votes

Rating (giving votes to) the feeds allows you to influence the algorithm for the "filtered" view, adapting it to your needs, but this choice has no effect on other users.

You can also rate individual articles (positively: from 1 to 5 stars, or negatively: from 1 to 5 thumbs-down). The rating can be assigned in the newsfeed, in the search page or in the detail page of the article:

![rating articles](images/6-2_rating.gif)

Rating articles has both an effect for you (they can affect the "suggested" view, see the [previous section](#algorithms)), and for the other members.

Actually the aggregator exploits the data generated by each user to help all the members find interesting news; therefore, a member rating articles can influence the "filtered" and "latest" views of other members. To be precise, simply reading an article already has an influence on other members' "filtered" and "latest" views (although much less than rating it). Even if an article is read by an anonymous user (who click on the button that sends to the original source from the redirection page, see the [first section](#first-page-and-full-text-search)), this information is used by the algorithm.

Another way to contribute to this collaborative news discovery mechanism is by tipping new feeds and articles.

We saw in the [first section](#first-page-and-full-text-search) that the calo.news platform aggregates from high-quality feed chosen by the members of the EIG; all members can contribute to make the information choice richer, by tipping a new feed. This collaborative mechanism of discovery of quality sources is essential, because from time to time certian sources are exhausted (the blogger makes a career jump or finds a new job ...) but new ones always appear !

The "Add a new feed" function can be reached by clicking on "feeds" and then "new" from the top bar, and then requires filling in some data:

![creating a new feed](images/6-1_newfeed.png)

the hardest thing here is to find the link to the RSS feed which is often well hidden ! However, if all goes well, in a few minutes the aggregator will automatically start to upload news from the new feed.

Sometimes you may find a single interesting article; you can also tip a single article by clicking on "new" from the home page; this function is easier to use than the previous one because you just have to enter the URL of the article and press submit ! Before inserting the new article is presents a preview (sometimes you have to clean the text a little bit and / or enter by hand the author (which is not always automatically detected).

Here is an example of how the article https://thewire.in/education/without-internet-kashmirs-doctoral-scholars-are-stumped-for-a-way-forward is tipped:

![tipping a new article](images/6-3_newarticle.gif)

After this, the EIG members will be able to read the full text of the article at this link: https://notizie.calomelano.it/article/1158377 while anonymous users as usual from there will be directed to the original link.

Reported articles are archived as if they came from the feed "AAA – tipped articles" which has as its logo the pointing hand (it is the pointer icon when the mouse hovers a link).

## Private / public conversations and blog 

By reading, voting and tipping articles and feeds on the calo.news platform members help each other to find interesting news, in a collaborative fashion.

Conversations are another great way to share skills, knowledge and interests and to build a common knowledge.

You can't talk about conversations without talking about the philosophy behind the EIG.

The sparse group who has been behind the collective "calomelano" blog for more than ten years at some point realized that almost all the information chaos we find ourselves in is due to the prevalence of visual content (videos and images), and the promiscuity between gossip and thoughtful content, which require more time to read and concentrate.

In the timeline (call it feed or dashboard) of any generalist social networks you find videos of cats, personal emotions, factual news and comments, all mixed up in a total mess.

So we thought of separating three moments: **slow reading** without the distraction of images and videos ([iconoclasm !](https://en.wikipedia.org/wiki/Iconoclasm)), **fast reading/writing**, and **slow writing**. From this break-down descend the three main components of the calo.news platform: the slow-reading aggregator, the private forum for quick conversations, and the slow-writing blog.

In fact many of us also have social accounts, so quick conversations can be split between private ones on the forum component of the calo.news platform, and public ones on various social networks.

A figure can clarify:

![aggregator + forum + blog](images/schema.png)

In practice, to get a private forum without reinventing the wheel, we choose [Discourse](https://www.discourse.org/), a great open-source product that is the result of a Web 2.0 rethinking of ancient things like usenets, bulletin boards and forums, equipped with a set of tools to manage the dark side of the human mind and steer people towards civilized discussion.

And of curse as a blog platform we choose [WordPress](https://wordpress.org).

Let's see how you can manage public and private conversations.

After reading an article you may want to comment on it. Nothing is easier ! Just type the text (short) and press "Send comment":

![commenting an article](images/7-0_comment.gif)

The comment is published on the private forum, where if one wants there are advanced functions for editing and adding images (because we are iconoclasts only in the aggregator):

![editing an article](images/7-1_editcomment.gif)

The aggregator interprets comments as an expression of interest on the article that is commented upon, so it promotes the commented articles in the newsfeed of other members, who are encouraged to participate in the conversation, for example with a "Like":

![liking a comment](images/7-2_like.gif)

and then maybe by replying to the comment:

![replying to a comment](images/7-3_reply.gif)

Discourse is a quite gamified forum platform, there are notifications, ballons, email notifications etc. all aimed at engaging people in the conversation. The author of the original comment (the one with the panda as an avatar) in this case will see several notifications, even on the first page of the aggregator, but to see the actual content she must go to the forum component:

![discourse notifications as seen in the aggregator](images/7-4_notifications.gif)

The advantage of using a private forum is that you are less likely to commit crimes such as libel or copyright infringement.

Using generalist social networks instead you have to be more careful what you say !

On a social network like Mastodon that allow you to fine-tune the audience (public posts, posts hidden by public timelines, posts reserved to friends, private messages), there are more degrees of freedom. Let's see how to share a hidden link on Mastodon:

![sharing an article on Mastodon](images/7-5_share.gif)

The post ("toot") will appear like this on the social network:

![shared article on Mastodon](images/7-6_mastodon.png)

keep in mind that if your friends on the social network are not members of the EIG, when they click on this link they do not see the full text and private comments like us, but they land on the usual redirection page that sends them to the original source for reading the full text (you can simulate what they see by opening the link in an incognito window of your browser).

## Premium feeds

Some of the feeds we want to contribute to and read from (such as professional sources with a subscription fee) have chosen to hide their articles behind a paywall (area reserved for subscribers). We think that this is wrong, as are DRM and all the other mechanisms that in order to protect copyright harm other rights. We want to move on from paper to digital media, however without losing the freedoms and rights that the good old printed paper gives us (and no one has found solutions to these problems yet) !

Faced with this evolving situation, in order to achieve the ethical goals that we have set ourselves, we have no other chance than to find a way to bypass the paywalls, in a way that is not too complicated because we are simple would-be hackers. This is done with some trivial Python scripts that for your delight you can also go and read [here](https://gitlab.com/simevo/calo.news/blob/master/py/poll_96.py). The ublishers could easily take countermeasures, but we do not want to get involved in an escalation: if we can get a copy of all the content without too much effort, OK; otherwise bye-bye and we will read something else.

The feeds (with an active subscription or that have had a subscription at some point in time) so aggregated by bypassing paywalls, are called **premium** and highlighted with a small golden cockade:

![premium feeds](images/8-1_premium.png)

Even for these feeds we have a copy of the full text of all the articles (at least as long as the EIG has been subscribed thanks to one of the members); these contents are normally inaccessible to non-subscribers as shown in this screen-cast where the link to the original source is opened in a tab, showing the paywall:

![paywall](images/8-2_paywall.gif)

In order to bypass paywalls we need to use the credentials of a digital subscription and pass them to the Python script, but there is no need to share username and password; there is a secure procedure described [here (Italian)](https://calomelano.it/?p=2064).

At this point it may seem that we are pirates who illegally redistribute copyrighted and paywall-protected content, in violation of the law and also of the EIG's Objective 2 as formulated in our [manifesto ](https://calomelano.it/?p=1550): "_to ensure fair remuneration to those who produce quality information_".

With regard to the **legality**, as already written above, we make copies of journalistic publications for private use and without commercial purposes, in accordance with the exception granted by Article 15 of the EU 2019/790 Copyright Directive in the digital single market.

For the **ethics**, see the [next section](#business-model).

## Business model

When User Generated Content appeared on the internet in the early 2000s (first with personal sites and then with blogs), there was a proliferation of aggregation services, made possible by the RSS standard for publishing feeds. But who still remembers today google reader, fever, newsgator, digg, reddit, delicious, etc. ? these services are all dead or acquired or converted, or used only in certain niches, for having failed to demonstrate a profitable business model, and for having being crushed by the [OTTs](https://en.wikipedia.org/wiki/Over-the-top_media_service).

It seems clear at this point that there can be no successful commercial business model for news aggregation: there is no spotify or netflix of information (actually the Dutch startup [blendle](https://blendle.com/getpremium) is trying, but without much success, it seems). On the one hand the publishers do not want intermediaries, on the other hand the OTTs and generalist social networks offer free services that overlap with those that would be offered by an aggregator ...

But we believe that there is room for an alternative: a non-profit and ethical intermediary, along the lines of the **Ethical Purchasing Groups** ([Italian: GAS](https://en.wikipedia.org/wiki/Gruppi_di_Acquisto_Solidale)) or of the English-speaking [farmer's markets](https://en.wikipedia.org/wiki/Farmers%27_market) and [CSA (**Community Supported Agriculture**)](https://en.wikipedia.org/wiki/Community-supported_agriculture): the **EIG** (Ethical Information Group).

As in many sectors, we are also following a path that starts from a pre-existing **intermediated** system (newspapers: publishers, distributors, newsagents), passes through a first wild phase of **dis-intermediation** (first years of the digital age of information: users seek direct contact with news producers and skip all the intermediaries) then comes the **anti-dis-intermediation ** (the digital landscape is consolidated, the OTTs become the new centralized intermediaries).

The EIG is a form of **counter-anti-dis-intermediation**, and aims to be an ethical intermediary between readers and information producers.

So what is the business model for the EIG ? There is no business model !

To be certainly a non-profit, the EIG currently operates without touching any money. We accept no donations !

The EIG members directly bear the costs for the operation of the platform, through personal donations and payments to the providers of the respective services. To ensure that everyone contributes equally, we keep track of the accounting and occasionally there are compensations between members.

A [homemade market analysis for the Italian market](https://calomelano.it/?p=2002) shows that every citizen (including infants) spends **140 €/year** for staying informed, of which 60 €/year without realizing it and without being able to choose (public service TV, taxes and advertising), and 80 €/year by means of a "free" decision.

On this basis, the member contribution for the [calomelano.it live instance](https://notizie.calomelano.it/) was set at **0.15 €/day**, equal to 54.75 €/year; this is the amount that every member must contribute on average.

Other instances can of course decide their own policies and contributions.

## Open-source and replicability

The source code that powers the calo.news platform is available here: https://gitlab.com/simevo/calo.news with the open-source license [AGPL-3](https://www.gnu.org/licenses/agpl-3.0.html).

As discussed in the [algorithms](#algorithms) and in the [premium feeds](#premium_feeds) sections, if you want to understand what goes on behind the scenes of the calo.news platform there is nothing better than reading the source code.

So the first advantage of open-source is ensuring **transparency** of the algorithms.

The other aspect is **replicability**: by copying the calo.news platform code, anyone can turn on their own independent instance, with other members, other policies, other feeds ... creating the beginning of a **decentralized network of aggregators**.

If you want to operate your own instance of the calo.news platform, have a look at the [calo.news system administration guide](https://gitlab.com/simevo/calo.news/-/blob/master/doc/ADMIN.md).

Actually, the code at the moment is not easy to use: there is a lot of work to make it really reusable ! But if anyone tries, the code would surely benefit in terms of quality.

If you want to contribute, have a look at the [calo.news development guide](https://gitlab.com/simevo/calo.news/-/blob/master/doc/DEVELOPER.md).

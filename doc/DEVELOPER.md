# Developer guide for the calo.news platform

How to hack on calo.news and contribute back to the project.

[[_TOC_]]

## Introduction

**calo.news** is an **open-source news platform** with aggregation, ranking and conversations, designed to be **self-hosted** and managed by a **community** of members that share a preferred language and common interests.

If is made up by four integrated components:

1. **news aggregation** engine

2. **news reading** web application

3. a forum for private social **conversations** based on [Discourse](https://www.discourse.org/)

4. a public **blog** based on [WordPress](https://wordpress.org)

The project repository is hosted on [gitlab.com](https://gitlab.com/simevo/calo.news) and contains the source code for the **first two components**, and the glue code required to integrate the forum and the blog components.

## Installation

Development is preferably done using docker to start an incomplete instance running from local filesystem; in this way changes to local files are reflected in the running services.

For a three-minute demo of docker-compose setup for development on the calo.news news platform watch this youtube video :

[![Three-minute demo of docker-compose setup for development on the calo.news news platform](http://img.youtube.com/vi/jWRidPQ9wWc/0.jpg)](http://www.youtube.com/watch?v=jWRidPQ9wWc "docker-compose setup")

Alternatively you can run a more complete **sandbox** instance in a lxc container, a VirtualBox virtual machine or cloud-hosted Virtual Private Server (VPS).
See the [Installation with Ansible section of yhe System administration guide](https://gitlab.com/simevo/calo.news/-/blob/master/doc/ADMIN.md#installation-with-ansible) for how to set up such a sanbox.

In this case use the `bin/update.sh` to update the files to your sandbox (will use the hostname AGGREGATOR_HOSTNAME in your `.env` file):

```shell
./bin/update.sh
```

### Preparation

Copy default environment variables from `.env.development` (optionally) adapting it:

```shell
cp .env.development .env
vi .env
```

Install requirements on Debian 12 (bookworm):

```shell
sudo apt install docker.io docker-compose yarnpkg webpack make composer
```

Read `/usr/share/doc/docker.io/README.Debian` !

Check service status:

    sudo systemctl status docker

Add yourself to the `docker` group (replace `paolog` with your userid):

    sudo adduser paolog docker
    
refresh group info (no need to log out and then in again):

    newgrp docker

verify the current user has been added to the 'docker` group:

    groups

expected output:

    docker cdrom floppy ... 

### docker-compose set-up

Pull, build and start all containers with a single command:

```shell
docker-compose up --build
```

Create www-data role, populate the db, force polling and precompute the views:

```shell
docker-compose exec db sh -c 'echo "create role \"www-data\"" | psql -U simevo calonews'
docker-compose exec db sh -c 'cat /sql/*.sql | psql -U simevo calonews -'
docker-compose exec backend ./py/poll.py
```

You should now be able to access the services:

- adminer UI: http://localhost:8090/?pgsql=db&username=simevo&db=calonews&ns=public (enter password: `simevo`)

- backend: http://localhost:8080/articles

- frontend: http://localhost:8100

- backend proxied by the frontend: http://localhost:8100/api/articles

Initially if you visit the newfeed, the message "Non ci sono ancora notizie per te, riprova tra qualche minuto !"; after the first access you must populate the precompute views with:
```shell
docker-compose exec backend ./py/precompute.py all
```
then refresh the newsfeed.

Besides adminer, you can enter the db container with a PostgreSQL interactive terminal (psql) or connect from host psql:

```shell
docker-compose exec db psql -U simevo calonews
psql -p 5433 -h localhost -U simevo calonews
```

To stop everything (add `-v` option if you also want to clear the persistent database storage):

```shell
docker-compose down
```

**NOTE**: the docker-compose set-up is incomplete because:

- it does not start the Discourse forum hence there is no SSO and just one dummy user

- it does not start the WordPress blog

- all the cron jobs (polling, translate, precompute ....) are disabled

## Development

Get the JavaScript dependencies (will be installed in `node_modules`):

```shell
yarnpkg install
```

Then install the files to `www`:

```shell
make
```

Get PHP dependencies (will be installed in `vendor`):

```shell
composer install
```

To upgrade all dependencies:

```shell
yarnpkg upgrade
composer update
rm -rf ansible/roles/ANXS.postgresql ansible/roles/geerlingguy.php ansible/roles/geerlingguy.nginx
ansible-galaxy install -r ansible/requirements.yml -p ansible/roles/
```

After every change to the Vue components in `src/`, run the `bin/build.sh` script to compile and copy them to `www/`:

```shell
./bin/build.sh
```

## Technologies

The news aggregation and news reading components of the calo.news platform use the following technologies:

- nginx web server

- HTML5 / CSS / JavaScript for the front-end; code is in ECMAScript 5 so no transpiling is required

- The Vue 2 JavaScript framework for routing, reactivity and components

- PHP 8.2 for server-side templating, with:

  - php-fpm to talk to nginx

  - [TCPDF](https://tcpdf.org/) to generate PDF documents

  - PHPMailer to send emails

- Python 3 with:

  - the bottle.py microframework for the API

  - [EbookLib](https://github.com/aerkalov/ebooklib) for epub generation

  - Amazon [KindleGen](https://www.amazon.com/gp/feature.html?ie=UTF8&docId=1000765211) for mobi generation

  - psycopg2 database driver

  - matplotlib to generate SVG plots

  - feedparser to parse RSS feeds

- The PostgreSQL 13 database

## Repository layout

- documentation:

  - `doc/ADMIN.md` (administration guide)

  - `doc/DEVELOPER.md` (this document)

  - `LICENSE` (GNU AGPLv3 license text)

  - `README.md` (main README)

  - `doc/USER.md` (user guide)

  - `doc/images/` (screenshots and screencasts used by the documentation)

- devops:

  - `ansible/`

  - `docker/`

  - `docker-compose.yml`

- PHP dependencies management:

  - `composer.json`

  - `composer.lock`

- JavaScript dependencies management:

  - `package.json`

  - `yarn.lock`

  - `Makefile` (installs the JavaScript dependencies and their static assets from `./node_modules` to `./www`)

- front-end:

  - `src/` (source code for the Vue components)

  - `bin/build.sh` (compiles the Vue components to HTML / JavaScript)

  - `bin/fontawesome.sh` (extracts the fontawesome icons we need to a single, compact SVG file)

  - `bin/vtc.js` (Compile a Vue component from a single file source)

  - `www/` (PHP code, static assets, JavaScript dependencies and compiled Vue components)

- back-end:

  - `bin/readability.js` (http service that exposes the Readability.js library, a standalone version of the readability library used for Firefox Reader View)

  - `py/` (python3 code of the API)

  - `views/` (jinja2 templates needed for the API)

- database set-up and migration scripts:

  - `sql/`

- helper scripts:

  - `bin/update.sh` (to push updates to the production server)

  - `bin/update_year.sh` (updates copyright year)

  - `bin/validate.sh` (validates JavaScript with jshint and HTML5 with nu.validator)

- non-public stuff:

  - `secrets/` and `secrets_dummy/` (here are stored passwords and API keys)

## Validation

Start the standalone checker HTTP service (see: https://validator.github.io/validator/#standalone)

```shell
java -cp /opt/vnu_16.6.29/vnu.jar nu.validator.servlet.Main 8888
```

Launch the validation script:

```shell
./bin/validate.sh
```

## Contributing

**Issues** and **Merge Requests** are welcome !

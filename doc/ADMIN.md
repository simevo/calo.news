# System administration guide for the calo.news platform

How to install and operate an instance of the calo.news platform.

[[_TOC_]]

## Introduction

**calo.news** is an **open-source news platform** with aggregation, ranking and conversations, designed to be **self-hosted** and managed by a **community** of members that share a preferred language and common interests.

If is made up by four integrated components:

1. **news aggregation** engine

2. **news reading** web application

3. a forum for private social **conversations** based on [Discourse](https://www.discourse.org/)

4. a public **blog** based on [WordPress](https://wordpress.org)

The project repository is hosted on [gitlab.com](https://gitlab.com/simevo/calo.news).

Account management is handled by the Discourse forum which acts as Single Sign On (SSO) provider for both the news-reading web application and the WordPress blog (using the [WP Discourse plugin](https://github.com/discourse/wp-discourse) SSO client setting).

## Requirements

The platform is only actively tested on the Debian 12 (bookworm) operating system, although it has run on previous versions in the past.

The [calomelano.it live instance](https://notizie.calomelano.it/) currently runs on an [Aruba cloud](https://cloud.it) VPS "Large" (12,50 €/month) virtual machine with these resources:

- 2 cores

- 4 GB RAM

- 80 GB SSD

- 12 TB/month of traffic

This should be enough for 1-100 users. Compare with the [Discourse hardware requirements](https://github.com/discourse/discourse/blob/master/docs/INSTALL.md#hardware-requirements).

## Third-party services

For production mode, you need access to a natural language machine translation API.

The platform comes pre-configured for the [Microsoft Translator Text API](https://azure.microsoft.com/en-us/pricing/details/cognitive-services/translator-text-api/) which has a free plan of 2M chars / month.

Alternatives you could consider:

- The [Yandex Translate API](https://tech.yandex.com/translate/) with its free tier of 1M characters / day but not more than 10M characters / month

- [Amazon Translate](https://aws.amazon.com/it/translate/pricing/) with a free plan of 2M characters / month for 12 months

- [deepl](https://www.deepl.com/pro.html#developer)

but you'd need to write adapters for those (see `py/translate.py`).

## Installation

### Preparation

Copy default environment variables from `.env.sandbox` (optionally) adapting it, then load them:

```shell
cp .env.sandbox .env
vi .env
set -a
. ./.env
set +a
```

For **production**, create a `secrets` directory, it should contain:

- `api_key`: the Discourse api key (no endline at the end !)

- `key`: the Microsoft translator API key (no endline at the end !)

- `passwd.client`: the exim4 smarthost credentials file that will end up in /etc/exim4/passwd.client

- `private_key.json`, `regr.json` and `meta.json`: the letsencrypt credentials

- `secret`: the server-side secret to sign the token cookies, base64 encoded

- `sso_secret`: the secret string used to hash SSO payloads when Discourse is the SSO provider (no endline at the end !)

- `tor_secret`: the HashedControlPassword for the tor control port

For the **sandbox**, create a `secrets_dummy` directory and populate it:

```shell
mkdir -p secrets_dummy
echo 'password' > secrets_dummy/wp_db_password
wget -O secrets_dummy/wp_salt https://api.wordpress.org/secret-key/1.1/salt/
```

### Installation with Ansible

**Sandbox** and **production** installs are supported only with [ansible](https://www.ansible.com/).

First install it on the controlling host. For Debian 12 (bookworm) hosts:
```shell
sudo apt install ansible
```

Then set-up a dedicated Debian 12 (bookworm) machine as a lxc / nspawn container, VirtualBox Virtual Machine (VM), cloud-hosted Virtual Private Server (VPS) or baremetal machine; for local tests and development, an lxc container is OK.

Assuming the dedicated machine can be reached as `192.168.1.166`, make sure your `/etc/hosts` has something like:

```
192.168.1.166   calonews-sandbox notizie.calonews-sandbox commenti.calonews-sandbox blog.calonews-sandbox
```

Set up passwordless ssh access for user `root` to the target, then install ansibile prerequisites:

```
ssh root@calonews-sandbox apt update
ssh root@calonews-sandbox apt install -y python
```

For **sandbox** launch the install with:

```
ansible-playbook -i ansible/hosts ansible/sandbox_local.yml
```

For **production** adapt `ansible/roles/discourse/tasks/app.yml` (DISCOURSE_SMTP_ADDRESS, ssh_key), make sure all roles are uncommented in `ansible/calonews.yml` then launch the ansible script:

```shell
ansible-playbook -i ansible/hosts ansible/calonews.yml
```

The install should fail during the last step with this error:

```
fatal: [sandbox_local]: FAILED! => {"changed": false, "msg": "Cannot create container when image is not specified!"}
```

Manually rebuild the Discourse container with:

```shell
ssh root@calonews-sandbox
/var/discourse/launcher rebuild app
```

Finalize the certbot install by executing:

```
sudo certbot certonly -t --force-renewal -vvv --webroot -w /var/www/html -d notizie.calomelano.it
sudo certbot certonly -t --force-renewal -vvv --webroot -w /var/www/html -d commenti.calomelano.it
```

### Manual post-install

Add this line to `/etc/hosts`:

```
127.0.0.1   calonews-sandbox notizie.calonews-sandbox commenti.calonews-sandbox blog.calonews-sandbox
```

Finalize configuration:

- for WordPress: https://blog.calonews-sandbox/wp-admin/install.php

- and for Discourse: https://commenti.calonews-sandbox/.

In **sandbox** mode the Discourse install will not have a functioning Mail Transfer Agent, so you can't activate the admin account. Either fix the MTA or create the admin account the hard way with:

```shell
cd /var/discourse
./launcher enter app
rake admin:create
```

For **production** the MTA must be configured; test email send from the target with something like:

```shell
ssh user@example.com
echo 'just a test' | mail -r user@example.com -s 'test' user1@example.com
```

then test email send from the Discourse docker container with something like:

```shell
ssh -p 2222 root@example.com
echo 'just a test' | mail -r user@example.com -s 'test' user1@example.com
```

Finally open https://notizie.calonews-sandbox and click on "Connetti".

Generate a key that operates on the Discourse forum on behalf of all users from: https://commenti.calonews-sandbox/admin/api/keys/new and copy that in `secrets/api_key` (without trailing newline) as in:

```shell
cd /srv/calo.news
echo -n b2e310c1f1721a2151f78aea8eb34f58d9b380de53157fa7505d2724f7591aaf > secrets/api_key
```

Create a group `soci` in https://commenti.calonews-sandbox/g/custom/new

Create the `notizie`, `fonti` and `gruppo di informazione` categories in https://commenti.calonews-sandbox/categories (second-row, top-right hamburger menu -> new category).
Create the subcategory `contabilità` inside `gruppo di informazione`.

Retrieve the group and category IDs from https://commenti.example.com/categories.json and update `.env` accordingly.

Make sure the `./py/poll.py` and `./py/precompute.py all` scripts run successfully (look at `crontab -l` for user `simevo`). You should then see some data pop up in the newsfeed.

Create webhooks in https://commenti.calonews-sandbox/admin/api/web_hooks:

- send to Payload URL https://notizie.calonews-sandbox/api/comment in json format on topic and message events

- send to Payload URL https://notizie.calonews-sandbox/api/user_event in json format on user and message events

Save the passphrase for both webhooks in `secrets/webhook_secret` (without trailing newline).

## Operation

### Feed polling

Most of the RSS feeds can be read every hour, but some of them must be read only at certain times (e.g. at 2:30 or 6:45 or only on the first day of the month).

Moreover, almost all sources have only one RSS feed to read, but for some you have to read more than one.

These "special" sources are managed with special scripts such as `py/poll_19.py` (multiple RSS feeds) or `py/poll_96.py` (scraping of paywalled source).

All these special scripts alongside with the polling of the "regular" feeds are scheduled by crontab, like this:

```
crontab -l

37 * * * cd /srv/calo.news && /bin/sh -a -c '. "$0" && exec "$@"' ./.env ./py/poll.py >> cron.log 2>&1
```

The `poll.py` scripts goes to the database to see if the sources need particular scripts, and if they should only be read at certain times.

The latter can be specified with a fragment (in JSON format) stored in the `frequency` column of the `feeds` table, for example:

```javascript
{
  "day": [1, 2, 3],
  "hour": [12, 13],
  "weekday": [7]
}
```
means: "_only when all these conditions occur: if today is one of the first three days of the month, if it is Sunday and if it is 12 or 13 UTC time_".

Feed management can be performed interactively (but only by members of the Discourse `staff` group) from each feed detail page: https://notizie.calomelano.it/feeds/1

Clicking on "settings" it allows you to:

- Modify:

  - the properties set at the time of creation (Name, homepage, RSS feed, Language and License)

  - the icon

  - the Tags (e.g. blog / magazine / italy / world etc.)

  - Premium source status

  - the parameters that determine how and when the source is read (Active, Script, Frequency, Incomplete, Salt url and Cookies)

- Interactively force source scan

- Display a graph of the number of articles read from the source

### Monitoring

There is a page to check the status of services and logs: https://notizie.calomelano.it/status.php

The page is visible in simplified form for anonymous users, with the mere indication of the operating status of the 4 main services:

![status page for anonymous users](images/status.png)

For EIG members there is also a section on logs, which allows you to browse and filter the logs of the last 7 days according to the channel (severity of the event) and the type of log:

![status page for staff members](images/status2.gif)

When some services are down, the status page is shown in simplified form also for logged-in users.

In addition, the statistics page on the sources https://notizie.calomelano.it/feed_stats.php shows in tabular form the number of items aggregated by source in the last 24 hours, 7 days or 30 days:

![feed stats page ](images/status3.png)

### Recipes

To reset Discourse:

```shell
cd /var/discourse
./launcher stop app
docker images
docker rmi -f HEX_ID_1 HEX_ID_2 HEX_ID_3 HEX_ID_4 ETC ETC
cd ..
rm -rf discourse
```

To restore the datadase:

```shell
cat > drop.sql
drop view articles_data;
drop table user_articles;
drop table user_feeds;
drop table users;
drop table articles;
drop table feeds;
^d
cat drop.sql | psql calonews
sed -i 's/OWNER TO simevo/OWNER TO paolog/g' db_20170429.sql
cat db_20170429.sql | psql calonews
```

## References:

- https://meta.discourse.org/t/emails-with-local-smtp/23645/28

- https://meta.discourse.org/t/setting-up-discourse-smtp-address-localhost/16027/17

- http://pkg-exim4.alioth.debian.org/README/update-exim4.conf.8.html

- https://docs.docker.com/engine/installation/linux/debian/

- https://github.com/discourse/discourse/blob/master/docs/INSTALL-cloud.md

- https://meta.discourse.org/t/running-other-websites-on-the-same-machine-as-discourse/17247

- https://meta.discourse.org/t/using-discourse-as-a-sso-provider/32974

Ciao {{ username }},

Il tuo conto virtuale è di nuovo superiore alla soglia di {{ final_threshold }} € quindi sei nuovamente soci* attivo del GIS (Gruppo di Informazione Solidale).

Puoi verificare il saldo e i movimenti qui:
https://{{ aggregator_hostname }}/settings.php#account

Grazie !

il GIS.

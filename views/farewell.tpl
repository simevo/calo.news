Ciao {{ username }},

il tuo conto virtuale è in rosso ed ha raggiunto la soglia di {{ final_threshold }} € quindi da oggi non sei più soci* attivo del GIS (Gruppo di Informazione Solidale).

D’ora in avanti il tuo accesso alla piattaforma sarà limitato:
- il tuo newsfeed non è più personalizzato
- cliccando sugli articoli vieni dirottat* sulla fonte originale
- non puoi più leggere le fonti a cui siamo abbonati
- non ricevi più la newsletter
- non puoi più scrivere sul blog.

Puoi riattivare la tua associazione al GIS in qualunque momento.

Oppure puoi lasciare tutto così indefinitamente: in tal caso potrai ancora partecipare alle discussioni e vedere le notizie segnalate dai lettori nella sezione commenti, ricevendo anche le relative notifiche.

O ancora se preferisci puoi richiedere la cancellazione dell'account e di tutti i dati non pubblici associati, rispondendo a questa mail in tal senso. Naturalmente il debito è del tutto virtuale quindi in caso di cancellazione nulla è dovuto.

Bye bye !

il GIS.

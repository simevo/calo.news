<?xml version="1.0" encoding="UTF-8" ?>
<rss version="2.0">
  <channel>
    <title>{{ feeds[0].title|escape }}</title>
    <description>{{ feeds[0].homepage }}</description>
    <link>{{ feeds[0].url }}</link>
    <pubDate>{{ articles[0].stamp }}</pubDate>
    {% for article in articles %}
    <item>
      <title>
        {% if article.language != 'it' %}[{{ article.title_original|escape }}] {% endif %}
        {% if article.title %}{{ article.title|escape }}{% endif %}
      </title>
      <description>{{ article.url }}</description>
      <link>https://{{ aggregator_hostname }}/article/{{ article.id }}</link>
      <guid isPermaLink="false">{{ article.id }}</guid>
      <pubDate>{{ article.stamp }}</pubDate>
      <dc:creator><![CDATA[{{ article.author|safe }}]]></dc:creator>
    </item>
    {% endfor %}
  </channel>
</rss>

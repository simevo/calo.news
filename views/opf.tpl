<?xml version="1.0" encoding="iso-8859-1"?>
<package unique-identifier="bookid" xmlns:opf="http://www.idpf.org/2007/opf" xmlns:asd="http://www.idpf.org/asdfaf">
  <metadata>
    <dc-metadata xmlns:dc="http://purl.org/metadata/dublin_core" xmlns:oebpackage="http://openebook.org/namespaces/oeb-package/1.0/">
      <dc:Title>{{ title }}</dc:Title>
      <dc:Language>it-it</dc:Language>
      <dc:identifier id="BookId" opf:scheme="uuid">urn:uuid:{{ uuid }}</dc:identifier>
      <dc:Creator>calo.news</dc:Creator>
      <dc:Publisher>calo.news</dc:Publisher>
      <dc:Subject BASICCode="REF027000">REFERENCE / Yearbooks & Annuals</dc:Subject>
      <dc:Description>{{ description }}</dc:Description>
      <dc:date>{{ date }}</dc:date>
    </dc-metadata>
    <x-metadata>
      <embeddedcover>{{ cover }}</embeddedcover>
    </x-metadata>
  </metadata>
  <manifest>
    <item id="toc" properties="nav" href="{{ toc_nav }}" media-type="application/xhtml+xml"/>
    <item id="page" media-type="text/x-oeb1-document" href="{{ html_stripped }}" />
    <item id="my_cover_image" media-type="image/jpeg" href="{{ cover }}" properties="cover-image" />
  </manifest>
  <spine>
    <itemref idref="toc"/>
  </spine>
</package>

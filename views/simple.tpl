<!DOCTYPE html>
<html lang="it">
  <head>
    <meta charset="UTF-8" />
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
    <title>Article list</title>
  </head>
  <body>
    <ul>
{% for article in articles %}
      <li><a href="/articles.html?ids={{ article.id }}">{{ article.title }} [{{ article.title_original }}]</a> {{ article.length}} bytes</li>
{% endfor %}
    </ul>
  </body>
</html>

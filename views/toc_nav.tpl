<!DOCTYPE html>
<html lang="it">
  <head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
    <title>Indice</title>
  </head>
  <body>
    <h1>Newsletter {{ date }}T{{ time }}</h1>
    <hr/>
    <h2>Indice</h2>
    <nav epub:type="toc">
      <ol>
{% for article in articles %}
        <li><a href="{{ html_stripped }}#ref{{ article.id }}">{% if article.language != 'it' %}[{{ article.title_original }}] {% endif %}{% if article.title %}{{ article.title }}{% endif %} – {{ article.stamp }} – {{ article.author }} – {{ article.feed }}</a> – Tempo di lettura stimato: {{ (article.length/6/300) | round | int }} minuti</li>
{% endfor %}
      </ol>
    </nav>
  </body>
</html>

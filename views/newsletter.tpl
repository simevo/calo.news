Ciao {{ username }},

di seguito troverai la tua newsletter in formato testo.

Per modificare le impostazioni relative alla newsletter visita:
https://{{ aggregator_hostname }}/settings.php#news

A presto !
{% for article in articles %}
{% if article.language != 'it' %}[{{ article.title_original }}] {% endif %}{% if article.title %}{{ article.title }}{% endif %}
Pubblicato: {{ article.stamp }}
Da: {{ article.feed }}
Tempo di lettura stimato: {{ (article.length/6/300) | round | int }} minuti
Leggi: https://{{ aggregator_hostname }}/article/{{ article.id }}
{% endfor %}

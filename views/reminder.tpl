Ciao {{ username }},

questa mail è per ricordarti che il tuo conto virtuale è in rosso di {{ account | round(2) }} €.
Puoi verificare il saldo e i movimenti qui:
https://{{ aggregator_hostname }}/settings.php#account

Quando il conto arriva a {{ final_threshold }} € non sei più soci* attivo del GIS (Gruppo di Informazione Solidale) e il tuo accesso alla piattaforma viene limitato: 
- il tuo newsfeed non sarà più personalizzato
- cliccando sugli articoli verrai dirottat* sulla fonte originale
- non potrai più leggere le fonti premium a cui siamo abbonati
- non riceverai più la newsletter
- non potrai più scrivere sul blog.

Come spiegato qua: https://{{ discourse_hostname }}/t/442
tutti i conti si scaricano alla velocità di {{ (daily_fee * 100) | round | int }} centesimi al giorno quindi il tuo arriverà alla soglia di {{ final_threshold }} € tra {{ days_to_go | round | int }} giorni (il {{ end_date }}).

Puoi ricaricare il conto quando e come vuoi:
- sottoscrivendo a titolo personale un abbonamento digitale (o facendo una donazione) ad una fonte di tua scelta
- oppure dando una somma a uno degli altri soci che ha il conto in attivo.
e registrando la transazione nella categoria contabilità:
https://{{ discourse_hostname }}/c/{{ eig_category_name }}/{{ accounting_category_name }}

Se hai deciso di sostenere una fonte, comincia segnalando agli altri quale abbonamento hai intenzione di sottoscrivere qui:
https://{{ discourse_hostname }}/c/{{ feeds_category_name }}

Grazie !

il GIS.
{% if account - final_threshold <= 1%}
P.S. questo è l'ultimo avviso.
{% else %}
P.S. ti manderemo ancora {{ (account - final_threshold) | round | int }} di questi avvisi.
{% endif %}


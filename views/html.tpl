<!DOCTYPE html>
<html lang="it">
  <head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
    <title>Newsletter {{ date }}T{{ time }}</title>
  </head>
  <body>
    <p>Estratto notizie dall'aggregatore del <b>G</b>ruppo di <b>I</b>nformazione <b>S</b>olidale (<b>GIS</b>). Per saperne di più leggi il <a href="http://calomelano.it/?p=1521">manifesto del GIS</a>.</p>
    <p>Per modificare le impostazioni relative alla newsletter visita: <a href="https://{{ aggregator_hostname }}/settings.php#news">https://{{ aggregator_hostname }}/settings.php#news</a></p>

{% for article in articles %}
    <hr/>
    <h2 id="ref{{ article.id }}">{% if article.language != 'it' %}[{{ article.title_original }}] {% endif %}{% if article.title %}{{ article.title }}{% endif %}</h2>
    <p>
      <strong>Pubblicato</strong>: {{ article.stamp }}
    </p>
    <p>
      <strong>Di</strong>: {{ article.author }} {{ article.license }}
    </p>
    <p>
      <strong>Da</strong>: {{ article.feed }}
    </p>
    <p>
      <strong>Tempo di lettura stimato</strong>: {{ (article.length/6/300) | round | int }} minuti
    </p>
    
{% if article.content %}
{% if article.language != 'it' %}
    <h3>Testo tradotto</h3>
    <a href="#native_{{article.id}}">Vai al testo in lingua originale</a>
{% endif %}
    <div style="text-align:justify;">
      {{ article.content }}
    </div>
{% endif %}
{% if article.language != 'it' %}
    <br/>
    <h3 id="native_{{article.id}}">Testo in lingua originale</h3>
    <div style="text-align:justify;">
      {{ article.content_original}}
    </div>
{% endif %}
    <p>
      <strong>Leggi nell'aggregatore</strong>: <a target="_blank" href="https://{{ aggregator_hostname }}/article/{{ article.id }}">https://{{ aggregator_hostname }}/article/{{ article.id }}</a>
    </p>
{% endfor %}
  </body>
</html>

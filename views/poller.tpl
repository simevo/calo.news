Hi I just run the poller script for feed {{ feed_id }}.

There were {{ count }} candidate links, {{ to_retrieve }} to be retrieved and {{ non_articles }} non-articles.

Of these links I skipped {{ skipped }} and actually retrieved {{ retrieved }}.

Bye

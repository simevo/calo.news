<?xml version="1.0" encoding="UTF-8" standalone="no"?>
<svg
   xmlns:dc="http://purl.org/dc/elements/1.1/"
   xmlns:cc="http://creativecommons.org/ns#"
   xmlns:rdf="http://www.w3.org/1999/02/22-rdf-syntax-ns#"
   xmlns:svg="http://www.w3.org/2000/svg"
   xmlns="http://www.w3.org/2000/svg"
   version="1.1"
   id="svg2"
   width="600"
   height="800"
   viewBox="0 0 600 800">
  <metadata
     id="metadata8">
    <rdf:RDF>
      <cc:Work
         rdf:about="">
        <dc:format>image/svg+xml</dc:format>
        <dc:type
           rdf:resource="http://purl.org/dc/dcmitype/StillImage" />
        <dc:title></dc:title>
      </cc:Work>
    </rdf:RDF>
  </metadata>
  <defs
     id="defs6" />
  <rect
     style="color:#000000;display:inline;overflow:visible;visibility:visible;opacity:1;fill:#817e7e;fill-opacity:1;fill-rule:nonzero;stroke:#000000;stroke-width:1.33000004;stroke-linecap:round;stroke-linejoin:miter;stroke-miterlimit:4;stroke-dasharray:none;stroke-dashoffset:0;stroke-opacity:1;marker:none;paint-order:markers fill stroke;enable-background:accumulate"
     id="rect4560"
     width="600"
     height="800"
     x="0.66500002"
     y="-0.66500002" />
  <path
     style="fill:#000000;fill-opacity:1;stroke-width:0.25453717"
     d="m 148.76208,669.6541 c -28.2331,-5.2154 -1.0917,-44.396 -11.2909,-59.7637 -19.3004,13.5688 -58.3716,66.5563 -68.3296,41.2561 -8.0979,-20.5745 23.6223,-43.0944 43.1243,-59.8596 -16.4236,-9.1169 -71.1048,15.9846 -67.7986,-9.109 4.2682,-32.3943 67.7233,-18.8088 101.2175,-14.8089 26.5058,5.8546 47.2879,-36.0995 40.5289,-56.8548 -12.9916,-2.9526 -26.2951,34.0936 -34.7523,6.7372 0.2173,-21.0345 34.4757,-42.8877 19.7639,-61.227 -3.4202,15.9053 -27.9683,26.4354 -23.0172,1.8638 2.7998,-9.9676 5.396,-49.1439 -6.292,-24.2219 -5.3228,11.3499 -22.6601,42.091 -31.0581,11.8374 -5.2498,-18.9124 14.5723,-63.9329 1.8716,-63.2269 -9.3433,0.5194 -20.5624,64.9596 -46.3742,34.6242 -15.7992,-20.7541 -4.8613,-47.7856 6.0897,-65.0157 -26.4239,20.6064 -45.33066,-23.5805 -16.1157,-32.9411 13.4823,-4.3197 35.2877,-20.3642 8.1795,-16.4154 -40.30905,5.8718 -30.7671,-45.4222 4.4193,-45.4036 17.7588,0.01 44.9981,9.4476 55.1279,2.9808 -2.2753,-20.4281 14.3662,-27.0179 32.8134,-32.6289 -8.2108,-12.8937 27.3137,-28.8408 32.7831,-38.0511 -27.934,-0.6068 -37.3371,-21.8763 -13.499,-27.6013 18.1697,-4.3637 46.0963,4.696 21.783,-13.8201 -18.3237,-13.9548 36.7024,-20.05606 48.803,-22.25003 7.286,-23.75824 50.1927,-15.58044 70.652,-8.1459 11.2151,-19.37935 51.1444,-15.09393 59.6707,10.84435 5.7953,17.63018 13.1349,14.26928 22.1057,1.71358 29.4489,-9.80923 22.5931,36.3894 33.9894,41.1786 25.4561,-12.7454 36.2564,18.6726 20.8625,33.9037 26.226,4.2868 12.7354,31.8399 -2.0993,36.9825 9.8077,13.0791 39.0562,20.1631 19.4237,38.8749 26.7968,7.5216 -21.0184,34.4676 12.9724,24.5442 29.6896,-12.7357 67.5225,-30.9847 97.7592,-10.2169 30.7326,26.1006 -19.2439,45.9195 -42.5421,45.1694 -7.8677,14.9535 31.5286,-3.7105 42.0969,0.6348 28.782,9.5851 -6.0301,27.3328 -22.047,27.3762 -18.2265,0.049 -25.0194,3.07 -2.2838,10.1548 22.8439,7.1187 33.0975,34.1693 8.2372,32.1074 -18.0771,-1.4993 -57.6333,-20.5572 -57.6261,-4.5511 17.8526,7.3848 65.8108,36.8112 30.3477,45.3915 -18.099,4.3791 -37.0728,-1.9755 -51.5666,-1.4748 26.399,12.8977 6.6172,32.8432 -13.7941,21.3423 -10.9809,-7.9771 -26.9425,-6.837 -8.5446,4.1727 23.2496,14.0684 -3.4989,23.8939 -18.4245,11.9815 -10.3443,-1.2996 -12.408,11.4772 -26.3406,9.6243 21.1009,25.5269 -22.7126,23.3728 -29.7639,18.6438 -0.4811,18.5247 -27.922,11.4209 -35.5249,24.1014 -10.4865,-0.3336 -29.1058,-16.0257 -14.5201,3.7031 21.1105,21.8031 34.7307,23.3401 78.379,38.5859 63.8812,22.3129 -3.1448,38.4527 -26.2567,26.1147 21.114,17.929 56.3514,56.2444 24.953,57.5863 -20.8532,0.8912 -51.4995,-69.1054 -52.8432,-19.5964 -0.6933,27.4097 -36.5629,21.2317 -28.824,-3.6592 7.9116,-25.4465 18.3403,-52.4285 -3.5727,-77.2326 -24.8044,-28.0771 -22.7974,-58.5152 -43.4225,-22.8956 -19.3556,33.4271 -43.2113,-40.7259 -47.3742,-3.9165 3.5835,24.6088 -4.2805,33.9064 -17.0906,15.2704 2.7636,19.3649 -12.9715,18.7727 -19.1301,3.2498 -12.9818,22.9513 -42.0629,36.8501 -42.1506,70.0346 -0.042,16.0475 3.5481,45.4851 -13.6847,42.3018 z m -21.8916,-441.7405 c -7.4328,-27.499 -66.6147,-16.3164 -52.5585,-46.8304 20.1469,-43.7362 69.3963,40.7423 52.5585,46.8304 z M 90.07578,114.07853 c 21.9029,-9.7974 38.9552,32.25677 39.5656,56.73447 -32.5424,-26.7947 -61.4684,-46.93706 -39.5656,-56.73447 z"
     id="path2384" />
  <path
     style="fill:#6a6a6a;fill-opacity:1;stroke:none;stroke-width:0.25453717"
     d="m 213.82598,547.619 c -5.6594,-39.7429 -5.5439,-42.9814 -7.3008,-58.8611 -6.1004,-55.1368 -16.4981,-80.5372 -14.2597,-142.8978 1.4449,-39.9652 32.7335,-79.4562 74.66818,-80.4176 33.4734,-4.2205 66.049,11.0935 89.5169,34.1957 19.9791,25.5075 17.5538,60.1484 1.4907,86.582 -13.8096,22.7253 -33.5173,41.027 -53.2766,58.375 -35.4848,27.1994 -67.54238,63.9965 -90.83868,103.0238 z"
     id="path4749" />
  <path
     style="fill:#ffffff;fill-opacity:1;stroke:none;stroke-width:0.25453717"
     d="m 358.82116,287.4461 c -18.5419,-3.98183 -31.7007,-15.4391 -37.7491,-26.9319 -12.1246,-24.5158 12.8213,-55.62701 38.151,-59.86851 27.6946,-4.6375 55.67,11.41641 53.1525,43.05651 -2.3338,29.3303 -29.1077,48.99377 -53.5544,43.7439 z"
     id="path4747" />
  <path
     style="fill:#ffffff;fill-opacity:1;stroke:none;stroke-width:0.25453717"
     d="m 242.41606,265.2953 c -34.47478,-1.1108 -51.11788,-51.2897 -22.40758,-74.08561 32.32818,-25.6683 80.72068,-3.6782 77.99718,31.84951 -2.2308,20.10824 -19.1206,28.86927 -35.4669,37.1815 -7.6773,3.90394 -12.8822,5.2879 -20.1227,5.0546 z"
     id="path2384-0-6-4" />
  <path
     style="fill:#000000;stroke-width:0.25453717"
     d="m 213.82558,549.61943 c -5.6594,-39.7429 -7.5435,-44.98183 -9.3004,-60.86153 -6.1004,-55.1368 -16.4981,-80.5372 -14.2597,-142.8978 1.4449,-39.9652 34.7335,-81.4562 76.6682,-82.4176 33.4734,-4.2205 68.049,11.0935 91.5169,34.1957 19.9791,25.5075 15.5538,62.1484 -0.5093,88.582 -13.8096,22.7253 -33.5173,43.027 -53.2766,60.375 -35.4848,27.1994 -67.5428,63.99693 -90.8391,103.02423 z M 358.82118,287.4461 c -24.9783,-3.0959 -21.4012,-49.2907 7.78,-42.3467 33.618,7.9998 13.3191,41.8448 1.1462,38.6994 31.2066,-2.7449 47.6394,-34.8292 33.3304,-61.1741 -12.2455,-22.54571 -44.603,-21.03511 -60.9721,-4.4774 -31.1747,31.5338 -13.8247,49.7286 14.164,68.1412 -6.16915,0.92493 -25.2514,-5.6633 -35.1976,-25.7743 -12.1246,-24.5158 14.8213,-57.62701 40.151,-61.86851 27.6946,-4.6375 58.43178,13.48627 55.1525,45.05651 -3.81685,36.74555 -33.28257,50.31537 -55.5544,43.7439 z m -116.4051,-20.1508 c -34.4748,-1.1108 -53.1179,-55.2897 -24.4076,-78.08561 32.3282,-25.6683 84.7207,-1.6782 81.9972,33.84951 -1.6234,21.1761 -26.16155,39.60246 -37.39133,37.1002 35.8887,-27.9022 34.45173,-66.76251 -4.62817,-73.58961 -26.5578,-4.6395 -51.297,17.4672 -45.6135,45.21821 1.692,8.2617 10.5453,32.4912 30.5988,31.8157 -13.5383,-5.148 -16.9359,-15.8457 -13.9997,-24.0363 14.0161,-39.09821 58.8464,5.1925 33.567,20.6733 -7.345,4.498 -12.8822,7.2879 -20.1227,7.0546 z m 51.3167,180.4573 c 29.9892,-24.6296 60.0594,-52.8803 68.7284,-92.4935 7.267,-30.9533 -14.5692,-59.7646 -42.4705,-73.6343 -38.6825,-19.2288 -93.3328,-11.4291 -112.8262,29.9259 -14.4523,30.6601 -10.5728,66.4207 -5.8004,99.0496 l 17.0069,116.2751 c 22.0119,-32.4057 47.6946,-54.4477 75.3618,-79.1228 z m -43.3228,-21.9581 c 9.8508,-38.9716 23.6511,-74.8805 35.3229,-114.3648 12.9226,-0.7807 -5.5078,41.0936 -10.6798,53.6938 -8.1386,19.8277 -10.0097,21.0205 -24.6431,60.671 z m 58.1991,-99.9218 c -12.4365,-13.2892 31.9752,-32.0316 12.93,-5.4838 -3.2993,3.2371 -8.1562,7.1116 -12.93,5.4838 z m -47.6761,-12.633 c -23.9009,-10.456 17.4924,-40.7371 9.5139,-8.8307 2.7902,7.3311 -3.9922,10.5186 -9.5139,8.8307 z"
     id="path2384-0" />
  <rect
     style="color:#000000;display:inline;overflow:visible;visibility:visible;opacity:1;fill:#ffffff;fill-opacity:1;fill-rule:nonzero;stroke:#00000a;stroke-width:17.53051949;stroke-linecap:round;stroke-linejoin:round;stroke-miterlimit:10;stroke-dasharray:none;stroke-dashoffset:0;stroke-opacity:1;marker:none;enable-background:accumulate"
     id="rect4487-6"
     width="246.24713"
     height="317.93356"
     x="49.021225"
     y="251.3053"
     transform="matrix(0.93777429,0.34724542,0,1,0,0)" />
  <path
     id="use4581"
     d="M 61.521416,487.55287 C 259.53821,560.4793 259.53821,560.4793 259.53821,560.4793"
     style="fill:none;fill-rule:evenodd;stroke:#000000;stroke-width:8.48816872;stroke-linecap:butt;stroke-linejoin:miter;stroke-miterlimit:4;stroke-dasharray:none;stroke-opacity:1" />
  <path
     id="use4585"
     d="M 61.521416,523.3851 C 259.53821,596.31153 259.53821,596.31153 259.53821,596.31153"
     style="fill:none;fill-rule:evenodd;stroke:#000000;stroke-width:8.48816872;stroke-linecap:butt;stroke-linejoin:miter;stroke-miterlimit:4;stroke-dasharray:none;stroke-opacity:1" />
  <path
     id="use4589"
     d="M 61.521416,559.21734 C 259.53821,632.14376 259.53821,632.14376 259.53821,632.14376"
     style="fill:none;fill-rule:evenodd;stroke:#000000;stroke-width:8.48816872;stroke-linecap:butt;stroke-linejoin:miter;stroke-miterlimit:4;stroke-dasharray:none;stroke-opacity:1" />
  <rect
     style="color:#000000;display:inline;overflow:visible;visibility:visible;opacity:1;fill:#ffffff;fill-opacity:1;fill-rule:nonzero;stroke:#00000a;stroke-width:17.53051949;stroke-linecap:round;stroke-linejoin:round;stroke-miterlimit:10;stroke-dasharray:none;stroke-dashoffset:0;stroke-opacity:1;marker:none;enable-background:accumulate"
     id="rect4487-6-7"
     width="246.24713"
     height="317.93356"
     x="295.11282"
     y="456.31226"
     transform="matrix(0.93777429,-0.34724542,0,1,0,0)" />
  <path
     id="use4563-0"
     d="M 292.29988,381.37006 C 490.31668,307.6505 490.31668,307.6505 490.31668,307.6505"
     style="fill:none;fill-rule:evenodd;stroke:#000000;stroke-width:8.48816872;stroke-linecap:butt;stroke-linejoin:miter;stroke-miterlimit:4;stroke-dasharray:none;stroke-opacity:1" />
  <path
     id="use4567-3"
     d="m 292.29988,417.2023 c 198.0168,-73.71956 198.0168,-73.71956 198.0168,-73.71956"
     style="fill:none;fill-rule:evenodd;stroke:#000000;stroke-width:8.48816872;stroke-linecap:butt;stroke-linejoin:miter;stroke-miterlimit:4;stroke-dasharray:none;stroke-opacity:1" />
  <path
     id="use4571-5"
     d="m 292.29988,453.03454 c 198.0168,-73.71957 198.0168,-73.71957 198.0168,-73.71957"
     style="fill:none;fill-rule:evenodd;stroke:#000000;stroke-width:8.48816872;stroke-linecap:butt;stroke-linejoin:miter;stroke-miterlimit:4;stroke-dasharray:none;stroke-opacity:1" />
  <path
     id="use4575-2"
     d="M 292.29988,488.86677 C 490.31668,415.1472 490.31668,415.1472 490.31668,415.1472"
     style="fill:none;fill-rule:evenodd;stroke:#000000;stroke-width:8.48816872;stroke-linecap:butt;stroke-linejoin:miter;stroke-miterlimit:4;stroke-dasharray:none;stroke-opacity:1" />
  <path
     id="use4579-8"
     d="m 292.29988,524.699 c 198.0168,-73.71957 198.0168,-73.71957 198.0168,-73.71957"
     style="fill:none;fill-rule:evenodd;stroke:#000000;stroke-width:8.48816872;stroke-linecap:butt;stroke-linejoin:miter;stroke-miterlimit:4;stroke-dasharray:none;stroke-opacity:1" />
  <path
     id="use4583-7"
     d="m 292.29988,560.53122 c 198.0168,-73.71956 198.0168,-73.71956 198.0168,-73.71956"
     style="fill:none;fill-rule:evenodd;stroke:#000000;stroke-width:8.48816872;stroke-linecap:butt;stroke-linejoin:miter;stroke-miterlimit:4;stroke-dasharray:none;stroke-opacity:1" />
  <path
     id="use4587-6"
     d="m 292.29988,596.36345 c 198.0168,-73.71957 198.0168,-73.71957 198.0168,-73.71957"
     style="fill:none;fill-rule:evenodd;stroke:#000000;stroke-width:8.48816872;stroke-linecap:butt;stroke-linejoin:miter;stroke-miterlimit:4;stroke-dasharray:none;stroke-opacity:1" />
  <path
     id="use4591-2"
     d="m 292.29988,632.19569 c 198.0168,-73.71957 198.0168,-73.71957 198.0168,-73.71957"
     style="fill:none;fill-rule:evenodd;stroke:#000000;stroke-width:8.48816872;stroke-linecap:butt;stroke-linejoin:miter;stroke-miterlimit:4;stroke-dasharray:none;stroke-opacity:1" />
  <ellipse
     transform="rotate(18)"
     style="color:#000000;display:inline;overflow:visible;visibility:visible;opacity:1;fill:#000000;fill-opacity:1;fill-rule:nonzero;stroke:#00000a;stroke-width:8.62302017;stroke-linecap:round;stroke-linejoin:round;stroke-miterlimit:4;stroke-dasharray:none;stroke-dashoffset:0;stroke-opacity:0.98054474;marker:none;paint-order:markers fill stroke;enable-background:accumulate"
     id="path4625"
     cx="185.02686"
     cy="437.84494"
     rx="30.90431"
     ry="16.748215" />
  <ellipse
     style="color:#000000;display:inline;overflow:visible;visibility:visible;opacity:1;fill:#000000;fill-opacity:1;fill-rule:nonzero;stroke:#00000a;stroke-width:10.00542355;stroke-linecap:round;stroke-linejoin:round;stroke-miterlimit:4;stroke-dasharray:none;stroke-dashoffset:0;stroke-opacity:0.98054474;marker:none;paint-order:markers fill stroke;enable-background:accumulate"
     id="path4629"
     cx="-265.04355"
     cy="445.99982"
     rx="26.807098"
     ry="9.2939463"
     transform="matrix(0.79966569,-0.60044548,0.58638503,0.81003246,0,0)" />
  <ellipse
     style="color:#000000;display:inline;overflow:visible;visibility:visible;opacity:1;fill:#000000;fill-opacity:1;fill-rule:nonzero;stroke:#00000a;stroke-width:7.25151587;stroke-linecap:round;stroke-linejoin:round;stroke-miterlimit:4;stroke-dasharray:none;stroke-dashoffset:0;stroke-opacity:0.98054474;marker:none;paint-order:markers fill stroke;enable-background:accumulate"
     id="path4634"
     cx="-578.90442"
     cy="-321.29041"
     rx="48.544094"
     ry="13.99068"
     transform="matrix(-0.97938871,-0.20198454,0.21774015,-0.97600678,0,0)" />
  <ellipse
     style="color:#000000;display:inline;overflow:visible;visibility:visible;opacity:1;fill:#000000;fill-opacity:1;fill-rule:nonzero;stroke:#00000a;stroke-width:6.27317333;stroke-linecap:round;stroke-linejoin:round;stroke-miterlimit:4;stroke-dasharray:none;stroke-dashoffset:0;stroke-opacity:0.98054474;marker:none;paint-order:markers fill stroke;enable-background:accumulate"
     id="path4634-3"
     cx="-642.69159"
     cy="-269.59662"
     rx="46.859661"
     ry="10.846583"
     transform="matrix(-0.97494267,-0.22245625,0.41991694,-0.90756254,0,0)" />
  <ellipse
     style="color:#000000;display:inline;overflow:visible;visibility:visible;opacity:1;fill:#000000;fill-opacity:1;fill-rule:nonzero;stroke:#00000a;stroke-width:5.16207266;stroke-linecap:round;stroke-linejoin:round;stroke-miterlimit:4;stroke-dasharray:none;stroke-dashoffset:0;stroke-opacity:0.98054474;marker:none;paint-order:markers fill stroke;enable-background:accumulate"
     id="path4634-6"
     cx="-636.32043"
     cy="-197.55463"
     rx="42.56625"
     ry="8.0853786"
     transform="matrix(-0.9576084,-0.28807318,0.51979631,-0.85429023,0,0)" />
  <text
     xml:space="preserve"
     style="font-style:normal;font-weight:normal;font-size:50.47893143px;line-height:23.66200066px;font-family:Sans;letter-spacing:0px;word-spacing:0px;fill:#000000;fill-opacity:1;stroke:none;stroke-width:0.94647998px;stroke-linecap:butt;stroke-linejoin:miter;stroke-opacity:1"
     x="160.15019"
     y="305.56229"
     id="text4489"
     transform="skewY(20)"><tspan
       id="tspan4487"
       x="160.15019"
       y="305.56229"
       style="font-size:35.33525085px;line-height:45.80963516px;text-align:center;text-anchor:middle;stroke-width:0.94647998px">Calomelano</tspan><tspan
       x="160.15019"
       y="351.37192"
       style="font-size:35.33525085px;line-height:45.80963516px;text-align:center;text-anchor:middle;stroke-width:0.94647998px"
       id="tspan4518">Newsletter</tspan><tspan
       x="160.15019"
       y="397.18155"
       style="font-size:35.33525085px;line-height:45.80963516px;text-align:center;text-anchor:middle;stroke-width:0.94647998px"
       id="tspan4514">{{ date }}</tspan><tspan
       x="160.15019"
       y="442.99121"
       style="font-size:35.33525085px;line-height:45.80963516px;text-align:center;text-anchor:middle;stroke-width:0.94647998px"
       id="tspan4516">{{ time }}</tspan></text>
  <path
     style="fill:#000000;stroke-width:0.25453717"
     d="m -1005.642,-214.86834 c 670.428,676.57889 335.214,338.28945 0,0 z"
     id="path2397-7" />
  <path
     id="path2385"
     d="M 51.535156 440.0625 C 27.232322 441.98321 1.2627211 449.85367 7.0859375 472.24219 C 16.801367 488.97689 26.712506 489.24358 40.541016 480.58008 C 47.313705 476.33703 51.515966 472.40543 54.611328 468.61914 L 51.535156 468.61914 L 51.535156 440.0625 z "
     style="fill:#000000;stroke-width:0.25453717" />
  <path
     style="fill:#000000;stroke-width:0.25453717"
     d="m 1.55812,201.8124 c 7.89476,-19.5198 49.96026,-4.9549 64.95186,23.7802 -14.8704,-9.8381 -72.84662,-4.2604 -64.95186,-23.7802 z m 15.4578,8.4136 c 546.15393,525.10991 273.07696,262.55496 0,0 z"
     id="path2389" />
  <path
     style="fill:#000000;stroke-width:0.25453717"
     d="m 64.07988,497.9473 c -35.62997,1.6792 -55.47815,34.3889 -38.24366,43.5312 9.40126,4.9871 25.96776,-30.3777 38.24366,-43.5312 z"
     id="path2391" />
  <path
     style="fill:#000000;stroke-width:0.25453717"
     d="m 477.46078,211.3202 c 14.1482,-13.0742 14.9435,-11.602 22.1032,-15.5923 13.1241,-7.3144 37.8573,-21.3314 22.2593,-36.2338 -24.1153,-23.0397 -42.867,32.7743 -44.3625,51.8261 z m 34.7091,-31.388 c 216.05129,545.30578 108.02564,272.65289 0,0 z"
     id="path2393" />
  <path
     style="fill:#000000;stroke-width:0.25453717"
     d="m 526.56758,259.9615 c 16.7116,-0.5121 72.3944,-17.3777 72.8267,-26.2268 0.7101,-14.5349 -25.7724,-9.7195 -40.2633,-4.2587 -24.8953,9.3817 -29.1343,22.2135 -32.5634,30.4855 z"
     id="path2395" />
  <path
     style="fill:#000000;stroke-width:0.25453717"
     d="m 525.68958,171.8361 c 12.3874,-10.641 31.1474,-11.3868 34.2843,-30.1257 0.5507,-10.4431 -7.2391,-8.4578 -17.463,-3.721 -13.5453,6.2755 -23.4893,31.2413 -16.8213,33.8467 z"
     id="path2397" />
  <path
     style="fill:#000000;stroke-width:0.25453717"
     d="m 534.91158,203.4257 c 16.8195,-8.7077 44.4827,-2.2245 35.57,-17.6185 -7.0083,-12.1045 -25.9613,2.1253 -35.57,17.6185 z"
     id="path2399" />
  <path
     id="path2401"
     d="M 503.66406 510.6582 L 503.66406 551.94531 C 505.90386 552.20261 508.40287 551.94511 511.2168 551.05078 C 529.19252 545.33772 520.51893 524.1738 503.66406 510.6582 z "
     style="fill:#000000;stroke-width:0.25453717" />
  <path
     style="fill:#000000;stroke-width:0.25453717"
     d="m 528.64908,463.6527 c 66.1843,-16.0925 89.5677,40.7 58.7721,39.858 -13.6882,-0.3619 -33.995,-35.2635 -58.7721,-39.858 z"
     id="path2403" />
  <path
     style="fill:#000000;stroke-width:0.25453717"
     d="m 532.42648,509.2583 c 56.3959,16.9936 24.2735,65.1263 0,0 z"
     id="path2405" />
  <path
     style="fill:#000000;stroke-width:0.25453717"
     d="m 502.96548,589.6846 c 60.9253,38.669 3.5003,83.0511 0,0 z"
     id="path2409" />
  <path
     style="fill:#000000;stroke-width:0.25453717"
     d="m 569.82258,531.0774 c 48.771,8.8293 21.0603,38.7339 0,0 z"
     id="path2411" />
  <path
     id="path2415-2"
     d="M 177.83594 635.50781 C 145.5033 676.35576 199.68158 714.17402 193.29883 635.50781 L 177.83594 635.50781 z "
     style="fill:#000000;stroke-width:0.25453717" />
</svg>

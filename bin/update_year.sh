#!/bin/bash
# update copyright year
#
# usage:
#   cd calo.news
#   OLDYEAR=2022 NEWYEAR=2023 ./bin/update_year.sh
#
# This file is part of calo.news: A news platform
#
# Copyright (C) 2017-2023 Paolo Greppi
# 
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
# You should have received a copy of the GNU Affero General Public License
# along with this program (file LICENSE).
# If not, see <https://www.gnu.org/licenses/>.

set -e

function process {
  test -f "$1"
  echo "-- update copyright year from $OLDYEAR to $NEWYEAR for $@"
  sed -i "s/\(Copyright .*\)$OLDYEAR Paolo Greppi/\1$NEWYEAR Paolo Greppi/g" "$@"
}

process www/footer.php README.md
process bin/*.sh
process py/*.py
process sql/*.sql
process bin/*.js

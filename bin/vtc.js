#!/usr/bin/env node
//
// Build a vue template from a single file component
// launch from the repository home:
//  ./vtc.js <single_file_component>
//
// Single file components can contain any javascript code, only the
// lines comprised between <template> </template> tags will be
// processed and removed.
// The render function body will be substitued to the ### marker.
// The staticRenderFns array of functions will be substitued to the $$$ marker.
//
// This file is part of calo.news: A news platform
//
// Copyright (C) 2018-2023 Paolo Greppi
// 
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Affero General Public License for more details.
// You should have received a copy of the GNU Affero General Public License
// along with this program (file LICENSE).
// If not, see <https://www.gnu.org/licenses/>.

var compiler = require('vue-template-compiler');
var process = require('process');
var fs = require('fs');

if (process.argv.length == 3) {
  var filename = process.argv[2];
  fs.readFile(filename, 'utf8', function(err, data) {
    if (err) throw err;
    var array = data.split("\n");
    var template_start = -1;
    var template_end = -1;
    for (var i in array) {
      if (array[i] == '<template>')
        template_start = i;
      if (array[i] == '</template>')
        template_end = i;
    }
    if (template_start == -1 || template_end == -1) {
      console.log('could not find template within <template> </template> tags');
      process.exit(-1);
    } else {
      var template = array.splice(template_start, template_end - template_start + 1);
      template.splice(template_end - template_start, 1);
      template.splice(0, 1);
      var compiled = compiler.compile(template.join('\n'));
      if (compiled.errors.length > 0) {
        console.log(compiled.errors);
        process.exit(-2);
      } else {
        var staticRenderFns = '[';
        for (var i in compiled.staticRenderFns) {
          staticRenderFns += 'function(_c) {' + compiled.staticRenderFns[i] + '},';
        }
        staticRenderFns += ']';
        for (var i in array) {
          console.log(array[i].replace('###', compiled.render).replace('$$$', staticRenderFns));
        }
      }
    }
  });
} else {
  console.log('usage: vtc.js <single_file_component>');
  process.exit(-3);
}

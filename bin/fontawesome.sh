#!/bin/bash
#
# extracts the fontawesome icons we need to a single, compact SVG file
#
# This file is part of calo.news: A news platform
#
# Copyright (C) 2017-2023 Paolo Greppi
# 
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
# You should have received a copy of the GNU Affero General Public License
# along with this program (file LICENSE).
# If not, see <https://www.gnu.org/licenses/>.

set -e

function process() {
  sed "s/<svg/<symbol id=\"$2\"/g" ./"node_modules/@fortawesome/fontawesome-free/svgs/$1/$2.svg" | sed 's/svg>/symbol>/g' | sed 's/xmlns=.http:..www.w3.org.2000.svg. //g' >> www/images/fontawesome.svg
  echo '' >> www/images/fontawesome.svg
}

echo '<svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" style="display: none">' > www/images/fontawesome.svg

process solid align-justify
process solid atom
process solid backward
process solid ban
process solid book
process solid check-square
process solid clock
process solid code
process solid cog
process solid download
process solid envelope
process solid eye
process solid fast-backward
process solid file-pdf
process solid forward
process solid globe
process solid home
process solid link
process solid newspaper
process solid pause
process solid pencil-alt
process solid play
process solid plus-circle
process solid question-circle
process solid rss-square
process solid share
process solid shopping-cart
process solid sliders-h
process solid sort-down
process solid sort-up
process solid square
process solid star
process solid stop
process solid sync-alt
process solid thumbs-down
process solid trash-alt
# 
process brands amazon
process brands facebook
process brands linkedin-in
process brands mastodon
process brands reddit
process brands telegram
process brands twitter
process brands whatsapp
# 

echo '</svg>' >> www/images/fontawesome.svg

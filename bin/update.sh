#!/bin/bash
# Update stuff on remote
# launch from the repository home:
#   ./update.sh
#
# This file is part of calo.news: A news platform
#
# Copyright (C) 2017-2023 Paolo Greppi
# 
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
# You should have received a copy of the GNU Affero General Public License
# along with this program (file LICENSE).
# If not, see <https://www.gnu.org/licenses/>.

set -e

set -a
. ./.env

git rev-parse --verify HEAD > commit
scp composer.json "simevo@${AGGREGATOR_HOSTNAME}:/srv/calo.news/."
scp composer.lock "simevo@${AGGREGATOR_HOSTNAME}:/srv/calo.news/."
scp commit "simevo@${AGGREGATOR_HOSTNAME}:/srv/calo.news/."
find www -type f | grep 'gz$' | xargs -r rm
find www -type f | grep -v 'php$' | grep -v 'kate-swp$' | xargs -r gzip -k
find www -type f | grep -v 'php$' | grep -v 'kate-swp$' | xargs -r touch
ssh -C -A -t "root@${AGGREGATOR_HOSTNAME}" chown -R simevo:www-data /srv/calo.news
rsync -avz bin/ "simevo@${AGGREGATOR_HOSTNAME}:/srv/calo.news/bin"
rsync -avz www/ "simevo@${AGGREGATOR_HOSTNAME}:/srv/calo.news/www"
rsync -avz py/ "simevo@${AGGREGATOR_HOSTNAME}:/srv/calo.news/py"
rsync -avz sql/ "simevo@${AGGREGATOR_HOSTNAME}:/srv/calo.news/sql"
rsync -avz views/ "simevo@${AGGREGATOR_HOSTNAME}:/srv/calo.news/views"
scp .env "simevo@${AGGREGATOR_HOSTNAME}:/srv/calo.news/."

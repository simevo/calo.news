#!/bin/sh
# Validate stuff
#
# First start the standalone checker HTTP service in another terminal:
#   java -cp /opt/vnu_16.6.29/vnu.jar nu.validator.servlet.Main 8888
# then launch from the repository home:
#   validate.sh
#
# This file is part of calo.news: A news platform
#
# Copyright (C) 2017-2023 Paolo Greppi
# 
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
# You should have received a copy of the GNU Affero General Public License
# along with this program (file LICENSE).
# If not, see <https://www.gnu.org/licenses/>.

set -e
set -a

jshint www/js/common.js
jshint www/js/article.js
jshint www/js/index.js
jshint www/js/feeds.js
jshint --extract auto www/article-item-template.html
jshint --extract auto www/news-item-template.html
jshint --extract auto www/news-list-template.php 
jshint --extract auto www/feed-item-template.html
jshint --extract auto www/selection-template.html
jshint www/templates/negative-rating-template.js
jshint www/templates/positive-rating-template.js

flake8 py/*.py

. ./.env

curl -o index.html "${AGGREGATOR_HOSTNAME}/index.php"
java -cp /opt/vnu_16.6.29/vnu.jar nu.validator.client.HttpClient index.html
curl -o new_article.html "${AGGREGATOR_HOSTNAME}/new_article.php"
java -cp /opt/vnu_16.6.29/vnu.jar nu.validator.client.HttpClient new_article.html
curl -o new_feed.html "${AGGREGATOR_HOSTNAME}/new_feed.php"
java -cp /opt/vnu_16.6.29/vnu.jar nu.validator.client.HttpClient new_feed.html
curl -o settings.html "${AGGREGATOR_HOSTNAME}/settings.php"
java -cp /opt/vnu_16.6.29/vnu.jar nu.validator.client.HttpClient settings.html
curl -o feeds.html "${AGGREGATOR_HOSTNAME}/feeds.php"
java -cp /opt/vnu_16.6.29/vnu.jar nu.validator.client.HttpClient feeds.html
curl -o article.html "${AGGREGATOR_HOSTNAME}/article.php?id=10"
java -cp /opt/vnu_16.6.29/vnu.jar nu.validator.client.HttpClient article.html

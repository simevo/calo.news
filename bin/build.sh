#!/bin/sh
# Build single file components into vue templates
# launch from the repository home:
#   ./build.sh
#
# This file is part of calo.news: A news platform
#
# Copyright (C) 2017-2023 Paolo Greppi
# 
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
# You should have received a copy of the GNU Affero General Public License
# along with this program (file LICENSE).
# If not, see <https://www.gnu.org/licenses/>.

set -e

set -a
. ./.env

envsubst < www/js/envsubst.template.js > www/js/envsubst.js

NAMES="actions app article-item download-send-share feed-item negative-rating news-grid news-item news-item-simple news-list news-list-simple news-panel positive-rating rating-toolbar"

for NAME in ${NAMES}; do
    echo "building ${NAME} ..."
    ./bin/vtc.js "src/${NAME}.sfc" > "www/templates/${NAME}.js"
done
echo "done"
